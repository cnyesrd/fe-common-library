# Common Library and Component @cnyes fedev [![codecov](https://codecov.io/gh/cnyes/fe-common-library/branch/master/graph/badge.svg)](https://codecov.io/gh/cnyes/fe-common-library) [![CircleCI](https://circleci.com/gh/cnyes/fe-common-library.svg?style=svg)](https://circleci.com/gh/cnyes/fe-common-library)

## Storybook Preview

### bitbucket page

https://cnyesrd.bitbucket.io/fe-common-library/

(若要更新請 push 到 https://bitbucket.org/cnyesrd/cnyesrd.bitbucket.org/src)
```
yarn build:storybook
clone .out/ to https://bitbucket.org/cnyesrd/cnyesrd.bitbucket.org/src/master/fe-common-library/
git push
```

### [Netlify](https://www.netlify.com/)
https://fe-common-library.netlify.com
#### How to deploy
```
auto: merge to master
manaul: yarn deploy:storybook
```
#### Slack Webhook
trigger when deploy start, success and fail
```
Webhook URL: https://hooks.slack.com/services/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
channel: netlify
```

### Local

```
yarn storybook
```

and open

```
http://localhost:9001
```

## Create New Component
1. add component to `src/components/` (including test case & storybook preview)
2. add entry add `webpack.dest.config.js`


## Import to React base project

### Header and Footer

```
import Header from 'fe-common-library/dest/components/Header';
import 'fe-common-library/dest/components/Header/style.css';

import Footer from 'fe-common-library/dest/components/Footer';
import 'fe-common-library/dest/components/Footer/style.css';

import { MobileMenu, MobileNavBoard } from 'fe-common-library/dest/components/MobileHeader'
import 'fe-common-library/dest/components/MobileHeader/style.css';

```

### Survey (這篇新聞對您在投資上是否有幫助？)

```
import Survey from 'fe-common-library/dest/components/Survey';
import 'fe-common-library/dest/components/Survey/style.css';

```

### localStorageWrapper

```
import localStorageWrapper from 'fe-common-library/dest/utils/localStorageWrapper';

localStorageWrapper.setItem('key', 'value');
localStorageWrapper.getItem('key');
localStorageWrapper.removeItem('key');
localStorageWrapper.filter(someFilterFunction);
```

## Export static cnYes desktop Header & Footer

Available Sites: cnYes 首頁 / 台股 / 國際股 / 外匯

### Steps

1. Install bundle tools and dependencies
2. Set configs for Header
3. Export html/style and get static files
4. Test and Debug

### 1. Install bundle tools and dependencies

Use `npm` or `yarn` to install `webpack` and other dependencies

```
$ npm install
```

or

```
$ yarn
```

### 2. Prepare configs for Nav Items

Before exporting html/style, we need to prepare configs in `src/components/Header/config/AllConfig.js`, such as channel name and navigation items, to generate proper html for each channel.

A channel config will look like the following, please feel free to extend it as you want:
```
fund: {
  name: '基金',
  navs: FundConfig.navs,
  catNavs: FundConfig.catNavs,
  displayChannelName: false,
},
```

	* `name` is channel name, it will show on the right of header logo.
	* `navs` are main channels in main header
	* `catNavs` are navs in sub-header. Or you can set `null` to hide sub-header.
  * `displayChannelName` is option, set it `false` to hide channel name. Default is true.

### 3. Export html/style and get static files

Open the command line and go to the root of the project.
And use `npm build:common` or `yarn` to export html and styles.

```
$ npm run build:common
```

After built, open `static/dist` folder and you will see the file structure like:

```
static
└── dist
    ├── common
    │   ├── aboutHeader
    │   │   └── index.html
    │   ├── (...headers..)
    │   ├── footer
    │   │   └── index.html
    │   ├── common.css
    │   ├── common.js
    └── fundsyes
        ├── fundsyes.css
        ├── fundsyes.js
        └── loginForm
            └── index.html
```

Open `static/dist/common` folder, and choose `common.css`, `common.js`, `footer/index.html` and your `(name)Header/index.html`

### 4. Test and Debug

There is a chrome extension for test.
https://chrome.google.com/webstore/detail/cnyes-common-header-foote/eocaipmoojfhbakkgbbnphpndejikmlh?hl=zh-TW&authuser=2

You can use it to upload the `static/dist/common` folder to preview the change on your sites.

## Export fundsyes static files

Available Sites: 鉅亨投顧的投資老司機登入授權靜態頁

### steps

Use `npm build:common` or `yarn` to export html and styles.

```
$ npm run build:common
```

After built, open `static/dist` folder and you will see the file structure like:

```
static
└── dist
    ├── common
    │   ├── aboutHeader
    │   │   └── index.html
    │   ├── (...headers..)
    │   ├── footer
    │   │   └── index.html
    │   ├── common.css
    │   ├── common.js
    └── fundsyes
        ├── fundsyes.css
        ├── fundsyes.js
        └── loginForm
            └── index.html
```

Open `static/dist/fundsyes` folder, and choose `fundsyes.css`, `fundsyes.js`, `footer/index.html`

## Add new static file bundle

情境：想獨立於 common 或 fundsyes 之外，bundle 一包自己的 js, css, html 檔

### Steps - 以建立 fundsyes 專用 bundle 為例

#### 1. 建立 webpack entry file

參考 `src/static.js` 的設定，建立 webpack bundle 的 entry file。下面例子將以 `src/staticFundsyes.js` 為例

#### 2. 處理 html 的輸出

目前專案使用 [StaticSiteGeneratorPlugin](https://github.com/markdalgleish/static-site-generator-webpack-plugin) 來幫忙處理 React 轉 html 檔的輸出

在 `src/staticFundsyes.js` 的 render function 中最後回傳的 object 內，key 將會是檔案名，value 將會是檔案內容

以 `{ '/fundsyes/loginForm': ReactDOM.renderToString(<FundsyesLoginForm />) }` 為例

表示將會在 `static/dist` 底下產生 `fundsyes/loginForm` 資料夾，底下會有 `index.html` 檔包含了 `ReactDOM.renderToString(<FundsyesLoginForm />)` 的內容

#### 3. 設定 webpack

在 `webpack.config.js` 中

將 `src/staticFundsyes.js` 新加一條 entry `fundsyes: ['./src/staticFundsyes.js']`，

並在 plugins 新增一個 StaticSiteGeneratorPlugin

```
new StaticSiteGeneratorPlugin({
  entry: 'fundsyes',
}),
```

#### 4. 打包

接著執行

```
$ npm run build:common
```

即可在 `static/dist` 看到打包後的檔案：

```
static
└── dist
    ├── common
    │   ├── aboutHeader
    │   │   └── index.html
    │   ├── (...headers..)
    │   ├── footer
    │   │   └── index.html
    │   ├── common.css
    │   ├── common.js
    └── fundsyes
        ├── fundsyes.css
        ├── fundsyes.js
        └── loginForm
            └── index.html
```

就完成了，fundsyes 底下就是相關的 bundle 檔


# Release
1. check current version in package.json
2. trigger [jenkins job](http://ci.cnyes.cool:8080/job/media-fe-static-lib/job/fe-common-lib-master-build/) with semver and new version number 
    ```
    patch  // 0.0.1 --> 0.0.2
    minor  // 0.0.1 --> 0.1.1
    major  // 0.0.1 --> 1.0.1
    ```
    ![Imgur](https://i.imgur.com/pzRtKLp.png)


# Analyze
```
ANALYZE=true yarn build
```
