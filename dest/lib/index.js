(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("prop-types"), require("react"), require("classnames"));
	else if(typeof define === 'function' && define.amd)
		define(["prop-types", "react", "classnames"], factory);
	else {
		var a = typeof exports === 'object' ? factory(require("prop-types"), require("react"), require("classnames")) : factory(root["prop-types"], root["react"], root["classnames"]);
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(typeof self !== 'undefined' ? self : this, function(__WEBPACK_EXTERNAL_MODULE_0__, __WEBPACK_EXTERNAL_MODULE_1__, __WEBPACK_EXTERNAL_MODULE_4__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/dest/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 214);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_0__;

/***/ }),

/***/ 1:
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_1__;

/***/ }),

/***/ 11:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var getDataObj = function getDataObj(titleObj) {
  var dataObj = {};
  var dataRegExp = /^data-.+/;

  // eslint-disable-next-line no-restricted-syntax
  for (var key in titleObj) {
    if (dataRegExp.test(key)) {
      Object.assign(dataObj, _defineProperty({}, key, titleObj[key]));
    }
  }

  return dataObj;
};

exports.default = getDataObj;

/***/ }),

/***/ 19:
/***/ (function(module, exports) {

// shim for using process in browser
var process = module.exports = {};

// cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout () {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
} ())
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        //normal enviroments in sane situations
        return setTimeout(fun, 0);
    }
    // if setTimeout wasn't available but was latter defined
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedSetTimeout(fun, 0);
    } catch(e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
            return cachedSetTimeout.call(null, fun, 0);
        } catch(e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
            return cachedSetTimeout.call(this, fun, 0);
        }
    }


}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        //normal enviroments in sane situations
        return clearTimeout(marker);
    }
    // if clearTimeout wasn't available but was latter defined
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedClearTimeout(marker);
    } catch (e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
            return cachedClearTimeout.call(null, marker);
        } catch (e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
            return cachedClearTimeout.call(this, marker);
        }
    }



}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) { return [] }

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };


/***/ }),

/***/ 214:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(215);


/***/ }),

/***/ 215:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SimpleModal = exports.Survey = exports.MobileNavBoard = exports.MobileMenu = exports.Header = exports.Footer = undefined;

var _Header = __webpack_require__(74);

var _Header2 = _interopRequireDefault(_Header);

var _Footer = __webpack_require__(84);

var _Footer2 = _interopRequireDefault(_Footer);

var _MobileMenu = __webpack_require__(86);

var _MobileMenu2 = _interopRequireDefault(_MobileMenu);

var _NavBoard = __webpack_require__(88);

var _NavBoard2 = _interopRequireDefault(_NavBoard);

var _Survey = __webpack_require__(95);

var _Survey2 = _interopRequireDefault(_Survey);

var _SimpleModal = __webpack_require__(40);

var _SimpleModal2 = _interopRequireDefault(_SimpleModal);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.Footer = _Footer2.default;
exports.Header = _Header2.default;
exports.MobileMenu = _MobileMenu2.default;
exports.MobileNavBoard = _NavBoard2.default;
exports.Survey = _Survey2.default;
exports.SimpleModal = _SimpleModal2.default;

/***/ }),

/***/ 26:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin
module.exports = {"cnyes-header-wrapper":"_2SRO2","nav":"_3wxEr","cat-menu":"_GG7q8","popup":"_1APmD","news-list":"_3LfcX","is-new":"_2H_kF","sub-header":"_2aFMd","cat-nav-item":"_1wbxF","wrapper-fixed":"_1GCgR","main-header":"_1Jtol","fixed":"_3ulCb","with-arrow":"_37HAC","active":"_1Z5Aq","submenu-title":"_2jyB4","link-wrapper":"_2JnIo","icon-new":"_2TGF6","header-menu":"_26crt","logo-wrapper":"_g8UAG","logo":"_1AtfL","channel-label":"_3lW78","actions":"_3nBCF","user-nav":"_swV7A","header-search":"_34URG","fixedHeader-wrapper":"_vQw6a","bottom":"_1VC46","subheader-wrapper":"_3Acd3","cat-nav-sub-item":"_1MKPP","hide":"_3NyqV"};

/***/ }),

/***/ 27:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin
module.exports = {"footerHeight":"140px","cnyes-footer-wrapper":"_qdV4M","main-footer":"_1B2qL","logo":"_1W3uT","nav":"_36Xgk","socials":"_7vG5K","social-item":"_1n8HS","title":"_2C4rE","social-icon":"_11SpT","cnyes-media-facebook":"_2WAgR","cnyes-media-line":"_1PYxM","cnyes-media-app":"_13are","copyright-anue":"_P9DLg","hidden-mobile":"_1uLvu","hidden-desktop":"_2pljx"};

/***/ }),

/***/ 3:
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || Function("return this")() || (1,eval)("this");
} catch(e) {
	// This works if the window reference is available
	if(typeof window === "object")
		g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),

/***/ 39:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = {
  setItem: function setItem(key, value) {
    try {
      window.localStorage.setItem(key, value);
    } catch (e) {
      // ignore
    }
  },
  getItem: function getItem(key) {
    try {
      return window.localStorage.getItem(key);
    } catch (e) {
      // ignore
    }

    return null;
  },
  removeItem: function removeItem(key) {
    try {
      window.localStorage.removeItem(key);
    } catch (e) {
      // ignore
    }
  },
  filter: function filter(filterFunction) {
    var localStorageLength = void 0;

    try {
      localStorageLength = window.localStorage.length;
    } catch (e) {
      // ignore

      return [];
    }

    var keys = [];

    for (var i = 0; i < localStorageLength; i += 1) {
      var key = window.localStorage.key(i);

      if (filterFunction(key)) keys.push(key);
    }

    return keys;
  }
};

/***/ }),

/***/ 4:
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_4__;

/***/ }),

/***/ 40:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(0);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _SimpleModal = __webpack_require__(41);

var _SimpleModal2 = _interopRequireDefault(_SimpleModal);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var SimpleModal = function (_PureComponent) {
  _inherits(SimpleModal, _PureComponent);

  function SimpleModal() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, SimpleModal);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = SimpleModal.__proto__ || Object.getPrototypeOf(SimpleModal)).call.apply(_ref, [this].concat(args))), _this), _this.handleClose = function (e) {
      if (e) {
        e.preventDefault();
      }

      if (_this.props.closeHandler) {
        _this.props.closeHandler(e);
      }
    }, _this.handleClickOverlay = function (e) {
      var _this$props = _this.props,
          enableOverlayCanClose = _this$props.enableOverlayCanClose,
          closeHandler = _this$props.closeHandler;


      if (enableOverlayCanClose && closeHandler) {
        closeHandler(e);
      }
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(SimpleModal, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          overlayStyle = _props.overlayStyle,
          containerStyle = _props.containerStyle,
          closeHandleStyle = _props.closeHandleStyle,
          enableClose = _props.enableClose,
          enableModal = _props.enableModal;


      if (enableModal) {
        return _react2.default.createElement(
          'div',
          { className: _SimpleModal2.default.wrapper, style: overlayStyle, onClick: this.handleClickOverlay },
          _react2.default.createElement(
            'div',
            { className: _SimpleModal2.default.container, style: containerStyle },
            enableClose && _react2.default.createElement('div', { className: _SimpleModal2.default['close-handle'], style: closeHandleStyle, onClick: this.handleClose }),
            this.props.children
          )
        );
      }

      return null;
    }
  }]);

  return SimpleModal;
}(_react.PureComponent);

SimpleModal.defaultProps = {
  overlayStyle: {},
  containerStyle: {},
  closeHandleStyle: {},
  enableModal: false,
  enableClose: true,
  closeHandler: undefined,
  enableOverlayCanClose: false
};
exports.default = SimpleModal;

/***/ }),

/***/ 41:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin
module.exports = {"wrapper":"_MXx3I","fadeIn":"_3NB-2","container":"_29Mdb","close-handle":"_1y-kA"};

/***/ }),

/***/ 5:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = getStyleName;
function getStyleName(styles, name) {
  return styles[name] || name;
}

/***/ }),

/***/ 7:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.adProfileType = exports.newsItemType = exports.userProfileType = exports.authType = exports.locationShape = exports.footerNavItem = exports.catNavsType = exports.catNavItemShape = exports.catNavSubItemShape = exports.navsMobileType = exports.navItemMobileShape = exports.navItemMobilShape = exports.accountNavsType = exports.accountNavItemShape = exports.navsType = exports.navItemShape = exports.navUrlShape = exports.navDownloadType = exports.navDownloadShape = exports.requestType = undefined;

var _propTypes = __webpack_require__(0);

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var requestType = exports.requestType = _propTypes2.default.func; /* eslint-disable import/prefer-default-export */
var navDownloadShape = exports.navDownloadShape = _propTypes2.default.shape({
  title: _propTypes2.default.string.isRequired,
  url: _propTypes2.default.string.isRequired
});

var navDownloadType = exports.navDownloadType = _propTypes2.default.arrayOf(navDownloadShape);

var navUrlShape = exports.navUrlShape = _propTypes2.default.shape({
  title: _propTypes2.default.string.isRequired,
  url: _propTypes2.default.string.isRequired
});

var navItemShape = exports.navItemShape = _propTypes2.default.shape({
  title: _propTypes2.default.string.isRequired,
  url: _propTypes2.default.string,
  leftList: _propTypes2.default.arrayOf(navUrlShape),
  rightListTitle: _propTypes2.default.string,
  rightList: _propTypes2.default.arrayOf(navUrlShape)
});

var navsType = exports.navsType = _propTypes2.default.arrayOf(navItemShape);

var accountNavItemShape = exports.accountNavItemShape = _propTypes2.default.shape({
  id: _propTypes2.default.string.isRequired,
  title: _propTypes2.default.string.isRequired,
  defaultUrl: _propTypes2.default.string.isRequired,
  notify: _propTypes2.default.arrayOf(_propTypes2.default.shape({
    id: _propTypes2.default.string.isRequired,
    gotoUrl: _propTypes2.default.string.isRequired
  }))
});

var accountNavsType = exports.accountNavsType = _propTypes2.default.arrayOf(accountNavItemShape);

var navItemMobilShape = exports.navItemMobilShape = _propTypes2.default.shape({
  name: _propTypes2.default.string.isRequired,
  title: _propTypes2.default.string.isRequired,
  url: _propTypes2.default.string.isRequired,
  external: _propTypes2.default.bool
});

var navItemMobileShape = exports.navItemMobileShape = _propTypes2.default.shape({
  name: _propTypes2.default.string.isRequired,
  title: _propTypes2.default.string.isRequired,
  items: _propTypes2.default.arrayOf(navItemMobilShape)
});

var navsMobileType = exports.navsMobileType = _propTypes2.default.arrayOf(navItemMobileShape);

var catNavSubItemShape = exports.catNavSubItemShape = _propTypes2.default.shape({
  name: _propTypes2.default.string,
  url: _propTypes2.default.string,
  title: _propTypes2.default.string,
  external: _propTypes2.default.bool
});

var catNavItemShape = exports.catNavItemShape = _propTypes2.default.shape({
  name: _propTypes2.default.string.isRequired,
  url: _propTypes2.default.string.isRequired,
  title: _propTypes2.default.string.isRequired,
  external: _propTypes2.default.bool,
  subItems: _propTypes2.default.arrayOf(catNavSubItemShape)
});

var catNavsType = exports.catNavsType = _propTypes2.default.arrayOf(catNavItemShape);

var footerNavItem = exports.footerNavItem = _propTypes2.default.shape({
  title: _propTypes2.default.string.isRequired,
  name: _propTypes2.default.string.isRequired,
  url: _propTypes2.default.string,
  onClick: _propTypes2.default.func
});

var locationShape = exports.locationShape = _propTypes2.default.shape({
  key: _propTypes2.default.string,
  pathname: _propTypes2.default.string,
  search: _propTypes2.default.string,
  hash: _propTypes2.default.string,
  state: _propTypes2.default.object
});

var authType = exports.authType = _propTypes2.default.shape({
  init: _propTypes2.default.func.isRequired,
  loginFB: _propTypes2.default.func.isRequired,
  loginGoogle: _propTypes2.default.func.isRequired,
  logout: _propTypes2.default.func.isRequired,
  showLogin: _propTypes2.default.func.isRequired,
  hideLogin: _propTypes2.default.func.isRequired,
  getToken: _propTypes2.default.func.isRequired,
  refreshToken: _propTypes2.default.func.isRequired,
  getProfile: _propTypes2.default.func.isRequired
});

var userProfileType = exports.userProfileType = _propTypes2.default.shape({
  uid: _propTypes2.default.string.isRequired,
  name: _propTypes2.default.string.isRequired,
  nickname: _propTypes2.default.string,
  email: _propTypes2.default.string,
  avatar: _propTypes2.default.string.isRequired,
  gender: _propTypes2.default.oneOf(['', 'male', 'female']),
  vip: _propTypes2.default.oneOf([0, 1])
});

var newsItemType = exports.newsItemType = _propTypes2.default.shape({
  newsId: _propTypes2.default.number.isRequired,
  title: _propTypes2.default.string.isRequired,
  hasCoverPhoto: _propTypes2.default.oneOf([0, 1]).isRequired,
  coverSrc: _propTypes2.default.shape({
    m: _propTypes2.default.shape({ src: _propTypes2.default.string }),
    l: _propTypes2.default.shape({ src: _propTypes2.default.string }),
    xl: _propTypes2.default.shape({ src: _propTypes2.default.string })
  })
});

var adProfileType = exports.adProfileType = _propTypes2.default.shape({
  name: _propTypes2.default.string.isRequired,
  path: _propTypes2.default.string.isRequired,
  hideOnInitial: _propTypes2.default.bool
});

/***/ }),

/***/ 74:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; /* eslint import/no-extraneous-dependencies: ["error", {"peerDependencies": true}] */


exports.findCatSlugFromUrl = findCatSlugFromUrl;

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(0);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _raf = __webpack_require__(75);

var _raf2 = _interopRequireDefault(_raf);

var _classnames = __webpack_require__(4);

var _classnames2 = _interopRequireDefault(_classnames);

var _propTypes3 = __webpack_require__(7);

var _getStyleName = __webpack_require__(5);

var _getStyleName2 = _interopRequireDefault(_getStyleName);

var _SubMenu = __webpack_require__(77);

var _SubMenu2 = _interopRequireDefault(_SubMenu);

var _SubNavItem = __webpack_require__(78);

var _SubNavItem2 = _interopRequireDefault(_SubNavItem);

var _Header = __webpack_require__(26);

var _Header2 = _interopRequireDefault(_Header);

var _ConstantCats = __webpack_require__(79);

var _ConstantUI = __webpack_require__(82);

var _navs = __webpack_require__(83);

var _navs2 = _interopRequireDefault(_navs);

var _gaDataset = __webpack_require__(8);

var _gaDataset2 = _interopRequireDefault(_gaDataset);

var _getDataObj = __webpack_require__(11);

var _getDataObj2 = _interopRequireDefault(_getDataObj);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var DISTANCE_OVER_HEADER = 74;
var DISTANCE_OVER_SUBHEADER = 114;

function findCatSlugFromUrl() {
  var pathname = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  var query = arguments[1];

  var regex = /^\/(news|columnists|projects|trending|search|forex|magazines)?\/?(\w+)?\/?(\w+)?/;
  var matches = pathname.match(regex);
  var target = null;

  if (matches) {
    if (matches[1] && matches[1] === 'columnists') {
      // /columnists
      target = 'celebrity_area';
    } else if (matches[1] && matches[1] === 'magazines') {
      // /magazines
      target = 'mag';
    } else if (matches[1] && matches[1] === 'search' && query && query.q === '虛擬貨幣') {
      // /search?group=1&q=虛擬貨幣
      target = 'searchCrypto';
    } else if (matches[1] && matches[1] === 'projects') {
      // /projects OR /projects/cat/:catSlug
      target = 'projects';
    } else if (matches[2] && matches[2] === 'cat') {
      // /news/cat/:catSlug
      target = matches[3];
    } else if (matches[1] && matches[1] === 'forex' && matches[2]) {
      // /forex/xxx
      target = matches[2];
    } else if (matches[1]) {
      target = matches[1];
    } else if (matches[2]) {
      target = matches[2];
    }
  }

  return _ConstantCats.CategoryMappingWithSubs[target] && _ConstantCats.CategoryMappingWithSubs[target].parentId !== 0 ? _ConstantCats.CategoryMappingWithSubs[target].parentSlug : target;
}

function renderNavs(channel, navs, dataPrefix) {
  return navs.map(function (nav, idx) {
    var className = nav.title === channel ? (0, _classnames2.default)((0, _getStyleName2.default)(_Header2.default, 'active'), 'theme-active') : '';
    var dataObj = (0, _getDataObj2.default)(nav);

    /* eslint-disable react/no-array-index-key */
    return _react2.default.createElement(_SubMenu2.default, _extends({ key: idx, className: className }, nav, { dataPrefix: dataPrefix, dataObj: dataObj }));
    /* eslint-enable react/no-array-index-key */
  });
}

var Header = function (_Component) {
  _inherits(Header, _Component);

  function Header(props) {
    _classCallCheck(this, Header);

    var _this = _possibleConstructorReturn(this, (Header.__proto__ || Object.getPrototypeOf(Header)).call(this, props));

    _this.isFixedHeader = function () {
      return _this.props.fixedHeaderType !== _ConstantUI.FIXED_HEADER_NONE;
    };

    _this.scrollHandler = function () {
      var fixedHeaderType = _this.props.fixedHeaderType;

      var scrollY = 'scrollY' in window ? window.scrollY : document.documentElement.scrollTop;
      var sticky = _this.isFixedHeader();
      var distance = DISTANCE_OVER_SUBHEADER;

      if (fixedHeaderType === _ConstantUI.FIXED_HEADER_FULL) {
        distance = DISTANCE_OVER_HEADER;
      }

      if (sticky) {
        if (scrollY >= distance && !_this.state.fixed) {
          _this._setNextState({ fixed: true });
        } else if (scrollY < distance && _this.state.fixed) {
          _this._setNextState({ fixed: false });
        }
      }
    };

    _this._setNextState = function (state) {
      if (_this._setNextStateAnimationFrameId) {
        _raf2.default.cancel(_this._setNextStateAnimationFrameId);
      }

      _this._setNextStateAnimationFrameId = (0, _raf2.default)(function () {
        _this._setNextStateAnimationFrameId = null;
        _this.setState({ fixed: state.fixed });
      });
    };

    _this.state = {
      fixed: false
    };
    return _this;
  }

  _createClass(Header, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      if (this.isFixedHeader()) {
        window.addEventListener('scroll', this.scrollHandler);
      }
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      window.removeEventListener('scroll', this.scrollHandler);
    }
  }, {
    key: 'renderFixedHeader',
    value: function renderFixedHeader() {
      var _props = this.props,
          catNavs = _props.catNavs,
          channel = _props.channel,
          navs = _props.navs,
          fixedHeaderType = _props.fixedHeaderType;
      var fixed = this.state.fixed;

      var isFixFullHeader = fixedHeaderType === _ConstantUI.FIXED_HEADER_FULL;
      var wrapperClass = (0, _classnames2.default)((0, _getStyleName2.default)(_Header2.default, 'fixedHeader-wrapper'), 'theme-fixedHeader-wrapper');

      if (!fixed) return null;
      if (!isFixFullHeader || isFixFullHeader && !(catNavs && catNavs.length > 0)) {
        wrapperClass = (0, _classnames2.default)(wrapperClass, (0, _getStyleName2.default)(_Header2.default, 'bottom'), 'theme-bottom');
      }

      return _react2.default.createElement(
        'div',
        { className: wrapperClass },
        isFixFullHeader && _react2.default.createElement(
          'nav',
          {
            className: (0, _classnames2.default)((0, _getStyleName2.default)(_Header2.default, 'nav'), 'theme-nav', (0, _getStyleName2.default)(_Header2.default, 'fixed'), 'theme-fixed')
          },
          renderNavs(channel, navs)
        ),
        catNavs && catNavs.length > 0 && this.renderSubHeader(true)
      );
    }
  }, {
    key: 'renderSubHeader',
    value: function renderSubHeader() {
      var isTopSubHeader = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
      var fixedHeaderType = this.props.fixedHeaderType;

      // .js-* className is for e2e test

      var subHeaderClass = (0, _classnames2.default)((0, _getStyleName2.default)(_Header2.default, 'sub-header'), 'theme-sub-header', 'js-header-sub-header');

      if (isTopSubHeader) {
        subHeaderClass = (0, _classnames2.default)(subHeaderClass, (0, _getStyleName2.default)(_Header2.default, 'fixed'), 'theme-fixed');

        if (fixedHeaderType === _ConstantUI.FIXED_HEADER_FULL) {
          subHeaderClass = (0, _classnames2.default)(subHeaderClass, (0, _getStyleName2.default)(_Header2.default, 'bottom'), 'theme-bottom');
        }
      }

      return _react2.default.createElement(
        'div',
        { className: subHeaderClass },
        _react2.default.createElement(
          'nav',
          { className: (0, _classnames2.default)((0, _getStyleName2.default)(_Header2.default, 'cat-menu'), 'theme-cat-menu') },
          this.renderCatMenu()
        )
      );
    }
  }, {
    key: 'renderCatMenu',
    value: function renderCatMenu() {
      var _props2 = this.props,
          catNavs = _props2.catNavs,
          location = _props2.location,
          Link = _props2.Link,
          dataPrefix = _props2.dataPrefix;

      var activeCatSlug = findCatSlugFromUrl(location.pathname, location.query);

      return catNavs.map(function (nav, idx) {
        return _react2.default.createElement(_SubNavItem2.default, {
          key: idx // eslint-disable-line react/no-array-index-key
          , url: nav.url,
          title: nav.title,
          isNew: nav.isNew,
          isActive: nav.name === activeCatSlug,
          subItems: nav.subItems,
          external: nav.external,
          Link: Link,
          dataPrefix: dataPrefix,
          dataObj: (0, _getDataObj2.default)(nav)
        });
      });
    }
  }, {
    key: 'renderSearch',
    value: function renderSearch() {
      var _this2 = this;

      return _react2.default.createElement(
        'form',
        {
          acceptCharset: 'UTF-8',
          action: 'https://so.cnyes.com/cnyessearch.aspx',
          className: (0, _getStyleName2.default)(_Header2.default, 'header-search'),
          target: '_blank'
        },
        _react2.default.createElement('input', { type: 'hidden', name: 'cx', value: '015486011444191663508:8ijuvgfglaq' }),
        _react2.default.createElement('input', {
          type: 'hidden',
          name: 'other',
          value: '',
          ref: function ref(_ref) {
            _this2.otherInput = _ref;
          }
        }),
        _react2.default.createElement('input', { type: 'hidden', name: 'ie', value: 'UTF-8' }),
        _react2.default.createElement('input', { type: 'hidden', name: 'ga', value: 'nav' }),
        _react2.default.createElement('input', {
          name: 'q',
          placeholder: '\u8ACB\u8F38\u5165\u95DC\u9375\u8A5E',
          onChange: function onChange(e) {
            _this2.otherInput.value = e.target.value;
          }
        }),
        _react2.default.createElement('button', { type: 'submit' })
      );
    }
  }, {
    key: 'render',
    value: function render() {
      var _props3 = this.props,
          catNavs = _props3.catNavs,
          channel = _props3.channel,
          displayChannelName = _props3.displayChannelName,
          navs = _props3.navs,
          Link = _props3.Link,
          dataPrefix = _props3.dataPrefix,
          customMenu = _props3.customMenu;


      return _react2.default.createElement(
        'div',
        { id: (0, _getStyleName2.default)(_Header2.default, 'cnyes-header-wrapper'), className: (0, _classnames2.default)('theme-wrapper', 'theme-header') },
        _react2.default.createElement(
          'header',
          { className: (0, _classnames2.default)((0, _getStyleName2.default)(_Header2.default, 'main-header'), 'theme-main-header') },
          _react2.default.createElement(
            'div',
            { className: (0, _getStyleName2.default)(_Header2.default, 'header-menu') },
            _react2.default.createElement(
              'span',
              { className: (0, _getStyleName2.default)(_Header2.default, 'logo-wrapper') },
              _react2.default.createElement('a', _extends({
                href: 'https://www.cnyes.com/',
                className: (0, _getStyleName2.default)(_Header2.default, 'logo')
              }, (0, _gaDataset2.default)({ dataPrefix: dataPrefix, category: 'Logo', action: 'click', label: 'home' }))),
              displayChannelName && (Link ? _react2.default.createElement(
                Link,
                { to: '/', className: (0, _getStyleName2.default)(_Header2.default, 'channel-label') },
                channel
              ) : _react2.default.createElement(
                'a',
                { href: '/', className: (0, _getStyleName2.default)(_Header2.default, 'channel-label') },
                channel
              ))
            ),
            _react2.default.createElement(
              'span',
              { className: (0, _getStyleName2.default)(_Header2.default, 'actions') },
              customMenu,
              _react2.default.createElement(
                'ul',
                { className: (0, _getStyleName2.default)(_Header2.default, 'user-nav') },
                _react2.default.createElement(
                  'li',
                  null,
                  _react2.default.createElement(
                    'a',
                    { href: 'https://www.facebook.com/anuetw/', target: '_blank', rel: 'noopener noreferrer' },
                    '\u7C89\u7D72\u5718'
                  )
                )
              ),
              this.renderSearch()
            )
          ),
          _react2.default.createElement(
            'nav',
            { className: (0, _classnames2.default)((0, _getStyleName2.default)(_Header2.default, 'nav'), 'theme-nav') },
            renderNavs(channel, navs, dataPrefix)
          )
        ),
        catNavs && catNavs.length > 0 && _react2.default.createElement(
          'div',
          { className: (0, _classnames2.default)((0, _getStyleName2.default)(_Header2.default, 'subheader-wrapper')) },
          this.renderSubHeader(false)
        ),
        this.renderFixedHeader()
      );
    }
  }]);

  return Header;
}(_react.Component);

Header.defaultProps = {
  catNavs: undefined,
  displayChannelName: true,
  fixedHeaderType: _ConstantUI.FIXED_HEADER_NONE,
  navs: _navs2.default.items,
  Link: undefined,
  dataPrefix: ['data-proj-ga'],
  customMenu: null
};
exports.default = Header;

/***/ }),

/***/ 75:
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {var now = __webpack_require__(76)
  , root = typeof window === 'undefined' ? global : window
  , vendors = ['moz', 'webkit']
  , suffix = 'AnimationFrame'
  , raf = root['request' + suffix]
  , caf = root['cancel' + suffix] || root['cancelRequest' + suffix]

for(var i = 0; !raf && i < vendors.length; i++) {
  raf = root[vendors[i] + 'Request' + suffix]
  caf = root[vendors[i] + 'Cancel' + suffix]
      || root[vendors[i] + 'CancelRequest' + suffix]
}

// Some versions of FF have rAF but not cAF
if(!raf || !caf) {
  var last = 0
    , id = 0
    , queue = []
    , frameDuration = 1000 / 60

  raf = function(callback) {
    if(queue.length === 0) {
      var _now = now()
        , next = Math.max(0, frameDuration - (_now - last))
      last = next + _now
      setTimeout(function() {
        var cp = queue.slice(0)
        // Clear queue here to prevent
        // callbacks from appending listeners
        // to the current frame's queue
        queue.length = 0
        for(var i = 0; i < cp.length; i++) {
          if(!cp[i].cancelled) {
            try{
              cp[i].callback(last)
            } catch(e) {
              setTimeout(function() { throw e }, 0)
            }
          }
        }
      }, Math.round(next))
    }
    queue.push({
      handle: ++id,
      callback: callback,
      cancelled: false
    })
    return id
  }

  caf = function(handle) {
    for(var i = 0; i < queue.length; i++) {
      if(queue[i].handle === handle) {
        queue[i].cancelled = true
      }
    }
  }
}

module.exports = function(fn) {
  // Wrap in a new function to prevent
  // `cancel` potentially being assigned
  // to the native rAF function
  return raf.call(root, fn)
}
module.exports.cancel = function() {
  caf.apply(root, arguments)
}
module.exports.polyfill = function(object) {
  if (!object) {
    object = root;
  }
  object.requestAnimationFrame = raf
  object.cancelAnimationFrame = caf
}

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(3)))

/***/ }),

/***/ 76:
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(process) {// Generated by CoffeeScript 1.12.2
(function() {
  var getNanoSeconds, hrtime, loadTime, moduleLoadTime, nodeLoadTime, upTime;

  if ((typeof performance !== "undefined" && performance !== null) && performance.now) {
    module.exports = function() {
      return performance.now();
    };
  } else if ((typeof process !== "undefined" && process !== null) && process.hrtime) {
    module.exports = function() {
      return (getNanoSeconds() - nodeLoadTime) / 1e6;
    };
    hrtime = process.hrtime;
    getNanoSeconds = function() {
      var hr;
      hr = hrtime();
      return hr[0] * 1e9 + hr[1];
    };
    moduleLoadTime = getNanoSeconds();
    upTime = process.uptime() * 1e9;
    nodeLoadTime = moduleLoadTime - upTime;
  } else if (Date.now) {
    module.exports = function() {
      return Date.now() - loadTime;
    };
    loadTime = Date.now();
  } else {
    module.exports = function() {
      return new Date().getTime() - loadTime;
    };
    loadTime = new Date().getTime();
  }

}).call(this);

//# sourceMappingURL=performance-now.js.map

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(19)))

/***/ }),

/***/ 77:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; /* eslint import/no-extraneous-dependencies: ["error", {"peerDependencies": true}] */


var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(0);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = __webpack_require__(4);

var _classnames2 = _interopRequireDefault(_classnames);

var _getStyleName = __webpack_require__(5);

var _getStyleName2 = _interopRequireDefault(_getStyleName);

var _gaDataset = __webpack_require__(8);

var _gaDataset2 = _interopRequireDefault(_gaDataset);

var _getDataObj = __webpack_require__(11);

var _getDataObj2 = _interopRequireDefault(_getDataObj);

var _Header = __webpack_require__(26);

var _Header2 = _interopRequireDefault(_Header);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var style = {
  narrowedPopup: {
    width: '120px',
    height: 'auto',
    overflow: 'visible'
  },
  narrowedList: {
    height: 'auto'
  }
};

var renderStaticList = function renderStaticList(list, className, wrapperStyle, listTitle, dataPrefix) {
  var content = list.map(function (link, id) {
    return _react2.default.createElement(
      'a',
      _extends({
        key: id // eslint-disable-line react/no-array-index-key
        , href: link.url,
        className: link.icon ? (0, _getStyleName2.default)(_Header2.default, 'icon-' + link.icon) : null,
        title: link.title
      }, (0, _gaDataset2.default)({ dataPrefix: dataPrefix, category: 'Nav', action: 'click', label: link.title }), (0, _getDataObj2.default)(link)),
      link.title
    );
  });

  return _react2.default.createElement(
    'nav',
    { className: className, style: wrapperStyle || {} },
    listTitle && _react2.default.createElement(
      'h5',
      null,
      listTitle
    ),
    content
  );
};

var SubMenu = function (_Component) {
  _inherits(SubMenu, _Component);

  function SubMenu() {
    _classCallCheck(this, SubMenu);

    return _possibleConstructorReturn(this, (SubMenu.__proto__ || Object.getPrototypeOf(SubMenu)).apply(this, arguments));
  }

  _createClass(SubMenu, [{
    key: 'renderPopupMenu',
    value: function renderPopupMenu() {
      var _props = this.props,
          leftList = _props.leftList,
          leftListTitle = _props.leftListTitle,
          rightList = _props.rightList,
          rightListTitle = _props.rightListTitle,
          dataPrefix = _props.dataPrefix;


      if (!leftList && !rightList) {
        // no left and no right, => no popup menu
        return null;
      }

      return _react2.default.createElement(
        'div',
        {
          className: (0, _classnames2.default)((0, _getStyleName2.default)(_Header2.default, 'popup'), 'theme-popup'),
          style: !rightList && style.narrowedPopup || {}
        },
        leftList && renderStaticList(leftList, (0, _classnames2.default)((0, _getStyleName2.default)(_Header2.default, 'link-wrapper'), 'theme-link-wrapper'), !rightList && style.narrowedList, leftListTitle, dataPrefix),
        rightList && renderStaticList(rightList, (0, _classnames2.default)((0, _getStyleName2.default)(_Header2.default, 'news-list'), 'theme-news-list'), undefined, rightListTitle, dataPrefix)
      );
    }
  }, {
    key: 'render',
    value: function render() {
      var _props2 = this.props,
          url = _props2.url,
          title = _props2.title,
          className = _props2.className,
          leftList = _props2.leftList,
          isNew = _props2.isNew,
          dataPrefix = _props2.dataPrefix,
          dataObj = _props2.dataObj;


      return _react2.default.createElement(
        'div',
        { className: className, onMouseEnter: this.loadNewsByCat },
        url.length ? _react2.default.createElement(
          'a',
          _extends({
            href: url,
            className: (0, _classnames2.default)((0, _getStyleName2.default)(_Header2.default, 'submenu-title'), _defineProperty({}, (0, _getStyleName2.default)(_Header2.default, 'is-new'), isNew))
          }, (0, _gaDataset2.default)({ dataPrefix: dataPrefix, category: 'Nav', action: 'click', label: title }), dataObj),
          title,
          leftList && _react2.default.createElement('span', { className: (0, _getStyleName2.default)(_Header2.default, 'with-arrow') })
        ) : _react2.default.createElement(
          'span',
          { className: (0, _getStyleName2.default)(_Header2.default, 'submenu-title') },
          title,
          leftList && _react2.default.createElement('span', { className: (0, _getStyleName2.default)(_Header2.default, 'with-arrow') })
        ),
        this.renderPopupMenu()
      );
    }
  }]);

  return SubMenu;
}(_react.Component);

SubMenu.defaultProps = {
  className: '',
  dataPrefix: ['data-proj-ga'],
  dataObj: {},
  leftList: undefined,
  leftListTitle: undefined,
  rightList: undefined,
  rightListTitle: undefined,
  isNew: false
};
exports.default = SubMenu;

/***/ }),

/***/ 78:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; /* eslint import/no-extraneous-dependencies: ["error", {"peerDependencies": true}] */


var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(0);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = __webpack_require__(4);

var _classnames2 = _interopRequireDefault(_classnames);

var _getStyleName = __webpack_require__(5);

var _getStyleName2 = _interopRequireDefault(_getStyleName);

var _gaDataset = __webpack_require__(8);

var _gaDataset2 = _interopRequireDefault(_gaDataset);

var _getDataObj = __webpack_require__(11);

var _getDataObj2 = _interopRequireDefault(_getDataObj);

var _Header = __webpack_require__(26);

var _Header2 = _interopRequireDefault(_Header);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function SubNavItemMenu(_ref) {
  var items = _ref.items,
      dataPrefix = _ref.dataPrefix;

  if (!items) {
    return null;
  }
  if (!items.length) {
    return null;
  }

  return _react2.default.createElement(
    'nav',
    { className: (0, _getStyleName2.default)(_Header2.default, 'cat-nav-sub-item') },
    items && items.map(function (item) {
      return _react2.default.createElement(
        'a',
        _extends({
          key: item.name,
          href: item.url,
          title: item.title
        }, (0, _gaDataset2.default)({ dataPrefix: dataPrefix, category: 'Nav', action: 'click', label: item.title }), (0, _getDataObj2.default)(item)),
        item.title
      );
    })
  );
}

SubNavItemMenu.defaultProps = {
  items: undefined,
  dataPrefix: ['data-proj-ga']
};

function SubNavItem(_ref2) {
  var _cx;

  var _ref2$url = _ref2.url,
      url = _ref2$url === undefined ? '/' : _ref2$url,
      title = _ref2.title,
      isNew = _ref2.isNew,
      isActive = _ref2.isActive,
      subItems = _ref2.subItems,
      _ref2$external = _ref2.external,
      external = _ref2$external === undefined ? false : _ref2$external,
      Link = _ref2.Link,
      dataPrefix = _ref2.dataPrefix,
      dataObj = _ref2.dataObj;

  var className = isActive ? (0, _classnames2.default)((0, _getStyleName2.default)(_Header2.default, 'active'), 'theme-active') : '';

  className = (0, _classnames2.default)(className, (_cx = {}, _defineProperty(_cx, (0, _getStyleName2.default)(_Header2.default, 'is-new'), isNew), _defineProperty(_cx, (0, _getStyleName2.default)(_Header2.default, 'with-arrow'), !!subItems), _cx));

  return _react2.default.createElement(
    'span',
    { className: (0, _getStyleName2.default)(_Header2.default, 'cat-nav-item') },
    external || !Link ? _react2.default.createElement(
      'a',
      _extends({
        className: className,
        href: url,
        title: title,
        'data-ga-category': 'subNavItem',
        'data-ga-action': title
      }, (0, _gaDataset2.default)({ dataPrefix: dataPrefix, category: 'Nav', action: 'click', label: title }), dataObj),
      title
    ) : _react2.default.createElement(
      Link,
      _extends({
        className: className,
        to: url,
        title: title,
        'data-ga-category': 'subNavItem',
        'data-ga-action': title
      }, (0, _gaDataset2.default)({ dataPrefix: dataPrefix, category: 'Nav', action: 'click', label: title }), dataObj),
      title
    ),
    subItems && _react2.default.createElement(SubNavItemMenu, { items: subItems, dataPrefix: dataPrefix })
  );
}

SubNavItem.defaultProps = {
  isNew: false,
  isActive: false,
  subItems: undefined,
  external: false,
  Link: undefined,
  dataPrefix: ['data-proj-ga'],
  dataObj: {}
};

exports.default = SubNavItem;

/***/ }),

/***/ 79:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CategoryMappingWithSubs = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _CategoriesProdRaw = __webpack_require__(80);

var _CategoriesProdRaw2 = _interopRequireDefault(_CategoriesProdRaw);

var _CategoriesBetaRaw = __webpack_require__(81);

var _CategoriesBetaRaw2 = _interopRequireDefault(_CategoriesBetaRaw);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } } /* eslint-disable import/prefer-default-export */
// prod: http://news.cnyes.cool/api/v3/categories
// eslint-disable-line import/no-named-as-default


// eslint-disable-line import/no-named-as-default

var CategoriesRaw =  true ? _CategoriesProdRaw2.default : _CategoriesBetaRaw2.default;

var BASE_CATEGORIES = [{
  slug: 'all',
  categoryId: 'all',
  name: '總覽',
  parentId: 0,
  parentSlug: '',
  api: '/api/v2/news',
  subs: null
}, {
  slug: 'headline',
  categoryId: 'headline',
  name: '即時頭條',
  parentId: 0,
  parentSlug: '',
  api: '/api/v3/news/category/headline',
  subs: null
}, {
  slug: 'news24h',
  categoryId: 'news24h',
  name: '24HR',
  parentId: 0,
  parentSlug: '',
  api: '/api/v1/news/24h',
  subs: null
}, {
  slug: 'popular',
  categoryId: 'popular',
  name: '人氣',
  parentId: 0,
  parentSlug: '',
  api: '/api/v1/news/popular',
  subs: null
}];
var Categories = [].concat(BASE_CATEGORIES, _toConsumableArray(CategoriesRaw.items));

var CategoryMappingWithSubs = exports.CategoryMappingWithSubs = Categories.reduce(function (pValue, cValue, cIndex, cats) {
  var cat = cats[cIndex];
  var subs = null;
  var parentSlug = '';

  // build subs
  if (cat.parentId === 0) {
    subs = Categories.reduce(function (previousValue, currentValue, currentIndex, array) {
      var subCat = array[currentIndex];

      if (subCat.parentId === cat.categoryId) {
        previousValue.push(subCat.slug);
      }

      return previousValue;
    }, []);

    subs = subs.length > 0 ? subs : null;
  } else {
    // find parent's slug
    var parentCat = Categories.filter(function (_cat) {
      return _cat.categoryId === cat.parentId;
    });

    if (parentCat && parentCat.length > 0) {
      parentSlug = parentCat[0].slug;
    }
  }

  var result = _extends({}, cat, {
    parentSlug: parentSlug,
    subs: subs
  });

  pValue[cat.slug] = result; // eslint-disable-line no-param-reassign

  return pValue;
}, {});

/***/ }),

/***/ 8:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var gaDataset = function gaDataset(_ref) {
  var dataPrefix = _ref.dataPrefix,
      category = _ref.category,
      action = _ref.action,
      label = _ref.label;

  if (dataPrefix.length === 0 || !dataPrefix) {
    return null;
  }

  var gaRegExp = /^data-.*ga$/; // format restriction
  var eventsObj = dataPrefix.reduce(function (p, prefix) {
    var _p = p;

    if (gaRegExp.test(prefix)) {
      _p[prefix + "-category"] = category;
      _p[prefix + "-action"] = action;
      _p[prefix + "-label"] = label;
    }

    return _p;
  }, {});

  return eventsObj;
};

exports.default = gaDataset;

/***/ }),

/***/ 80:
/***/ (function(module, exports) {

module.exports = {"items":[{"categoryId":867,"name":"公告","parentId":0,"slug":"announcement"},{"categoryId":860,"name":"鉅亨新視界","parentId":0,"slug":"celebrity_area"},{"categoryId":865,"name":"其他表格","parentId":862,"slug":"chart"},{"categoryId":848,"name":"大陸房市","parentId":846,"slug":"cn_housenews"},{"categoryId":837,"name":"大陸政經","parentId":834,"slug":"cn_macro"},{"categoryId":834,"name":"A股港股","parentId":0,"slug":"cn_stock"},{"categoryId":846,"name":"房產","parentId":0,"slug":"cnyeshouse"},{"categoryId":869,"name":"興櫃公告","parentId":867,"slug":"eme_bull"},{"categoryId":841,"name":"能源","parentId":839,"slug":"energy"},{"categoryId":832,"name":"歐亞股","parentId":830,"slug":"eu_asia_stock"},{"categoryId":838,"name":"外匯","parentId":0,"slug":"forex"},{"categoryId":866,"name":"表一表八","parentId":862,"slug":"form1_8"},{"categoryId":852,"name":"基金","parentId":851,"slug":"fund"},{"categoryId":845,"name":"債券","parentId":839,"slug":"futu_bond"},{"categoryId":842,"name":"農作","parentId":839,"slug":"futu_produce"},{"categoryId":839,"name":"期貨","parentId":0,"slug":"future"},{"categoryId":849,"name":"香港房市","parentId":846,"slug":"hk_housenews"},{"categoryId":836,"name":"港股","parentId":834,"slug":"hk_stock"},{"categoryId":840,"name":"黃金","parentId":839,"slug":"precious_metals"},{"categoryId":843,"name":"原物料","parentId":839,"slug":"raw"},{"categoryId":835,"name":"A股","parentId":834,"slug":"sh_stock"},{"categoryId":853,"name":"消費","parentId":0,"slug":"spending"},{"categoryId":868,"name":"台股公告","parentId":867,"slug":"tw_bull"},{"categoryId":862,"name":"台股表格","parentId":0,"slug":"tw_calc"},{"categoryId":863,"name":"三大法人","parentId":862,"slug":"tw_fr"},{"categoryId":847,"name":"台灣房市","parentId":846,"slug":"tw_housenews"},{"categoryId":854,"name":"保險","parentId":851,"slug":"tw_insurance"},{"categoryId":828,"name":"台灣政經","parentId":826,"slug":"tw_macro"},{"categoryId":851,"name":"理財","parentId":0,"slug":"tw_money"},{"categoryId":829,"name":"台股盤勢","parentId":826,"slug":"tw_quo"},{"categoryId":826,"name":"台股","parentId":0,"slug":"tw_stock"},{"categoryId":827,"name":"台股新聞","parentId":826,"slug":"tw_stock_news"},{"categoryId":864,"name":"融資券","parentId":862,"slug":"tw_zq"},{"categoryId":831,"name":"美股","parentId":830,"slug":"us_stock"},{"categoryId":850,"name":"海外房市","parentId":846,"slug":"wd_housenews"},{"categoryId":833,"name":"國際政經","parentId":830,"slug":"wd_macro"},{"categoryId":830,"name":"國際股","parentId":0,"slug":"wd_stock"},{"categoryId":870,"name":"投資情報","parentId":0,"slug":"advertorial"},{"categoryId":871,"name":"晨星專欄","parentId":851,"slug":"morningstar"},{"categoryId":873,"name":"區塊鏈","parentId":0,"slug":"bc"},{"categoryId":875,"name":"區塊鏈應用","parentId":873,"slug":"bc_application"},{"categoryId":876,"name":"虛擬貨幣","parentId":873,"slug":"bc_virtual"},{"categoryId":877,"name":"指數","parentId":839,"slug":"index_futures"},{"categoryId":878,"name":"股票","parentId":839,"slug":"stock_futures"}],"message":"成功","statusCode":200}

/***/ }),

/***/ 81:
/***/ (function(module, exports) {

module.exports = {"items":[{"categoryId":867,"name":"公告","parentId":0,"slug":"announcement"},{"categoryId":860,"name":"鉅亨新視界","parentId":0,"slug":"celebrity_area"},{"categoryId":865,"name":"其他表格","parentId":862,"slug":"chart"},{"categoryId":848,"name":"大陸房市","parentId":846,"slug":"cn_housenews"},{"categoryId":837,"name":"大陸政經","parentId":834,"slug":"cn_macro"},{"categoryId":834,"name":"A股港股","parentId":0,"slug":"cn_stock"},{"categoryId":846,"name":"房產","parentId":0,"slug":"cnyeshouse"},{"categoryId":869,"name":"興櫃公告","parentId":867,"slug":"eme_bull"},{"categoryId":841,"name":"能源","parentId":839,"slug":"energy"},{"categoryId":832,"name":"歐亞股","parentId":830,"slug":"eu_asia_stock"},{"categoryId":838,"name":"外匯","parentId":0,"slug":"forex"},{"categoryId":866,"name":"表一表八","parentId":862,"slug":"form1_8"},{"categoryId":852,"name":"基金","parentId":851,"slug":"fund"},{"categoryId":845,"name":"債券","parentId":839,"slug":"futu_bond"},{"categoryId":842,"name":"農作","parentId":839,"slug":"futu_produce"},{"categoryId":839,"name":"期貨","parentId":0,"slug":"future"},{"categoryId":849,"name":"香港房市","parentId":846,"slug":"hk_housenews"},{"categoryId":836,"name":"港股","parentId":834,"slug":"hk_stock"},{"categoryId":840,"name":"黃金","parentId":839,"slug":"precious_metals"},{"categoryId":843,"name":"原物料","parentId":839,"slug":"raw"},{"categoryId":835,"name":"A股","parentId":834,"slug":"sh_stock"},{"categoryId":853,"name":"消費","parentId":0,"slug":"spending"},{"categoryId":868,"name":"台股公告","parentId":867,"slug":"tw_bull"},{"categoryId":862,"name":"台股表格","parentId":0,"slug":"tw_calc"},{"categoryId":863,"name":"三大法人","parentId":862,"slug":"tw_fr"},{"categoryId":847,"name":"台灣房市","parentId":846,"slug":"tw_housenews"},{"categoryId":854,"name":"保險","parentId":851,"slug":"tw_insurance"},{"categoryId":828,"name":"台灣政經","parentId":826,"slug":"tw_macro"},{"categoryId":851,"name":"理財","parentId":0,"slug":"tw_money"},{"categoryId":829,"name":"台股盤勢","parentId":826,"slug":"tw_quo"},{"categoryId":826,"name":"台股","parentId":0,"slug":"tw_stock"},{"categoryId":827,"name":"台股新聞","parentId":826,"slug":"tw_stock_news"},{"categoryId":864,"name":"融資券","parentId":862,"slug":"tw_zq"},{"categoryId":831,"name":"美股","parentId":830,"slug":"us_stock"},{"categoryId":850,"name":"海外房市","parentId":846,"slug":"wd_housenews"},{"categoryId":833,"name":"國際政經","parentId":830,"slug":"wd_macro"},{"categoryId":830,"name":"國際股","parentId":0,"slug":"wd_stock"},{"categoryId":870,"name":"投資情報","parentId":0,"slug":"advertorial"},{"categoryId":871,"name":"晨星專欄","parentId":851,"slug":"morningstar"},{"categoryId":873,"name":"區塊鏈","parentId":0,"slug":"bc"},{"categoryId":875,"name":"區塊鏈應用","parentId":873,"slug":"bc_application"},{"categoryId":876,"name":"虛擬貨幣","parentId":873,"slug":"bc_virtual"},{"categoryId":877,"name":"指數","parentId":839,"slug":"index_futures"},{"categoryId":878,"name":"股票","parentId":839,"slug":"stock_futures"}],"message":"成功","statusCode":200}

/***/ }),

/***/ 82:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
/* eslint-disable import/prefer-default-export */
var FIXED_HEADER_NONE = exports.FIXED_HEADER_NONE = 'FIXED_HEADER_NONE';
var FIXED_HEADER_FULL = exports.FIXED_HEADER_FULL = 'FIXED_HEADER_FULL';
var FIXED_HEADER_SUB = exports.FIXED_HEADER_SUB = 'FIXED_HEADER_SUB';

/***/ }),

/***/ 83:
/***/ (function(module, exports) {

module.exports = {"items":[{"title":"首頁","url":"https://www.cnyes.com/"},{"title":"新聞","url":"http://news.cnyes.com/","leftList":[{"title":"頭條","url":"https://news.cnyes.com/news/cat/headline"},{"title":"人氣","url":"https://news.cnyes.com/trending"},{"title":"台股","url":"https://news.cnyes.com/news/cat/tw_stock"},{"title":"國際股","url":"https://news.cnyes.com/news/cat/wd_stock"},{"title":"Ａ股港股","url":"https://news.cnyes.com/news/cat/cn_stock"},{"title":"外匯","url":"https://news.cnyes.com/news/cat/forex"},{"title":"期貨","url":"https://news.cnyes.com/news/cat/future"}]},{"title":"台股","url":"http://www.cnyes.com/twstock/index.htm","leftList":[{"title":"台指期","url":"http://www.cnyes.com/twfutures/index.htm"},{"title":"興櫃","url":"http://www.cnyes.com/presh/index.htm"},{"title":"未上市","url":"http://www.cnyes.com/pre/index.htm"},{"title":"台股下單","url":"https://stock.cnyes.com?asideTab=1"}]},{"title":"股市Talk","url":"https://campaign.cnyes.com/topics/anuestock/","leftList":[{"title":"台股股市Talk","url":"https://stock.cnyes.com"},{"title":"國際股市Talk","url":"https://global-stock.cnyes.com"},{"title":"智慧選股","url":"https://campaigns.cnyes.com/anue-with-tdsc/"}]},{"title":"外匯","url":"https://www.cnyes.com/forex/","leftList":[{"title":"新聞","url":"https://news.cnyes.com/news/cat/forex"},{"title":"路透即時外匯","url":"https://www.cnyes.com/forex/reuters"},{"title":"交叉匯率","url":"https://www.cnyes.com/forex/crosslist"},{"title":"新台幣","url":"https://www.cnyes.com/forex/twd"},{"title":"人民幣","url":"https://www.cnyes.com/forex/rmb"},{"title":"日元","url":"https://www.cnyes.com/forex/jpy"},{"title":"歐元","url":"https://www.cnyes.com/forex/eur"},{"title":"研報","url":"https://news.cnyes.com/news/cat/fx_report"}]},{"title":"全球市場","url":"http://www.cnyes.com/global/IndexImmediateQuotedPrice/","leftList":[{"title":"國際股","url":"http://www.cnyes.com/global/IndexImmediateQuotedPrice/"},{"title":"美股","url":"http://www.cnyes.com/usstock"},{"title":"港股","url":"http://www.cnyes.com/hkstock/"},{"title":"滬深股","url":"https://www.cnyes.com/cnstock"},{"title":"日股","url":"http://www.cnyes.com/JP/index.htm"},{"title":"國際期貨","url":"http://www.cnyes.com/futures/index.htm"},{"title":"債券","url":"http://www.cnyes.com/bond/index.htm"},{"title":"黃金","url":"http://www.cnyes.com/gold/index.htm"},{"title":"全球央行","url":"http://www.cnyes.com/CentralBank/index.htm"},{"title":"經濟指標","url":"http://www.cnyes.com/economy/indicator/Page/schedule.aspx"},{"title":"StockQ","url":"http://money.cnyes.com/StockQ.aspx"}]},{"title":"基金","url":"http://fund.cnyes.com/index.htm","leftList":[{"title":"我的基金","url":"http://fund.cnyes.com/MyFunds.aspx"},{"title":"基金搜尋","url":"http://fund.cnyes.com/Search.aspx"},{"title":"基金排行","url":"http://fund.cnyes.com/ranking/index.htm"},{"title":"債券專區","url":"http://fund.cnyes.com/Fixedincome/"},{"title":"配息專區","url":"http://fund.cnyes.com/dividend/index.htm"},{"title":"研究報告","url":"https://news.cnyes.com/news/cat/fund_comment"},{"title":"基金交易平台","url":"https://www.anuefund.com/?utm_source=cnyes&utm_medium=index_menu"},{"title":"AI 理財","url":"https://www.anuefund.com/EC/ROBO/?utm_source=cnyes&utm_medium=index_menu"},{"title":"趨勢寶","url":"https://www.anuefund.com/EC/VFUND/?utm_source=cnyes&utm_medium=index_menu"},{"title":"基金比賽","url":"https://campaign.cnyes.com/topics/fundevent2019/?utm_source=cnyes&utm_medium=index_menu"}]},{"title":"交易","url":"","leftList":[{"title":"基金交易","url":"https://www.anuefund.com/Index.aspx?utm_source=cnyes&utm_medium=channel_mainpage"}],"rightListTitle":"鉅亨基金交易平台","rightList":[{"title":"帳戶登入","url":"https://www.anuefund.com/Login.aspx?utm_source=cnyes&utm_medium=channel_login_test"},{"title":"免費開戶","url":"https://www.anuefund.com/Account/open_notice.aspx?utm_source=cnyes&utm_medium=channel_newuser_test&utm_term=fundindex_test"},{"title":"基金申購","url":"https://www.anuefund.com/Fund/Catalogue.aspx?utm_source=cnyes&utm_medium=channel_newbuy_test"},{"title":"最新優惠","url":"https://www.anuefund.com/Guide/Expenses.aspx?utm_source=cnyes&utm_medium=channel_preferential"},{"title":"投資雷達","url":"https://www.anuefund.com/Tool/SuggestionList.aspx?utm_source=cnyes&utm_medium=channel_radar"},{"title":"AI 理財","url":"https://www.anuefund.com/EC/ROBO/?utm_source=cnyes&utm_medium=channel_login"},{"title":"趨勢寶","url":"https://www.anuefund.com/EC/VFUND/?utm_source=cnyes&utm_medium=channel_login"}]},{"title":"理財","url":"http://www.cnyes.com/money/index.htm","leftList":[{"title":"銀行服務","url":"http://www.cnyes.com/money/BankService.aspx"},{"title":"試算工具","url":"http://www.cnyes.com/money/BankCalculation.aspx"},{"title":"理財新聞","url":"https://news.cnyes.com/news/cat/tw_money"},{"title":"固定收益","url":"http://www.cnyes.com/fixedincome/index.htm"}]},{"title":"部落格","url":"http://blog.cnyes.com/","leftList":[{"title":"名家專區","url":"http://blog.cnyes.com/Excerpt.aspx"},{"title":"人氣排行榜","url":"http://blog.cnyes.com/SearchBlog.aspx?Purpose=Hit&ga=nav"},{"title":"最新文章","url":"http://blog.cnyes.com/WorldFresh.aspx"}]},{"title":"基金老司機","url":"https://invest.anue.com?utm_source=cnyes&utm_medium=header&utm_campaign=news"},{"title":"影音","url":"https://www.cnyes.com/video?utm_source=cnyes&utm_medium=header&utm_campaign=news","isNew":true}]}

/***/ }),

/***/ 84:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(0);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = __webpack_require__(4);

var _classnames2 = _interopRequireDefault(_classnames);

var _getStyleName = __webpack_require__(5);

var _getStyleName2 = _interopRequireDefault(_getStyleName);

var _Footer = __webpack_require__(27);

var _Footer2 = _interopRequireDefault(_Footer);

var _links = __webpack_require__(85);

var _links2 = _interopRequireDefault(_links);

var _propTypes3 = __webpack_require__(7);

var _gaDataset = __webpack_require__(8);

var _gaDataset2 = _interopRequireDefault(_gaDataset);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /* eslint import/no-extraneous-dependencies: ["error", {"peerDependencies": true}] */


var ANUE_INDEX = 'https://www.cnyes.com';

var Footer = function (_PureComponent) {
  _inherits(Footer, _PureComponent);

  function Footer() {
    _classCallCheck(this, Footer);

    return _possibleConstructorReturn(this, (Footer.__proto__ || Object.getPrototypeOf(Footer)).apply(this, arguments));
  }

  _createClass(Footer, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          mobileNavs = _props.mobileNavs,
          desktopNavs = _props.desktopNavs,
          socials = _props.socials,
          now = _props.now,
          Link = _props.Link,
          dataPrefix = _props.dataPrefix;

      var thisYear = new Date(now).getFullYear();
      var logoClass = (0, _classnames2.default)((0, _getStyleName2.default)(_Footer2.default, 'logo'), (0, _getStyleName2.default)(_Footer2.default, 'hidden-mobile'));
      var socialsClass = (0, _classnames2.default)((0, _getStyleName2.default)(_Footer2.default, 'socials'), (0, _getStyleName2.default)(_Footer2.default, 'hidden-mobile'));

      /* eslint-disable jsx-a11y/accessible-emoji */
      return _react2.default.createElement(
        'div',
        { id: (0, _getStyleName2.default)(_Footer2.default, 'cnyes-footer-wrapper'), className: (0, _classnames2.default)('theme-footer-wrapper') },
        _react2.default.createElement(
          'footer',
          { className: (0, _getStyleName2.default)(_Footer2.default, 'main-footer') },
          _react2.default.createElement(
            'div',
            { className: logoClass },
            Link ? _react2.default.createElement(Link, _extends({ to: '/' }, (0, _gaDataset2.default)({ dataPrefix: dataPrefix, category: 'Footer', action: 'click', label: 'home' }))) : _react2.default.createElement('a', _extends({ href: ANUE_INDEX }, (0, _gaDataset2.default)({ dataPrefix: dataPrefix, category: 'Footer', action: 'click', label: 'home' })))
          ),
          _react2.default.createElement(
            'div',
            { className: (0, _getStyleName2.default)(_Footer2.default, 'nav') },
            _react2.default.createElement(
              'nav',
              { className: (0, _classnames2.default)((0, _getStyleName2.default)(_Footer2.default, 'mobile-nav'), (0, _getStyleName2.default)(_Footer2.default, 'hidden-desktop')) },
              this.constructor.renderNavs(mobileNavs, dataPrefix)
            ),
            _react2.default.createElement(
              'nav',
              { className: (0, _classnames2.default)((0, _getStyleName2.default)(_Footer2.default, 'desktop-nav'), (0, _getStyleName2.default)(_Footer2.default, 'hidden-mobile')) },
              this.constructor.renderNavs(desktopNavs, dataPrefix)
            ),
            _react2.default.createElement(
              'small',
              { className: (0, _getStyleName2.default)(_Footer2.default, 'copyright-anue') },
              _react2.default.createElement(
                'span',
                null,
                '\xA9'
              ),
              _react2.default.createElement(
                'span',
                { className: (0, _getStyleName2.default)(_Footer2.default, 'hidden-mobile') },
                ' Copyright'
              ),
              _react2.default.createElement(
                'span',
                null,
                ' 2000-',
                thisYear,
                ' Anue.com All rights reserved.'
              ),
              _react2.default.createElement(
                'span',
                { className: (0, _getStyleName2.default)(_Footer2.default, 'hidden-mobile') },
                ' \u672A\u7D93\u6388\u6B0A \u4E0D\u5F97\u8F49\u8F09'
              )
            )
          ),
          _react2.default.createElement(
            'div',
            { className: socialsClass },
            this.constructor.renderSocials(socials, dataPrefix)
          )
        )
      );
    }
  }], [{
    key: 'renderNavs',
    value: function renderNavs(navs, dataPrefix) {
      return navs.map(function (item) {
        var url = item.url,
            name = item.name,
            title = item.title,
            onClick = item.onClick;


        return _react2.default.createElement(
          'a',
          _extends({
            href: url,
            onClick: onClick,
            key: 'footer-nav-' + name,
            target: '_blank',
            rel: 'noopener noreferrer'
          }, (0, _gaDataset2.default)({ dataPrefix: dataPrefix, category: 'Footer', action: 'click', label: title })),
          title
        );
      });
    }
  }, {
    key: 'renderSocials',
    value: function renderSocials(socials, dataPrefix) {
      return socials.map(function (item) {
        return _react2.default.createElement(
          'div',
          { className: (0, _getStyleName2.default)(_Footer2.default, 'social-item'), key: 'footer-socials-' + item.name },
          _react2.default.createElement(
            'div',
            { className: (0, _getStyleName2.default)(_Footer2.default, 'title') },
            item.title
          ),
          _react2.default.createElement(
            'a',
            _extends({
              className: (0, _getStyleName2.default)(_Footer2.default, 'social-icon') + ' ' + (0, _getStyleName2.default)(_Footer2.default, 'cnyes-media-' + item.name),
              href: item.url,
              rel: 'noopener noreferrer',
              target: '_blank'
            }, (0, _gaDataset2.default)({ dataPrefix: dataPrefix, category: 'Footer', action: 'click', label: item.title })),
            item.title
          )
        );
      });
    }
  }]);

  return Footer;
}(_react.PureComponent);

Footer.defaultProps = {
  mobileNavs: _links2.default.mobileNavs,
  desktopNavs: _links2.default.desktopNavs,
  socials: _links2.default.socials,
  Link: undefined,
  dataPrefix: ['data-proj-ga']
};
exports.default = Footer;

/***/ }),

/***/ 85:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var footerLinks = {
  mobileNavs: [{
    title: '電腦版',
    name: 'pc',
    url: '/'
  }, {
    title: '著作權',
    name: 'copyright',
    url: '/copyright'
  }, {
    title: '意見與回饋',
    name: 'feedback',
    onClick: function onClick(e) {
      e.preventDefault();

      if (typeof window !== 'undefined') {
        var formURI = 'https://docs.google.com/forms/d/e/1FAIpQLSepnhnP4FgnRXiqezmVme7YX7xqXhxPsd57qbaJ09yJbxTt0g/viewform' + ('?usp=pp_url&entry.364853985=' + document.location.href);

        window.open(formURI, '_blank');
      }
    }
  }],
  desktopNavs: [{
    title: '關於我們',
    name: 'about',
    url: 'http://www.cnyes.com/cnyes_about/cnyes_about.html'
  }, {
    title: '集團簡介',
    name: 'anueCorp',
    url: 'https://www.anuegroup.com.tw/'
  }, {
    title: '廣告服務',
    name: 'ad',
    url: 'http://www.cnyes.com/cnyes_about/cnyes_AD01.html'
  }, {
    title: '金融資訊元件',
    name: 'financial',
    url: 'http://www.cnyes.com/cnyes_about/cnyes_pas01.html'
  }, {
    title: '聯絡我們',
    name: 'contact',
    url: 'http://www.cnyes.com/cnyes_about/cnyes_ctcUsTpe.html'
  }, {
    title: '徵才',
    name: 'job',
    url: 'http://www.104.com.tw/jobbank/custjob/index.php?r=cust&j=5e6042253446402330683b1d1d1d1d5f2443a363189j01'
  }, {
    title: '網站地圖',
    name: 'sitemap',
    url: 'http://www.cnyes.com/cnyes_about/site_map.html'
  }, {
    title: '法律聲明',
    name: 'legal',
    url: 'http://www.cnyes.com/cnyes_about/cnyes_sos01.html'
  }],
  socials: [{
    title: '粉 絲 團',
    name: 'facebook',
    url: 'https://www.facebook.com/anuetw/'
  }, {
    title: '鉅亨Line',
    name: 'line',
    url: 'https://line.me/ti/p/@ZLU0489G'
  }, {
    title: '鉅亨APP',
    name: 'app',
    url: 'http://www.cnyes.com/app?utm_source=cnyes&utm_medium=desktop&utm_campaign=desktop_footer'
  }]
};

exports.default = footerLinks;

/***/ }),

/***/ 86:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(0);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = __webpack_require__(4);

var _classnames2 = _interopRequireDefault(_classnames);

var _MobileMenu = __webpack_require__(87);

var _MobileMenu2 = _interopRequireDefault(_MobileMenu);

var _getStyleName = __webpack_require__(5);

var _getStyleName2 = _interopRequireDefault(_getStyleName);

var _gaDataset = __webpack_require__(8);

var _gaDataset2 = _interopRequireDefault(_gaDataset);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function MobileMenu(_ref) {
  var showCatBoard = _ref.showCatBoard,
      channelName = _ref.channelName,
      hideTopBar = _ref.hideTopBar,
      dataPrefix = _ref.dataPrefix;

  return _react2.default.createElement(
    'div',
    { className: (0, _classnames2.default)(_defineProperty({}, (0, _getStyleName2.default)(_MobileMenu2.default, 'index-header-top-bar'), !hideTopBar)) },
    _react2.default.createElement(
      'a',
      _extends({
        href: 'https://m.cnyes.com/news',
        className: _MobileMenu2.default['index-header-logo']
      }, (0, _gaDataset2.default)({ dataPrefix: dataPrefix, category: 'Logo', action: 'click', label: 'home' })),
      '\u9245\u4EA8'
    ),
    channelName && channelName.length && _react2.default.createElement(
      'div',
      { className: _MobileMenu2.default['header-channel-label'] },
      channelName
    ),
    _react2.default.createElement(
      'nav',
      null,
      _react2.default.createElement('span', { className: _MobileMenu2.default['index-header-menu'], onClick: showCatBoard })
    )
  );
}

MobileMenu.defaultProps = {
  channelName: undefined,
  hideTopBar: false,
  dataPrefix: ['data-proj-ga']
};

exports.default = MobileMenu;

/***/ }),

/***/ 87:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin
module.exports = {"index-header-top-bar":"_3Ny_k","index-header":"_27f3x","index-header-menu":"_3Wta4","index-header-logo":"_3KhXq","header-channel-label":"_3_hO3","index-header-tabs":"_3vTaA","index-search-hint":"_3ZBZ7","hint-close":"_3XvKH","index-header-search":"_2mXhz"};

/***/ }),

/***/ 88:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(0);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = __webpack_require__(4);

var _classnames2 = _interopRequireDefault(_classnames);

var _NavItem = __webpack_require__(89);

var _NavItem2 = _interopRequireDefault(_NavItem);

var _NavDownloadItem = __webpack_require__(91);

var _NavDownloadItem2 = _interopRequireDefault(_NavDownloadItem);

var _navConfig = __webpack_require__(93);

var _navConfig2 = _interopRequireDefault(_navConfig);

var _NavBoard = __webpack_require__(94);

var _NavBoard2 = _interopRequireDefault(_NavBoard);

var _getStyleName = __webpack_require__(5);

var _getStyleName2 = _interopRequireDefault(_getStyleName);

var _propTypes3 = __webpack_require__(7);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var NavBoard = function (_PureComponent) {
  _inherits(NavBoard, _PureComponent);

  function NavBoard() {
    _classCallCheck(this, NavBoard);

    return _possibleConstructorReturn(this, (NavBoard.__proto__ || Object.getPrototypeOf(NavBoard)).apply(this, arguments));
  }

  _createClass(NavBoard, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          hideTopBar = _props.hideTopBar,
          isCatBoardOpen = _props.isCatBoardOpen,
          hideCatBoard = _props.hideCatBoard,
          navs = _props.navs,
          downloadLinks = _props.downloadLinks,
          dataPrefix = _props.dataPrefix;

      var catBoardClassName = isCatBoardOpen ? _NavBoard2.default['nav-board-show'] : _NavBoard2.default['nav-board'];

      return _react2.default.createElement(
        'aside',
        {
          className: (0, _classnames2.default)(catBoardClassName, 'theme-gradient', _defineProperty({}, (0, _getStyleName2.default)(_NavBoard2.default, 'nav-board-top-bar'), !hideTopBar))
        },
        _react2.default.createElement(
          'header',
          null,
          _react2.default.createElement(
            'span',
            { className: _NavBoard2.default['nav-board-logo'] },
            '\u9245\u4EA8'
          ),
          _react2.default.createElement('button', { className: _NavBoard2.default['nav-board-btn-close'], onClick: hideCatBoard })
        ),
        _react2.default.createElement(
          'main',
          null,
          downloadLinks.map(function (link) {
            return [_react2.default.createElement(
              'h4',
              { key: 'link-' + link.title, className: _NavBoard2.default['cat-board-nav-link'] },
              _react2.default.createElement(_NavDownloadItem2.default, { title: link.title, url: link.url })
            )];
          }),
          navs.map(function (nav) {
            return [_react2.default.createElement(
              'h4',
              { key: nav.name + '-h4', className: 'nav-board-subtitle theme-nav-board-subtitle' },
              nav.title
            ), _react2.default.createElement(
              'nav',
              { key: nav.name + '-nav', className: _NavBoard2.default['cat-board-nav'] },
              nav.items && nav.items.map(function (item) {
                return _react2.default.createElement(_NavItem2.default, {
                  key: item.name,
                  navTitle: nav.title,
                  item: item,
                  onClick: hideCatBoard,
                  dataPrefix: dataPrefix
                });
              })
            )];
          })
        )
      );
    }
  }]);

  return NavBoard;
}(_react.PureComponent);

NavBoard.defaultProps = {
  isCatBoardOpen: false,
  hideCatBoard: undefined,
  hideTopBar: false,
  navs: _navConfig2.default,
  downloadLinks: [],
  dataPrefix: ['data-proj-ga']
};
exports.default = NavBoard;

/***/ }),

/***/ 89:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(0);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _gaDataset = __webpack_require__(8);

var _gaDataset2 = _interopRequireDefault(_gaDataset);

var _NavItem = __webpack_require__(90);

var _NavItem2 = _interopRequireDefault(_NavItem);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function NavItem(_ref) {
  var item = _ref.item,
      navTitle = _ref.navTitle,
      onClick = _ref.onClick,
      dataPrefix = _ref.dataPrefix;

  return _react2.default.createElement(
    'a',
    _extends({
      key: item.name,
      className: _NavItem2.default['nav-board-link'],
      href: item.url,
      'data-ga-category': '\u5074\u9078\u55AE',
      'data-ga-action': navTitle,
      'data-ga-label': item.title
    }, (0, _gaDataset2.default)({ dataPrefix: dataPrefix, category: 'Header', action: 'click', label: navTitle + '_' + item.title }), {
      onClick: onClick
    }),
    item.title
  );
}

NavItem.defaultProps = {
  onClick: undefined,
  dataPrefix: ['data-proj-ga']
};

exports.default = NavItem;

/***/ }),

/***/ 90:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin
module.exports = {"nav-board-link":"_3Nksy"};

/***/ }),

/***/ 91:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(0);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _NavDownloadItem = __webpack_require__(92);

var _NavDownloadItem2 = _interopRequireDefault(_NavDownloadItem);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var handleClick = function handleClick(e, url) {
  e.preventDefault();
  window.open(url);
};

var NavDownloadItem = function NavDownloadItem(_ref) {
  var title = _ref.title,
      url = _ref.url;
  return _react2.default.createElement(
    'a',
    { className: _NavDownloadItem2.default['nav-board-link-full-width'], onClick: function onClick(e) {
        return handleClick(e, url);
      } },
    title
  );
};

exports.default = NavDownloadItem;

/***/ }),

/***/ 92:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin
module.exports = {"nav-board-link-full-width":"_KvlCd"};

/***/ }),

/***/ 93:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = [{
  name: 'mweb',
  title: '新聞',
  items: [{
    name: 'headline',
    title: '即時頭條',
    url: 'https://m.cnyes.com/news/cat/headline'
  }, {
    name: 'news24h',
    title: '主編精選',
    url: 'https://m.cnyes.com/news/cat/news24h'
  }, {
    name: 'tw_stock',
    title: '台股',
    url: 'https://m.cnyes.com/news/cat/tw_stock'
  }, {
    name: 'wd_stock',
    title: '國際股',
    url: 'https://m.cnyes.com/news/cat/wd_stock'
  }, {
    name: 'forex',
    title: '外匯',
    url: 'https://m.cnyes.com/news/cat/forex'
  }, {
    name: 'future',
    title: '期貨',
    url: 'https://m.cnyes.com/news/cat/future'
  }, {
    name: 'tw_money',
    title: '理財',
    url: 'https://m.cnyes.com/news/cat/tw_money'
  }, {
    name: 'cn_stock',
    title: 'A股港股',
    url: 'https://m.cnyes.com/news/cat/cn_stock'
  }, {
    name: 'cnyeshouse',
    title: '房產',
    url: 'https://m.cnyes.com/news/cat/cnyeshouse'
  }, {
    name: 'celebrity_area',
    title: '鉅亨新視界',
    url: 'https://m.cnyes.com/news/cat/celebrity_area'
  }, {
    name: 'popular',
    title: '人氣新聞',
    url: 'https://m.cnyes.com/news/cat/popular'
  }, {
    name: 'topic',
    title: '專題報導',
    url: 'https://topics.cnyes.com'
  }]
}, {
  name: 'mweb2',
  title: '影音',
  items: [{
    name: 'world',
    title: 'Allen看世界',
    url: 'https://m.cnyes.com/video/cat/world'
  }, {
    name: 'invest',
    title: '理財芳程式',
    url: 'https://m.cnyes.com/video/cat/invest'
  }, {
    name: 'fund',
    title: '鉅亨FUND大鏡',
    url: 'https://m.cnyes.com/video/cat/fund'
  }, {
    name: 'hkstock',
    title: '港股大講堂',
    url: 'https://m.cnyes.com/video/cat/hkstock'
  }, {
    name: 'videonews',
    title: '影音新聞',
    url: 'https://m.cnyes.com/video/cat/videonews'
  }]
}, {
  name: 'forex',
  title: '外匯',
  items: [{
    name: 'USD',
    title: '美元',
    url: 'https://forex.cnyes.com/currency/USD/TWD'
  }, {
    name: 'JPY',
    title: '日幣',
    url: 'https://forex.cnyes.com/currency/JPY/TWD'
  }, {
    name: 'EUR',
    title: '歐元',
    url: 'https://forex.cnyes.com/currency/EUR/TWD'
  }, {
    name: 'KRW',
    title: '韓幣',
    url: 'https://forex.cnyes.com/currency/KRW/TWD'
  }, {
    name: 'CNY',
    title: '人民幣',
    url: 'https://forex.cnyes.com/currency/CNY/TWD'
  }, {
    name: 'HKD',
    title: '港幣',
    url: 'https://forex.cnyes.com/currency/HKD/TWD'
  }, {
    name: 'ZAR',
    title: '南非幣',
    url: 'https://forex.cnyes.com/currency/ZAR/TWD'
  }, {
    name: 'AUD',
    title: '澳幣',
    url: 'https://forex.cnyes.com/currency/AUD/TWD'
  }]
}, {
  name: 'crypto',
  title: '虛擬貨幣',
  items: [{
    name: 'BTC',
    title: '比特幣',
    url: 'https://crypto.cnyes.com/BTC/24h'
  }, {
    name: 'ETH',
    title: '以太幣',
    url: 'https://crypto.cnyes.com/ETH/24h'
  }, {
    name: 'XRP',
    title: 'Ripple',
    url: 'https://crypto.cnyes.com/XRP/24h'
  }, {
    name: 'BCH',
    title: 'Bitcoin Cash',
    url: 'https://crypto.cnyes.com/BCH/24h'
  }, {
    name: 'LTC',
    title: 'Litcoin',
    url: 'https://crypto.cnyes.com/LTC/24h'
  }, {
    name: 'EOS',
    title: 'EOS',
    url: 'https://crypto.cnyes.com/EOS/24h'
  }]
}, {
  name: 'market',
  title: '市場',
  items: [{
    name: 'market_taiwan',
    title: '台股指數',
    url: 'https://m.cnyes.com/market/#market_taiwan'
  }, {
    name: 'market_global',
    title: '國際指數',
    url: 'https://m.cnyes.com/market/#market_global'
  }, {
    name: 'market_currency',
    title: '國際外匯',
    url: 'https://m.cnyes.com/market/#market_currency'
  }, {
    name: 'index_tse',
    title: '台股',
    url: 'https://m.cnyes.com/twstock/index_tse.aspx'
  }, {
    name: 'mystock',
    title: '自選股',
    url: 'https://m.cnyes.com/person/mystock.aspx'
  }, {
    name: 'news/search',
    title: '個股查詢',
    url: 'https://m.cnyes.com/news/search'
  }, {
    name: 'fund',
    title: '基金',
    url: 'https://fund.cnyes.com'
  }]
}, {
  name: 'channels',
  title: '頻道',
  items: [{
    name: 'blog',
    title: 'Blog',
    url: 'http://m.blog.cnyes.com'
  }]
}];

/***/ }),

/***/ 94:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin
module.exports = {"cat-board-nav":"_wwdaL","cat-board-nav-link":"_1C1Ae","nav-board-top-bar":"_2EEsr","nav-board":"_3RjfV","nav-board-show":"_38glh","nav-board-logo":"_23siC","nav-board-btn-close":"_-kDn3"};

/***/ }),

/***/ 95:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(0);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _getStyleName = __webpack_require__(5);

var _getStyleName2 = _interopRequireDefault(_getStyleName);

var _localStorageWrapper = __webpack_require__(39);

var _localStorageWrapper2 = _interopRequireDefault(_localStorageWrapper);

var _Survey = __webpack_require__(96);

var _Survey2 = _interopRequireDefault(_Survey);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var EXPIRED_AFTER = 6 * 30 * 24 * 60 * 60 * 1000; // 六個月

var Survey = function (_PureComponent) {
  _inherits(Survey, _PureComponent);

  function Survey(props) {
    _classCallCheck(this, Survey);

    var _this = _possibleConstructorReturn(this, (Survey.__proto__ || Object.getPrototypeOf(Survey)).call(this, props));

    _this.removeExpiredSurveyRecords = function () {
      var now = new Date();

      _localStorageWrapper2.default.filter(function (key) {
        return key.includes('surveyNewsId:') && now - _localStorageWrapper2.default.getItem(key) > EXPIRED_AFTER;
      }).forEach(_localStorageWrapper2.default.removeItem);
    };

    _this.handleClick = function () {
      _localStorageWrapper2.default.setItem('surveyNewsId:' + _this.props.newsId, new Date().getTime());

      _this.setState({ clicked: true });
    };

    _this.state = {
      clicked: false
    };

    _this.removeExpiredSurveyRecords();

    // 如果 localStorage 裡有包含該新聞 ID 的 key，代表六個內使用者回答過該新聞的調查，便不再顯示調查框
    if (_localStorageWrapper2.default.getItem('surveyNewsId:' + _this.props.newsId)) _this.surveyed = true;
    return _this;
  }

  _createClass(Survey, [{
    key: 'render',
    value: function render() {
      var clicked = this.state.clicked;


      if (this.surveyed) return null;

      if (clicked) {
        return _react2.default.createElement(
          'div',
          { className: (0, _getStyleName2.default)(_Survey2.default, 'cnyes-survey') },
          '\u611F\u8B1D\u60A8\u7684\u5BF6\u8CB4\u610F\u898B'
        );
      }

      var newsId = this.props.newsId;


      return _react2.default.createElement(
        'div',
        { className: (0, _getStyleName2.default)(_Survey2.default, 'cnyes-survey') },
        _react2.default.createElement(
          'div',
          { className: (0, _getStyleName2.default)(_Survey2.default, 'cnyes-survey-question') },
          '\u9019\u7BC7\u65B0\u805E\u5C0D\u60A8\u5728\u6295\u8CC7\u4E0A\u662F\u5426\u6709\u5E6B\u52A9\uFF1F'
        ),
        _react2.default.createElement(
          'div',
          { className: (0, _getStyleName2.default)(_Survey2.default, 'cnyes-survey-options') },
          _react2.default.createElement(
            'button',
            {
              'data-ga-category': '\u5167\u5BB9\u662F\u5426\u6709\u52A9\u6295\u8CC7',
              'data-ga-action': '\u6709\u5E6B\u52A9',
              'data-ga-label': newsId,
              onClick: this.handleClick
            },
            '\u662F'
          ),
          _react2.default.createElement(
            'button',
            {
              'data-ga-category': '\u5167\u5BB9\u662F\u5426\u6709\u52A9\u6295\u8CC7',
              'data-ga-action': '\u6C92\u5E6B\u52A9',
              'data-ga-label': newsId,
              onClick: this.handleClick
            },
            '\u5426'
          )
        )
      );
    }
  }]);

  return Survey;
}(_react.PureComponent);

exports.default = Survey;

/***/ }),

/***/ 96:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin
module.exports = {"cnyes-survey":"_1UwPA","cnyes-survey-question":"_35aIo","cnyes-survey-options":"_28KgD"};

/***/ })

/******/ });
});