(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("prop-types"), require("react"));
	else if(typeof define === 'function' && define.amd)
		define(["prop-types", "react"], factory);
	else {
		var a = typeof exports === 'object' ? factory(require("prop-types"), require("react")) : factory(root["prop-types"], root["react"]);
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(typeof self !== 'undefined' ? self : this, function(__WEBPACK_EXTERNAL_MODULE_0__, __WEBPACK_EXTERNAL_MODULE_1__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/dest/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 221);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_0__;

/***/ }),

/***/ 1:
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_1__;

/***/ }),

/***/ 221:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(222);


/***/ }),

/***/ 222:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(0);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _SimpleModal = __webpack_require__(40);

var _SimpleModal2 = _interopRequireDefault(_SimpleModal);

var _LoginModal = __webpack_require__(223);

var _LoginModal2 = _interopRequireDefault(_LoginModal);

var _propTypes3 = __webpack_require__(7);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var LoginModal = function (_PureComponent) {
  _inherits(LoginModal, _PureComponent);

  function LoginModal() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, LoginModal);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = LoginModal.__proto__ || Object.getPrototypeOf(LoginModal)).call.apply(_ref, [this].concat(args))), _this), _this.closeHandler = function () {
      _this.props.setVisible(false);
    }, _this.loginFactory = function (loginMethod, loginMethodName) {
      return function () {
        _this.props.setVisible(false);
        _this.props.callLoadingModal(true);

        var auth = _this.context.auth;

        // prettier-ignore

        auth[loginMethod]().then(undefined, function (e) {
          console.error('login error', e);
          alert(loginMethodName + ' \u767B\u5165\u6642\u767C\u751F\u554F\u984C\uFF0C\u8ACB\u7A0D\u5019\u518D\u8A66\u3002');
        }).then(function () {
          return _this.props.callLoadingModal(false);
        });
      };
    }, _this.loginFB = _this.loginFactory('loginFB', 'Facebook'), _this.loginGoogle = _this.loginFactory('loginGoogle', 'Google'), _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(LoginModal, [{
    key: 'render',
    value: function render() {
      var visible = this.props.visible;


      return _react2.default.createElement(
        _SimpleModal2.default,
        { enableModal: visible, closeHandler: this.closeHandler },
        _react2.default.createElement(
          'div',
          { className: _LoginModal2.default['login-modal-wrapper'] },
          _react2.default.createElement(
            'h2',
            null,
            '\u767B\u5165'
          ),
          _react2.default.createElement('button', { className: _LoginModal2.default['login-button-facebook'], onClick: this.loginFB }),
          _react2.default.createElement('button', { className: _LoginModal2.default['login-button-google'], onClick: this.loginGoogle }),
          _react2.default.createElement(
            'div',
            { className: _LoginModal2.default.footer },
            _react2.default.createElement(
              'p',
              null,
              '\u8A3B\u518A\u524D\u8ACB\u8A73\u95B1',
              ' ',
              _react2.default.createElement(
                'a',
                { href: '/agreement', target: '_blank', rel: 'noopener noreferrer', className: _LoginModal2.default.agreement },
                '\u6703\u54E1\u670D\u52D9\u689D\u6B3E'
              )
            ),
            _react2.default.createElement(
              'p',
              null,
              '\u5982\u60A8\u767B\u5165\u6703\u54E1\uFF0C\u5373\u8996\u70BA\u60A8\u5DF2\u66B8\u89E3\u4E26\u540C\u610F\u6B64\u689D\u6B3E\u3002'
            )
          )
        )
      );
    }
  }]);

  return LoginModal;
}(_react.PureComponent);

LoginModal.contextTypes = {
  auth: _propTypes3.authType
};
exports.default = LoginModal;

/***/ }),

/***/ 223:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin
module.exports = {"login-modal-wrapper":"_eFJnb","footer":"_3zioM","agreement":"_3ygvl","login-button-facebook":"_2gRbk","login-button-google":"_Q6gsH"};

/***/ }),

/***/ 40:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(0);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _SimpleModal = __webpack_require__(41);

var _SimpleModal2 = _interopRequireDefault(_SimpleModal);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var SimpleModal = function (_PureComponent) {
  _inherits(SimpleModal, _PureComponent);

  function SimpleModal() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, SimpleModal);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = SimpleModal.__proto__ || Object.getPrototypeOf(SimpleModal)).call.apply(_ref, [this].concat(args))), _this), _this.handleClose = function (e) {
      if (e) {
        e.preventDefault();
      }

      if (_this.props.closeHandler) {
        _this.props.closeHandler(e);
      }
    }, _this.handleClickOverlay = function (e) {
      var _this$props = _this.props,
          enableOverlayCanClose = _this$props.enableOverlayCanClose,
          closeHandler = _this$props.closeHandler;


      if (enableOverlayCanClose && closeHandler) {
        closeHandler(e);
      }
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(SimpleModal, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          overlayStyle = _props.overlayStyle,
          containerStyle = _props.containerStyle,
          closeHandleStyle = _props.closeHandleStyle,
          enableClose = _props.enableClose,
          enableModal = _props.enableModal;


      if (enableModal) {
        return _react2.default.createElement(
          'div',
          { className: _SimpleModal2.default.wrapper, style: overlayStyle, onClick: this.handleClickOverlay },
          _react2.default.createElement(
            'div',
            { className: _SimpleModal2.default.container, style: containerStyle },
            enableClose && _react2.default.createElement('div', { className: _SimpleModal2.default['close-handle'], style: closeHandleStyle, onClick: this.handleClose }),
            this.props.children
          )
        );
      }

      return null;
    }
  }]);

  return SimpleModal;
}(_react.PureComponent);

SimpleModal.defaultProps = {
  overlayStyle: {},
  containerStyle: {},
  closeHandleStyle: {},
  enableModal: false,
  enableClose: true,
  closeHandler: undefined,
  enableOverlayCanClose: false
};
exports.default = SimpleModal;

/***/ }),

/***/ 41:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin
module.exports = {"wrapper":"_MXx3I","fadeIn":"_3NB-2","container":"_29Mdb","close-handle":"_1y-kA"};

/***/ }),

/***/ 7:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.adProfileType = exports.newsItemType = exports.userProfileType = exports.authType = exports.locationShape = exports.footerNavItem = exports.catNavsType = exports.catNavItemShape = exports.catNavSubItemShape = exports.navsMobileType = exports.navItemMobileShape = exports.navItemMobilShape = exports.accountNavsType = exports.accountNavItemShape = exports.navsType = exports.navItemShape = exports.navUrlShape = exports.navDownloadType = exports.navDownloadShape = exports.requestType = undefined;

var _propTypes = __webpack_require__(0);

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var requestType = exports.requestType = _propTypes2.default.func; /* eslint-disable import/prefer-default-export */
var navDownloadShape = exports.navDownloadShape = _propTypes2.default.shape({
  title: _propTypes2.default.string.isRequired,
  url: _propTypes2.default.string.isRequired
});

var navDownloadType = exports.navDownloadType = _propTypes2.default.arrayOf(navDownloadShape);

var navUrlShape = exports.navUrlShape = _propTypes2.default.shape({
  title: _propTypes2.default.string.isRequired,
  url: _propTypes2.default.string.isRequired
});

var navItemShape = exports.navItemShape = _propTypes2.default.shape({
  title: _propTypes2.default.string.isRequired,
  url: _propTypes2.default.string,
  leftList: _propTypes2.default.arrayOf(navUrlShape),
  rightListTitle: _propTypes2.default.string,
  rightList: _propTypes2.default.arrayOf(navUrlShape)
});

var navsType = exports.navsType = _propTypes2.default.arrayOf(navItemShape);

var accountNavItemShape = exports.accountNavItemShape = _propTypes2.default.shape({
  id: _propTypes2.default.string.isRequired,
  title: _propTypes2.default.string.isRequired,
  defaultUrl: _propTypes2.default.string.isRequired,
  notify: _propTypes2.default.arrayOf(_propTypes2.default.shape({
    id: _propTypes2.default.string.isRequired,
    gotoUrl: _propTypes2.default.string.isRequired
  }))
});

var accountNavsType = exports.accountNavsType = _propTypes2.default.arrayOf(accountNavItemShape);

var navItemMobilShape = exports.navItemMobilShape = _propTypes2.default.shape({
  name: _propTypes2.default.string.isRequired,
  title: _propTypes2.default.string.isRequired,
  url: _propTypes2.default.string.isRequired,
  external: _propTypes2.default.bool
});

var navItemMobileShape = exports.navItemMobileShape = _propTypes2.default.shape({
  name: _propTypes2.default.string.isRequired,
  title: _propTypes2.default.string.isRequired,
  items: _propTypes2.default.arrayOf(navItemMobilShape)
});

var navsMobileType = exports.navsMobileType = _propTypes2.default.arrayOf(navItemMobileShape);

var catNavSubItemShape = exports.catNavSubItemShape = _propTypes2.default.shape({
  name: _propTypes2.default.string,
  url: _propTypes2.default.string,
  title: _propTypes2.default.string,
  external: _propTypes2.default.bool
});

var catNavItemShape = exports.catNavItemShape = _propTypes2.default.shape({
  name: _propTypes2.default.string.isRequired,
  url: _propTypes2.default.string.isRequired,
  title: _propTypes2.default.string.isRequired,
  external: _propTypes2.default.bool,
  subItems: _propTypes2.default.arrayOf(catNavSubItemShape)
});

var catNavsType = exports.catNavsType = _propTypes2.default.arrayOf(catNavItemShape);

var footerNavItem = exports.footerNavItem = _propTypes2.default.shape({
  title: _propTypes2.default.string.isRequired,
  name: _propTypes2.default.string.isRequired,
  url: _propTypes2.default.string,
  onClick: _propTypes2.default.func
});

var locationShape = exports.locationShape = _propTypes2.default.shape({
  key: _propTypes2.default.string,
  pathname: _propTypes2.default.string,
  search: _propTypes2.default.string,
  hash: _propTypes2.default.string,
  state: _propTypes2.default.object
});

var authType = exports.authType = _propTypes2.default.shape({
  init: _propTypes2.default.func.isRequired,
  loginFB: _propTypes2.default.func.isRequired,
  loginGoogle: _propTypes2.default.func.isRequired,
  logout: _propTypes2.default.func.isRequired,
  showLogin: _propTypes2.default.func.isRequired,
  hideLogin: _propTypes2.default.func.isRequired,
  getToken: _propTypes2.default.func.isRequired,
  refreshToken: _propTypes2.default.func.isRequired,
  getProfile: _propTypes2.default.func.isRequired
});

var userProfileType = exports.userProfileType = _propTypes2.default.shape({
  uid: _propTypes2.default.string.isRequired,
  name: _propTypes2.default.string.isRequired,
  nickname: _propTypes2.default.string,
  email: _propTypes2.default.string,
  avatar: _propTypes2.default.string.isRequired,
  gender: _propTypes2.default.oneOf(['', 'male', 'female']),
  vip: _propTypes2.default.oneOf([0, 1])
});

var newsItemType = exports.newsItemType = _propTypes2.default.shape({
  newsId: _propTypes2.default.number.isRequired,
  title: _propTypes2.default.string.isRequired,
  hasCoverPhoto: _propTypes2.default.oneOf([0, 1]).isRequired,
  coverSrc: _propTypes2.default.shape({
    m: _propTypes2.default.shape({ src: _propTypes2.default.string }),
    l: _propTypes2.default.shape({ src: _propTypes2.default.string }),
    xl: _propTypes2.default.shape({ src: _propTypes2.default.string })
  })
});

var adProfileType = exports.adProfileType = _propTypes2.default.shape({
  name: _propTypes2.default.string.isRequired,
  path: _propTypes2.default.string.isRequired,
  hideOnInitial: _propTypes2.default.bool
});

/***/ })

/******/ });
});