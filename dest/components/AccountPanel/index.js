(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("prop-types"), require("react"), require("classnames"));
	else if(typeof define === 'function' && define.amd)
		define(["prop-types", "react", "classnames"], factory);
	else {
		var a = typeof exports === 'object' ? factory(require("prop-types"), require("react"), require("classnames")) : factory(root["prop-types"], root["react"], root["classnames"]);
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(typeof self !== 'undefined' ? self : this, function(__WEBPACK_EXTERNAL_MODULE_0__, __WEBPACK_EXTERNAL_MODULE_1__, __WEBPACK_EXTERNAL_MODULE_4__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/dest/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 326);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_0__;

/***/ }),

/***/ 1:
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_1__;

/***/ }),

/***/ 126:
/***/ (function(module, exports) {

module.exports = "https://sfiles.cnyes.cool/fe-common/5dddd900dc/5fbdce716ea9936da801688b9979630e.svg";

/***/ }),

/***/ 127:
/***/ (function(module, exports) {

module.exports = "https://sfiles.cnyes.cool/fe-common/5dddd900dc/eb76e4028e56f908b84c418dff18f51b.svg";

/***/ }),

/***/ 326:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(327);


/***/ }),

/***/ 327:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(0);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = __webpack_require__(4);

var _classnames2 = _interopRequireDefault(_classnames);

var _propTypes3 = __webpack_require__(7);

var _AccountPanel = __webpack_require__(328);

var _AccountPanel2 = _interopRequireDefault(_AccountPanel);

var _getStyleName = __webpack_require__(5);

var _getStyleName2 = _interopRequireDefault(_getStyleName);

var _userDefault = __webpack_require__(126);

var _userDefault2 = _interopRequireDefault(_userDefault);

var _userLogin = __webpack_require__(127);

var _userLogin2 = _interopRequireDefault(_userLogin);

var _icon_more = __webpack_require__(329);

var _icon_more2 = _interopRequireDefault(_icon_more);

var _icon_anue_rainbow = __webpack_require__(330);

var _icon_anue_rainbow2 = _interopRequireDefault(_icon_anue_rainbow);

var _navs = __webpack_require__(331);

var _navs2 = _interopRequireDefault(_navs);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /* eslint-disable quotes */


var getNavConfig = function getNavConfig(navCofig, cnyesBaseUrl) {
  var nav = navCofig || _navs2.default.items;
  var replaceLink = function replaceLink(str) {
    return str.replace('{cnyesBaseUrl}', cnyesBaseUrl);
  };

  nav.forEach(function (item, i) {
    // defaultUrl
    nav[i].defaultUrl = replaceLink(item.defaultUrl);
    // notify
    if (item.notify) {
      item.notify.forEach(function (n, j) {
        var gotoUrl = nav[i].notify[j].gotoUrl;

        nav[i].notify[j].gotoUrl = replaceLink(gotoUrl);
      });
    }
  });

  return nav;
};

var timeout = 3000;

var AccountPanel = function (_PureComponent) {
  _inherits(AccountPanel, _PureComponent);

  function AccountPanel(props) {
    _classCallCheck(this, AccountPanel);

    var _this = _possibleConstructorReturn(this, (AccountPanel.__proto__ || Object.getPrototypeOf(AccountPanel)).call(this, props));

    _this.handleHoverOn = function () {
      _this.handleClearTimeout();
    };

    _this.handleHoverOff = function () {
      _this.accountPanelTimeout = setTimeout(_this.onClickClose, timeout);
    };

    _this.handleClearTimeout = function () {
      if (_this.accountPanelTimeout) {
        clearTimeout(_this.accountPanelTimeout);
      }
    };

    _this.navConfig = getNavConfig(props.navs, props.cnyesBaseUrl);
    _this.memberInfoLink = 'https://www.' + props.cnyesBaseUrl + '/member/info';
    _this.accountPanelTimeout = null;
    _this.state = {
      isOpenState: false
    };
    _this.onClickClose = _this.onClickClose.bind(_this);
    _this.onClickNav = _this.onClickNav.bind(_this);
    return _this;
  }

  _createClass(AccountPanel, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.accountPanelRef.addEventListener('mouseleave', this.handleHoverOff);
      this.accountPanelRef.addEventListener('mouseenter', this.handleHoverOn);
    }
  }, {
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(nextProps) {
      var isOpen = nextProps.isOpen;
      var isOpenState = this.state.isOpenState;


      if (isOpen !== isOpenState) {
        this.setState({ isOpenState: isOpen });
      }
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      this.accountPanelRef.removeEventListener('mouseleave', this.handleHoverOff);
      this.accountPanelRef.removeEventListener('mouseenter', this.handleHoverOn);
      this.handleClearTimeout();
    }
  }, {
    key: 'onClickClose',
    value: function onClickClose() {
      this.setState({ isOpenState: false });
      this.props.onClosePanel();
      this.handleClearTimeout();
    }
  }, {
    key: 'onClickNav',
    value: function onClickNav() {
      this.setState({ isOpenState: false });
      this.props.onClickNav();
    }
  }, {
    key: 'renderMemberInfo',
    value: function renderMemberInfo() {
      var _props = this.props,
          isLogin = _props.isLogin,
          profile = _props.profile,
          login = _props.login;

      var avatar = profile.avatar;

      if (isLogin) {
        var userAvatar = avatar || _userLogin2.default;

        return _react2.default.createElement(
          'a',
          { href: this.memberInfoLink, className: _AccountPanel2.default.panel__member, 'data-tid': 'account-panel-member' },
          _react2.default.createElement('img', { className: _AccountPanel2.default.panel__member__avatar, src: userAvatar, alt: profile.name }),
          _react2.default.createElement(
            'div',
            { className: (0, _classnames2.default)(_AccountPanel2.default.panel__member__info, _AccountPanel2.default['panel__member__info--login']) },
            _react2.default.createElement(
              'div',
              null,
              profile.name
            ),
            _react2.default.createElement(
              'span',
              null,
              '\u67E5\u770B\u500B\u4EBA\u8CC7\u8A0A'
            )
          ),
          _react2.default.createElement('img', { className: _AccountPanel2.default.panel__member__more, alt: '\u524D\u5F80\u67E5\u770B\u500B\u4EBA\u8CC7\u8A0A', src: _icon_more2.default })
        );
      }

      return _react2.default.createElement(
        'div',
        { className: _AccountPanel2.default.panel__member },
        _react2.default.createElement('img', { className: _AccountPanel2.default.panel__member__avatar, src: _userDefault2.default, alt: '\u767B\u5165\u6703\u54E1' }),
        _react2.default.createElement(
          'div',
          { className: _AccountPanel2.default.panel__member__info },
          _react2.default.createElement(
            'div',
            null,
            'Hello, \u9245\u4EA8\u6703\u54E1'
          )
        ),
        _react2.default.createElement(
          'button',
          { className: _AccountPanel2.default.panel__member__login, onClick: login, 'data-tid': 'account-panel-login' },
          '\u767B\u5165'
        )
      );
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props2 = this.props,
          isLogin = _props2.isLogin,
          logout = _props2.logout,
          highlights = _props2.highlights;
      var isOpenState = this.state.isOpenState;


      return _react2.default.createElement(
        'aside',
        {
          className: (0, _classnames2.default)(_AccountPanel2.default.panel__wrapper, 'theme-gradient', _defineProperty({}, (0, _getStyleName2.default)(_AccountPanel2.default, 'panel__wrapper--active'), isOpenState))
          // eslint-disable-next-line no-return-assign
          , ref: function ref(_ref) {
            return _this2.accountPanelRef = _ref;
          }
        },
        _react2.default.createElement(
          'header',
          null,
          _react2.default.createElement(
            'div',
            { className: _AccountPanel2.default.header__title },
            _react2.default.createElement('img', { className: _AccountPanel2.default.header__icon, alt: 'Anue \u6703\u54E1\u4E2D\u5FC3', src: _icon_anue_rainbow2.default }),
            _react2.default.createElement(
              'span',
              { className: _AccountPanel2.default.header__txt },
              '\u6703\u54E1\u4E2D\u5FC3'
            )
          ),
          _react2.default.createElement('button', { className: _AccountPanel2.default.header__close, onClick: this.onClickClose, 'data-tid': 'account-panel-close' })
        ),
        _react2.default.createElement(
          'main',
          null,
          this.renderMemberInfo(),
          _react2.default.createElement(
            'nav',
            { className: _AccountPanel2.default.panel__nav },
            this.navConfig.map(function (item) {
              var isHighlight = false;
              var link = item.defaultUrl;

              if (item.notify) {
                var notify = item.notify;
                var filteredNotify = notify.filter(function (n) {
                  return highlights.indexOf(n.id) > -1;
                });

                if (filteredNotify.length > 0) {
                  isHighlight = true;
                  link = filteredNotify[0].gotoUrl;
                }
              }

              return _react2.default.createElement(
                'a',
                {
                  className: (0, _classnames2.default)(_AccountPanel2.default.panel__link, _defineProperty({}, (0, _getStyleName2.default)(_AccountPanel2.default, 'panel__link--highlight'), isHighlight)),
                  key: item.id,
                  href: link,
                  onClick: _this2.onClickNav,
                  'data-tid': 'account-panel-' + item.id
                },
                item.title,
                _react2.default.createElement('img', { className: _AccountPanel2.default.panel__link__more, alt: '\u524D\u5F80' + item.title, src: _icon_more2.default })
              );
            }),
            _react2.default.createElement('div', { className: _AccountPanel2.default.nav__line }),
            isLogin && _react2.default.createElement(
              'button',
              { className: _AccountPanel2.default.nav__logout, onClick: logout, 'data-tid': 'account-panel-logout' },
              '\u767B\u51FA\u5E33\u865F'
            )
          )
        )
      );
    }
  }]);

  return AccountPanel;
}(_react.PureComponent);

AccountPanel.defaultProps = {
  isOpen: false,
  isLogin: false,
  profile: {
    identity_id: '',
    name: '',
    avatar: ''
  },
  onClosePanel: function onClosePanel() {},
  onClickNav: function onClickNav() {},
  login: function login() {},
  logout: function logout() {},
  highlights: [],
  cnyesBaseUrl: 'cnyes.com',
  navs: undefined
};
exports.default = AccountPanel;

/***/ }),

/***/ 328:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin
module.exports = {"panel__member":"_2PQkB","panel__member__info":"_26yuU","panel__wrapper":"_eMBo3","panel__nav":"_3f8r8","panel__link":"_2W1Sq","nav__line":"_3Ck30","nav__logout":"_1YrU6","panel__member__more":"_1slSJ","panel__link__more":"_1I8jH","panel__member__login":"_163df","panel__wrapper--active":"_3eCHy","header__title":"_25UTC","header__icon":"_2s4s0","header__txt":"_DCjd1","header__close":"_2Na7E","panel__member__avatar":"_2IAxu","panel__member__info--login":"_3SglQ","panel__link--highlight":"_2tgOR"};

/***/ }),

/***/ 329:
/***/ (function(module, exports) {

module.exports = "https://sfiles.cnyes.cool/fe-common/5dddd900dc/f1cae93a0d0ae20a370d25cd6640f3a3.svg";

/***/ }),

/***/ 330:
/***/ (function(module, exports) {

module.exports = "https://sfiles.cnyes.cool/fe-common/5dddd900dc/76bfdbe59e3e432ffe6d5d203a37e64d.svg";

/***/ }),

/***/ 331:
/***/ (function(module, exports) {

module.exports = {"items":[{"id":"myTag","title":"新聞追蹤","defaultUrl":"https://www.{cnyesBaseUrl}/member/mytag/all","notify":[{"id":"inWebTagNewsNotify","gotoUrl":"https://www.{cnyesBaseUrl}/member/mytag/all"}]},{"id":"videoCourse","title":"投資駕訓班","defaultUrl":"https://www.{cnyesBaseUrl}/video/course/my","notify":[{"id":"inWebOrderNotify","gotoUrl":"https://www.{cnyesBaseUrl}/video/course/messages/order"},{"id":"inWebActivityNotify","gotoUrl":"https://www.{cnyesBaseUrl}/video/course/messages/activity"}]}]}

/***/ }),

/***/ 4:
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_4__;

/***/ }),

/***/ 5:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = getStyleName;
function getStyleName(styles, name) {
  return styles[name] || name;
}

/***/ }),

/***/ 7:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.adProfileType = exports.newsItemType = exports.userProfileType = exports.authType = exports.locationShape = exports.footerNavItem = exports.catNavsType = exports.catNavItemShape = exports.catNavSubItemShape = exports.navsMobileType = exports.navItemMobileShape = exports.navItemMobilShape = exports.accountNavsType = exports.accountNavItemShape = exports.navsType = exports.navItemShape = exports.navUrlShape = exports.navDownloadType = exports.navDownloadShape = exports.requestType = undefined;

var _propTypes = __webpack_require__(0);

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var requestType = exports.requestType = _propTypes2.default.func; /* eslint-disable import/prefer-default-export */
var navDownloadShape = exports.navDownloadShape = _propTypes2.default.shape({
  title: _propTypes2.default.string.isRequired,
  url: _propTypes2.default.string.isRequired
});

var navDownloadType = exports.navDownloadType = _propTypes2.default.arrayOf(navDownloadShape);

var navUrlShape = exports.navUrlShape = _propTypes2.default.shape({
  title: _propTypes2.default.string.isRequired,
  url: _propTypes2.default.string.isRequired
});

var navItemShape = exports.navItemShape = _propTypes2.default.shape({
  title: _propTypes2.default.string.isRequired,
  url: _propTypes2.default.string,
  leftList: _propTypes2.default.arrayOf(navUrlShape),
  rightListTitle: _propTypes2.default.string,
  rightList: _propTypes2.default.arrayOf(navUrlShape)
});

var navsType = exports.navsType = _propTypes2.default.arrayOf(navItemShape);

var accountNavItemShape = exports.accountNavItemShape = _propTypes2.default.shape({
  id: _propTypes2.default.string.isRequired,
  title: _propTypes2.default.string.isRequired,
  defaultUrl: _propTypes2.default.string.isRequired,
  notify: _propTypes2.default.arrayOf(_propTypes2.default.shape({
    id: _propTypes2.default.string.isRequired,
    gotoUrl: _propTypes2.default.string.isRequired
  }))
});

var accountNavsType = exports.accountNavsType = _propTypes2.default.arrayOf(accountNavItemShape);

var navItemMobilShape = exports.navItemMobilShape = _propTypes2.default.shape({
  name: _propTypes2.default.string.isRequired,
  title: _propTypes2.default.string.isRequired,
  url: _propTypes2.default.string.isRequired,
  external: _propTypes2.default.bool
});

var navItemMobileShape = exports.navItemMobileShape = _propTypes2.default.shape({
  name: _propTypes2.default.string.isRequired,
  title: _propTypes2.default.string.isRequired,
  items: _propTypes2.default.arrayOf(navItemMobilShape)
});

var navsMobileType = exports.navsMobileType = _propTypes2.default.arrayOf(navItemMobileShape);

var catNavSubItemShape = exports.catNavSubItemShape = _propTypes2.default.shape({
  name: _propTypes2.default.string,
  url: _propTypes2.default.string,
  title: _propTypes2.default.string,
  external: _propTypes2.default.bool
});

var catNavItemShape = exports.catNavItemShape = _propTypes2.default.shape({
  name: _propTypes2.default.string.isRequired,
  url: _propTypes2.default.string.isRequired,
  title: _propTypes2.default.string.isRequired,
  external: _propTypes2.default.bool,
  subItems: _propTypes2.default.arrayOf(catNavSubItemShape)
});

var catNavsType = exports.catNavsType = _propTypes2.default.arrayOf(catNavItemShape);

var footerNavItem = exports.footerNavItem = _propTypes2.default.shape({
  title: _propTypes2.default.string.isRequired,
  name: _propTypes2.default.string.isRequired,
  url: _propTypes2.default.string,
  onClick: _propTypes2.default.func
});

var locationShape = exports.locationShape = _propTypes2.default.shape({
  key: _propTypes2.default.string,
  pathname: _propTypes2.default.string,
  search: _propTypes2.default.string,
  hash: _propTypes2.default.string,
  state: _propTypes2.default.object
});

var authType = exports.authType = _propTypes2.default.shape({
  init: _propTypes2.default.func.isRequired,
  loginFB: _propTypes2.default.func.isRequired,
  loginGoogle: _propTypes2.default.func.isRequired,
  logout: _propTypes2.default.func.isRequired,
  showLogin: _propTypes2.default.func.isRequired,
  hideLogin: _propTypes2.default.func.isRequired,
  getToken: _propTypes2.default.func.isRequired,
  refreshToken: _propTypes2.default.func.isRequired,
  getProfile: _propTypes2.default.func.isRequired
});

var userProfileType = exports.userProfileType = _propTypes2.default.shape({
  uid: _propTypes2.default.string.isRequired,
  name: _propTypes2.default.string.isRequired,
  nickname: _propTypes2.default.string,
  email: _propTypes2.default.string,
  avatar: _propTypes2.default.string.isRequired,
  gender: _propTypes2.default.oneOf(['', 'male', 'female']),
  vip: _propTypes2.default.oneOf([0, 1])
});

var newsItemType = exports.newsItemType = _propTypes2.default.shape({
  newsId: _propTypes2.default.number.isRequired,
  title: _propTypes2.default.string.isRequired,
  hasCoverPhoto: _propTypes2.default.oneOf([0, 1]).isRequired,
  coverSrc: _propTypes2.default.shape({
    m: _propTypes2.default.shape({ src: _propTypes2.default.string }),
    l: _propTypes2.default.shape({ src: _propTypes2.default.string }),
    xl: _propTypes2.default.shape({ src: _propTypes2.default.string })
  })
});

var adProfileType = exports.adProfileType = _propTypes2.default.shape({
  name: _propTypes2.default.string.isRequired,
  path: _propTypes2.default.string.isRequired,
  hideOnInitial: _propTypes2.default.bool
});

/***/ })

/******/ });
});