(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("prop-types"), require("react"));
	else if(typeof define === 'function' && define.amd)
		define(["prop-types", "react"], factory);
	else {
		var a = typeof exports === 'object' ? factory(require("prop-types"), require("react")) : factory(root["prop-types"], root["react"]);
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(typeof self !== 'undefined' ? self : this, function(__WEBPACK_EXTERNAL_MODULE_0__, __WEBPACK_EXTERNAL_MODULE_1__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/dest/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 323);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_0__;

/***/ }),

/***/ 1:
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_1__;

/***/ }),

/***/ 126:
/***/ (function(module, exports) {

module.exports = "https://sfiles.cnyes.cool/fe-common/5dddd900dc/5fbdce716ea9936da801688b9979630e.svg";

/***/ }),

/***/ 127:
/***/ (function(module, exports) {

module.exports = "https://sfiles.cnyes.cool/fe-common/5dddd900dc/eb76e4028e56f908b84c418dff18f51b.svg";

/***/ }),

/***/ 323:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(324);


/***/ }),

/***/ 324:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(0);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _bind = __webpack_require__(9);

var _bind2 = _interopRequireDefault(_bind);

var _AccountMenu = __webpack_require__(325);

var _AccountMenu2 = _interopRequireDefault(_AccountMenu);

var _userDefault = __webpack_require__(126);

var _userDefault2 = _interopRequireDefault(_userDefault);

var _userLogin = __webpack_require__(127);

var _userLogin2 = _interopRequireDefault(_userLogin);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /* eslint-disable react/no-array-index-key */


var cx = _bind2.default.bind(_AccountMenu2.default);

var AccountMenu = function (_PureComponent) {
  _inherits(AccountMenu, _PureComponent);

  function AccountMenu() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, AccountMenu);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = AccountMenu.__proto__ || Object.getPrototypeOf(AccountMenu)).call.apply(_ref, [this].concat(args))), _this), _this.rect = { left: 0, top: 0 }, _this.renderPopup = function () {
      var _this$props = _this.props,
          popup = _this$props.popup,
          align = _this$props.align;


      if (popup && popup.row) {
        return _react2.default.createElement(
          'ul',
          { className: cx('account-menu__popup', 'account-menu__popup-' + align) },
          popup.row.map(function (row, rowIndex) {
            return _react2.default.createElement(
              'li',
              null,
              row.map(function (_ref2, index) {
                var className = _ref2.className,
                    style = _ref2.style,
                    onClick = _ref2.onClick,
                    link = _ref2.link,
                    name = _ref2.name;
                return _react2.default.createElement(
                  'a',
                  {
                    key: rowIndex + '_' + index,
                    className: className,
                    style: style,
                    onClick: onClick,
                    href: link,
                    'data-tid': 'account-popup-' + index
                  },
                  name
                );
              })
            );
          })
        );
      }

      return null;
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(AccountMenu, [{
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      this.renderPopup();
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          isLogin = _props.isLogin,
          hideName = _props.hideName,
          name = _props.name,
          avatar = _props.avatar,
          handleClick = _props.handleClick,
          highlight = _props.highlight;

      var displayName = isLogin ? name : '登入';
      var userAvatar = avatar || _userLogin2.default;
      var displayAvatar = isLogin ? userAvatar : _userDefault2.default;

      return _react2.default.createElement(
        'div',
        {
          ref: function ref(_ref3) {
            _this2.ref = _ref3;
          },
          className: cx('account-menu__wrapper', { withHighlight: highlight }),
          onClick: handleClick,
          'data-tid': isLogin ? 'account-logout' : 'account-login'
        },
        _react2.default.createElement('img', { className: cx('account-menu__avatar', { withAvatar: !!avatar }), src: displayAvatar, alt: displayName }),
        !hideName && _react2.default.createElement(
          'span',
          { className: cx('account-menu__name') },
          displayName
        ),
        isLogin && this.renderPopup()
      );
    }
  }]);

  return AccountMenu;
}(_react.PureComponent);

AccountMenu.defaultProps = {
  isLogin: false,
  highlight: false,
  hideName: false,
  name: null,
  avatar: null,
  handleClick: function handleClick() {},
  popup: null,
  align: 'left'
};
exports.default = AccountMenu;

/***/ }),

/***/ 325:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin
module.exports = {"account-menu__wrapper":"_rVz-R","account-menu__popup":"_1KDzL","withHighlight":"_1l3mi","account-menu__popup-left":"_rgpG1","account-menu__popup-right":"_8wJLW","account-menu__avatar":"_1dYjT","withAvatar":"_3T9U5","account-menu__name":"_38hSw"};

/***/ }),

/***/ 9:
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*!
  Copyright (c) 2016 Jed Watson.
  Licensed under the MIT License (MIT), see
  http://jedwatson.github.io/classnames
*/
/* global define */

(function () {
	'use strict';

	var hasOwn = {}.hasOwnProperty;

	function classNames () {
		var classes = [];

		for (var i = 0; i < arguments.length; i++) {
			var arg = arguments[i];
			if (!arg) continue;

			var argType = typeof arg;

			if (argType === 'string' || argType === 'number') {
				classes.push(this && this[arg] || arg);
			} else if (Array.isArray(arg)) {
				classes.push(classNames.apply(this, arg));
			} else if (argType === 'object') {
				for (var key in arg) {
					if (hasOwn.call(arg, key) && arg[key]) {
						classes.push(this && this[key] || key);
					}
				}
			}
		}

		return classes.join(' ');
	}

	if (typeof module !== 'undefined' && module.exports) {
		module.exports = classNames;
	} else if (true) {
		// register as 'classnames', consistent with npm package name
		!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_RESULT__ = (function () {
			return classNames;
		}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	} else {
		window.classNames = classNames;
	}
}());


/***/ })

/******/ });
});