(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("prop-types"), require("react"));
	else if(typeof define === 'function' && define.amd)
		define(["prop-types", "react"], factory);
	else {
		var a = typeof exports === 'object' ? factory(require("prop-types"), require("react")) : factory(root["prop-types"], root["react"]);
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(typeof self !== 'undefined' ? self : this, function(__WEBPACK_EXTERNAL_MODULE_0__, __WEBPACK_EXTERNAL_MODULE_1__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/dest/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 242);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_0__;

/***/ }),

/***/ 1:
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_1__;

/***/ }),

/***/ 242:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(243);


/***/ }),

/***/ 243:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(0);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _bind = __webpack_require__(9);

var _bind2 = _interopRequireDefault(_bind);

var _ScrollingQuote = __webpack_require__(244);

var _ScrollingQuote2 = _interopRequireDefault(_ScrollingQuote);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var cx = _bind2.default.bind(_ScrollingQuote2.default);

var ScrollingQuote = function (_React$Component) {
  _inherits(ScrollingQuote, _React$Component);

  function ScrollingQuote() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, ScrollingQuote);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = ScrollingQuote.__proto__ || Object.getPrototypeOf(ScrollingQuote)).call.apply(_ref, [this].concat(args))), _this), _this.getScrollerElementWidth = function () {
      return _this.scrollerElement.getBoundingClientRect().width;
    }, _this.getWrapperElementWidth = function () {
      return _this.scrollerElement.parentNode.getBoundingClientRect().width;
    }, _this.setScrollerElement = function (element) {
      _this.scrollerElement = element;
    }, _this.scroll = function () {
      var _this$props = _this.props,
          handleScrollEnd = _this$props.handleScrollEnd,
          speed = _this$props.speed;


      if (_this.shift < -_this.scrollerElementWidth) {
        _this.shift = _this.getWrapperElementWidth();
        _this.scrollerElement.style.transform = 'translateX(' + _this.shift + 'px)';

        handleScrollEnd();

        return;
      }

      _this.shift = _this.shift - speed;
      _this.scrollerElement.style.transform = 'translateX(' + _this.shift + 'px)';

      if (window && window.requestAnimationFrame) {
        _this.rafId = window.requestAnimationFrame(_this.scroll);
      }
    }, _this.startScroll = function () {
      _this.stopScroll();

      if (window && window.requestAnimationFrame) {
        _this.rafId = window.requestAnimationFrame(_this.scroll);
      }
    }, _this.stopScroll = function () {
      if (window && window.cancelAnimationFrame && _this.rafId) {
        window.cancelAnimationFrame(_this.rafId);
      }
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(ScrollingQuote, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.scrollerElementWidth = this.getScrollerElementWidth();
      this.shift = this.getWrapperElementWidth();

      this.startScroll();
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps) {
      if (this.props.quotes !== prevProps.quotes) {
        this.scrollerElementWidth = this.getScrollerElementWidth();

        this.startScroll();
      }
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      this.stopScroll();
    }
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props,
          inverted = _props.inverted,
          quotes = _props.quotes;


      return _react2.default.createElement(
        'div',
        { className: cx('wrapper', { inverted: inverted }), onMouseEnter: this.stopScroll, onMouseLeave: this.startScroll },
        _react2.default.createElement(
          'div',
          { className: cx('scroller'), ref: this.setScrollerElement },
          quotes.map(function (_ref2) {
            var name = _ref2.name,
                value = _ref2.value,
                netChange = _ref2.netChange,
                changeInPercentage = _ref2.changeInPercentage;

            var sign = netChange[0];
            var positive = sign === '+';
            var negative = sign === '-';

            return _react2.default.createElement(
              'span',
              { key: name, className: cx('quote', { positive: positive, negative: negative }) },
              _react2.default.createElement(
                'span',
                { className: cx('name') },
                name
              ),
              _react2.default.createElement(
                'span',
                null,
                value
              ),
              _react2.default.createElement('span', { className: cx('mark') }),
              _react2.default.createElement(
                'span',
                { className: cx('change') },
                netChange
              ),
              _react2.default.createElement(
                'span',
                { className: cx('change') },
                changeInPercentage
              )
            );
          })
        )
      );
    }
  }]);

  return ScrollingQuote;
}(_react2.default.Component);

ScrollingQuote.defaultProps = {
  inverted: false,
  speed: 1,
  quotes: []
};
exports.default = ScrollingQuote;

/***/ }),

/***/ 244:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin
module.exports = {"wrapper":"_eMX1o","scroller":"_776v3","quote":"_1vSIt","name":"_1kWHa","change":"_3O4up","positive":"_2_CgW","mark":"_1BwVu","negative":"_1fHsT","inverted":"_jPoch"};

/***/ }),

/***/ 9:
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*!
  Copyright (c) 2016 Jed Watson.
  Licensed under the MIT License (MIT), see
  http://jedwatson.github.io/classnames
*/
/* global define */

(function () {
	'use strict';

	var hasOwn = {}.hasOwnProperty;

	function classNames () {
		var classes = [];

		for (var i = 0; i < arguments.length; i++) {
			var arg = arguments[i];
			if (!arg) continue;

			var argType = typeof arg;

			if (argType === 'string' || argType === 'number') {
				classes.push(this && this[arg] || arg);
			} else if (Array.isArray(arg)) {
				classes.push(classNames.apply(this, arg));
			} else if (argType === 'object') {
				for (var key in arg) {
					if (hasOwn.call(arg, key) && arg[key]) {
						classes.push(this && this[key] || key);
					}
				}
			}
		}

		return classes.join(' ');
	}

	if (typeof module !== 'undefined' && module.exports) {
		module.exports = classNames;
	} else if (true) {
		// register as 'classnames', consistent with npm package name
		!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_RESULT__ = (function () {
			return classNames;
		}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	} else {
		window.classNames = classNames;
	}
}());


/***/ })

/******/ });
});