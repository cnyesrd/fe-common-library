(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("prop-types"), require("react"), require("classnames"));
	else if(typeof define === 'function' && define.amd)
		define(["prop-types", "react", "classnames"], factory);
	else {
		var a = typeof exports === 'object' ? factory(require("prop-types"), require("react"), require("classnames")) : factory(root["prop-types"], root["react"], root["classnames"]);
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(typeof self !== 'undefined' ? self : this, function(__WEBPACK_EXTERNAL_MODULE_0__, __WEBPACK_EXTERNAL_MODULE_1__, __WEBPACK_EXTERNAL_MODULE_4__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/dest/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 218);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_0__;

/***/ }),

/***/ 1:
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_1__;

/***/ }),

/***/ 218:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(219);


/***/ }),

/***/ 219:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MobileNavBoard = exports.MobileMenu = undefined;

var _MobileMenu = __webpack_require__(86);

var _MobileMenu2 = _interopRequireDefault(_MobileMenu);

var _NavBoard = __webpack_require__(88);

var _NavBoard2 = _interopRequireDefault(_NavBoard);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.MobileMenu = _MobileMenu2.default;
exports.MobileNavBoard = _NavBoard2.default;

/***/ }),

/***/ 4:
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_4__;

/***/ }),

/***/ 5:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = getStyleName;
function getStyleName(styles, name) {
  return styles[name] || name;
}

/***/ }),

/***/ 7:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.adProfileType = exports.newsItemType = exports.userProfileType = exports.authType = exports.locationShape = exports.footerNavItem = exports.catNavsType = exports.catNavItemShape = exports.catNavSubItemShape = exports.navsMobileType = exports.navItemMobileShape = exports.navItemMobilShape = exports.accountNavsType = exports.accountNavItemShape = exports.navsType = exports.navItemShape = exports.navUrlShape = exports.navDownloadType = exports.navDownloadShape = exports.requestType = undefined;

var _propTypes = __webpack_require__(0);

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var requestType = exports.requestType = _propTypes2.default.func; /* eslint-disable import/prefer-default-export */
var navDownloadShape = exports.navDownloadShape = _propTypes2.default.shape({
  title: _propTypes2.default.string.isRequired,
  url: _propTypes2.default.string.isRequired
});

var navDownloadType = exports.navDownloadType = _propTypes2.default.arrayOf(navDownloadShape);

var navUrlShape = exports.navUrlShape = _propTypes2.default.shape({
  title: _propTypes2.default.string.isRequired,
  url: _propTypes2.default.string.isRequired
});

var navItemShape = exports.navItemShape = _propTypes2.default.shape({
  title: _propTypes2.default.string.isRequired,
  url: _propTypes2.default.string,
  leftList: _propTypes2.default.arrayOf(navUrlShape),
  rightListTitle: _propTypes2.default.string,
  rightList: _propTypes2.default.arrayOf(navUrlShape)
});

var navsType = exports.navsType = _propTypes2.default.arrayOf(navItemShape);

var accountNavItemShape = exports.accountNavItemShape = _propTypes2.default.shape({
  id: _propTypes2.default.string.isRequired,
  title: _propTypes2.default.string.isRequired,
  defaultUrl: _propTypes2.default.string.isRequired,
  notify: _propTypes2.default.arrayOf(_propTypes2.default.shape({
    id: _propTypes2.default.string.isRequired,
    gotoUrl: _propTypes2.default.string.isRequired
  }))
});

var accountNavsType = exports.accountNavsType = _propTypes2.default.arrayOf(accountNavItemShape);

var navItemMobilShape = exports.navItemMobilShape = _propTypes2.default.shape({
  name: _propTypes2.default.string.isRequired,
  title: _propTypes2.default.string.isRequired,
  url: _propTypes2.default.string.isRequired,
  external: _propTypes2.default.bool
});

var navItemMobileShape = exports.navItemMobileShape = _propTypes2.default.shape({
  name: _propTypes2.default.string.isRequired,
  title: _propTypes2.default.string.isRequired,
  items: _propTypes2.default.arrayOf(navItemMobilShape)
});

var navsMobileType = exports.navsMobileType = _propTypes2.default.arrayOf(navItemMobileShape);

var catNavSubItemShape = exports.catNavSubItemShape = _propTypes2.default.shape({
  name: _propTypes2.default.string,
  url: _propTypes2.default.string,
  title: _propTypes2.default.string,
  external: _propTypes2.default.bool
});

var catNavItemShape = exports.catNavItemShape = _propTypes2.default.shape({
  name: _propTypes2.default.string.isRequired,
  url: _propTypes2.default.string.isRequired,
  title: _propTypes2.default.string.isRequired,
  external: _propTypes2.default.bool,
  subItems: _propTypes2.default.arrayOf(catNavSubItemShape)
});

var catNavsType = exports.catNavsType = _propTypes2.default.arrayOf(catNavItemShape);

var footerNavItem = exports.footerNavItem = _propTypes2.default.shape({
  title: _propTypes2.default.string.isRequired,
  name: _propTypes2.default.string.isRequired,
  url: _propTypes2.default.string,
  onClick: _propTypes2.default.func
});

var locationShape = exports.locationShape = _propTypes2.default.shape({
  key: _propTypes2.default.string,
  pathname: _propTypes2.default.string,
  search: _propTypes2.default.string,
  hash: _propTypes2.default.string,
  state: _propTypes2.default.object
});

var authType = exports.authType = _propTypes2.default.shape({
  init: _propTypes2.default.func.isRequired,
  loginFB: _propTypes2.default.func.isRequired,
  loginGoogle: _propTypes2.default.func.isRequired,
  logout: _propTypes2.default.func.isRequired,
  showLogin: _propTypes2.default.func.isRequired,
  hideLogin: _propTypes2.default.func.isRequired,
  getToken: _propTypes2.default.func.isRequired,
  refreshToken: _propTypes2.default.func.isRequired,
  getProfile: _propTypes2.default.func.isRequired
});

var userProfileType = exports.userProfileType = _propTypes2.default.shape({
  uid: _propTypes2.default.string.isRequired,
  name: _propTypes2.default.string.isRequired,
  nickname: _propTypes2.default.string,
  email: _propTypes2.default.string,
  avatar: _propTypes2.default.string.isRequired,
  gender: _propTypes2.default.oneOf(['', 'male', 'female']),
  vip: _propTypes2.default.oneOf([0, 1])
});

var newsItemType = exports.newsItemType = _propTypes2.default.shape({
  newsId: _propTypes2.default.number.isRequired,
  title: _propTypes2.default.string.isRequired,
  hasCoverPhoto: _propTypes2.default.oneOf([0, 1]).isRequired,
  coverSrc: _propTypes2.default.shape({
    m: _propTypes2.default.shape({ src: _propTypes2.default.string }),
    l: _propTypes2.default.shape({ src: _propTypes2.default.string }),
    xl: _propTypes2.default.shape({ src: _propTypes2.default.string })
  })
});

var adProfileType = exports.adProfileType = _propTypes2.default.shape({
  name: _propTypes2.default.string.isRequired,
  path: _propTypes2.default.string.isRequired,
  hideOnInitial: _propTypes2.default.bool
});

/***/ }),

/***/ 8:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var gaDataset = function gaDataset(_ref) {
  var dataPrefix = _ref.dataPrefix,
      category = _ref.category,
      action = _ref.action,
      label = _ref.label;

  if (dataPrefix.length === 0 || !dataPrefix) {
    return null;
  }

  var gaRegExp = /^data-.*ga$/; // format restriction
  var eventsObj = dataPrefix.reduce(function (p, prefix) {
    var _p = p;

    if (gaRegExp.test(prefix)) {
      _p[prefix + "-category"] = category;
      _p[prefix + "-action"] = action;
      _p[prefix + "-label"] = label;
    }

    return _p;
  }, {});

  return eventsObj;
};

exports.default = gaDataset;

/***/ }),

/***/ 86:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(0);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = __webpack_require__(4);

var _classnames2 = _interopRequireDefault(_classnames);

var _MobileMenu = __webpack_require__(87);

var _MobileMenu2 = _interopRequireDefault(_MobileMenu);

var _getStyleName = __webpack_require__(5);

var _getStyleName2 = _interopRequireDefault(_getStyleName);

var _gaDataset = __webpack_require__(8);

var _gaDataset2 = _interopRequireDefault(_gaDataset);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function MobileMenu(_ref) {
  var showCatBoard = _ref.showCatBoard,
      channelName = _ref.channelName,
      hideTopBar = _ref.hideTopBar,
      dataPrefix = _ref.dataPrefix;

  return _react2.default.createElement(
    'div',
    { className: (0, _classnames2.default)(_defineProperty({}, (0, _getStyleName2.default)(_MobileMenu2.default, 'index-header-top-bar'), !hideTopBar)) },
    _react2.default.createElement(
      'a',
      _extends({
        href: 'https://m.cnyes.com/news',
        className: _MobileMenu2.default['index-header-logo']
      }, (0, _gaDataset2.default)({ dataPrefix: dataPrefix, category: 'Logo', action: 'click', label: 'home' })),
      '\u9245\u4EA8'
    ),
    channelName && channelName.length && _react2.default.createElement(
      'div',
      { className: _MobileMenu2.default['header-channel-label'] },
      channelName
    ),
    _react2.default.createElement(
      'nav',
      null,
      _react2.default.createElement('span', { className: _MobileMenu2.default['index-header-menu'], onClick: showCatBoard })
    )
  );
}

MobileMenu.defaultProps = {
  channelName: undefined,
  hideTopBar: false,
  dataPrefix: ['data-proj-ga']
};

exports.default = MobileMenu;

/***/ }),

/***/ 87:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin
module.exports = {"index-header-top-bar":"_3Ny_k","index-header":"_27f3x","index-header-menu":"_3Wta4","index-header-logo":"_3KhXq","header-channel-label":"_3_hO3","index-header-tabs":"_3vTaA","index-search-hint":"_3ZBZ7","hint-close":"_3XvKH","index-header-search":"_2mXhz"};

/***/ }),

/***/ 88:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(0);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = __webpack_require__(4);

var _classnames2 = _interopRequireDefault(_classnames);

var _NavItem = __webpack_require__(89);

var _NavItem2 = _interopRequireDefault(_NavItem);

var _NavDownloadItem = __webpack_require__(91);

var _NavDownloadItem2 = _interopRequireDefault(_NavDownloadItem);

var _navConfig = __webpack_require__(93);

var _navConfig2 = _interopRequireDefault(_navConfig);

var _NavBoard = __webpack_require__(94);

var _NavBoard2 = _interopRequireDefault(_NavBoard);

var _getStyleName = __webpack_require__(5);

var _getStyleName2 = _interopRequireDefault(_getStyleName);

var _propTypes3 = __webpack_require__(7);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var NavBoard = function (_PureComponent) {
  _inherits(NavBoard, _PureComponent);

  function NavBoard() {
    _classCallCheck(this, NavBoard);

    return _possibleConstructorReturn(this, (NavBoard.__proto__ || Object.getPrototypeOf(NavBoard)).apply(this, arguments));
  }

  _createClass(NavBoard, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          hideTopBar = _props.hideTopBar,
          isCatBoardOpen = _props.isCatBoardOpen,
          hideCatBoard = _props.hideCatBoard,
          navs = _props.navs,
          downloadLinks = _props.downloadLinks,
          dataPrefix = _props.dataPrefix;

      var catBoardClassName = isCatBoardOpen ? _NavBoard2.default['nav-board-show'] : _NavBoard2.default['nav-board'];

      return _react2.default.createElement(
        'aside',
        {
          className: (0, _classnames2.default)(catBoardClassName, 'theme-gradient', _defineProperty({}, (0, _getStyleName2.default)(_NavBoard2.default, 'nav-board-top-bar'), !hideTopBar))
        },
        _react2.default.createElement(
          'header',
          null,
          _react2.default.createElement(
            'span',
            { className: _NavBoard2.default['nav-board-logo'] },
            '\u9245\u4EA8'
          ),
          _react2.default.createElement('button', { className: _NavBoard2.default['nav-board-btn-close'], onClick: hideCatBoard })
        ),
        _react2.default.createElement(
          'main',
          null,
          downloadLinks.map(function (link) {
            return [_react2.default.createElement(
              'h4',
              { key: 'link-' + link.title, className: _NavBoard2.default['cat-board-nav-link'] },
              _react2.default.createElement(_NavDownloadItem2.default, { title: link.title, url: link.url })
            )];
          }),
          navs.map(function (nav) {
            return [_react2.default.createElement(
              'h4',
              { key: nav.name + '-h4', className: 'nav-board-subtitle theme-nav-board-subtitle' },
              nav.title
            ), _react2.default.createElement(
              'nav',
              { key: nav.name + '-nav', className: _NavBoard2.default['cat-board-nav'] },
              nav.items && nav.items.map(function (item) {
                return _react2.default.createElement(_NavItem2.default, {
                  key: item.name,
                  navTitle: nav.title,
                  item: item,
                  onClick: hideCatBoard,
                  dataPrefix: dataPrefix
                });
              })
            )];
          })
        )
      );
    }
  }]);

  return NavBoard;
}(_react.PureComponent);

NavBoard.defaultProps = {
  isCatBoardOpen: false,
  hideCatBoard: undefined,
  hideTopBar: false,
  navs: _navConfig2.default,
  downloadLinks: [],
  dataPrefix: ['data-proj-ga']
};
exports.default = NavBoard;

/***/ }),

/***/ 89:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(0);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _gaDataset = __webpack_require__(8);

var _gaDataset2 = _interopRequireDefault(_gaDataset);

var _NavItem = __webpack_require__(90);

var _NavItem2 = _interopRequireDefault(_NavItem);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function NavItem(_ref) {
  var item = _ref.item,
      navTitle = _ref.navTitle,
      onClick = _ref.onClick,
      dataPrefix = _ref.dataPrefix;

  return _react2.default.createElement(
    'a',
    _extends({
      key: item.name,
      className: _NavItem2.default['nav-board-link'],
      href: item.url,
      'data-ga-category': '\u5074\u9078\u55AE',
      'data-ga-action': navTitle,
      'data-ga-label': item.title
    }, (0, _gaDataset2.default)({ dataPrefix: dataPrefix, category: 'Header', action: 'click', label: navTitle + '_' + item.title }), {
      onClick: onClick
    }),
    item.title
  );
}

NavItem.defaultProps = {
  onClick: undefined,
  dataPrefix: ['data-proj-ga']
};

exports.default = NavItem;

/***/ }),

/***/ 90:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin
module.exports = {"nav-board-link":"_3Nksy"};

/***/ }),

/***/ 91:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(0);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _NavDownloadItem = __webpack_require__(92);

var _NavDownloadItem2 = _interopRequireDefault(_NavDownloadItem);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var handleClick = function handleClick(e, url) {
  e.preventDefault();
  window.open(url);
};

var NavDownloadItem = function NavDownloadItem(_ref) {
  var title = _ref.title,
      url = _ref.url;
  return _react2.default.createElement(
    'a',
    { className: _NavDownloadItem2.default['nav-board-link-full-width'], onClick: function onClick(e) {
        return handleClick(e, url);
      } },
    title
  );
};

exports.default = NavDownloadItem;

/***/ }),

/***/ 92:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin
module.exports = {"nav-board-link-full-width":"_KvlCd"};

/***/ }),

/***/ 93:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = [{
  name: 'mweb',
  title: '新聞',
  items: [{
    name: 'headline',
    title: '即時頭條',
    url: 'https://m.cnyes.com/news/cat/headline'
  }, {
    name: 'news24h',
    title: '主編精選',
    url: 'https://m.cnyes.com/news/cat/news24h'
  }, {
    name: 'tw_stock',
    title: '台股',
    url: 'https://m.cnyes.com/news/cat/tw_stock'
  }, {
    name: 'wd_stock',
    title: '國際股',
    url: 'https://m.cnyes.com/news/cat/wd_stock'
  }, {
    name: 'forex',
    title: '外匯',
    url: 'https://m.cnyes.com/news/cat/forex'
  }, {
    name: 'future',
    title: '期貨',
    url: 'https://m.cnyes.com/news/cat/future'
  }, {
    name: 'tw_money',
    title: '理財',
    url: 'https://m.cnyes.com/news/cat/tw_money'
  }, {
    name: 'cn_stock',
    title: 'A股港股',
    url: 'https://m.cnyes.com/news/cat/cn_stock'
  }, {
    name: 'cnyeshouse',
    title: '房產',
    url: 'https://m.cnyes.com/news/cat/cnyeshouse'
  }, {
    name: 'celebrity_area',
    title: '鉅亨新視界',
    url: 'https://m.cnyes.com/news/cat/celebrity_area'
  }, {
    name: 'popular',
    title: '人氣新聞',
    url: 'https://m.cnyes.com/news/cat/popular'
  }, {
    name: 'topic',
    title: '專題報導',
    url: 'https://topics.cnyes.com'
  }]
}, {
  name: 'mweb2',
  title: '影音',
  items: [{
    name: 'world',
    title: 'Allen看世界',
    url: 'https://m.cnyes.com/video/cat/world'
  }, {
    name: 'invest',
    title: '理財芳程式',
    url: 'https://m.cnyes.com/video/cat/invest'
  }, {
    name: 'fund',
    title: '鉅亨FUND大鏡',
    url: 'https://m.cnyes.com/video/cat/fund'
  }, {
    name: 'hkstock',
    title: '港股大講堂',
    url: 'https://m.cnyes.com/video/cat/hkstock'
  }, {
    name: 'videonews',
    title: '影音新聞',
    url: 'https://m.cnyes.com/video/cat/videonews'
  }]
}, {
  name: 'forex',
  title: '外匯',
  items: [{
    name: 'USD',
    title: '美元',
    url: 'https://forex.cnyes.com/currency/USD/TWD'
  }, {
    name: 'JPY',
    title: '日幣',
    url: 'https://forex.cnyes.com/currency/JPY/TWD'
  }, {
    name: 'EUR',
    title: '歐元',
    url: 'https://forex.cnyes.com/currency/EUR/TWD'
  }, {
    name: 'KRW',
    title: '韓幣',
    url: 'https://forex.cnyes.com/currency/KRW/TWD'
  }, {
    name: 'CNY',
    title: '人民幣',
    url: 'https://forex.cnyes.com/currency/CNY/TWD'
  }, {
    name: 'HKD',
    title: '港幣',
    url: 'https://forex.cnyes.com/currency/HKD/TWD'
  }, {
    name: 'ZAR',
    title: '南非幣',
    url: 'https://forex.cnyes.com/currency/ZAR/TWD'
  }, {
    name: 'AUD',
    title: '澳幣',
    url: 'https://forex.cnyes.com/currency/AUD/TWD'
  }]
}, {
  name: 'crypto',
  title: '虛擬貨幣',
  items: [{
    name: 'BTC',
    title: '比特幣',
    url: 'https://crypto.cnyes.com/BTC/24h'
  }, {
    name: 'ETH',
    title: '以太幣',
    url: 'https://crypto.cnyes.com/ETH/24h'
  }, {
    name: 'XRP',
    title: 'Ripple',
    url: 'https://crypto.cnyes.com/XRP/24h'
  }, {
    name: 'BCH',
    title: 'Bitcoin Cash',
    url: 'https://crypto.cnyes.com/BCH/24h'
  }, {
    name: 'LTC',
    title: 'Litcoin',
    url: 'https://crypto.cnyes.com/LTC/24h'
  }, {
    name: 'EOS',
    title: 'EOS',
    url: 'https://crypto.cnyes.com/EOS/24h'
  }]
}, {
  name: 'market',
  title: '市場',
  items: [{
    name: 'market_taiwan',
    title: '台股指數',
    url: 'https://m.cnyes.com/market/#market_taiwan'
  }, {
    name: 'market_global',
    title: '國際指數',
    url: 'https://m.cnyes.com/market/#market_global'
  }, {
    name: 'market_currency',
    title: '國際外匯',
    url: 'https://m.cnyes.com/market/#market_currency'
  }, {
    name: 'index_tse',
    title: '台股',
    url: 'https://m.cnyes.com/twstock/index_tse.aspx'
  }, {
    name: 'mystock',
    title: '自選股',
    url: 'https://m.cnyes.com/person/mystock.aspx'
  }, {
    name: 'news/search',
    title: '個股查詢',
    url: 'https://m.cnyes.com/news/search'
  }, {
    name: 'fund',
    title: '基金',
    url: 'https://fund.cnyes.com'
  }]
}, {
  name: 'channels',
  title: '頻道',
  items: [{
    name: 'blog',
    title: 'Blog',
    url: 'http://m.blog.cnyes.com'
  }]
}];

/***/ }),

/***/ 94:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin
module.exports = {"cat-board-nav":"_wwdaL","cat-board-nav-link":"_1C1Ae","nav-board-top-bar":"_2EEsr","nav-board":"_3RjfV","nav-board-show":"_38glh","nav-board-logo":"_23siC","nav-board-btn-close":"_-kDn3"};

/***/ })

/******/ });
});