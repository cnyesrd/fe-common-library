(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("prop-types"), require("react"), require("classnames"));
	else if(typeof define === 'function' && define.amd)
		define(["prop-types", "react", "classnames"], factory);
	else {
		var a = typeof exports === 'object' ? factory(require("prop-types"), require("react"), require("classnames")) : factory(root["prop-types"], root["react"], root["classnames"]);
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(typeof self !== 'undefined' ? self : this, function(__WEBPACK_EXTERNAL_MODULE_0__, __WEBPACK_EXTERNAL_MODULE_1__, __WEBPACK_EXTERNAL_MODULE_4__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/dest/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 217);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_0__;

/***/ }),

/***/ 1:
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_1__;

/***/ }),

/***/ 217:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(84);


/***/ }),

/***/ 27:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin
module.exports = {"footerHeight":"140px","cnyes-footer-wrapper":"_qdV4M","main-footer":"_1B2qL","logo":"_1W3uT","nav":"_36Xgk","socials":"_7vG5K","social-item":"_1n8HS","title":"_2C4rE","social-icon":"_11SpT","cnyes-media-facebook":"_2WAgR","cnyes-media-line":"_1PYxM","cnyes-media-app":"_13are","copyright-anue":"_P9DLg","hidden-mobile":"_1uLvu","hidden-desktop":"_2pljx"};

/***/ }),

/***/ 4:
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_4__;

/***/ }),

/***/ 5:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = getStyleName;
function getStyleName(styles, name) {
  return styles[name] || name;
}

/***/ }),

/***/ 7:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.adProfileType = exports.newsItemType = exports.userProfileType = exports.authType = exports.locationShape = exports.footerNavItem = exports.catNavsType = exports.catNavItemShape = exports.catNavSubItemShape = exports.navsMobileType = exports.navItemMobileShape = exports.navItemMobilShape = exports.accountNavsType = exports.accountNavItemShape = exports.navsType = exports.navItemShape = exports.navUrlShape = exports.navDownloadType = exports.navDownloadShape = exports.requestType = undefined;

var _propTypes = __webpack_require__(0);

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var requestType = exports.requestType = _propTypes2.default.func; /* eslint-disable import/prefer-default-export */
var navDownloadShape = exports.navDownloadShape = _propTypes2.default.shape({
  title: _propTypes2.default.string.isRequired,
  url: _propTypes2.default.string.isRequired
});

var navDownloadType = exports.navDownloadType = _propTypes2.default.arrayOf(navDownloadShape);

var navUrlShape = exports.navUrlShape = _propTypes2.default.shape({
  title: _propTypes2.default.string.isRequired,
  url: _propTypes2.default.string.isRequired
});

var navItemShape = exports.navItemShape = _propTypes2.default.shape({
  title: _propTypes2.default.string.isRequired,
  url: _propTypes2.default.string,
  leftList: _propTypes2.default.arrayOf(navUrlShape),
  rightListTitle: _propTypes2.default.string,
  rightList: _propTypes2.default.arrayOf(navUrlShape)
});

var navsType = exports.navsType = _propTypes2.default.arrayOf(navItemShape);

var accountNavItemShape = exports.accountNavItemShape = _propTypes2.default.shape({
  id: _propTypes2.default.string.isRequired,
  title: _propTypes2.default.string.isRequired,
  defaultUrl: _propTypes2.default.string.isRequired,
  notify: _propTypes2.default.arrayOf(_propTypes2.default.shape({
    id: _propTypes2.default.string.isRequired,
    gotoUrl: _propTypes2.default.string.isRequired
  }))
});

var accountNavsType = exports.accountNavsType = _propTypes2.default.arrayOf(accountNavItemShape);

var navItemMobilShape = exports.navItemMobilShape = _propTypes2.default.shape({
  name: _propTypes2.default.string.isRequired,
  title: _propTypes2.default.string.isRequired,
  url: _propTypes2.default.string.isRequired,
  external: _propTypes2.default.bool
});

var navItemMobileShape = exports.navItemMobileShape = _propTypes2.default.shape({
  name: _propTypes2.default.string.isRequired,
  title: _propTypes2.default.string.isRequired,
  items: _propTypes2.default.arrayOf(navItemMobilShape)
});

var navsMobileType = exports.navsMobileType = _propTypes2.default.arrayOf(navItemMobileShape);

var catNavSubItemShape = exports.catNavSubItemShape = _propTypes2.default.shape({
  name: _propTypes2.default.string,
  url: _propTypes2.default.string,
  title: _propTypes2.default.string,
  external: _propTypes2.default.bool
});

var catNavItemShape = exports.catNavItemShape = _propTypes2.default.shape({
  name: _propTypes2.default.string.isRequired,
  url: _propTypes2.default.string.isRequired,
  title: _propTypes2.default.string.isRequired,
  external: _propTypes2.default.bool,
  subItems: _propTypes2.default.arrayOf(catNavSubItemShape)
});

var catNavsType = exports.catNavsType = _propTypes2.default.arrayOf(catNavItemShape);

var footerNavItem = exports.footerNavItem = _propTypes2.default.shape({
  title: _propTypes2.default.string.isRequired,
  name: _propTypes2.default.string.isRequired,
  url: _propTypes2.default.string,
  onClick: _propTypes2.default.func
});

var locationShape = exports.locationShape = _propTypes2.default.shape({
  key: _propTypes2.default.string,
  pathname: _propTypes2.default.string,
  search: _propTypes2.default.string,
  hash: _propTypes2.default.string,
  state: _propTypes2.default.object
});

var authType = exports.authType = _propTypes2.default.shape({
  init: _propTypes2.default.func.isRequired,
  loginFB: _propTypes2.default.func.isRequired,
  loginGoogle: _propTypes2.default.func.isRequired,
  logout: _propTypes2.default.func.isRequired,
  showLogin: _propTypes2.default.func.isRequired,
  hideLogin: _propTypes2.default.func.isRequired,
  getToken: _propTypes2.default.func.isRequired,
  refreshToken: _propTypes2.default.func.isRequired,
  getProfile: _propTypes2.default.func.isRequired
});

var userProfileType = exports.userProfileType = _propTypes2.default.shape({
  uid: _propTypes2.default.string.isRequired,
  name: _propTypes2.default.string.isRequired,
  nickname: _propTypes2.default.string,
  email: _propTypes2.default.string,
  avatar: _propTypes2.default.string.isRequired,
  gender: _propTypes2.default.oneOf(['', 'male', 'female']),
  vip: _propTypes2.default.oneOf([0, 1])
});

var newsItemType = exports.newsItemType = _propTypes2.default.shape({
  newsId: _propTypes2.default.number.isRequired,
  title: _propTypes2.default.string.isRequired,
  hasCoverPhoto: _propTypes2.default.oneOf([0, 1]).isRequired,
  coverSrc: _propTypes2.default.shape({
    m: _propTypes2.default.shape({ src: _propTypes2.default.string }),
    l: _propTypes2.default.shape({ src: _propTypes2.default.string }),
    xl: _propTypes2.default.shape({ src: _propTypes2.default.string })
  })
});

var adProfileType = exports.adProfileType = _propTypes2.default.shape({
  name: _propTypes2.default.string.isRequired,
  path: _propTypes2.default.string.isRequired,
  hideOnInitial: _propTypes2.default.bool
});

/***/ }),

/***/ 8:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var gaDataset = function gaDataset(_ref) {
  var dataPrefix = _ref.dataPrefix,
      category = _ref.category,
      action = _ref.action,
      label = _ref.label;

  if (dataPrefix.length === 0 || !dataPrefix) {
    return null;
  }

  var gaRegExp = /^data-.*ga$/; // format restriction
  var eventsObj = dataPrefix.reduce(function (p, prefix) {
    var _p = p;

    if (gaRegExp.test(prefix)) {
      _p[prefix + "-category"] = category;
      _p[prefix + "-action"] = action;
      _p[prefix + "-label"] = label;
    }

    return _p;
  }, {});

  return eventsObj;
};

exports.default = gaDataset;

/***/ }),

/***/ 84:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(1);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(0);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = __webpack_require__(4);

var _classnames2 = _interopRequireDefault(_classnames);

var _getStyleName = __webpack_require__(5);

var _getStyleName2 = _interopRequireDefault(_getStyleName);

var _Footer = __webpack_require__(27);

var _Footer2 = _interopRequireDefault(_Footer);

var _links = __webpack_require__(85);

var _links2 = _interopRequireDefault(_links);

var _propTypes3 = __webpack_require__(7);

var _gaDataset = __webpack_require__(8);

var _gaDataset2 = _interopRequireDefault(_gaDataset);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /* eslint import/no-extraneous-dependencies: ["error", {"peerDependencies": true}] */


var ANUE_INDEX = 'https://www.cnyes.com';

var Footer = function (_PureComponent) {
  _inherits(Footer, _PureComponent);

  function Footer() {
    _classCallCheck(this, Footer);

    return _possibleConstructorReturn(this, (Footer.__proto__ || Object.getPrototypeOf(Footer)).apply(this, arguments));
  }

  _createClass(Footer, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          mobileNavs = _props.mobileNavs,
          desktopNavs = _props.desktopNavs,
          socials = _props.socials,
          now = _props.now,
          Link = _props.Link,
          dataPrefix = _props.dataPrefix;

      var thisYear = new Date(now).getFullYear();
      var logoClass = (0, _classnames2.default)((0, _getStyleName2.default)(_Footer2.default, 'logo'), (0, _getStyleName2.default)(_Footer2.default, 'hidden-mobile'));
      var socialsClass = (0, _classnames2.default)((0, _getStyleName2.default)(_Footer2.default, 'socials'), (0, _getStyleName2.default)(_Footer2.default, 'hidden-mobile'));

      /* eslint-disable jsx-a11y/accessible-emoji */
      return _react2.default.createElement(
        'div',
        { id: (0, _getStyleName2.default)(_Footer2.default, 'cnyes-footer-wrapper'), className: (0, _classnames2.default)('theme-footer-wrapper') },
        _react2.default.createElement(
          'footer',
          { className: (0, _getStyleName2.default)(_Footer2.default, 'main-footer') },
          _react2.default.createElement(
            'div',
            { className: logoClass },
            Link ? _react2.default.createElement(Link, _extends({ to: '/' }, (0, _gaDataset2.default)({ dataPrefix: dataPrefix, category: 'Footer', action: 'click', label: 'home' }))) : _react2.default.createElement('a', _extends({ href: ANUE_INDEX }, (0, _gaDataset2.default)({ dataPrefix: dataPrefix, category: 'Footer', action: 'click', label: 'home' })))
          ),
          _react2.default.createElement(
            'div',
            { className: (0, _getStyleName2.default)(_Footer2.default, 'nav') },
            _react2.default.createElement(
              'nav',
              { className: (0, _classnames2.default)((0, _getStyleName2.default)(_Footer2.default, 'mobile-nav'), (0, _getStyleName2.default)(_Footer2.default, 'hidden-desktop')) },
              this.constructor.renderNavs(mobileNavs, dataPrefix)
            ),
            _react2.default.createElement(
              'nav',
              { className: (0, _classnames2.default)((0, _getStyleName2.default)(_Footer2.default, 'desktop-nav'), (0, _getStyleName2.default)(_Footer2.default, 'hidden-mobile')) },
              this.constructor.renderNavs(desktopNavs, dataPrefix)
            ),
            _react2.default.createElement(
              'small',
              { className: (0, _getStyleName2.default)(_Footer2.default, 'copyright-anue') },
              _react2.default.createElement(
                'span',
                null,
                '\xA9'
              ),
              _react2.default.createElement(
                'span',
                { className: (0, _getStyleName2.default)(_Footer2.default, 'hidden-mobile') },
                ' Copyright'
              ),
              _react2.default.createElement(
                'span',
                null,
                ' 2000-',
                thisYear,
                ' Anue.com All rights reserved.'
              ),
              _react2.default.createElement(
                'span',
                { className: (0, _getStyleName2.default)(_Footer2.default, 'hidden-mobile') },
                ' \u672A\u7D93\u6388\u6B0A \u4E0D\u5F97\u8F49\u8F09'
              )
            )
          ),
          _react2.default.createElement(
            'div',
            { className: socialsClass },
            this.constructor.renderSocials(socials, dataPrefix)
          )
        )
      );
    }
  }], [{
    key: 'renderNavs',
    value: function renderNavs(navs, dataPrefix) {
      return navs.map(function (item) {
        var url = item.url,
            name = item.name,
            title = item.title,
            onClick = item.onClick;


        return _react2.default.createElement(
          'a',
          _extends({
            href: url,
            onClick: onClick,
            key: 'footer-nav-' + name,
            target: '_blank',
            rel: 'noopener noreferrer'
          }, (0, _gaDataset2.default)({ dataPrefix: dataPrefix, category: 'Footer', action: 'click', label: title })),
          title
        );
      });
    }
  }, {
    key: 'renderSocials',
    value: function renderSocials(socials, dataPrefix) {
      return socials.map(function (item) {
        return _react2.default.createElement(
          'div',
          { className: (0, _getStyleName2.default)(_Footer2.default, 'social-item'), key: 'footer-socials-' + item.name },
          _react2.default.createElement(
            'div',
            { className: (0, _getStyleName2.default)(_Footer2.default, 'title') },
            item.title
          ),
          _react2.default.createElement(
            'a',
            _extends({
              className: (0, _getStyleName2.default)(_Footer2.default, 'social-icon') + ' ' + (0, _getStyleName2.default)(_Footer2.default, 'cnyes-media-' + item.name),
              href: item.url,
              rel: 'noopener noreferrer',
              target: '_blank'
            }, (0, _gaDataset2.default)({ dataPrefix: dataPrefix, category: 'Footer', action: 'click', label: item.title })),
            item.title
          )
        );
      });
    }
  }]);

  return Footer;
}(_react.PureComponent);

Footer.defaultProps = {
  mobileNavs: _links2.default.mobileNavs,
  desktopNavs: _links2.default.desktopNavs,
  socials: _links2.default.socials,
  Link: undefined,
  dataPrefix: ['data-proj-ga']
};
exports.default = Footer;

/***/ }),

/***/ 85:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var footerLinks = {
  mobileNavs: [{
    title: '電腦版',
    name: 'pc',
    url: '/'
  }, {
    title: '著作權',
    name: 'copyright',
    url: '/copyright'
  }, {
    title: '意見與回饋',
    name: 'feedback',
    onClick: function onClick(e) {
      e.preventDefault();

      if (typeof window !== 'undefined') {
        var formURI = 'https://docs.google.com/forms/d/e/1FAIpQLSepnhnP4FgnRXiqezmVme7YX7xqXhxPsd57qbaJ09yJbxTt0g/viewform' + ('?usp=pp_url&entry.364853985=' + document.location.href);

        window.open(formURI, '_blank');
      }
    }
  }],
  desktopNavs: [{
    title: '關於我們',
    name: 'about',
    url: 'http://www.cnyes.com/cnyes_about/cnyes_about.html'
  }, {
    title: '集團簡介',
    name: 'anueCorp',
    url: 'https://www.anuegroup.com.tw/'
  }, {
    title: '廣告服務',
    name: 'ad',
    url: 'http://www.cnyes.com/cnyes_about/cnyes_AD01.html'
  }, {
    title: '金融資訊元件',
    name: 'financial',
    url: 'http://www.cnyes.com/cnyes_about/cnyes_pas01.html'
  }, {
    title: '聯絡我們',
    name: 'contact',
    url: 'http://www.cnyes.com/cnyes_about/cnyes_ctcUsTpe.html'
  }, {
    title: '徵才',
    name: 'job',
    url: 'http://www.104.com.tw/jobbank/custjob/index.php?r=cust&j=5e6042253446402330683b1d1d1d1d5f2443a363189j01'
  }, {
    title: '網站地圖',
    name: 'sitemap',
    url: 'http://www.cnyes.com/cnyes_about/site_map.html'
  }, {
    title: '法律聲明',
    name: 'legal',
    url: 'http://www.cnyes.com/cnyes_about/cnyes_sos01.html'
  }],
  socials: [{
    title: '粉 絲 團',
    name: 'facebook',
    url: 'https://www.facebook.com/anuetw/'
  }, {
    title: '鉅亨Line',
    name: 'line',
    url: 'https://line.me/ti/p/@ZLU0489G'
  }, {
    title: '鉅亨APP',
    name: 'app',
    url: 'http://www.cnyes.com/app?utm_source=cnyes&utm_medium=desktop&utm_campaign=desktop_footer'
  }]
};

exports.default = footerLinks;

/***/ })

/******/ });
});