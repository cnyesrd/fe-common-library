(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("axios"));
	else if(typeof define === 'function' && define.amd)
		define(["axios"], factory);
	else {
		var a = typeof exports === 'object' ? factory(require("axios")) : factory(root["axios"]);
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(typeof self !== 'undefined' ? self : this, function(__WEBPACK_EXTERNAL_MODULE_130__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/dest/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 503);
/******/ })
/************************************************************************/
/******/ ({

/***/ 129:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; /* globals: __SERVER__:false */


var _axios = __webpack_require__(130);

var _axios2 = _interopRequireDefault(_axios);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// eslint-disable-line no-duplicate-imports

var DEFAULT_PROJECT_NAME = 'fe-default';
var DEFAULT_APP_NAME = 'default';

function makeHandleAuthHeader(getToken, appName) {
  return function handleAuthHeader(config) {
    if (config && config.cnyesauth) {
      var token = getToken();
      // console.log('token', token);

      switch (config.cnyesauth) {
        case 'required':
          if (!token) {
            throw new Error('Auth is Required');
          }
          // eslint-disable-next-line no-param-reassign
          config.headers = _extends({}, config.headers, {
            Authorization: token
          }, appName ? { 'X-CNYES-APP': appName } : {});
          break;
        case 'optional':
          if (token) {
            // eslint-disable-next-line no-param-reassign
            config.headers = _extends({}, config.headers, {
              Authorization: token
            }, appName ? { 'X-CNYES-APP': appName } : {});
          }
          break;
        default:
        // do nothing
      }
    }

    return config;
  };
}

function makeHandleAuthFailedRefresh(refreshToken) {
  return function handleAuthFailedRefresh(error) {
    if ((typeof error === 'undefined' ? 'undefined' : _typeof(error)) === 'object' && error !== null && error.response) {
      var errorResponse = error.response;

      if (errorResponse.status === 401) {
        return refreshToken().then(function () {
          var originalConfig = errorResponse.config;
          var client = _axios2.default.create();

          if (originalConfig.getToken) {
            client.interceptors.request.use(makeHandleAuthHeader(originalConfig.getToken));
          }

          return client.request(originalConfig);
        });
      }
    }

    return Promise.reject(error);
  };
}

/**
 * @param {Object} defaultConfig - default config for axios
 * @param {string} defaultConfig.baseURL - baseURL for axios
 * @param {Object} param - config for customized functions
 * @param {Function} param.getToken - get auth token
 * @param {Function} param.refreshToken - refresh auth token
 * @param {string} param.hostname - hostname to be filled in user-agent header
 * @param {string} param.projectName - project name to be filled in user-agent header
 * @param {string} param.appName - app name for auth request
 * @returns {Function} api client
 */
function apiClientFactory(defaultConfig) {
  var _ref = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
      getToken = _ref.getToken,
      refreshToken = _ref.refreshToken,
      _ref$hostname = _ref.hostname,
      hostname = _ref$hostname === undefined ? '' : _ref$hostname,
      _ref$projectName = _ref.projectName,
      projectName = _ref$projectName === undefined ? DEFAULT_PROJECT_NAME : _ref$projectName,
      _ref$appName = _ref.appName,
      appName = _ref$appName === undefined ? DEFAULT_APP_NAME : _ref$appName;

  var userAgentHeader = {};

  if (global.__SERVER__) {
    var agentUa = 'axios'; // TODO determine axios version
    var ua = agentUa + ' ' + projectName + ' ' + hostname;

    userAgentHeader['User-Agent'] = ua;
  }

  var client = _axios2.default.create(_extends({
    headers: _extends({}, userAgentHeader, defaultConfig.headers),
    getToken: getToken,
    refreshToken: refreshToken
  }, defaultConfig));

  if (getToken) {
    client.interceptors.request.use(makeHandleAuthHeader(getToken, appName));
  }
  if (refreshToken) {
    client.interceptors.response.use(undefined, makeHandleAuthFailedRefresh(refreshToken));
  }

  return client;
}

exports.default = apiClientFactory;
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(3)))

/***/ }),

/***/ 130:
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_130__;

/***/ }),

/***/ 3:
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || Function("return this")() || (1,eval)("this");
} catch(e) {
	// This works if the window reference is available
	if(typeof window === "object")
		g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),

/***/ 503:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(129);


/***/ })

/******/ });
});