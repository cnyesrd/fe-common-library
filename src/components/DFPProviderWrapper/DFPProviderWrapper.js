import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { DFPProvider } from 'cnyes-dfp';
import AdProfiles from './AdProfiles';

class DFPProviderWrapper extends Component {
  static propTypes = {
    children: PropTypes.oneOfType([PropTypes.node, PropTypes.arrayOf(PropTypes.node)]).isRequired,
  };

  handleSlotRenderEnded = (provider, event) => {
    const adElement = document.getElementById(`${event.slot.getSlotElementId()}-container`);

    switch (event.slot.getAdUnitPath()) {
      case AdProfiles.detailNativeRelated.path: {
        if (!event.isEmpty && adElement) {
          adElement.style.display = 'block';
        }

        const placeholder = document.querySelector(`#${event.slot.getSlotElementId()}-container + article`);

        setTimeout(() => {
          if (placeholder) {
            placeholder.style.display = 'none';
          }
        }, 1000);

        break;
      }
      default:
        break;
    }

    if (event.isEmpty && adElement) {
      adElement.style.display = 'none';
    }
  };

  render() {
    return <DFPProvider onSlotRenderEnded={this.handleSlotRenderEnded}>{this.props.children}</DFPProvider>;
  }
}

export default DFPProviderWrapper;
