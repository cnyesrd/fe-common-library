const AdProfiles = {
  detailNativeRelated: {
    path: '/1018855/cnyes_news_related_native/LeftColumn',
    size: ['fluid'],
    hideOnInitial: true,
    className: 'cnyes-dfp-detail-native-column',
  },
  detailNativePopular: {
    path: '/1018855/cnyes_news_related_native/RightColumn',
    size: ['fluid'],
    hideOnInitial: true,
    className: 'cnyes-dfp-detail-native-column',
  },
  asideBannerTop: {
    name: 'asideBannerTop',
    path: '/1018855/cnyes_news_insideright_300*250',
    size: [300, 250],
    className: 'cnyes-dfp-banner',
  },
  asideBannerMid: {
    name: 'asideBannerMid',
    path: '/1018855/cnyes_news_aside_middle',
    size: [[300, 250], [300, 600]],
    className: 'cnyes-dfp-banner',
  },
  asideBannerBottom: {
    name: 'asideBannerBottom',
    path: '/1018855/cnyes_news_inside_300*100',
    size: [300, 100],
    className: 'cnyes-dfp-banner',
  },
};

export default AdProfiles;
