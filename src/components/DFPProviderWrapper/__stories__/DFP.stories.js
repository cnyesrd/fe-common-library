/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import { storiesOf } from '@storybook/react';
import { AdSlot } from 'cnyes-dfp';
import DFPProviderWrapper from '../DFPProviderWrapper';
import AdProfiles from '../AdProfiles';
import styles from './DFP.stories.scss';

const stories = storiesOf('DFP', module);

stories.addDecorator(story => <DFPProviderWrapper>{story()}</DFPProviderWrapper>);

stories.add('news-AsideAds', () => {
  const newsAsideAds = ['asideBannerTop', 'asideBannerMid', 'asideBannerBottom'];

  return (
    <div style={{ padding: '20px' }}>
      {newsAsideAds.map(key => (
        <div key={key}>
          <h1 className={styles.title}>{key}</h1>
          <AdSlot profile={AdProfiles[key]} lazyLoading={false} />
        </div>
      ))}
    </div>
  );
});

stories.add('news-related & popular', () => (
  <div style={{ padding: '20px' }}>
    <h1 className={styles.title}>detailNativeRelated</h1>
    <div style={{ width: '300px', height: '70px' }}>
      <AdSlot profile={AdProfiles.detailNativeRelated} lazyLoading={false} />
    </div>
    <h1 className={styles.title}>detailNativePopular</h1>
    <div style={{ width: '300px', height: '70px' }}>
      <AdSlot profile={AdProfiles.detailNativePopular} lazyLoading={false} />
    </div>
  </div>
));
