/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { withNotes } from '@storybook/addon-notes';
import * as knobs from '@storybook/addon-knobs/react';
import ScrollToTop from '../ScrollToTop';
import Footer from '../../Footer/Footer';
import defaultNotes from './ScrollToTop.md';

const stories = storiesOf('ScrollToTop', module);

stories.addDecorator(knobs.withKnobs);
stories.addDecorator(story => (
  <div>
    <div style={{ width: '100%', height: '1500px', padding: '40px 200px' }}>
      <p>long content here...</p>
      <p>long content here...</p>
      <p>long content here...</p>
      <p>long content here...</p>
      <p>long content here...</p>
      <p>long content here...</p>
      <p>long content here...</p>
      <p>long content here...</p>
      <p>long content here...</p>
      <p>long content here...</p>
      <p>long content here...</p>
      <p>scroll down...</p>
    </div>
    {story()}
    <Footer />
  </div>
));

stories.add(
  'ScrollToTop',
  withNotes(defaultNotes)(
    withInfo()(() => {
      return <ScrollToTop />;
    })
  )
);

stories.add(
  'ScrollToTop - showOffset',
  withNotes(defaultNotes)(
    withInfo()(() => {
      return <ScrollToTop showOffset={100} />;
    })
  )
);

stories.add(
  'ScrollToTop - hasFooter',
  withNotes(defaultNotes)(
    withInfo()(() => {
      return <ScrollToTop hasFooter />;
    })
  )
);

stories.add(
  'ScrollToTop - size m',
  withNotes(defaultNotes)(
    withInfo()(() => {
      return <ScrollToTop hasFooter size="m" />;
    })
  )
);

stories.add(
  'ScrollToTop - custom children',
  withNotes(defaultNotes)(
    withInfo()(() => {
      return (
        <ScrollToTop>
          <button
            style={{
              position: 'fixed',
              top: 100,
              left: 100,
              cursor: 'pointer',
            }}
          >
            click me to scroll to top
          </button>
        </ScrollToTop>
      );
    })
  )
);

storiesOf('ScrollToTop', module).add('ScrollToTop - custom Footer', () => {
  const customFooterStyle = {
    width: '100%',
    height: '200px',
    background: '#666',
    color: '#fff',
    textAlign: 'center',
  };

  return (
    <div>
      <div style={{ width: '100%', height: '1500px', padding: '40px 200px' }}>
        <p>there is a custom Footer below</p>
        <p>Please scroll down...</p>
      </div>
      <ScrollToTop customFooterHeight={220} />;
      <div style={customFooterStyle}>Custom Footer and Height:200px</div>
    </div>
  );
});
