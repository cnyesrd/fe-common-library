## Usage

```
import ScrollToTop from 'fe-common-library/dest/components/ScrollToTop/ScrollToTop';
import 'fe-common-library/dest/components/ScrollToTop/style.css';

// ...

render() {
  return (
    <ScrollToTop showOffset={100}>
  );
}
```