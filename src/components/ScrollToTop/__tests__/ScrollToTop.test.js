jest.mock('react-scroll', () => ({
  animateScroll: {
    scrollToTop: jest.fn(),
  },
}));

/* eslint-disable import/first */
import React from 'react';
import { shallow } from 'enzyme';
import { animateScroll } from 'react-scroll';
import ScrollToTop from '../ScrollToTop';

describe('component <ScrollToTop />', () => {
  let makeSubject;

  beforeEach(() => {
    jest.resetModules();

    makeSubject = (props = {}) => {
      return shallow(<ScrollToTop {...props} />);
    };
  });

  it('should be same as snapshot', () => {
    const subject = makeSubject();

    expect(subject).toMatchSnapshot();
  });

  describe('scroll to top button', () => {
    it('should hide if state: show = false as default ', () => {
      const subject = makeSubject();

      expect(subject.find('.scroll-to-top').length).toBe(0);
    });

    it('should show if state: show = true', () => {
      const subject = makeSubject();

      subject.setState({ show: true });
      expect(subject.find('.scroll-to-top').length).toBe(1);
    });

    it('should call animateScroll.scrollToTop on click', () => {
      const subject = makeSubject();

      subject.setState({ show: true });
      subject.find('.scroll-to-top').simulate('click');
      expect(animateScroll.scrollToTop).toHaveBeenCalled();
    });

    it('should call animateScroll.scrollToTop on click custom children', () => {
      const subject = makeSubject({ children: <div className="custom-scroll-to-top" /> });

      subject.setState({ show: true });
      subject.find('.custom-scroll-to-top').simulate('click');
      expect(animateScroll.scrollToTop).toHaveBeenCalled();
    });

    it('should call scrollCb once', () => {
      const spyScrollCb = jest.fn();
      const subject = makeSubject({
        scrollCb: spyScrollCb,
      });

      subject.setState({ show: true });
      subject.find('.scroll-to-top').simulate('click');
      expect(animateScroll.scrollToTop).toHaveBeenCalled();
      expect(spyScrollCb).toHaveBeenCalled();
    });
  });
});
