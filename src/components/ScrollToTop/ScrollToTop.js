import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames/bind';
import { animateScroll } from 'react-scroll';
import throttle from 'lodash.throttle';
import styles from './ScrollToTop.scss';
import footerStyles from '../Footer/Footer.scss';
import getDataObj from '../../utils/getDataObj';

const cx = classnames.bind(styles);
const defaultBottom = 46;
const defaultFooterHeight = parseInt(footerStyles.footerHeight, 10) + 24; // margin 24px

class ScrollToTop extends React.Component {
  static propTypes = {
    showOffset: PropTypes.number,
    customFooterHeight: PropTypes.number,
    hasFooter: PropTypes.bool,
    children: PropTypes.element,
    size: PropTypes.oneOf(['s', 'm']),
    scrollCb: PropTypes.func,
  };

  static defaultProps = {
    showOffset: 225,
    children: undefined,
    customFooterHeight: 0,
    hasFooter: false,
    size: 's',
    scrollCb: null,
  };

  state = {
    show: false,
    bottom: defaultBottom,
  };

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll = throttle(() => {
    const { hasFooter, customFooterHeight } = this.props;
    let customBottom = defaultBottom;

    if (window.pageYOffset >= this.props.showOffset) {
      if (hasFooter || customFooterHeight) {
        const footerHeight = customFooterHeight || defaultFooterHeight;
        const scrollHeight = document.body.scrollHeight;
        const offsetTop = window.innerHeight + window.pageYOffset;
        const offsetBottom = scrollHeight - offsetTop;
        const diff = footerHeight - defaultBottom;

        if (diff >= offsetBottom) {
          customBottom = footerHeight - offsetBottom;
        }
      }
      this.setState({ show: true, bottom: customBottom });
    } else {
      this.setState({ show: false });
    }
  }, 50);

  scrollToTop = e => {
    const { scrollCb } = this.props;

    if (e) {
      e.preventDefault();
    }

    animateScroll.scrollToTop();
    if (scrollCb) {
      scrollCb();
    }
  };

  render() {
    const { children, size } = this.props;
    const { bottom } = this.state;
    const dataObj = getDataObj(this.props);

    if (this.state.show) {
      return children ? (
        React.cloneElement(children, { onClick: this.scrollToTop })
      ) : (
        <button
          onClick={this.scrollToTop}
          className={cx('scroll-to-top', `scroll-to-top--${size}`)}
          style={{ bottom: `${bottom}px` }}
          {...dataObj}
        />
      );
    }

    return null;
  }
}

export default ScrollToTop;
