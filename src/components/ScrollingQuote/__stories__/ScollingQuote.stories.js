/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { withNotes } from '@storybook/addon-notes';
import { withState } from '@dump247/storybook-state';
import * as knobs from '@storybook/addon-knobs/react';
import ScrollingQuote from '../ScrollingQuote';
import defaultNotes from './ScrollingQuote.md';

const quotesA = [
  {
    name: '日經225',
    value: '22,497.00',
    netChange: '+89.00',
    changeInPercentage: '+0.40%',
  },
  {
    name: '香港恆生',
    value: '30,825.00',
    netChange: '-289.00',
    changeInPercentage: '-0.95%',
  },
  {
    name: '道瓊指數',
    value: '24,542.54',
    netChange: '0.00',
    changeInPercentage: '0.00%',
  },
];

const quotesB = [
  {
    name: '日經225',
    value: '22,497.00',
    netChange: '-89.00',
    changeInPercentage: '-0.40%',
  },
  {
    name: '香港恆生',
    value: '30,825.00',
    netChange: '+289.00',
    changeInPercentage: '+0.95%',
  },
  {
    name: '道瓊指數',
    value: '24,542.54',
    netChange: '0.00',
    changeInPercentage: '0.00%',
  },
];

const stories = storiesOf('ScrollingQuote', module);

stories.addDecorator(knobs.withKnobs);

stories.add(
  'ScrollingQuote',
  withNotes(defaultNotes)(
    withState({ isQuotesA: true })(
      withInfo()(({ store }) => {
        const { isQuotesA } = store.state;
        const quotes = isQuotesA ? quotesA : quotesB;

        return <ScrollingQuote quotes={quotes} handleScrollEnd={() => store.set({ isQuotesA: !isQuotesA })} />;
      })
    )
  )
);

stories.add(
  'ScrollingQuote - inverted',
  withNotes(defaultNotes)(
    withState({ isQuotesA: true })(
      withInfo()(({ store }) => {
        const { isQuotesA } = store.state;
        const quotes = isQuotesA ? quotesA : quotesB;

        return (
          <ScrollingQuote
            quotes={quotes}
            handleScrollEnd={() => store.set({ isQuotesA: !isQuotesA })}
            inverted={knobs.boolean('inverted', true)}
          />
        );
      })
    )
  )
);

stories.add(
  'ScrollingQuote - speed x2',
  withNotes(defaultNotes)(
    withState({ isQuotesA: true })(
      withInfo()(({ store }) => {
        const { isQuotesA } = store.state;
        const quotes = isQuotesA ? quotesA : quotesB;

        return (
          <ScrollingQuote
            quotes={quotes}
            handleScrollEnd={() => store.set({ isQuotesA: !isQuotesA })}
            speed={knobs.number('speed', 2)}
          />
        );
      })
    )
  )
);
