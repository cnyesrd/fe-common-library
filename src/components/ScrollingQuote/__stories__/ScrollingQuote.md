## Usage

```
import ScrollingQuote from 'fe-common-library/dest/components/ScrollingQuote';
import 'fe-common-library/dest/components/ScrollingQuote/style.css';

// ...

render() {
  return (
    <ScrollingQuote speed={2} inverted quotes={[
      {
        name: '日經225',
        value: '22,497.00',
        netChange: '+89.00',
        changeInPercentage: '+0.40%',
      },
      {
        name: '香港恆生',
        value: '30,825.00',
        netChange: '-289.00',
        changeInPercentage: '-0.95%',
      },
      {
        name: '道瓊指數',
        value: '24,542.54',
        netChange: '0.00',
        changeInPercentage: '0.00%',
      },
    ]} />
  );
}
```