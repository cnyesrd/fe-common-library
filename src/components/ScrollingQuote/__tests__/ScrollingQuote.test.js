import React from 'react';
import { mount } from 'enzyme';
import ScrollingQuote from '../ScrollingQuote';

describe('component <ScrollingQuote />', () => {
  let makeSubject;

  beforeEach(() => {
    jest.resetModules();

    window.requestAnimationFrame = jest.fn(() => 'rafId');
    window.cancelAnimationFrame = jest.fn();

    const testProps = {
      quotes: [
        {
          name: '日經225',
          value: '22,497.00',
          netChange: '+89.00',
          changeInPercentage: '+0.40%',
        },
        {
          name: '香港恆生',
          value: '30,825.00',
          netChange: '-289.00',
          changeInPercentage: '-0.95%',
        },
        {
          name: '道瓊指數',
          value: '24,542.54',
          netChange: '0.00',
          changeInPercentage: '0.00%',
        },
      ],
      handleScrollEnd: () => {},
    };

    makeSubject = (params = {}) => {
      const props = {
        ...testProps,
        ...params,
      };

      return mount(<ScrollingQuote {...props} />);
    };
  });

  it('should render wrapper', () => {
    const subject = makeSubject();

    expect(subject.find('.wrapper').length).toBe(1);
    expect(window.requestAnimationFrame).toHaveBeenCalledTimes(1);
  });

  it('should render quote spans', () => {
    const subject = makeSubject();

    expect(subject.find('.quote').length).toBe(subject.props().quotes.length);
  });

  it('should be same as snapshot', () => {
    const subject = makeSubject();

    expect(subject).toMatchSnapshot();
  });

  describe('wrapper', () => {
    it('should not have className: inverted', () => {
      const subject = makeSubject();

      expect(subject.find('.wrapper').hasClass('inverted')).toBeFalsy();
    });

    it('should have className: inverted if props: inverted = true', () => {
      const subject = makeSubject({ inverted: true });

      expect(subject.find('.wrapper').hasClass('inverted')).toBeTruthy();
    });
  });

  describe('quote span', () => {
    it('should have className: positive if quote.netChange starts with `+`', () => {
      const subject = makeSubject();

      expect(
        subject
          .find('.quote')
          .at(0)
          .hasClass('positive')
      ).toBeTruthy();
      expect(
        subject
          .find('.quote')
          .at(0)
          .hasClass('negative')
      ).toBeFalsy();
    });

    it('should have className: negative if quote.netChange starts with `-`', () => {
      const subject = makeSubject();

      expect(
        subject
          .find('.quote')
          .at(1)
          .hasClass('positive')
      ).toBeFalsy();
      expect(
        subject
          .find('.quote')
          .at(1)
          .hasClass('negative')
      ).toBeTruthy();
    });

    it('should have className: negative if quote.netChange not starts with `+` or `-`', () => {
      const subject = makeSubject();

      expect(
        subject
          .find('.quote')
          .at(2)
          .hasClass('positive')
      ).toBeFalsy();
      expect(
        subject
          .find('.quote')
          .at(2)
          .hasClass('negative')
      ).toBeFalsy();
    });
  });

  describe('requestAnimationFrame & cancelAnimationFrame', () => {
    it('should call requestAnimationFrame when componentDidMount', () => {
      makeSubject();

      expect(window.requestAnimationFrame).toHaveBeenCalledTimes(1);
    });

    it('should call cancelAnimationFrame onMounseEnter wrapper', () => {
      const subject = makeSubject();

      window.cancelAnimationFrame.mockClear();
      subject.find('.wrapper').simulate('mouseEnter');
      expect(window.cancelAnimationFrame).toHaveBeenCalledTimes(1);
    });

    it('should call requestAnimationFrame onMouseLeave wrapper', () => {
      const subject = makeSubject();

      expect(window.requestAnimationFrame).toHaveBeenCalledTimes(1);
      subject.find('.wrapper').simulate('mouseLeave');
      expect(window.requestAnimationFrame).toHaveBeenCalledTimes(2);
    });

    it('should call cancelAnimationFrame when componentWillUnmount', () => {
      const subject = makeSubject();

      window.cancelAnimationFrame.mockClear();
      subject.unmount();
      expect(window.cancelAnimationFrame).toHaveBeenCalledTimes(1);
    });
  });
});
