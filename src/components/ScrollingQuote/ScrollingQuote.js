import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';

import styles from './ScrollingQuote.scss';

const cx = classNames.bind(styles);

class ScrollingQuote extends React.Component {
  static propTypes = {
    inverted: PropTypes.bool,
    speed: PropTypes.number,
    quotes: PropTypes.arrayOf(
      PropTypes.shape({
        name: PropTypes.oneOfType([PropTypes.string, PropTypes.element]).isRequired,
        value: PropTypes.string.isRequired,
        netChange: PropTypes.string.isRequired,
        changeInPercentage: PropTypes.string.isRequired,
      })
    ),
    handleScrollEnd: PropTypes.func.isRequired,
  };

  static defaultProps = {
    inverted: false,
    speed: 1,
    quotes: [],
  };

  componentDidMount() {
    this.scrollerElementWidth = this.getScrollerElementWidth();
    this.shift = this.getWrapperElementWidth();

    this.startScroll();
  }

  componentDidUpdate(prevProps) {
    if (this.props.quotes !== prevProps.quotes) {
      this.scrollerElementWidth = this.getScrollerElementWidth();

      this.startScroll();
    }
  }

  componentWillUnmount() {
    this.stopScroll();
  }

  getScrollerElementWidth = () => this.scrollerElement.getBoundingClientRect().width;

  getWrapperElementWidth = () => this.scrollerElement.parentNode.getBoundingClientRect().width;

  setScrollerElement = element => {
    this.scrollerElement = element;
  };

  scroll = () => {
    const { handleScrollEnd, speed } = this.props;

    if (this.shift < -this.scrollerElementWidth) {
      this.shift = this.getWrapperElementWidth();
      this.scrollerElement.style.transform = `translateX(${this.shift}px)`;

      handleScrollEnd();

      return;
    }

    this.shift = this.shift - speed;
    this.scrollerElement.style.transform = `translateX(${this.shift}px)`;

    if (window && window.requestAnimationFrame) {
      this.rafId = window.requestAnimationFrame(this.scroll);
    }
  };

  startScroll = () => {
    this.stopScroll();

    if (window && window.requestAnimationFrame) {
      this.rafId = window.requestAnimationFrame(this.scroll);
    }
  };

  stopScroll = () => {
    if (window && window.cancelAnimationFrame && this.rafId) {
      window.cancelAnimationFrame(this.rafId);
    }
  };

  render() {
    const { inverted, quotes } = this.props;

    return (
      <div className={cx('wrapper', { inverted })} onMouseEnter={this.stopScroll} onMouseLeave={this.startScroll}>
        <div className={cx('scroller')} ref={this.setScrollerElement}>
          {quotes.map(({ name, value, netChange, changeInPercentage }) => {
            const sign = netChange[0];
            const positive = sign === '+';
            const negative = sign === '-';

            return (
              <span key={name} className={cx('quote', { positive, negative })}>
                <span className={cx('name')}>{name}</span>
                <span>{value}</span>
                <span className={cx('mark')} />
                <span className={cx('change')}>{netChange}</span>
                <span className={cx('change')}>{changeInPercentage}</span>
              </span>
            );
          })}
        </div>
      </div>
    );
  }
}

export default ScrollingQuote;
