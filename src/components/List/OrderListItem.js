import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames/bind';
import styles from './OrderListItem.scss';
import SmartTime from '../SmartTime/SmartTime';
import { rwdWidthMode } from './constants';

const cx = classnames.bind(styles);
const MAX_TITLE_LENGTH_WIDE = 35;
const MAX_TITLE_LENGTH_SHORT = 26;
const ELLIPSIS_LENGTH = 4;

class OrderListItem extends Component {
  formatTitle = () => {
    const { widthMode, title } = this.props;
    const lengthLimit = widthMode === rwdWidthMode.short ? MAX_TITLE_LENGTH_SHORT : MAX_TITLE_LENGTH_WIDE;

    if (title.length > lengthLimit + ELLIPSIS_LENGTH) {
      return `${title.slice(0, lengthLimit)} ...`;
    }

    return title;
  };

  render() {
    const { order, title, time, now, handleClick, adElement } = this.props;
    const formattedTitle = this.formatTitle();

    return (
      <div className={cx('list-item')} onClick={handleClick}>
        <span>{order}.</span>
        <h3 title={title}>{formattedTitle}</h3>
        {time && <SmartTime second={time} now={now} />}
        {adElement && <div className={cx('list-item__ads')}>{adElement}</div>}
      </div>
    );
  }
}

OrderListItem.propTypes = {
  order: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  time: PropTypes.number,
  now: PropTypes.number,
  widthMode: PropTypes.oneOf(Object.values(rwdWidthMode)),
  handleClick: PropTypes.func,
  adElement: PropTypes.element,
};

OrderListItem.defaultProps = {
  time: null,
  now: null,
  widthMode: rwdWidthMode.short,
  handleClick: () => {},
  adElement: null,
};

export default OrderListItem;
