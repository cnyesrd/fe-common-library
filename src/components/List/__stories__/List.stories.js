/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { withNotes } from '@storybook/addon-notes';
import { withState } from '@dump247/storybook-state';
import { action } from '@storybook/addon-actions';
import { AdSlot } from 'cnyes-dfp';
import cx from 'classnames';
import DFPProviderWrapper from '../../DFPProviderWrapper/DFPProviderWrapper';
import AdProfiles from '../../DFPProviderWrapper/AdProfiles';
import OrderListItem from '../OrderListItem';
import List from '../List';
import defaultNotes from './List.md';
import styles from './List.stories.scss';

const stories = storiesOf('List', module);

stories.addDecorator(story => <DFPProviderWrapper>{story()}</DFPProviderWrapper>);

stories.add(
  'OrderList',
  withNotes(defaultNotes)(
    withInfo()(() => {
      const now = Date.now();
      const items = [
        { id: 'a', title: '台股期權盤後－三角收斂依舊 台股打底作撐', time: now / 1000 },
        { id: 'b', title: '唱衰美元聲量大 但和歐元相比 美元明年仍續強', time: now / 1000 - 60 * 60 * 24 },
        {
          id: 'c',
          title: '上市櫃前三季營收、獲利雙創10年同期新高 但這家公司明年3月恐下市',
          time: now / 1000 - 60 * 60 * 24 * 30,
        },
      ];

      return (
        <div style={{ padding: '20px', background: '#F1F1F1', height: '100%' }}>
          <h3 style={{ fontSize: '20px', margin: '10px' }}>widthMode: SHORT</h3>
          <List>
            {items.map(({ id, title, time }, index) => (
              <OrderListItem
                key={id}
                order={index + 1}
                title={title}
                time={time}
                now={now}
                handleClick={action(title)}
              />
            ))}
          </List>
          <h3 style={{ fontSize: '20px', margin: '10px' }}>widthMode: WIDE</h3>
          <List widthMode="WIDE">
            {items.map(({ id, title, time }, index) => (
              <OrderListItem
                key={id}
                order={index + 1}
                title={title}
                time={time}
                now={now}
                handleClick={action(title)}
              />
            ))}
          </List>
        </div>
      );
    })
  )
);

stories.add(
  'OrderList-with Ads',
  withNotes(defaultNotes)(
    withState({ widthMode: 'SHORT' })(
      withInfo()(({ store }) => {
        const renderRelatedAds = () => <AdSlot profile={AdProfiles.detailNativeRelated} lazyLoading={false} />;
        const now = Date.now();
        const items = [
          { id: 'a', title: '台股期權盤後－三角收斂依舊 台股打底作撐', time: now / 1000 },
          {
            id: 'b',
            title: '唱衰美元聲量大 但和歐元相比 美元明年仍續強',
            time: now / 1000 - 60 * 60 * 24,
            adElement: renderRelatedAds(),
          },
          {
            id: 'c',
            title: '上市櫃前三季營收、獲利雙創10年同期新高 但這家公司明年3月恐下市',
            time: now / 1000 - 60 * 60 * 24 * 30,
          },
        ];

        return (
          <div style={{ padding: '20px', background: '#F1F1F1', height: '100%' }}>
            <button
              className={cx(styles.button, { active: store.state.widthMode === 'SHORT' })}
              onClick={() => store.set({ widthMode: 'SHORT' })}
            >
              SHORT
            </button>
            <button
              className={cx(styles.button, { active: store.state.widthMode === 'WIDE' })}
              onClick={() => store.set({ widthMode: 'WIDE' })}
            >
              WIDE
            </button>
            <List widthMode={store.state.widthMode}>
              {items.map(({ id, title, time, adElement }, index) => (
                <OrderListItem
                  key={id}
                  order={index + 1}
                  title={title}
                  time={time}
                  now={now}
                  handleClick={action(title)}
                  adElement={adElement}
                />
              ))}
            </List>
          </div>
        );
      })
    )
  )
);
