import React from 'react';
import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import List from '../List';
import OrderListItem from '../OrderListItem';

describe('<List />', () => {
  let makeSubject;
  let subject;

  beforeEach(() => {
    jest.resetModules();
    makeSubject = ({ items, widthMode = 'SHORT' }) =>
      mount(
        <List widthMode={widthMode}>
          {items.map(({ key, title }, index) => <OrderListItem key={key} order={index + 1} title={title} />)}
        </List>
      );
  });

  it('should be same as snapshot', () => {
    const items = [
      { key: 'a', title: '唱衰美元聲量大 但和歐元相比 美元明年仍續強' },
      { key: 'b', title: '台股期權盤後－三角收斂依舊 台股打底作撐' },
    ];

    subject = makeSubject({ items });
    expect(toJson(subject)).toMatchSnapshot();
  });

  it('should pass widthMode to children component', () => {
    const items = [
      { key: 'a', title: '唱衰美元聲量大 但和歐元相比 美元明年仍續強' },
      { key: 'b', title: '台股期權盤後－三角收斂依舊 台股打底作撐' },
    ];
    const widthMode = 'WIDE';

    subject = makeSubject({ items, widthMode });
    expect(
      subject
        .find(OrderListItem)
        .first()
        .prop('widthMode')
    ).toEqual(widthMode);
  });
});
