import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import styles from './FastLinkBlock.scss';
import NotFoundFastLinksConfig from './NotFoundFastLinksConfig';

const FastLinkBlock = ({ blockWrapperClassName, blockClassName, mode, fastLinkConfig }) => {
  const wrapperClassName =
    mode === 'responsive' ? styles['fast-link-container'] : styles[`fast-link-${mode}-container`];
  const defaultBlockClassName = mode === 'responsive' ? styles['fast-link-block'] : styles[`fast-link-${mode}-block`];

  return (
    <div className={cx(blockWrapperClassName, wrapperClassName)}>
      {fastLinkConfig.map((link, idx) => (
        // eslint-disable-next-line react/no-array-index-key
        <a className={cx(blockClassName, defaultBlockClassName)} key={idx} href={link.anueProdUrl}>
          {link.imgUrl ? (
            <div style={{ backgroundImage: `url(${link.imgUrl})` }} className={styles[`link-img-${link.imgSize}`]} />
          ) : (
            <div className={styles.title}>{link.title}</div>
          )}
          <div className={styles.text}>{link.text}</div>
        </a>
      ))}
    </div>
  );
};

FastLinkBlock.propTypes = {
  blockWrapperClassName: PropTypes.string,
  blockClassName: PropTypes.string,
  mode: PropTypes.oneOf(['mobile', 'desktop', 'responsive']),
  fastLinkConfig: PropTypes.arrayOf({
    type: PropTypes.string,
    title: PropTypes.string,
    imgUrl: PropTypes.string,
    imgSize: PropTypes.oneOf(['s', 'm', 'l']),
    text: PropTypes.string,
    anueProdUrl: PropTypes.string,
  }),
};

FastLinkBlock.defaultProps = {
  blockWrapperClassName: '',
  blockClassName: '',
  mode: 'responsive',
  fastLinkConfig: NotFoundFastLinksConfig,
};

export default FastLinkBlock;
