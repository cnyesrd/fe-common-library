import anueBrandLogo from '../assets/anue_brand_logo.svg';
import driverBrandLogo from '../assets/driver_brand_logo.svg';

export default [
  {
    type: 'img',
    title: 'Anue鉅亨',
    imgUrl: anueBrandLogo,
    imgSize: 'm',
    text: '立即前往',
    anueProdUrl: 'https://www.cnyes.com/',
  },
  {
    type: 'title',
    title: '影音專區',
    imgUrl: '',
    imgSize: '',
    text: '立即前往',
    anueProdUrl: 'https://www.cnyes.com/video/cat/all',
  },
  {
    type: 'img',
    title: '老司機',
    imgUrl: driverBrandLogo,
    imgSize: 'l',
    text: '立即前往',
    anueProdUrl: 'https://invest.anue.com/',
  },
];
