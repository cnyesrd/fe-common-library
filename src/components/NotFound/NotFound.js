import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import cx from 'classnames';
import styles from './NotFound.scss';
import FastLinkBlock from './FastLinkBlock/FastLinkBlock';
import NotFoundFastLinksConfig from './FastLinkBlock/NotFoundFastLinksConfig';

const NotFound = ({ rootStyle, containerClassName, blockWrapperClassName, blockClassName, mode, fastLinkConfig }) => {
  const rootClassName = mode === 'responsive' ? styles['notfound-section'] : styles[`notfound-${mode}-section`];

  return (
    <section className={rootClassName} style={rootStyle}>
      <div className={cx(containerClassName, styles.notfound)}>
        <h1>404</h1>
        <h2>糟糕找不到網頁</h2>
        <h3>休息一下，人生需要點空白，有時候投資也是。</h3>
        <FastLinkBlock
          blockWrapperClassName={blockWrapperClassName}
          blockClassName={blockClassName}
          mode={mode}
          fastLinkConfig={fastLinkConfig}
        />
      </div>
    </section>
  );
};

NotFound.propTypes = {
  rootStyle: stylePropType,
  containerClassName: PropTypes.string,
  blockWrapperClassName: PropTypes.string,
  blockClassName: PropTypes.string,
  mode: PropTypes.oneOf(['mobile', 'desktop', 'responsive']),
  fastLinkConfig: PropTypes.arrayOf({
    type: PropTypes.string,
    title: PropTypes.string,
    imgUrl: PropTypes.string,
    imgSize: PropTypes.oneOf(['s', 'm', 'l']),
    text: PropTypes.string,
    anueProdUrl: PropTypes.string,
  }),
};

NotFound.defaultProps = {
  rootStyle: {},
  containerClassName: '',
  blockWrapperClassName: '',
  blockClassName: '',
  mode: 'responsive',
  fastLinkConfig: NotFoundFastLinksConfig,
};

export default NotFound;
