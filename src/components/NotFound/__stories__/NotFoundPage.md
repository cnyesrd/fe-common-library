## Usage

```
import NotFound from 'fe-common-library/dest/components/NotFound';
import 'fe-common-library/dest/components/NotFound/style.css';

// ...

render() {
  return ( <NotFound/> );
}
```
