import anueBrandLogo from './driver_brand_logo.svg';
import driverBrandLogo from './anue_brand_logo.svg';

export default [
  {
    type: 'img',
    title: 'Anue鉅亨',
    imgUrl: anueBrandLogo,
    imgSize: 'l',
    text: '點擊前往',
    anueProdUrl: 'https://www.cnyes.com/',
  },
  {
    type: 'title',
    title: '影音專區',
    imgUrl: '',
    imgSize: '',
    text: '點擊前往',
    anueProdUrl: 'https://www.cnyes.com/video/cat/all',
  },
  {
    type: 'img',
    title: '老司機',
    imgUrl: driverBrandLogo,
    imgSize: 'm',
    text: '點擊前往',
    anueProdUrl: 'https://invest.anue.com/',
  },
];
