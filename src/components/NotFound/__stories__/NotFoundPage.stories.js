/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { withNotes } from '@storybook/addon-notes';
import * as knobs from '@storybook/addon-knobs/react';

import NotFound from '../NotFound';
import Header from '../../Header/Header';
import Footer from '../../Footer/Footer';

import { MobileMenu } from '../../MobileHeader/index';
import mobileStyles from './MobileHeaderStyle.scss';
import styles from './styles.scss';
import fastLinkConfig from './faskLinkConfig';

import * as fixedHeaderTypes from '../../Header/ConstantUI';
import * as newsConfig from '../../Header/__stories__/config/newsConfig';

import defaultNotes from './NotFoundPage.md';

const stories = storiesOf('NotFound', module);

stories.addDecorator(knobs.withKnobs);

stories.add(
  'NotFoundPage with fixed header and footer',
  withNotes(defaultNotes)(
    withInfo()(() => {
      return (
        <div>
          <Header
            channel="新聞"
            navs={newsConfig.navs}
            catNavs={newsConfig.catNavs}
            location={window.location}
            fixedHeaderType={fixedHeaderTypes.FIXED_HEADER_FULL}
          />
          <NotFound />
          <Footer now={Date.now()} />
        </div>
      );
    })
  )
);

stories.add(
  'NotFoundPage with customized style',
  withNotes(defaultNotes)(
    withInfo()(() => {
      return (
        <div>
          <Header
            channel="新聞"
            navs={newsConfig.navs}
            catNavs={newsConfig.catNavs}
            location={window.location}
            fixedHeaderType={fixedHeaderTypes.FIXED_HEADER_FULL}
          />
          <NotFound
            rootStyle={{ height: 'calc(100vh - 40px)' }}
            containerClassName={styles.container}
            blockWrapperClassName={styles.blockWrapper}
            blockClassName={styles.block}
          />
          <Footer now={Date.now()} />
        </div>
      );
    })
  )
);

stories.add(
  'NotFoundPage with mobile header',
  withNotes(defaultNotes)(
    withInfo()(() => {
      return (
        <div>
          <header className={mobileStyles.header}>
            <MobileMenu channelName="外匯" />
          </header>
          <NotFound />
          <Footer now={Date.now()} />
        </div>
      );
    })
  )
);

stories.add(
  'NotFoundPage(force to mobile mode) with fixed header',
  withNotes(defaultNotes)(
    withInfo()(() => {
      return (
        <div>
          <header className={mobileStyles.header}>
            <MobileMenu channelName="外匯" />
          </header>
          <NotFound mode={'mobile'} />
          <Footer now={Date.now()} />
        </div>
      );
    })
  )
);

stories.add(
  'NotFoundPage(force to desktop mode) with fixed header',
  withNotes(defaultNotes)(
    withInfo()(() => {
      return (
        <div>
          <Header
            channel="新聞"
            navs={newsConfig.navs}
            catNavs={newsConfig.catNavs}
            location={window.location}
            fixedHeaderType={fixedHeaderTypes.FIXED_HEADER_FULL}
          />
          <NotFound mode={'desktop'} />
          <Footer now={Date.now()} />
        </div>
      );
    })
  )
);

stories.add(
  'NotFoundPage with customized config  ',
  withNotes(defaultNotes)(
    withInfo()(() => {
      return (
        <div>
          <Header
            channel="新聞"
            navs={newsConfig.navs}
            catNavs={newsConfig.catNavs}
            location={window.location}
            fixedHeaderType={fixedHeaderTypes.FIXED_HEADER_FULL}
          />
          <NotFound fastLinkConfig={fastLinkConfig} />
          <Footer now={Date.now()} />
        </div>
      );
    })
  )
);
