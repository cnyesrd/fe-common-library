import React from 'react';
import { mount } from 'enzyme';
import FastLinkBlock from '../FastLinkBlock/FastLinkBlock';
import NotFoundFastLinksConfig from '../FastLinkBlock/NotFoundFastLinksConfig';

describe('<FastLinkBlock />', () => {
  const makeSubject = () => {
    return mount(<FastLinkBlock />);
  };

  it('should be nice', () => {
    const subject = makeSubject();
    const fastLinkContainer = subject.find('.fast-link-container');
    const fastLinks = subject.find('.fast-link-block');

    expect(fastLinkContainer.length).toEqual(1);
    expect(fastLinks.length).toEqual(NotFoundFastLinksConfig.length);

    subject.find('.fast-link-block').forEach((link, idx) => {
      expect(link.find('a').prop('href')).toBe(NotFoundFastLinksConfig[idx].anueProdUrl);
    });

    expect(subject.debug()).toMatchSnapshot();
  });
});
