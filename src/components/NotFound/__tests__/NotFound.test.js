import React from 'react';
import { mount } from 'enzyme';
import NotFound from '../NotFound';

describe('<NotFound />', () => {
  const makeSubject = () => {
    return mount(<NotFound />);
  };

  it('should be nice', () => {
    const subject = makeSubject();

    expect(subject.find('.notfound').length).toEqual(1);
    expect(subject.find('h1').length).toEqual(1);
    expect(subject.find('h2').length).toEqual(1);
    expect(subject.find('h3').length).toEqual(1);
    expect(subject.debug()).toMatchSnapshot();
  });
});
