import React from 'react';
import PropTypes from 'prop-types';
import { timeDisplayFactory, shortISOString, timeFormat } from './timeHelper';

function SmartTime({ second, nowMs, format }) {
  const ms = second * 1000;

  return <time dateTime={shortISOString(ms)}>{format ? timeFormat(ms, format) : timeDisplayFactory(ms, nowMs)}</time>;
}

SmartTime.propTypes = {
  second: PropTypes.number.isRequired,
  nowMs: PropTypes.number,
  format: PropTypes.string,
};

SmartTime.defaultProps = {
  nowMs: Date.now(),
  format: null,
};

export default SmartTime;
