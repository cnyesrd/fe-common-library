/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { withNotes } from '@storybook/addon-notes';
import * as knobs from '@storybook/addon-knobs/react';
import SmartTime from '../SmartTime';
import defaultNotes from './SmartTime.md';

const stories = storiesOf('SmartTime', module);

stories.addDecorator(knobs.withKnobs);

stories.add(
  'Base on now',
  withNotes(defaultNotes)(
    withInfo()(() => {
      const nowMs = Date.now();
      const types = [
        { type: 'today', second: nowMs / 1000 },
        { type: 'this year', second: nowMs / 1000 - 60 * 60 * 24 * 30 },
        { type: 'last year', second: nowMs / 1000 - 60 * 60 * 24 * 365 },
      ];

      return types.map(({ type, second }) => (
        <div key={type} style={{ padding: '10px' }}>
          <h1 style={{ fontSize: '30px', marginBottom: '5px' }}>{type}</h1>
          <SmartTime second={second} nowMs={nowMs} />
        </div>
      ));
    })
  )
);

stories.add(
  'Custom format',
  withNotes(defaultNotes)(
    withInfo()(() => {
      const nowMs = Date.now();
      const types = [{ type: 'YYYY/MM/DD', second: nowMs / 1000 }, { type: 'YYYY/MM/DD HH:mm', second: nowMs / 1000 }];

      return types.map(({ type, second }) => (
        <div key={type} style={{ padding: '10px' }}>
          <h1 style={{ fontSize: '30px', marginBottom: '5px' }}>{type}</h1>
          <SmartTime second={second} format={type} />
        </div>
      ));
    })
  )
);
