/* eslint-disable import/first,func-names,global-require,prefer-arrow-callback,prefer-promise-reject-errors */

import React from 'react';
import { mount } from 'enzyme';
import NavItem from '../NavItem';
import mockData from '../navConfig';

describe('component <NavItem />', () => {
  let makeSubject;

  beforeEach(() => {
    jest.resetModules();

    const testParams = {
      onClick: jest.fn(),
      item: mockData[0].items[0],
      navTitle: mockData[0].title,
    };

    makeSubject = (params = {}) => {
      const props = {
        ...testParams,
        ...params,
      };

      return mount(<NavItem {...props} />);
    };
  });

  it('should be nice', () => {
    const subject = makeSubject();
    const item = subject.props().item;

    expect(subject.find('a')).toHaveText(item.title);
  });
});
