/* eslint-disable import/first,func-names,global-require,prefer-arrow-callback,prefer-promise-reject-errors */

import React from 'react';
import { mount } from 'enzyme';
import NavDownloadItem from '../NavDownloadItem';
import mockData from '../navDownloadLinkConfig';

describe('<NavDownloadItem />', () => {
  let makeSubject;

  beforeEach(() => {
    jest.resetModules();

    window.open = jest.fn();

    const testParams = {
      title: mockData[0].title,
      url: mockData[0].url,
    };

    makeSubject = (params = {}) => {
      const props = {
        ...testParams,
        ...params,
      };

      return mount(<NavDownloadItem {...props} />);
    };
  });

  it('render text', () => {
    const subject = makeSubject();
    const title = subject.props().title;

    expect(subject.find('a')).toHaveText(title);
  });

  it('show open url when click the area', () => {
    const subject = makeSubject();
    const url = subject.props().url;

    subject.find('a').simulate('click');
    expect(window.open).toHaveBeenCalledWith(url);
  });
});
