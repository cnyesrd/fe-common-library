import React from 'react';
import PropTypes from 'prop-types';

import styles from './NavDownloadItem.scss';

const handleClick = (e, url) => {
  e.preventDefault();
  window.open(url);
};

const NavDownloadItem = ({ title, url }) => (
  <a className={styles['nav-board-link-full-width']} onClick={e => handleClick(e, url)}>
    {title}
  </a>
);

NavDownloadItem.propTypes = {
  title: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
};

export default NavDownloadItem;
