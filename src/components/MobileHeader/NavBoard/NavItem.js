import React from 'react';
import PropTypes from 'prop-types';
import gaDataset from '../../../utils/gaDataset';
import styles from './NavItem.scss';

function NavItem({ item, navTitle, onClick, dataPrefix }) {
  return (
    <a
      key={item.name}
      className={styles['nav-board-link']}
      href={item.url}
      data-ga-category="側選單"
      data-ga-action={navTitle}
      data-ga-label={item.title}
      {...gaDataset({ dataPrefix, category: 'Header', action: 'click', label: `${navTitle}_${item.title}` })}
      onClick={onClick}
    >
      {item.title}
    </a>
  );
}

NavItem.propTypes = {
  item: PropTypes.shape({
    name: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
  }).isRequired,
  navTitle: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  dataPrefix: PropTypes.arrayOf(PropTypes.string),
};

NavItem.defaultProps = {
  onClick: undefined,
  dataPrefix: ['data-proj-ga'],
};

export default NavItem;
