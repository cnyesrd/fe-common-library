export default [
  {
    name: 'mweb',
    title: '新聞',
    items: [
      {
        name: 'headline',
        title: '即時頭條',
        url: 'https://m.cnyes.com/news/cat/headline',
      },
      {
        name: 'news24h',
        title: '主編精選',
        url: 'https://m.cnyes.com/news/cat/news24h',
      },
      {
        name: 'tw_stock',
        title: '台股',
        url: 'https://m.cnyes.com/news/cat/tw_stock',
      },
      {
        name: 'wd_stock',
        title: '國際股',
        url: 'https://m.cnyes.com/news/cat/wd_stock',
      },
      {
        name: 'forex',
        title: '外匯',
        url: 'https://m.cnyes.com/news/cat/forex',
      },
      {
        name: 'future',
        title: '期貨',
        url: 'https://m.cnyes.com/news/cat/future',
      },
      {
        name: 'tw_money',
        title: '理財',
        url: 'https://m.cnyes.com/news/cat/tw_money',
      },
      {
        name: 'cn_stock',
        title: 'A股港股',
        url: 'https://m.cnyes.com/news/cat/cn_stock',
      },
      {
        name: 'cnyeshouse',
        title: '房產',
        url: 'https://m.cnyes.com/news/cat/cnyeshouse',
      },
      {
        name: 'celebrity_area',
        title: '鉅亨新視界',
        url: 'https://m.cnyes.com/news/cat/celebrity_area',
      },
      {
        name: 'popular',
        title: '人氣新聞',
        url: 'https://m.cnyes.com/news/cat/popular',
      },
      {
        name: 'topic',
        title: '專題報導',
        url: 'https://topics.cnyes.com',
      },
    ],
  },
  {
    name: 'mweb2',
    title: '影音',
    items: [
      {
        name: 'world',
        title: 'Allen看世界',
        url: 'https://m.cnyes.com/video/cat/world',
      },
      {
        name: 'invest',
        title: '理財芳程式',
        url: 'https://m.cnyes.com/video/cat/invest',
      },
      {
        name: 'fund',
        title: '鉅亨FUND大鏡',
        url: 'https://m.cnyes.com/video/cat/fund',
      },
      {
        name: 'hkstock',
        title: '港股大講堂',
        url: 'https://m.cnyes.com/video/cat/hkstock',
      },
      {
        name: 'videonews',
        title: '影音新聞',
        url: 'https://m.cnyes.com/video/cat/videonews',
      },
    ],
  },
  {
    name: 'forex',
    title: '外匯',
    items: [
      {
        name: 'USD',
        title: '美元',
        url: 'https://forex.cnyes.com/currency/USD/TWD',
      },
      {
        name: 'JPY',
        title: '日幣',
        url: 'https://forex.cnyes.com/currency/JPY/TWD',
      },
      {
        name: 'EUR',
        title: '歐元',
        url: 'https://forex.cnyes.com/currency/EUR/TWD',
      },
      {
        name: 'KRW',
        title: '韓幣',
        url: 'https://forex.cnyes.com/currency/KRW/TWD',
      },
      {
        name: 'CNY',
        title: '人民幣',
        url: 'https://forex.cnyes.com/currency/CNY/TWD',
      },
      {
        name: 'HKD',
        title: '港幣',
        url: 'https://forex.cnyes.com/currency/HKD/TWD',
      },
      {
        name: 'ZAR',
        title: '南非幣',
        url: 'https://forex.cnyes.com/currency/ZAR/TWD',
      },
      {
        name: 'AUD',
        title: '澳幣',
        url: 'https://forex.cnyes.com/currency/AUD/TWD',
      },
    ],
  },
  {
    name: 'crypto',
    title: '虛擬貨幣',
    items: [
      {
        name: 'BTC',
        title: '比特幣',
        url: 'https://crypto.cnyes.com/BTC/24h',
      },
      {
        name: 'ETH',
        title: '以太幣',
        url: 'https://crypto.cnyes.com/ETH/24h',
      },
      {
        name: 'XRP',
        title: 'Ripple',
        url: 'https://crypto.cnyes.com/XRP/24h',
      },
      {
        name: 'BCH',
        title: 'Bitcoin Cash',
        url: 'https://crypto.cnyes.com/BCH/24h',
      },
      {
        name: 'LTC',
        title: 'Litcoin',
        url: 'https://crypto.cnyes.com/LTC/24h',
      },
      {
        name: 'EOS',
        title: 'EOS',
        url: 'https://crypto.cnyes.com/EOS/24h',
      },
    ],
  },
  {
    name: 'market',
    title: '市場',
    items: [
      {
        name: 'market_taiwan',
        title: '台股指數',
        url: 'https://m.cnyes.com/market/#market_taiwan',
      },
      {
        name: 'market_global',
        title: '國際指數',
        url: 'https://m.cnyes.com/market/#market_global',
      },
      {
        name: 'market_currency',
        title: '國際外匯',
        url: 'https://m.cnyes.com/market/#market_currency',
      },
      {
        name: 'index_tse',
        title: '台股',
        url: 'https://m.cnyes.com/twstock/index_tse.aspx',
      },
      {
        name: 'mystock',
        title: '自選股',
        url: 'https://m.cnyes.com/person/mystock.aspx',
      },
      {
        name: 'news/search',
        title: '個股查詢',
        url: 'https://m.cnyes.com/news/search',
      },
      {
        name: 'fund',
        title: '基金',
        url: 'https://fund.cnyes.com',
      },
    ],
  },
  {
    name: 'channels',
    title: '頻道',
    items: [
      {
        name: 'blog',
        title: 'Blog',
        url: 'http://m.blog.cnyes.com',
      },
    ],
  },
];
