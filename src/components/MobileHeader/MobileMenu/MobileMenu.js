import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import styles from './MobileMenu.scss';
import getStyleName from '../../../utils/getStyleName';
import gaDataset from '../../../utils/gaDataset';

function MobileMenu({ showCatBoard, channelName, hideTopBar, dataPrefix }) {
  return (
    <div className={cx({ [getStyleName(styles, 'index-header-top-bar')]: !hideTopBar })}>
      <a
        href="https://m.cnyes.com/news"
        className={styles['index-header-logo']}
        {...gaDataset({ dataPrefix, category: 'Logo', action: 'click', label: 'home' })}
      >
        鉅亨
      </a>
      {channelName && channelName.length && <div className={styles['header-channel-label']}>{channelName}</div>}
      <nav>
        <span className={styles['index-header-menu']} onClick={showCatBoard} />
      </nav>
    </div>
  );
}

MobileMenu.propTypes = {
  showCatBoard: PropTypes.func.isRequired,
  channelName: PropTypes.string,
  hideTopBar: PropTypes.bool,
  dataPrefix: PropTypes.arrayOf(PropTypes.string),
};

MobileMenu.defaultProps = {
  channelName: undefined,
  hideTopBar: false,
  dataPrefix: ['data-proj-ga'],
};

export default MobileMenu;
