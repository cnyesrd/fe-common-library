## Usage

```
import { MobileMenu, MobileNavBoard } from 'fe-common-library/dest/components/MobileHeader'
import 'fe-common-library/dest/components/MobileHeader/style.css';

...

render() {
  return (
    <div>
      <header className="customize-your-header-styles">
        <MobileMenu
          showCatBoard={() => {
            this._navBoard.openNavBoard();
          }}
        />
      </header>
      <MobileNavBoard
        ref={navBoard => { this._navBoard = navBoard; }}
      />
    </div>
  );
}
```