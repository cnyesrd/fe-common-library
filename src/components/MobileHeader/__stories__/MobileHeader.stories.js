import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { withNotes } from '@storybook/addon-notes';
import * as knobs from '@storybook/addon-knobs/react';
import { withState } from '@dump247/storybook-state';
import { MobileMenu, MobileNavBoard } from '../index';
import styles from './Container.scss';
import note from './MobileHeader.md';
import navDownloadLinkConfig from '../NavBoard/navDownloadLinkConfig';

const stories = storiesOf('MobileHeader', module);

stories.addDecorator(knobs.withKnobs);

stories.add(
  'default',
  withState({ isCatBoardOpen: false })(
    withNotes(note)(
      withInfo()(({ store }) => {
        return (
          <div>
            <header className={styles.header}>
              <MobileMenu
                channelName="外匯"
                showCatBoard={() => {
                  store.set({ isCatBoardOpen: true });
                }}
              />
            </header>
            <MobileNavBoard
              isCatBoardOpen={store.state.isCatBoardOpen}
              hideCatBoard={() => {
                store.set({ isCatBoardOpen: false });
              }}
            />
          </div>
        );
      })
    )
  )
);

stories.add(
  'With DownloadLinks ',
  withState({ isCatBoardOpen: false })(
    withNotes(note)(
      withInfo()(({ store }) => {
        return (
          <div>
            <header className={styles.header}>
              <MobileMenu
                channelName="外匯"
                showCatBoard={() => {
                  store.set({ isCatBoardOpen: true });
                }}
              />
            </header>
            <MobileNavBoard
              isCatBoardOpen={store.state.isCatBoardOpen}
              hideCatBoard={() => {
                store.set({ isCatBoardOpen: false });
              }}
              downloadLinks={navDownloadLinkConfig}
            />
          </div>
        );
      })
    )
  )
);
