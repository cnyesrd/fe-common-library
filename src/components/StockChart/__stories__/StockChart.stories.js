/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { withNotes } from '@storybook/addon-notes';
import * as knobs from '@storybook/addon-knobs/react';
import StockChart from '../StockChart';
import mockData from './ChartData.json';
import defaultNotes from './StockChart.md';

const stories = storiesOf('StockChart', module);

stories.addDecorator(knobs.withKnobs);

stories.add(
  'Default',
  withNotes(defaultNotes)(
    withInfo()(() => {
      const periods = ['24h', '3m', '1y', '3y'];

      return (
        <div>
          {periods.map(period => (
            <div key={period}>
              <h3>{period}</h3>
              <StockChart history={mockData[period]} period={period} />
            </div>
          ))}
        </div>
      );
    })
  )
);

stories.add(
  'lastClosePrice',
  withNotes(defaultNotes)(
    withInfo()(() => {
      return <StockChart history={mockData['24h']} period="24h" lastClosePrice={6400} />;
    })
  )
);

stories.add(
  'isLoading',
  withNotes(defaultNotes)(
    withInfo()(() => {
      return <StockChart history={mockData['24h']} period="24h" isLoading />;
    })
  )
);

stories.add(
  'disableAnimation',
  withNotes(defaultNotes)(
    withInfo()(() => {
      return <StockChart history={mockData['24h']} period="24h" disableAnimation />;
    })
  )
);

stories.add(
  'customStyle',
  withNotes(defaultNotes)(
    withInfo()(() => {
      return (
        <StockChart
          history={mockData['3y']}
          period="3y"
          customStyle={{
            chartHeight: 300,
            chartWidth: 600,
            lineColor: 'rgba(13, 204, 74, 0.8)',
            closeLineColor: 'goldenrod',
            fillColor: 'rgba(13, 204, 74, 0.3)',
            lineWidth: 5,
            lastCloseLineWidth: 8,
          }}
          lastClosePrice={6400}
        />
      );
    })
  )
);

stories.add(
  'hide axis',
  withNotes(defaultNotes)(
    withInfo()(() => {
      return (
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            width: '200px',
            height: '120px',
            fontSize: '14px',
          }}
        >
          <div
            style={{
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              marginBottom: '5px',
            }}
          >
            <div style={{ padding: '3px' }}>香港恆生</div>
            <div style={{ padding: '3px', color: 'rgba(13, 204, 74, 0.8)', backgroundColor: 'rgba(13, 204, 74, 0.1)' }}>
              +89.00
            </div>
          </div>
          <div
            style={{
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}
          >
            <div style={{ padding: '3px' }}>30,825.00</div>
            <div style={{ padding: '3px', color: 'rgba(13, 204, 74, 0.8)', backgroundColor: 'rgba(13, 204, 74, 0.1)' }}>
              +0.95%
            </div>
          </div>
          <StockChart
            history={mockData['3y']}
            period="3y"
            lastClosePrice={6400}
            customStyle={{
              chartHeight: 80,
              chartWidth: 200,
              lineColor: 'rgba(13, 204, 74, 0.8)',
              fillColor: 'rgba(13, 204, 74, 0.1)',
              lineWidth: 3,
            }}
            hidexAxis
            hideyAxis
          />
        </div>
      );
    })
  )
);
