## Usage

```
import StockChart from 'fe-common-library/dest/components/StockChart';

// ...

render() {
  return (
    <StockChart
      history={data}
      period="24h"
      isLoading={isLoading}
      disableAnimation={disableAnimation}
      customStyle={{
        chartHeight,
        chartWidth,
        lineColor,
        closeLineColor,
        fillColor,
        lineWidth,
        lastCloseLineWidth,
      }}
      hidexAxis
      hideyAxis
    />
  );
}
```