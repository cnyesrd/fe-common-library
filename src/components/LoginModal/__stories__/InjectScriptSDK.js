import { Component } from 'react';

class InjectScriptSDK extends Component {
  componentDidMount() {
    const fbScript = document.createElement('script');

    fbScript.src = 'https://connect.facebook.net/zh_TW/sdk.js?c=1#xfbml=1&version=v2.8';
    document.head.appendChild(fbScript);
    this.fbScript = fbScript;
    const googleScript = document.createElement('script');

    googleScript.src = 'https://apis.google.com/js/auth2.js?onload=gAsyncInit';

    document.head.appendChild(googleScript);
    this.googleScript = googleScript;
  }

  componentWillUnMount() {
    document.removeChild(this.fbScript);
    document.removeChild(this.googleScript);
  }

  render() {
    return null;
  }
}

export default InjectScriptSDK;
