import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import LoginButton from './LoginButton';

const isLoginSelector = state => {
  return (state.me && state.me.auth && state.me.auth.status === 'connected') || false;
};
const userProfileSelector = state => (state.me && state.me.profile) || null;
const authIsFetchingSelector = state => state.me && state.me.auth && state.me.auth.isFetching;

const LoginButtonContainer = connect(() => {
  const selector = createStructuredSelector({
    isLogin: isLoginSelector,
    userProfile: userProfileSelector,
    authIsFetching: authIsFetchingSelector,
  });

  return state => selector(state);
})(LoginButton);

export default LoginButtonContainer;
