/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { withNotes } from '@storybook/addon-notes';
import { withState } from '@dump247/storybook-state';
import * as knobs from '@storybook/addon-knobs/react';
import loginModalNotes from './loginModalNotes.md';
import InjectScriptSDK from './InjectScriptSDK';
import styles from './LoginModal.stories.scss';

import LoginModal from '../LoginModal';
import CnyesProvider from '../../CnyesProvider/CnyesProvider';
import LoginButtonContainer from './LoginButtonContainer';
import { auth } from '../../../utils/cnyesAuth';
import apiClientFactory from '../../../utils/apiClientFactory';
import me from '../../../utils/cnyesAuth/reducer';
import authConfig from './authConfig';

auth.init(authConfig);

const EMPTY_FUNC = function() {};

const client = apiClientFactory(
  {},
  {
    getToken: auth.getToken,
    refreshToken: auth.refreshToken,
    appName: 'fe-common-library',
    projectName: 'storybook',
  }
);

const stories = storiesOf('cnyesAuth', module);

stories.addDecorator(knobs.withKnobs);

stories.add(
  'LoginModal',
  withInfo()(
    withNotes(loginModalNotes)(() => {
      return [
        <InjectScriptSDK key="InjectScriptSDK" />,
        <CnyesProvider key="CnyesProvider" auth={auth} request={client}>
          <LoginModal visible={knobs.boolean('visible', true)} setVisible={EMPTY_FUNC} callLoadingModal={EMPTY_FUNC} />
        </CnyesProvider>,
      ];
    })
  )
);

// LoginModalWithState =============================================>

stories.add(
  'LoginModalWithState',
  withNotes(loginModalNotes)(
    withState({ visible: false })(
      withInfo()(({ store }) => {
        return [
          <InjectScriptSDK key="InjectScriptSDK" />,
          <CnyesProvider key="CnyesProvider" auth={auth} request={client}>
            <div>
              <button className={styles['login-button']} onClick={() => store.set({ visible: true })}>
                登入
              </button>
              <LoginModal
                visible={store.state.visible}
                setVisible={() => store.set({ visible: !store.state.visible })}
                callLoadingModal={EMPTY_FUNC}
              />
            </div>
          </CnyesProvider>,
        ];
      })
    )
  )
);

// LoginModalWithRedux =============================================>

const reduxStore = createStore(
  combineReducers({
    me,
  }),
  {}
);

auth.syncAuthWithStore(reduxStore);

stories.add(
  'LoginModalWithRedux',
  withNotes(loginModalNotes)(
    withState({ visible: false })(
      withInfo()(({ store }) => {
        return [
          <InjectScriptSDK key="InjectScriptSDK" />,
          <Provider key="Provider" store={reduxStore}>
            <CnyesProvider auth={auth} request={client}>
              <div>
                <LoginButtonContainer onClick={() => store.set({ visible: true })} />
                <LoginModal
                  visible={store.state.visible}
                  setVisible={() => store.set({ visible: !store.state.visible })}
                  callLoadingModal={EMPTY_FUNC}
                />
              </div>
            </CnyesProvider>
          </Provider>,
        ];
      })
    )
  )
);
