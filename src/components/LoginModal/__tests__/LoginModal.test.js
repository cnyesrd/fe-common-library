/* eslint-disable import/first,func-names,global-require,prefer-arrow-callback */

import React from 'react';
import { mount } from 'enzyme';
import { authType } from '../../../utils/propTypes';

describe('LoginModal', function() {
  let LoginModal;
  let auth;

  beforeEach(() => {
    jest.resetModules();
    LoginModal = require('../LoginModal').default;
    jest.mock('../../../utils/cnyesAuth/auth');
    auth = require('../../../utils/cnyesAuth/auth').default;
    auth.loginFB.mockImplementation(() => Promise.resolve());

    this.params = {
      visible: true,
      setVisible: jest.fn(),
      callLoadingModal: jest.fn(),
    };

    this.makeSubject = () => {
      const { visible, setVisible, callLoadingModal } = this.params;

      return mount(<LoginModal visible={visible} setVisible={setVisible} callLoadingModal={callLoadingModal} />, {
        context: {
          auth,
        },
        childContextType: {
          auth: authType,
        },
      });
    };
  });

  it('should be same as snapshot', () => {
    const subject = this.makeSubject();
    const tree = subject.html();

    expect(tree).toMatchSnapshot();
  });

  it('should call setVisible function with `false` after calling the `closeHandler` function', () => {
    const subject = this.makeSubject();

    subject.instance().closeHandler();

    expect(this.params.setVisible).toBeCalledWith(false);
  });

  it('should call setVisible and callLoadingModal functions after calling the `showAgreementModal` function', () => {
    const subject = this.makeSubject();

    subject.instance().loginFB();

    expect(this.params.setVisible).toBeCalledWith(false);
    expect(this.params.callLoadingModal).toBeCalledWith(true);
  });
});
