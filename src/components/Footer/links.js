const footerLinks = {
  mobileNavs: [
    {
      title: '電腦版',
      name: 'pc',
      url: '/',
    },
    {
      title: '著作權',
      name: 'copyright',
      url: '/copyright',
    },
    {
      title: '意見與回饋',
      name: 'feedback',
      onClick: e => {
        e.preventDefault();

        if (typeof window !== 'undefined') {
          const formURI =
            'https://docs.google.com/forms/d/e/1FAIpQLSepnhnP4FgnRXiqezmVme7YX7xqXhxPsd57qbaJ09yJbxTt0g/viewform' +
            `?usp=pp_url&entry.364853985=${document.location.href}`;

          window.open(formURI, '_blank');
        }
      },
    },
  ],
  desktopNavs: [
    {
      title: '關於我們',
      name: 'about',
      url: 'http://www.cnyes.com/cnyes_about/cnyes_about.html',
    },
    {
      title: '集團簡介',
      name: 'anueCorp',
      url: 'https://www.anuegroup.com.tw/',
    },
    {
      title: '廣告服務',
      name: 'ad',
      url: 'http://www.cnyes.com/cnyes_about/cnyes_AD01.html',
    },
    {
      title: '金融資訊元件',
      name: 'financial',
      url: 'http://www.cnyes.com/cnyes_about/cnyes_pas01.html',
    },
    {
      title: '聯絡我們',
      name: 'contact',
      url: 'http://www.cnyes.com/cnyes_about/cnyes_ctcUsTpe.html',
    },
    {
      title: '徵才',
      name: 'job',
      url: 'http://www.104.com.tw/jobbank/custjob/index.php?r=cust&j=5e6042253446402330683b1d1d1d1d5f2443a363189j01',
    },
    {
      title: '網站地圖',
      name: 'sitemap',
      url: 'http://www.cnyes.com/cnyes_about/site_map.html',
    },
    {
      title: '法律聲明',
      name: 'legal',
      url: 'http://www.cnyes.com/cnyes_about/cnyes_sos01.html',
    },
  ],
  socials: [
    {
      title: '粉 絲 團',
      name: 'facebook',
      url: 'https://www.facebook.com/anuetw/',
    },
    {
      title: '鉅亨Line',
      name: 'line',
      url: 'https://line.me/ti/p/@ZLU0489G',
    },
    {
      title: '鉅亨APP',
      name: 'app',
      url: 'http://www.cnyes.com/app?utm_source=cnyes&utm_medium=desktop&utm_campaign=desktop_footer',
    },
  ],
};

export default footerLinks;
