/* eslint-disable import/first,func-names,global-require,prefer-arrow-callback */
import React from 'react';
import { mount, shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Footer from '../Footer';

describe('Footer component test', () => {
  let Link;
  let now;

  beforeEach(() => {
    jest.resetModules();

    Link = jest.fn(() => <div className="mock-link" />);
    now = +new Date(2016, 9, 1);
  });

  it('should be same as snapshot', () => {
    const wrapper = mount(<Footer now={now} />);
    const tree = toJson(wrapper);

    expect(tree).toMatchSnapshot();
  });

  it('should render logo link correctly', () => {
    const wrapperWithLink = mount(<Footer now={now} Link={Link} />);

    expect(wrapperWithLink.find('.logo a')).not.toExist();

    const wrapperWithoutLink = mount(<Footer now={now} />);

    expect(wrapperWithoutLink.find('.logo a')).toExist();
  });

  it('should render mobile nav correctly', () => {
    const wrapper = mount(<Footer now={now} Link={Link} />);

    expect(wrapper.find('.mobile-nav a').length).toEqual(wrapper.props().mobileNavs.length);
  });

  it('should render mobile nav correctly', () => {
    const wrapper = mount(<Footer now={now} Link={Link} />);

    expect(wrapper.find('.desktop-nav a').length).toEqual(wrapper.props().desktopNavs.length);
  });

  it('should has a <footer> tag which has class named `main-footer`', () => {
    const wrapper = shallow(<Footer now={now} />);

    expect(
      wrapper
        .find('footer')
        .first()
        .hasClass('main-footer')
    ).toBeTruthy();
  });
});
