/* eslint import/no-extraneous-dependencies: ["error", {"peerDependencies": true}] */
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import getStyleName from '../../utils/getStyleName';
import styles from './Footer.scss';
import footerLinks from './links';
import { footerNavItem } from '../../utils/propTypes';
import gaDataset from '../../utils/gaDataset';

const ANUE_INDEX = 'https://www.cnyes.com';

class Footer extends PureComponent {
  static propTypes = {
    mobileNavs: PropTypes.arrayOf(footerNavItem),
    desktopNavs: PropTypes.arrayOf(footerNavItem),
    socials: PropTypes.arrayOf(footerNavItem),
    now: PropTypes.number.isRequired,
    Link: PropTypes.func,
    dataPrefix: PropTypes.arrayOf(PropTypes.string),
  };

  static defaultProps = {
    mobileNavs: footerLinks.mobileNavs,
    desktopNavs: footerLinks.desktopNavs,
    socials: footerLinks.socials,
    Link: undefined,
    dataPrefix: ['data-proj-ga'],
  };

  static renderNavs(navs, dataPrefix) {
    return navs.map(item => {
      const { url, name, title, onClick } = item;

      return (
        <a
          href={url}
          onClick={onClick}
          key={`footer-nav-${name}`}
          target="_blank"
          rel="noopener noreferrer"
          {...gaDataset({ dataPrefix, category: 'Footer', action: 'click', label: title })}
        >
          {title}
        </a>
      );
    });
  }

  static renderSocials(socials, dataPrefix) {
    return socials.map(item => (
      <div className={getStyleName(styles, 'social-item')} key={`footer-socials-${item.name}`}>
        <div className={getStyleName(styles, 'title')}>{item.title}</div>
        <a
          className={`${getStyleName(styles, 'social-icon')} ${getStyleName(styles, `cnyes-media-${item.name}`)}`}
          href={item.url}
          rel="noopener noreferrer"
          target="_blank"
          {...gaDataset({ dataPrefix, category: 'Footer', action: 'click', label: item.title })}
        >
          {item.title}
        </a>
      </div>
    ));
  }

  render() {
    const { mobileNavs, desktopNavs, socials, now, Link, dataPrefix } = this.props;
    const thisYear = new Date(now).getFullYear();
    const logoClass = cx(getStyleName(styles, 'logo'), getStyleName(styles, 'hidden-mobile'));
    const socialsClass = cx(getStyleName(styles, 'socials'), getStyleName(styles, 'hidden-mobile'));

    /* eslint-disable jsx-a11y/accessible-emoji */
    return (
      <div id={getStyleName(styles, 'cnyes-footer-wrapper')} className={cx('theme-footer-wrapper')}>
        <footer className={getStyleName(styles, 'main-footer')}>
          <div className={logoClass}>
            {Link ? (
              <Link to="/" {...gaDataset({ dataPrefix, category: 'Footer', action: 'click', label: 'home' })} />
            ) : (
              <a href={ANUE_INDEX} {...gaDataset({ dataPrefix, category: 'Footer', action: 'click', label: 'home' })} />
            )}
          </div>
          <div className={getStyleName(styles, 'nav')}>
            <nav className={cx(getStyleName(styles, 'mobile-nav'), getStyleName(styles, 'hidden-desktop'))}>
              {this.constructor.renderNavs(mobileNavs, dataPrefix)}
            </nav>
            <nav className={cx(getStyleName(styles, 'desktop-nav'), getStyleName(styles, 'hidden-mobile'))}>
              {this.constructor.renderNavs(desktopNavs, dataPrefix)}
            </nav>
            <small className={getStyleName(styles, 'copyright-anue')}>
              <span>©</span>
              <span className={getStyleName(styles, 'hidden-mobile')}> Copyright</span>
              <span> 2000-{thisYear} Anue.com All rights reserved.</span>
              <span className={getStyleName(styles, 'hidden-mobile')}> 未經授權 不得轉載</span>
            </small>
          </div>
          <div className={socialsClass}>{this.constructor.renderSocials(socials, dataPrefix)}</div>
        </footer>
      </div>
    );
  }
}

export default Footer;
