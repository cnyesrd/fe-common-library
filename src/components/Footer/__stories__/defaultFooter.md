## Usage

```
import Footer from 'fe-common-library/dest/components/Footer';
import 'fe-common-library/dest/components/Footer/style.css';

// ...

render() {
  return (
    <Footer now={Date.now()} />;
  );
}

```