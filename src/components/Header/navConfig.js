export default [
  {
    title: '首頁',
    url: 'https://www.cnyes.com/',
  },
  {
    title: '新聞',
    url: 'http://news.cnyes.com/',
    leftList: [
      { title: '頭條', url: 'https://news.cnyes.com/news/cat/headline' },
      { title: '人氣', url: 'https://news.cnyes.com/trending' },
      { title: '台股', url: 'https://news.cnyes.com/news/cat/tw_stock' },
      { title: '國際股', url: 'https://news.cnyes.com/news/cat/wd_stock' },
      { title: 'Ａ股港股', url: 'https://news.cnyes.com/news/cat/cn_stock' },
      { title: '外匯', url: 'https://news.cnyes.com/news/cat/forex' },
      { title: '期貨', url: 'https://news.cnyes.com/news/cat/future' },
    ],
  },
  {
    title: '台股',
    url: 'http://www.cnyes.com/twstock/index.htm',
    leftList: [
      { title: '台指期', url: 'http://www.cnyes.com/twfutures/index.htm' },
      { title: '興櫃', url: 'http://www.cnyes.com/presh/index.htm' },
      { title: '未上市', url: 'http://www.cnyes.com/pre/index.htm' },
    ],
  },
  {
    title: '股市Talk',
    url: 'https://stock.cnyes.com/',
  },
  {
    title: '外匯',
    url: 'https://www.cnyes.com/forex',
    leftList: [
      { title: '新聞', url: 'https://news.cnyes.com/news/cat/forex' },
      { title: '路透即時外匯', url: 'https://www.cnyes.com/forex/reuters' },
      { title: '交叉匯率', url: 'https://www.cnyes.com/forex/crosslist' },
      { title: '新台幣', url: 'https://www.cnyes.com/forex/twd' },
      { title: '人民幣', url: 'https://www.cnyes.com/forex/rmb' },
      { title: '日元', url: 'https://www.cnyes.com/forex/jpy' },
      { title: '歐元', url: 'https://www.cnyes.com/forex/eur' },
      { title: '研報', url: 'https://www.cnyes.com/fx_report/report.aspx?ga=nav' },
    ],
  },
  {
    title: '全球市場',
    url: 'http://www.cnyes.com/global/IndexImmediateQuotedPrice/',
    leftList: [
      { title: '國際股', url: 'http://www.cnyes.com/global/IndexImmediateQuotedPrice/' },
      { title: '美股', url: 'http://www.cnyes.com/USASTOCK/index.htm' },
      { title: '港股', url: 'http://www.cnyes.com/hkstock/' },
      { title: '滬深股', url: 'http://www.cnyes.com/shstock/index.htm' },
      { title: '日股', url: 'http://www.cnyes.com/JP/index.htm' },
      { title: '國際期貨', url: 'http://www.cnyes.com/futures/index.htm' },
      { title: '債券', url: 'http://www.cnyes.com/bond/index.htm' },
      { title: '黃金', url: 'http://www.cnyes.com/gold/index.htm' },
      { title: '全球央行', url: 'http://www.cnyes.com/CentralBank/index.htm' },
      { title: '經濟指標', url: 'http://www.cnyes.com/economy/indicator/Page/schedule.aspx' },
      { title: 'StockQ', url: 'http://money.cnyes.com/StockQ.aspx' },
    ],
  },
  {
    title: '基金',
    url: 'http://fund.cnyes.com/index.htm',
    leftList: [
      { title: '我的基金', url: 'http://fund.cnyes.com/MyFunds.aspx' },
      { title: '基金搜尋', url: 'http://fund.cnyes.com/Search.aspx' },
      { title: '基金排行', url: 'http://fund.cnyes.com/ranking/index.htm' },
      { title: '債券專區', url: 'http://fund.cnyes.com/Fixedincome/' },
      { title: '配息專區', url: 'http://fund.cnyes.com/dividend/index.htm' },
      { title: '研究報告', url: 'http://fund.cnyes.com/report/report/index.htm' },
      { title: '基金交易平台', url: 'https://www.anuefund.com/?utm_source=cnyes&utm_medium=index_menu' },
      { title: '趨勢寶', url: 'https://www.anuefund.com/EC/VFUND/?utm_source=cnyes&utm_medium=index_menu' },
      { title: '阿發總管', url: 'https://www.anuefund.com/EC/ROBO/?utm_source=cnyes&utm_medium=index_menu' },
    ],
  },
  {
    title: '交易',
    url: '',
    leftList: [
      { title: '基金交易', url: 'https://www.anuefund.com/Index.aspx?utm_source=cnyes&utm_medium=channel_mainpage' },
    ],
    rightListTitle: '鉅亨基金交易平台',
    rightList: [
      { title: '交易登入', url: 'https://www.anuefund.com/Login.aspx?utm_source=cnyes&utm_medium=channel_login' },
      {
        title: '免費開戶',
        url: 'https://lihi.cc/O99if',
      },
      {
        title: '最新優惠',
        url: 'https://www.anuefund.com/Guide/Expenses.aspx?utm_source=cnyes&utm_medium=channel_preferential',
      },
      {
        title: '投資雷達',
        url: 'https://www.anuefund.com/Tool/SuggestionList.aspx?utm_source=cnyes&utm_medium=channel_radar',
      },
      {
        title: '趨勢寶',
        url: 'https://www.anuefund.com/EC/VFUND/?utm_source=cnyes&utm_medium=channel_login',
      },
      {
        title: '阿發總管',
        url: 'https://www.anuefund.com/EC/ROBO/?utm_source=cnyes&utm_medium=channel_login',
      },
    ],
  },
  {
    title: '理財',
    url: 'http://www.cnyes.com/money/index.htm',
    leftList: [
      { title: '銀行服務', url: 'http://www.cnyes.com/money/BankService.aspx' },
      { title: '試算工具', url: 'http://www.cnyes.com/money/BankCalculation.aspx' },
      { title: '理財新聞', url: 'https://news.cnyes.com/news/cat/tw_money' },
      { title: '固定收益', url: 'http://www.cnyes.com/fixedincome/index.htm' },
    ],
  },
  {
    title: '部落格',
    url: 'http://blog.cnyes.com/',
    leftList: [
      { title: '名家專區', url: 'http://blog.cnyes.com/Excerpt.aspx' },
      { title: '人氣排行榜', url: 'http://blog.cnyes.com/SearchBlog.aspx?Purpose=Hit&ga=nav' },
      { title: '最新文章', url: 'http://blog.cnyes.com/WorldFresh.aspx' },
    ],
  },
  {
    title: '投資老司機',
    url: 'https://invest.anue.com/campaigns/passengers/landing?utm_source=cnyes&utm_medium=header&utm_campaign=news',
    isNew: true,
  },
  {
    title: '影音',
    url: 'https://www.cnyes.com/video/cat/all?utm_source=cnyes&utm_medium=header&utm_campaign=news',
    isNew: true,
  },
];
