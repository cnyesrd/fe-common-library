/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { withNotes } from '@storybook/addon-notes';
import * as knobs from '@storybook/addon-knobs/react';
import Link from './mockLink';
import Header from '../Header';
// import '../StaticHeader.scss';
import defaultNotes from './Header.md';
import * as fixedHeaderTypes from '../ConstantUI';
import * as simpleConfig from './config/simpleConfig';
import * as newsConfig from './config/newsConfig';
import AccountMenu from '../../AccountMenu/AccountMenu';

const stories = storiesOf('Header', module);

// stories.addDecorator((story, context) => withInfo()(story)(context));
stories.addDecorator(knobs.withKnobs);
stories.addDecorator(story => (
  <div>
    {story()}
    <div style={{ width: '100%', height: '1500px', padding: '80px 0' }}>long content here...</div>
  </div>
));

stories.add(
  'default config',
  withNotes(defaultNotes)(
    withInfo()(() => {
      const channelOptions = simpleConfig.navs.map(o => o.title);

      return (
        <Header
          channel={knobs.select('channel', channelOptions, channelOptions[0])}
          displayChannelName={knobs.boolean('displayChannelName', true)}
          location={window.location}
        />
      );
    })
  )
);

stories.add(
  'change channel state',
  withNotes(defaultNotes)(
    withInfo()(() => {
      const channelOptions = simpleConfig.navs.map(o => o.title);

      return (
        <Header
          channel={knobs.select('channel', channelOptions, channelOptions[0])}
          displayChannelName={knobs.boolean('displayChannelName', true)}
          navs={simpleConfig.navs}
          location={window.location}
        />
      );
    })
  )
);

stories.add(
  'fixed full header',
  withNotes(defaultNotes)(
    withInfo()(() => {
      return (
        <Header
          channel="新聞"
          navs={newsConfig.navs}
          catNavs={newsConfig.catNavs}
          location={window.location}
          fixedHeaderType={fixedHeaderTypes.FIXED_HEADER_FULL}
        />
      );
    })
  )
);

stories.add(
  'fixed sub header',
  withNotes(defaultNotes)(
    withInfo()(() => {
      return (
        <Header
          channel="新聞"
          navs={newsConfig.navs}
          catNavs={newsConfig.catNavs}
          location={window.location}
          fixedHeaderType={fixedHeaderTypes.FIXED_HEADER_SUB}
        />
      );
    })
  )
);

stories.add(
  'customize Link',
  withNotes(defaultNotes)(
    withInfo()(() => {
      const useMockLink = knobs.boolean('use customized Link with action logger?', true);

      return (
        <Header
          channel="新聞"
          Link={useMockLink ? Link : undefined}
          navs={newsConfig.navs}
          catNavs={newsConfig.catNavs}
          location={window.location}
          fixedHeaderType={fixedHeaderTypes.FIXED_HEADER_FULL}
        />
      );
    })
  )
);

stories.add(
  'with dataPrefix',
  withNotes(defaultNotes)(
    withInfo()(() => {
      return (
        <Header
          channel="新聞"
          navs={newsConfig.navs}
          catNavs={newsConfig.catNavs}
          location={window.location}
          fixedHeaderType={fixedHeaderTypes.FIXED_HEADER_FULL}
          dataPrefix={['data-proj-ga']}
        />
      );
    })
  )
);

stories.add(
  'with account menu',
  withNotes(defaultNotes)(
    withInfo()(() => {
      return (
        <Header
          channel="新聞"
          navs={newsConfig.navs}
          catNavs={newsConfig.catNavs}
          location={window.location}
          fixedHeaderType={fixedHeaderTypes.FIXED_HEADER_FULL}
          customMenu={<AccountMenu />}
        />
      );
    })
  )
);
