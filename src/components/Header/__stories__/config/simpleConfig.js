export const navs = [
  {
    title: '首頁',
    url: 'https://www.cnyes.com',
  },
  {
    title: '新聞',
    url: 'https://news.cnyes.com/',
  },
  {
    title: '基金',
    url: 'https://fund.cnyes.com/',
  },
  {
    title: '投資老司機',
    isNew: true,
    url: 'https://invest.anue.com/campaigns/passengers/landing',
  },
];

export const catNavs = null;
