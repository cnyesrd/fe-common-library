import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { action } from '@storybook/addon-actions';

export default class Link extends Component {
  static propTypes = {
    className: PropTypes.string.isRequired,
    to: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    children: PropTypes.node.isRequired,
  };

  handleClick = e => {
    const { to, children } = this.props;

    if (e) {
      e.preventDefault();
    }

    action('click Link')(children, to);
  };

  render() {
    const { to, title, children, ...rest } = this.props;

    return (
      <a href={to} {...rest} onClick={this.handleClick}>
        {children}
      </a>
    );
  }
}
