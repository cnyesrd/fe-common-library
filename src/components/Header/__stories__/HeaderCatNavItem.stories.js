/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { withNotes } from '@storybook/addon-notes';
import * as knobs from '@storybook/addon-knobs/react';
import Link from './mockLink';
import Header from '../Header';
// import '../StaticHeader.scss';
import defaultNotes from './Header.md';
import * as fixedHeaderTypes from '../ConstantUI';
import { navs, catNavs } from './config/newsConfig';

const stories = storiesOf('Header/CatNav Item', module);

stories.addDecorator((story, context) => withInfo()(story)(context));
stories.addDecorator(knobs.withKnobs);
stories.addDecorator(story => (
  <div>
    {story()}
    <div style={{ width: '100%', height: '1500px', padding: '80px 0' }}>long content here...</div>
  </div>
));

stories.add(
  'default',
  withNotes(defaultNotes)(() => {
    return (
      <Header
        channel="新聞"
        navs={navs}
        catNavs={knobs.object('catNavs', catNavs)}
        location={window.location}
        fixedHeaderType={fixedHeaderTypes.FIXED_HEADER_FULL}
      />
    );
  })
);

stories.add(
  'config a catNav item',
  withNotes(defaultNotes)(() => {
    const useMockLink = knobs.boolean('use customized Link with action logger?', true);
    const customizedItem = {
      name: knobs.text('name', 'searchCrypto'),
      url: knobs.text('url', 'https://news.cnyes.com/search?group=1&q=虛擬貨幣'),
      title: knobs.text('title', '虛擬貨幣'),
      isNew: knobs.boolean('isNew', true),
      external: knobs.boolean('external', false),
    };
    const displayCatNavs = [catNavs[0], customizedItem];

    return (
      <Header
        channel="新聞"
        Link={useMockLink ? Link : undefined}
        navs={navs}
        catNavs={displayCatNavs}
        location={window.location}
        fixedHeaderType={fixedHeaderTypes.FIXED_HEADER_FULL}
      />
    );
  })
);
