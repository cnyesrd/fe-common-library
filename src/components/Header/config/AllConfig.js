import * as FundConfig from './catNavs/fund.json';
import * as TwstockConfig from './catNavs/twstock.json';
import * as ForexConfig from './catNavs/forex.json';
import * as GlobalConfig from './catNavs/global.json';
import * as MoneyConfig from './catNavs/money.json';
import * as AboutConfig from './catNavs/about.json';

const config = {
  index: {
    name: 'noActiveChannel', // for static main header without active channel
    displayChannelName: false,
  },
  fund: {
    name: '基金',
    catNavs: FundConfig.catNavs,
  },
  twstock: {
    name: '台股',
    catNavs: TwstockConfig.catNavs,
  },
  forex: {
    name: '外匯',
    catNavs: ForexConfig.catNavs,
  },
  global: {
    name: '全球市場',
    catNavs: GlobalConfig.catNavs,
  },
  money: {
    name: '理財',
    catNavs: MoneyConfig.catNavs,
  },
  about: {
    name: '關於我們',
    displayChannelName: false,
    catNavs: AboutConfig.catNavs,
  },
};

export default config;
