/* eslint import/no-extraneous-dependencies: ["error", {"peerDependencies": true}] */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import getStyleName from '../../utils/getStyleName';
import gaDataset from '../../utils/gaDataset';
import getDataObj from '../../utils/getDataObj';
import styles from './Header.scss';

const itemType = PropTypes.shape({
  title: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
});

const style = {
  narrowedPopup: {
    width: '120px',
    height: 'auto',
    overflow: 'visible',
  },
  narrowedList: {
    height: 'auto',
  },
};

const renderStaticList = (list, className, wrapperStyle, listTitle, dataPrefix) => {
  const content = list.map((link, id) => (
    <a
      key={id} // eslint-disable-line react/no-array-index-key
      href={link.url}
      className={link.icon ? getStyleName(styles, `icon-${link.icon}`) : null}
      title={link.title}
      {...gaDataset({ dataPrefix, category: 'Nav', action: 'click', label: link.title })}
      {...getDataObj(link)}
    >
      {link.title}
    </a>
  ));

  return (
    <nav className={className} style={wrapperStyle || {}}>
      {listTitle && <h5>{listTitle}</h5>}
      {content}
    </nav>
  );
};

export default class SubMenu extends Component {
  static propTypes = {
    // ownProps
    /* tab label */
    title: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
    className: PropTypes.string,
    dataPrefix: PropTypes.arrayOf(PropTypes.string),
    dataObj: PropTypes.shape({}),
    /* Menu */
    leftList: PropTypes.arrayOf(itemType),
    leftListTitle: PropTypes.string,
    rightList: PropTypes.arrayOf(itemType),
    rightListTitle: PropTypes.string,
    isNew: PropTypes.bool,
  };

  static defaultProps = {
    className: '',
    dataPrefix: ['data-proj-ga'],
    dataObj: {},
    leftList: undefined,
    leftListTitle: undefined,
    rightList: undefined,
    rightListTitle: undefined,
    isNew: false,
  };

  renderPopupMenu() {
    const { leftList, leftListTitle, rightList, rightListTitle, dataPrefix } = this.props;

    if (!leftList && !rightList) {
      // no left and no right, => no popup menu
      return null;
    }

    return (
      <div
        className={classNames(getStyleName(styles, 'popup'), 'theme-popup')}
        style={(!rightList && style.narrowedPopup) || {}}
      >
        {leftList &&
          renderStaticList(
            leftList,
            classNames(getStyleName(styles, 'link-wrapper'), 'theme-link-wrapper'),
            !rightList && style.narrowedList,
            leftListTitle,
            dataPrefix
          )}
        {rightList &&
          renderStaticList(
            rightList,
            classNames(getStyleName(styles, 'news-list'), 'theme-news-list'),
            undefined,
            rightListTitle,
            dataPrefix
          )}
      </div>
    );
  }

  render() {
    const { url, title, className, leftList, isNew, dataPrefix, dataObj } = this.props;

    return (
      <div className={className} onMouseEnter={this.loadNewsByCat}>
        {url.length ? (
          <a
            href={url}
            className={classNames(getStyleName(styles, 'submenu-title'), { [getStyleName(styles, 'is-new')]: isNew })}
            {...gaDataset({ dataPrefix, category: 'Nav', action: 'click', label: title })}
            {...dataObj}
          >
            {title}
            {leftList && <span className={getStyleName(styles, 'with-arrow')} />}
          </a>
        ) : (
          <span className={getStyleName(styles, 'submenu-title')}>
            {title}
            {leftList && <span className={getStyleName(styles, 'with-arrow')} />}
          </span>
        )}
        {this.renderPopupMenu()}
      </div>
    );
  }
}
