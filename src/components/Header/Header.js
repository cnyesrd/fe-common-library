/* eslint import/no-extraneous-dependencies: ["error", {"peerDependencies": true}] */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import raf from 'raf';
import classNames from 'classnames';
import { navsType, catNavsType, locationShape } from '../../utils/propTypes';
import getStyleName from '../../utils/getStyleName';
import SubMenu from './SubMenu';
import SubNavItem from './SubNavItem';
import styles from './Header.scss';
import { CategoryMappingWithSubs } from './ConstantCats';
import { FIXED_HEADER_NONE, FIXED_HEADER_FULL, FIXED_HEADER_SUB } from './ConstantUI';
import headerConfig from './config/navs.json';
import gaDataset from '../../utils/gaDataset';
import getDataObj from '../../utils/getDataObj';

const DISTANCE_OVER_HEADER = 74;
const DISTANCE_OVER_SUBHEADER = 114;

export function findCatSlugFromUrl(pathname = '', query) {
  const regex = /^\/(news|columnists|projects|trending|search|forex|magazines)?\/?(\w+)?\/?(\w+)?/;
  const matches = pathname.match(regex);
  let target = null;

  if (matches) {
    if (matches[1] && matches[1] === 'columnists') {
      // /columnists
      target = 'celebrity_area';
    } else if (matches[1] && matches[1] === 'magazines') {
      // /magazines
      target = 'mag';
    } else if (matches[1] && matches[1] === 'search' && query && query.q === '虛擬貨幣') {
      // /search?group=1&q=虛擬貨幣
      target = 'searchCrypto';
    } else if (matches[1] && matches[1] === 'projects') {
      // /projects OR /projects/cat/:catSlug
      target = 'projects';
    } else if (matches[2] && matches[2] === 'cat') {
      // /news/cat/:catSlug
      target = matches[3];
    } else if (matches[1] && matches[1] === 'forex' && matches[2]) {
      // /forex/xxx
      target = matches[2];
    } else if (matches[1]) {
      target = matches[1];
    } else if (matches[2]) {
      target = matches[2];
    }
  }

  return CategoryMappingWithSubs[target] && CategoryMappingWithSubs[target].parentId !== 0
    ? CategoryMappingWithSubs[target].parentSlug
    : target;
}

function renderNavs(channel, navs, dataPrefix) {
  return navs.map((nav, idx) => {
    const className = nav.title === channel ? classNames(getStyleName(styles, 'active'), 'theme-active') : '';
    const dataObj = getDataObj(nav);

    /* eslint-disable react/no-array-index-key */
    return <SubMenu key={idx} className={className} {...nav} dataPrefix={dataPrefix} dataObj={dataObj} />;
    /* eslint-enable react/no-array-index-key */
  });
}

class Header extends Component {
  static propTypes = {
    catNavs: catNavsType,
    channel: PropTypes.string.isRequired,
    displayChannelName: PropTypes.bool,
    fixedHeaderType: PropTypes.oneOf([FIXED_HEADER_NONE, FIXED_HEADER_FULL, FIXED_HEADER_SUB]),
    location: locationShape.isRequired,
    navs: navsType,
    Link: PropTypes.func,
    dataPrefix: PropTypes.arrayOf(PropTypes.string),
    customMenu: PropTypes.element,
  };

  static defaultProps = {
    catNavs: undefined,
    displayChannelName: true,
    fixedHeaderType: FIXED_HEADER_NONE,
    navs: headerConfig.items,
    Link: undefined,
    dataPrefix: ['data-proj-ga'],
    customMenu: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      fixed: false,
    };
  }

  componentDidMount() {
    if (this.isFixedHeader()) {
      window.addEventListener('scroll', this.scrollHandler);
    }
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.scrollHandler);
  }

  isFixedHeader = () => {
    return this.props.fixedHeaderType !== FIXED_HEADER_NONE;
  };

  scrollHandler = () => {
    const { fixedHeaderType } = this.props;
    const scrollY = 'scrollY' in window ? window.scrollY : document.documentElement.scrollTop;
    const sticky = this.isFixedHeader();
    let distance = DISTANCE_OVER_SUBHEADER;

    if (fixedHeaderType === FIXED_HEADER_FULL) {
      distance = DISTANCE_OVER_HEADER;
    }

    if (sticky) {
      if (scrollY >= distance && !this.state.fixed) {
        this._setNextState({ fixed: true });
      } else if (scrollY < distance && this.state.fixed) {
        this._setNextState({ fixed: false });
      }
    }
  };

  _setNextState = state => {
    if (this._setNextStateAnimationFrameId) {
      raf.cancel(this._setNextStateAnimationFrameId);
    }

    this._setNextStateAnimationFrameId = raf(() => {
      this._setNextStateAnimationFrameId = null;
      this.setState({ fixed: state.fixed });
    });
  };

  renderFixedHeader() {
    const { catNavs, channel, navs, fixedHeaderType } = this.props;
    const { fixed } = this.state;
    const isFixFullHeader = fixedHeaderType === FIXED_HEADER_FULL;
    let wrapperClass = classNames(getStyleName(styles, 'fixedHeader-wrapper'), 'theme-fixedHeader-wrapper');

    if (!fixed) return null;
    if (!isFixFullHeader || (isFixFullHeader && !(catNavs && catNavs.length > 0))) {
      wrapperClass = classNames(wrapperClass, getStyleName(styles, 'bottom'), 'theme-bottom');
    }

    return (
      <div className={wrapperClass}>
        {isFixFullHeader && (
          <nav
            className={classNames(
              getStyleName(styles, 'nav'),
              'theme-nav',
              getStyleName(styles, 'fixed'),
              'theme-fixed'
            )}
          >
            {renderNavs(channel, navs)}
          </nav>
        )}
        {catNavs && catNavs.length > 0 && this.renderSubHeader(true)}
      </div>
    );
  }

  renderSubHeader(isTopSubHeader = false) {
    const { fixedHeaderType } = this.props;

    // .js-* className is for e2e test
    let subHeaderClass = classNames(getStyleName(styles, 'sub-header'), 'theme-sub-header', 'js-header-sub-header');

    if (isTopSubHeader) {
      subHeaderClass = classNames(subHeaderClass, getStyleName(styles, 'fixed'), 'theme-fixed');

      if (fixedHeaderType === FIXED_HEADER_FULL) {
        subHeaderClass = classNames(subHeaderClass, getStyleName(styles, 'bottom'), 'theme-bottom');
      }
    }

    return (
      <div className={subHeaderClass}>
        <nav className={classNames(getStyleName(styles, 'cat-menu'), 'theme-cat-menu')}>{this.renderCatMenu()}</nav>
      </div>
    );
  }

  renderCatMenu() {
    const { catNavs, location, Link, dataPrefix } = this.props;
    const activeCatSlug = findCatSlugFromUrl(location.pathname, location.query);

    return catNavs.map((nav, idx) => {
      return (
        <SubNavItem
          key={idx} // eslint-disable-line react/no-array-index-key
          url={nav.url}
          title={nav.title}
          isNew={nav.isNew}
          isActive={nav.name === activeCatSlug}
          subItems={nav.subItems}
          external={nav.external}
          Link={Link}
          dataPrefix={dataPrefix}
          dataObj={getDataObj(nav)}
        />
      );
    });
  }

  renderSearch() {
    return (
      <form
        acceptCharset="UTF-8"
        action="https://so.cnyes.com/cnyessearch.aspx"
        className={getStyleName(styles, 'header-search')}
        target="_blank"
      >
        <input type="hidden" name="cx" value="015486011444191663508:8ijuvgfglaq" />
        <input
          type="hidden"
          name="other"
          value=""
          ref={ref => {
            this.otherInput = ref;
          }}
        />
        <input type="hidden" name="ie" value="UTF-8" />
        <input type="hidden" name="ga" value="nav" />
        <input
          name="q"
          placeholder="請輸入關鍵詞"
          onChange={e => {
            this.otherInput.value = e.target.value;
          }}
        />
        <button type="submit" />
      </form>
    );
  }

  render() {
    const { catNavs, channel, displayChannelName, navs, Link, dataPrefix, customMenu } = this.props;

    return (
      <div id={getStyleName(styles, 'cnyes-header-wrapper')} className={classNames('theme-wrapper', 'theme-header')}>
        <header className={classNames(getStyleName(styles, 'main-header'), 'theme-main-header')}>
          <div className={getStyleName(styles, 'header-menu')}>
            <span className={getStyleName(styles, 'logo-wrapper')}>
              <a
                href="https://www.cnyes.com/"
                className={getStyleName(styles, 'logo')}
                {...gaDataset({ dataPrefix, category: 'Logo', action: 'click', label: 'home' })}
              />
              {displayChannelName &&
                (Link ? (
                  <Link to="/" className={getStyleName(styles, 'channel-label')}>
                    {channel}
                  </Link>
                ) : (
                  <a href="/" className={getStyleName(styles, 'channel-label')}>
                    {channel}
                  </a>
                ))}
            </span>
            <span className={getStyleName(styles, 'actions')}>
              {customMenu}
              <ul className={getStyleName(styles, 'user-nav')}>
                <li>
                  <a href="https://www.facebook.com/anuetw/" target="_blank" rel="noopener noreferrer">
                    粉絲團
                  </a>
                </li>
              </ul>
              {this.renderSearch()}
            </span>
          </div>
          <nav className={classNames(getStyleName(styles, 'nav'), 'theme-nav')}>
            {renderNavs(channel, navs, dataPrefix)}
          </nav>
        </header>
        {catNavs &&
          catNavs.length > 0 && (
            <div className={classNames(getStyleName(styles, 'subheader-wrapper'))}>{this.renderSubHeader(false)}</div>
          )}
        {this.renderFixedHeader()}
      </div>
    );
  }
}

export default Header;
