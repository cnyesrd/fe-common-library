/* eslint-disable import/first,func-names,global-require,prefer-arrow-callback */
import React from 'react';
import { mount } from 'enzyme';
import SubMenu from '../SubMenu';
import headerConfig from '../config/navs.json';

const navs = headerConfig.items;

describe('<SubMenu/>', function() {
  let makeSubject;

  beforeEach(() => {
    jest.resetModules();

    global.__SERVER__ = false;
    global.__CLIENT__ = true;

    makeSubject = params => {
      const props = params || navs[2];

      return mount(<SubMenu {...props} />);
    };
  });

  describe('render submenu-title', () => {
    it('should be nice if url.length', () => {
      const subject = makeSubject();

      expect(subject.props().url.length).not.toEqual(0);
      expect(subject.find('a.submenu-title').length).toEqual(1);
    });

    it('should be nice if !url.length', () => {
      const subject = makeSubject({
        ...navs[0],
        url: '',
      });

      expect(subject.props().url.length).toEqual(0);
      expect(subject.find('a.submenu-title').length).toEqual(0);
    });
  });

  describe('renderPopupMenu', () => {
    it('should not renderPopupMenu if no left and no right', () => {
      const subject = makeSubject(navs[0]);

      expect(subject.find('.popup').length).toEqual(0);
    });

    it('should be nice if leftList and rightList', () => {
      const subject = makeSubject(navs[7]);

      expect(subject.find('.link-wrapper').length).toEqual(1);
      expect(subject.find('.news-list').length).toEqual(1);

      expect(subject.find('.link-wrapper').html()).toContain('基金交易');
      expect(subject.find('.news-list').html()).toContain('帳戶登入');
      expect(subject.find('.news-list').html()).toContain('鉅亨基金交易平台');
    });
  });

  describe('render some link right', () => {
    it('should be nice', () => {
      const subject = makeSubject({
        ...navs[4],
      });

      expect(subject.find('.popup .link-wrapper a').length).toEqual(subject.props().leftList.length);
      expect(subject.find('.popup .news-list a').length).toEqual(0);
    });
  });
});
