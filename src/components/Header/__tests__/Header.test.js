/* eslint-disable import/first,func-names,global-require,prefer-arrow-callback */
import React from 'react';
import { mount } from 'enzyme';
import Header, { findCatSlugFromUrl } from '../Header';
import headerConfig from '../config/navs.json';
import * as FundConfig from '../config/catNavs/fund.json';

const navs = headerConfig.items;
const catNavs = FundConfig.catNavs;

describe('<Header/>', function() {
  let makeSubject;
  let Link;

  beforeEach(() => {
    jest.resetModules();

    const testParams = {
      catNavs,
      channel: '基金',
      location: {
        pathname: '/news/cat/headline',
        search: '',
        state: {},
        action: '',
        key: '',
      },
      navs,
      newsBaseUrl: '',
      toggleFixedHeader: jest.fn(),
    };

    Link = jest.fn(() => <div className="mock-link" />);
    makeSubject = (params = {}) => {
      const props = {
        ...testParams,
        ...params,
      };

      return mount(<Header {...props} />);
    };
  });

  describe('catNavs', () => {
    it('should be nice if it is undefined', () => {
      const subject = makeSubject({
        catNavs: undefined,
      });

      expect(subject.find('SubNavItem').length).toEqual(0);
    });

    it('should be nice if it has no data', () => {
      const subject = makeSubject({
        catNavs: [],
      });

      expect(subject.find('SubNavItem').length).toEqual(0);
    });

    it('should be nice if it has data', () => {
      const subject = makeSubject();

      expect(subject.find('SubNavItem').length).not.toEqual(0);
    });

    it('should be add isNew icon if name = "search"', () => {
      const subject = makeSubject();

      expect(subject.find('.is-new').length).toEqual(2); // header and fixed header
      expect(subject.find('.is-new').get(1).props.title).toEqual('AI 理財');
    });

    it('should be add isNew icon if isNew=true', () => {
      const subject = makeSubject({
        catNavs: [
          {
            name: 'searchCrypto',
            url: '/search?group=1&q=虛擬貨幣',
            title: '虛擬貨幣',
            isNew: true,
          },
        ],
      });

      expect(subject.find('.is-new').length).toEqual(2);
      expect(subject.find('.is-new').get(1).props.title).toEqual('虛擬貨幣');
    });
  });

  describe('displayChannelName', () => {
    it('should be nice if it is true', () => {
      const subject = makeSubject();
      const channelName = subject.props().channel;

      expect(subject.props().displayChannelName).toEqual(true);
      expect(subject.find('.logo-wrapper').html()).toContain(channelName);
    });

    it('should be nice if it is false', () => {
      const subject = makeSubject({
        displayChannelName: false,
      });
      const channelName = subject.props().channelName;

      expect(subject.props().displayChannelName).toEqual(false);
      expect(subject.find('.logo-wrapper').html()).not.toContain(channelName);
    });

    it('should be a Link component', () => {
      const subject = makeSubject({ Link });

      expect(subject.props().Link).toBeDefined();
      expect(subject.find('.logo-wrapper .mock-link').length).toEqual(1);
    });

    it('should be an a tag', () => {
      const subject = makeSubject();

      expect(subject.props().Link).not.toBeDefined();
      expect(subject.find('.logo-wrapper .mock-link').length).toEqual(0);
      expect(subject.find('.logo-wrapper').html()).toContain('channel-label');
    });
  });
});

describe('findCatSlugFromUrl', function() {
  describe('news channel', function() {
    it('headline', () => {
      expect(findCatSlugFromUrl('/news/cat/headline')).toEqual('headline');
    });

    it('trending', () => {
      expect(findCatSlugFromUrl('/trending')).toEqual('trending');
    });

    it('tw stock', () => {
      const pathnames = ['/news/cat/tw_stock', '/news/cat/tw_macro', '/news/cat/tw_quo', '/news/cat/tw_stock_news'];

      pathnames.forEach(pathname => {
        expect(findCatSlugFromUrl(pathname)).toEqual('tw_stock');
      });
    });

    it('wd stock', () => {
      const pathnames = ['/news/cat/wd_stock', '/news/cat/eu_asia_stock', '/news/cat/us_stock', '/news/cat/wd_macro'];

      pathnames.forEach(pathname => {
        expect(findCatSlugFromUrl(pathname)).toEqual('wd_stock');
      });
    });

    it('cn stock', () => {
      const pathnames = ['/news/cat/cn_stock', '/news/cat/cn_macro', '/news/cat/hk_stock', '/news/cat/sh_stock'];

      pathnames.forEach(pathname => {
        expect(findCatSlugFromUrl(pathname)).toEqual('cn_stock');
      });
    });

    it('forex', () => {
      expect(findCatSlugFromUrl('/news/cat/forex')).toEqual('forex');
    });

    it('future', () => {
      const pathnames = [
        '/news/cat/future',
        '/news/cat/energy',
        '/news/cat/futu_bond',
        '/news/cat/futu_produce',
        '/news/cat/precious_metals',
        '/news/cat/index_futures',
        '/news/cat/stock_futures',
      ];

      pathnames.forEach(pathname => {
        expect(findCatSlugFromUrl(pathname)).toEqual('future');
      });
    });

    it('cnyeshouse', () => {
      const pathnames = [
        '/news/cat/cnyeshouse',
        '/news/cat/cn_housenews',
        '/news/cat/hk_housenews',
        '/news/cat/tw_housenews',
        '/news/cat/wd_housenews',
      ];

      pathnames.forEach(pathname => {
        expect(findCatSlugFromUrl(pathname)).toEqual('cnyeshouse');
      });
    });

    it('tw_money', () => {
      const pathnames = ['/news/cat/tw_money', '/news/cat/fund', '/news/cat/tw_insurance', '/news/cat/morningstar'];

      pathnames.forEach(pathname => {
        expect(findCatSlugFromUrl(pathname)).toEqual('tw_money');
      });
    });

    it('mag', () => {
      const pathnames = ['/news/cat/mag', '/magazines'];

      pathnames.forEach(pathname => {
        expect(findCatSlugFromUrl(pathname)).toEqual('mag');
      });
    });

    it('celebrity_area', () => {
      const pathnames = ['/news/cat/celebrity_area', '/columnists'];

      pathnames.forEach(pathname => {
        expect(findCatSlugFromUrl(pathname)).toEqual('celebrity_area');
      });
    });

    it('projects', () => {
      const pathnames = [
        '/projects/cat/all',
        '/projects/cat/finance',
        '/projects/cat/trend',
        '/projects/cat/event',
        '/projects/cat/fund',
      ];

      pathnames.forEach(pathname => {
        expect(findCatSlugFromUrl(pathname)).toEqual('projects');
      });
    });

    it('bc', () => {
      const pathnames = ['/news/cat/bc', '/news/cat/bc_application', '/news/cat/bc_virtual'];

      pathnames.forEach(pathname => {
        expect(findCatSlugFromUrl(pathname)).toEqual('bc');
      });
    });

    it('searchCrypto', () => {
      expect(findCatSlugFromUrl('/search', { q: '虛擬貨幣' })).toEqual('searchCrypto');
    });
  });

  describe('www.cnyes.com', function() {
    it('usastock', () => {
      expect(findCatSlugFromUrl('/usastock')).toEqual('usastock');
    });

    it('hkstock', () => {
      expect(findCatSlugFromUrl('/hkstock')).toEqual('hkstock');
    });

    it('shstock', () => {
      expect(findCatSlugFromUrl('/shstock')).toEqual('shstock');
    });
  });
});
