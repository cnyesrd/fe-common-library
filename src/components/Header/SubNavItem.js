/* eslint import/no-extraneous-dependencies: ["error", {"peerDependencies": true}] */
import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import getStyleName from '../../utils/getStyleName';
import gaDataset from '../../utils/gaDataset';
import getDataObj from '../../utils/getDataObj';
import styles from './Header.scss';

function SubNavItemMenu({ items, dataPrefix }) {
  if (!items) {
    return null;
  }
  if (!items.length) {
    return null;
  }

  return (
    <nav className={getStyleName(styles, 'cat-nav-sub-item')}>
      {items &&
        items.map(item => {
          return (
            <a
              key={item.name}
              href={item.url}
              title={item.title}
              {...gaDataset({ dataPrefix, category: 'Nav', action: 'click', label: item.title })}
              {...getDataObj(item)}
            >
              {item.title}
            </a>
          );
        })}
    </nav>
  );
}

SubNavItemMenu.propTypes = {
  items: PropTypes.arrayOf(PropTypes.object),
  dataPrefix: PropTypes.arrayOf(PropTypes.string),
};

SubNavItemMenu.defaultProps = {
  items: undefined,
  dataPrefix: ['data-proj-ga'],
};

function SubNavItem({ url = '/', title, isNew, isActive, subItems, external = false, Link, dataPrefix, dataObj }) {
  let className = isActive ? cx(getStyleName(styles, 'active'), 'theme-active') : '';

  className = cx(className, {
    [getStyleName(styles, 'is-new')]: isNew,
    [getStyleName(styles, 'with-arrow')]: !!subItems,
  });

  return (
    <span className={getStyleName(styles, 'cat-nav-item')}>
      {external || !Link ? (
        <a
          className={className}
          href={url}
          title={title}
          data-ga-category="subNavItem"
          data-ga-action={title}
          {...gaDataset({ dataPrefix, category: 'Nav', action: 'click', label: title })}
          {...dataObj}
        >
          {title}
        </a>
      ) : (
        <Link
          className={className}
          to={url}
          title={title}
          data-ga-category="subNavItem"
          data-ga-action={title}
          {...gaDataset({ dataPrefix, category: 'Nav', action: 'click', label: title })}
          {...dataObj}
        >
          {title}
        </Link>
      )}
      {subItems && <SubNavItemMenu items={subItems} dataPrefix={dataPrefix} />}
    </span>
  );
}

SubNavItem.propTypes = {
  url: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  isNew: PropTypes.bool,
  isActive: PropTypes.bool,
  subItems: PropTypes.arrayOf(PropTypes.object),
  external: PropTypes.bool,
  Link: PropTypes.func,
  dataPrefix: PropTypes.arrayOf(PropTypes.string),
  dataObj: PropTypes.shape({}),
};

SubNavItem.defaultProps = {
  isNew: false,
  isActive: false,
  subItems: undefined,
  external: false,
  Link: undefined,
  dataPrefix: ['data-proj-ga'],
  dataObj: {},
};

export default SubNavItem;
