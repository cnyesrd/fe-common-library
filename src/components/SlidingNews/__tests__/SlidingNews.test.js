/* eslint-disable react/prop-types, import/first */
jest.mock('cnyes-dfp', () => ({
  DFPProvider: ({ children }) => <div>{children}</div>,
}));

import React from 'react';
import { mount } from 'enzyme';
import SlidingNews from '../SlidingNews';

describe('component <SlidingNews />', () => {
  let makeSubject;

  beforeEach(() => {
    jest.resetModules();

    const testProps = {
      items: [
        {
          newsId: 4257985,
          title: '繼中國禁售令之後 德國暫停銷售iPhone 7和iPhone 8',
          hasCoverPhoto: 1,
          coverSrc: {
            m: { src: 'https://cimg.cnyes.cool/prod/news/4257985/m/04690339c84abdce487064140b136e2a.jpg' },
            l: { src: 'https://cimg.cnyes.cool/prod/news/4257985/l/04690339c84abdce487064140b136e2a.jpg' },
            xl: { src: 'https://cimg.cnyes.cool/prod/news/4257985/xl/04690339c84abdce487064140b136e2a.jpg' },
          },
        },
        {
          newsId: 4258122,
          title: '馬蒂斯辭任美國國防部長 稱與川普存在分歧',
          hasCoverPhoto: 1,
          coverSrc: {
            m: { src: 'https://cimg.cnyes.cool/prod/news/4258122/m/8e02b6a7af860076a90f41944a7927ba.jpg' },
            l: { src: 'https://cimg.cnyes.cool/prod/news/4258122/l/8e02b6a7af860076a90f41944a7927ba.jpg' },
            xl: { src: 'https://cimg.cnyes.cool/prod/news/4258122/xl/8e02b6a7af860076a90f41944a7927ba.jpg' },
          },
        },
        {
          newsId: 4258104,
          title: '道瓊指數連兩日大跌 美財政部長：市場對鮑爾談話反應過度',
          hasCoverPhoto: 1,
          coverSrc: {
            m: { src: 'https://cimg.cnyes.cool/prod/news/4258104/m/dc61cb343479aa56e53679bf2176898e.jpg' },
            l: { src: 'https://cimg.cnyes.cool/prod/news/4258104/l/dc61cb343479aa56e53679bf2176898e.jpg' },
            xl: { src: 'https://cimg.cnyes.cool/prod/news/4258104/xl/dc61cb343479aa56e53679bf2176898e.jpg' },
          },
        },
      ],
    };

    makeSubject = (params = {}) => {
      const props = {
        ...testProps,
        ...params,
      };

      return mount(<SlidingNews {...props} />);
    };
  });

  it('should be same as snapshot', () => {
    const subject = makeSubject();

    expect(subject).toMatchSnapshot();
  });

  it('should render wrapper', () => {
    const subject = makeSubject();

    expect(subject.find('.wrapper').length).toBe(1);
  });

  it('should render news divs', () => {
    const subject = makeSubject();

    expect(subject.find('.news').length).toBe(subject.props().items.length + 1);
  });

  describe('sliding behaviors', () => {
    it('should at initial index', () => {
      const subject = makeSubject();

      expect(subject.state().index).toBe(0);
      expect(subject.state().mode).toBe(SlidingNews.MODE.AUTO_SLIDE_TO_NEXT);
    });

    it('should set mode on mouse enter or leave wrapper', () => {
      const subject = makeSubject();

      subject.find('.wrapper').simulate('mouseEnter');
      expect(subject.state().mode).toBe(SlidingNews.MODE.STOP);

      subject.find('.wrapper').simulate('mouseLeave');
      expect(subject.state().mode).toBe(SlidingNews.MODE.AUTO_SLIDE_TO_NEXT);
    });

    it('should set mode and go to last if first on mouse enter prev button', () => {
      const subject = makeSubject();

      subject.find('.prev').simulate('mouseEnter');
      expect(subject.state().index).toBe(subject.state().displayedItemsLength);
      expect(subject.state().mode).toBe(SlidingNews.MODE.SLIDE_TO_PREV);
    });

    it('should set mode and go to last if first on mouse enter next button', () => {
      const subject = makeSubject();

      subject.setState({ index: subject.state().displayedItemsLength });
      subject.find('.next').simulate('mouseEnter');
      expect(subject.state().index).toBe(0);
      expect(subject.state().mode).toBe(SlidingNews.MODE.SLIDE_TO_NEXT);
    });

    it('should set mode and go to last if first on mouse enter dot button', () => {
      const subject = makeSubject();

      subject.setState({ index: subject.state().displayedItemsLength });
      subject
        .find('.dot')
        .at(0)
        .simulate('mouseEnter');
      expect(subject.state().index).toBe(0);
      expect(subject.state().mode).toBe(SlidingNews.MODE.SLIDE_TO_INDEX);
    });

    it('should go to next index on click next button', () => {
      const subject = makeSubject();

      subject.find('.next').simulate('click');
      expect(subject.state().index).toBe(1);
    });

    it('should go to prev index on click prev button', () => {
      const subject = makeSubject();

      subject.setState({ index: 3 });
      subject.find('.prev').simulate('click');
      expect(subject.state().index).toBe(2);
    });

    it('should go to target index on click dot button', () => {
      const subject = makeSubject();

      subject
        .find('.dot')
        .at(2)
        .simulate('click');
      expect(subject.state().index).toBe(2);
    });
  });
});
