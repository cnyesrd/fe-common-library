const AdProfiles = {
  indexBigNativeA: {
    name: 'indexBigNativeA',
    path: '/1018855/cnyes_index_big_native_A',
    size: ['fluid'],
    hideOnInitial: true,
  },
  indexBigNativeB: {
    name: 'indexBigNativeB',
    path: '/1018855/cnyes_index_big_native_B',
    size: ['fluid'],
    hideOnInitial: true,
  },
};

export default AdProfiles;
