import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { withNotes } from '@storybook/addon-notes';
import * as knobs from '@storybook/addon-knobs/react';
import { DFPProvider } from 'cnyes-dfp';
import SlidingNews from '../SlidingNews';
import defaultNotes from './SlidingNews.md';
import AdProfiles from './AdProfiles';

const items = [
  {
    newsId: 4257985,
    title: '繼中國禁售令之後 德國暫停銷售iPhone 7和iPhone 8',
    hasCoverPhoto: 1,
    coverSrc: {
      m: { src: 'https://cimg.cnyes.cool/prod/news/4257985/m/04690339c84abdce487064140b136e2a.jpg' },
      l: { src: 'https://cimg.cnyes.cool/prod/news/4257985/l/04690339c84abdce487064140b136e2a.jpg' },
      xl: { src: 'https://cimg.cnyes.cool/prod/news/4257985/xl/04690339c84abdce487064140b136e2a.jpg' },
    },
    utmParams: 'utm_source=cnyes_homepage&utm_medium=headline&utm_content=list',
  },
  {
    newsId: 4258122,
    title: '馬蒂斯辭任美國國防部長 稱與川普存在分歧',
    hasCoverPhoto: 1,
    coverSrc: {
      m: { src: 'https://cimg.cnyes.cool/prod/news/4258122/m/8e02b6a7af860076a90f41944a7927ba.jpg' },
      l: { src: 'https://cimg.cnyes.cool/prod/news/4258122/l/8e02b6a7af860076a90f41944a7927ba.jpg' },
      xl: { src: 'https://cimg.cnyes.cool/prod/news/4258122/xl/8e02b6a7af860076a90f41944a7927ba.jpg' },
    },
  },
  AdProfiles.indexBigNativeA,
  {
    newsId: 4258104,
    title: '道瓊指數連兩日大跌 美財政部長：市場對鮑爾談話反應過度',
    hasCoverPhoto: 1,
    coverSrc: {
      m: { src: 'https://cimg.cnyes.cool/prod/news/4258104/m/dc61cb343479aa56e53679bf2176898e.jpg' },
      l: { src: 'https://cimg.cnyes.cool/prod/news/4258104/l/dc61cb343479aa56e53679bf2176898e.jpg' },
      xl: { src: 'https://cimg.cnyes.cool/prod/news/4258104/xl/dc61cb343479aa56e53679bf2176898e.jpg' },
    },
  },
  {
    newsId: 4259295,
    title: '國際廢鋼挫近4% 豐興本周廢鋼、鋼筋雙雙走跌',
    hasCoverPhoto: 1,
    coverSrc: {
      m: { src: 'https://cimg.cnyes.cool/prod/news/4259295/m/67299790b46f51f46aadeeb807eaa604.jpg' },
      l: { src: 'https://cimg.cnyes.cool/prod/news/4259295/l/67299790b46f51f46aadeeb807eaa604.jpg' },
      xl: { src: 'https://cimg.cnyes.cool/prod/news/4259295/xl/67299790b46f51f46aadeeb807eaa604.jpg' },
    },
  },
  AdProfiles.indexBigNativeB,
];

const stories = storiesOf('SlidingNews', module);

stories.addDecorator(story => <DFPProvider>{story()}</DFPProvider>);

stories.addDecorator(knobs.withKnobs);

stories.add(
  'SlidingNews',
  withNotes(defaultNotes)(
    withInfo()(() => {
      return (
        <div style={{ width: '560px', height: '360px' }}>
          <SlidingNews items={items} />
        </div>
      );
    })
  )
);

stories.add(
  'SlidingNews - responsive',
  withNotes(defaultNotes)(
    withInfo()(() => {
      return (
        <div style={{ width: knobs.number('width', 768), height: knobs.number('height', 360) }}>
          <SlidingNews items={items} />
        </div>
      );
    })
  )
);

stories.add(
  'SlidingNews - speed',
  withNotes(defaultNotes)(
    withInfo()(() => {
      return (
        <div style={{ width: '560px', height: '360px' }}>
          <SlidingNews items={items} speed={knobs.number('speed', 500)} />
        </div>
      );
    })
  )
);

stories.add(
  'SlidingNews - duration',
  withNotes(defaultNotes)(
    withInfo()(() => {
      return (
        <div style={{ width: '560px', height: '360px' }}>
          <SlidingNews items={items} duration={knobs.number('duration', 1000)} />
        </div>
      );
    })
  )
);
