## Usage

```
import SlidingNews from 'fe-common-library/dest/components/SlidingNews';
import 'fe-common-library/dest/components/SlidingNews/style.css';

// ...

render() {
  return (
    <div style={{ width: '128px', height: '128px' }}> 
      <SlidingNews speed={500} duration={1000} newsList={[
        {
          title: '繼中國禁售令之後 德國暫停銷售iPhone 7和iPhone 8',
          hasCoverPhoto: 1,
          coverSrc: {
            m: { src: 'https://cimg.cnyes.cool/prod/news/4257985/m/04690339c84abdce487064140b136e2a.jpg' },
            l: { src: 'https://cimg.cnyes.cool/prod/news/4257985/l/04690339c84abdce487064140b136e2a.jpg' },
            xl: { src: 'https://cimg.cnyes.cool/prod/news/4257985/xl/04690339c84abdce487064140b136e2a.jpg' },
          },
        }, {
          title: '馬蒂斯辭任美國國防部長 稱與川普存在分歧',
          hasCoverPhoto: 1,
          coverSrc: {
            m: { src: 'https://cimg.cnyes.cool/prod/news/4258122/m/8e02b6a7af860076a90f41944a7927ba.jpg' },
            l: { src: 'https://cimg.cnyes.cool/prod/news/4258122/l/8e02b6a7af860076a90f41944a7927ba.jpg' },
            xl: { src: 'https://cimg.cnyes.cool/prod/news/4258122/xl/8e02b6a7af860076a90f41944a7927ba.jpg' },
          },
        }, {
          title: '道瓊指數連兩日大跌 美財政部長：市場對鮑爾談話反應過度',
          hasCoverPhoto: 1,
          coverSrc: {
            m: { src: 'https://cimg.cnyes.cool/prod/news/4258104/m/dc61cb343479aa56e53679bf2176898e.jpg' },
            l: { src: 'https://cimg.cnyes.cool/prod/news/4258104/l/dc61cb343479aa56e53679bf2176898e.jpg' },
            xl: { src: 'https://cimg.cnyes.cool/prod/news/4258104/xl/dc61cb343479aa56e53679bf2176898e.jpg' },
          },
        },
      ]} />
    </div>
  );
}
```