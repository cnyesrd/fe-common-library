import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import { DFPProvider, AdSlot } from 'cnyes-dfp';
import getDataObj from '../../utils/getDataObj';
import { newsItemType, adProfileType } from '../../utils/propTypes';
import styles from './SlidingNews.scss';

const cx = classNames.bind(styles);

class SlidingNews extends React.Component {
  static propTypes = {
    speed: PropTypes.number,
    duration: PropTypes.number,
    items: PropTypes.arrayOf(PropTypes.oneOfType([newsItemType, adProfileType])),
    defaultCoverImage: PropTypes.string,
    coverImageSize: PropTypes.oneOf(['xs', 's', 'm', 'l', 'xl']), // xs: 100, s: 200, m: 414, l: 680, xl: 1020
    openNewTab: PropTypes.bool,
    linkClickCb: PropTypes.func,
  };

  static defaultProps = {
    speed: 1000,
    duration: 3000,
    items: [],
    defaultCoverImage: null,
    coverImageSize: 'l',
    openNewTab: true,
    linkClickCb: null,
  };

  static MODE = {
    AUTO_SLIDE_TO_NEXT: 'AUTO_SLIDE_TO_NEXT',
    SLIDE_TO_NEXT: 'SLIDE_TO_NEXT',
    SLIDE_TO_INDEX: 'SLIDE_TO_INDEX',
    SLIDE_TO_PREV: 'SLIDE_TO_PREV',
    STOP: 'STOP',
  };

  constructor(props) {
    super(props);

    const displayedItemsLength = props.items.reduce((length, item) => {
      return item.newsId ? length + 1 : length;
    }, 0);

    this.state = {
      index: 0,
      animate: false,
      displayedItemsLength,
      mode: SlidingNews.MODE.AUTO_SLIDE_TO_NEXT,
    };

    this.queue = [];
    this.timeoutId = null;
    this.adUnitPaths = props.items.reduce((paths, item) => {
      return item.path ? [...paths, item.path] : paths;
    }, []);
  }

  componentDidMount() {
    this.sliderElement.addEventListener('transitionend', this.handleTransitionEnd);
    this.autoSlideToNext();
  }

  componentDidUpdate(prevProps, prevState) {
    const { displayedItemsLength } = this.state;

    if (prevState.displayedItemsLength !== displayedItemsLength) {
      this.autoSlideToNext();
    }
  }

  componentWillUnmount() {
    this.sliderElement.removeEventListener('transitionend', this.handleTransitionEnd);
    this.stop();
  }

  setSliderElement = element => {
    this.sliderElement = element;
  };

  handleSlotRenderEnded = (provider, event) => {
    const adElement = document.getElementById(`${event.slot.getSlotElementId()}-container`);

    const isSlidingAdSlot = this.adUnitPaths.find(path => path === event.slot.getAdUnitPath());

    if (isSlidingAdSlot && !event.isEmpty) {
      this.queue.push(() => {
        adElement.setAttribute('style', 'display:block;width:100%;flex-shrink:0;');
        this.setState(({ displayedItemsLength }) => ({
          displayedItemsLength: displayedItemsLength + 1,
        }));
      });
    }
  };

  autoSlideToNext = () => {
    const { duration } = this.props;
    const { displayedItemsLength } = this.state;

    this.stop();
    if (displayedItemsLength > 1) {
      this.timeoutId = setTimeout(this.slideToNext, duration);
    }
  };

  stop = () => {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }
  };

  goTo = (nextIndex, animate) => {
    if (this.state.animate || this.state.index === nextIndex) return;

    this.setState(
      {
        index: nextIndex,
        animate,
      },
      () => {
        if (nextIndex === 0) {
          this.queue.forEach(func => func && func());
          this.queue = [];
        }
      }
    );
  };

  slideToNext = () => {
    const { index } = this.state;

    this.goTo(index + 1, true);
  };

  slideToPrev = () => {
    const { index } = this.state;

    this.goTo(index - 1, true);
  };

  slideToIndex = index => {
    this.goTo(index, true);
  };

  jumpToLastIfFirst = () => {
    const { index, displayedItemsLength } = this.state;

    if (index === 0) {
      this.goTo(displayedItemsLength, false);
    }
  };

  jumpToFirstIfLast = () => {
    const { index, displayedItemsLength } = this.state;

    if (index === displayedItemsLength) {
      this.goTo(0, false);
    }
  };

  handleClick = e => {
    const { SLIDE_TO_PREV, SLIDE_TO_INDEX, SLIDE_TO_NEXT } = SlidingNews.MODE;
    const mode = e.currentTarget.getAttribute('data-mode');
    const index = +e.currentTarget.getAttribute('data-index');

    switch (mode) {
      case SLIDE_TO_PREV:
        this.slideToPrev();
        break;
      case SLIDE_TO_INDEX:
        this.slideToIndex(index);
        break;
      case SLIDE_TO_NEXT:
      default:
        this.slideToNext();
        break;
    }
  };

  handleTransitionEnd = () => {
    const { SLIDE_TO_PREV, SLIDE_TO_INDEX, SLIDE_TO_NEXT, AUTO_SLIDE_TO_NEXT, STOP } = SlidingNews.MODE;
    const { mode } = this.state;

    this.setState({ animate: false }, () => {
      switch (mode) {
        case SLIDE_TO_PREV:
          this.jumpToLastIfFirst();
          break;
        case SLIDE_TO_INDEX:
        case SLIDE_TO_NEXT:
          this.jumpToFirstIfLast();
          break;
        case AUTO_SLIDE_TO_NEXT:
          this.jumpToFirstIfLast();
          this.autoSlideToNext();
          break;
        case STOP:
        default:
          break;
      }
    });
  };

  handleMouseEnter = e => {
    const { SLIDE_TO_PREV, SLIDE_TO_INDEX, SLIDE_TO_NEXT, AUTO_SLIDE_TO_NEXT, STOP } = SlidingNews.MODE;
    const mode = e.currentTarget.getAttribute('data-mode');

    switch (mode) {
      case SLIDE_TO_PREV:
        this.jumpToLastIfFirst();
        break;
      case SLIDE_TO_INDEX:
      case SLIDE_TO_NEXT:
        this.jumpToFirstIfLast();
        break;
      case STOP:
        this.stop();
        break;
      case AUTO_SLIDE_TO_NEXT:
      default:
        break;
    }
    this.setState({ mode });
  };

  handleMouseLeave = e => {
    const { SLIDE_TO_PREV, SLIDE_TO_INDEX, SLIDE_TO_NEXT, STOP, AUTO_SLIDE_TO_NEXT } = SlidingNews.MODE;
    const mode = e.currentTarget.getAttribute('data-mode');

    switch (mode) {
      case SLIDE_TO_PREV:
      case SLIDE_TO_INDEX:
      case SLIDE_TO_NEXT:
        this.setState({ mode: STOP });
        break;
      case STOP:
      default:
        this.jumpToFirstIfLast();
        this.autoSlideToNext();
        this.setState({ mode: AUTO_SLIDE_TO_NEXT });
    }
  };

  render() {
    const { SLIDE_TO_PREV, SLIDE_TO_INDEX, SLIDE_TO_NEXT, STOP } = SlidingNews.MODE;
    const { index, animate, displayedItemsLength } = this.state;
    const { speed, items, defaultCoverImage, coverImageSize, openNewTab, linkClickCb } = this.props;
    const itemsLength = items.length;
    const displayBtn = displayedItemsLength > 1;
    const sliderElementStyle = {
      transform: `translateX(${-index * 100}%)`,
    };

    if (animate) {
      sliderElementStyle.transition = `transform ${speed}ms`;
    }

    return (
      <DFPProvider onSlotRenderEnded={this.handleSlotRenderEnded} loadGPT={false}>
        <div
          className={cx('wrapper')}
          data-mode={STOP}
          onMouseEnter={this.handleMouseEnter}
          onMouseLeave={this.handleMouseLeave}
        >
          <div className={cx('slider')} ref={this.setSliderElement} style={sliderElementStyle}>
            {itemsLength > 0 &&
              [...items, items[0]].map((item, i) => {
                const { newsId, utmParams, title, coverSrc, path } = item;
                const dataObj = getDataObj(item);

                if (path) {
                  return <AdSlot profile={item} key={path} />;
                }

                const defaultSrc =
                  (coverSrc && coverSrc[coverImageSize] && coverSrc[coverImageSize].src) || defaultCoverImage;
                const newsLink = utmParams
                  ? `https://news.cnyes.com/news/id/${newsId}?${utmParams}`
                  : `https://news.cnyes.com/news/id/${newsId}`;

                return (
                  <a
                    className={cx('news')}
                    // eslint-disable-next-line react/no-array-index-key
                    key={`${newsId}-${i}`}
                    href={newsLink}
                    target={openNewTab ? '_blank' : '_self'}
                    rel={openNewTab ? 'noopener noreferrer' : ''}
                    style={{ backgroundImage: `url(${defaultSrc})` }}
                    onClick={() => {
                      if (linkClickCb) linkClickCb(item);
                    }}
                    {...dataObj}
                  >
                    <div className={cx('title')}>
                      <p>{title}</p>
                    </div>
                  </a>
                );
              })}
          </div>
          {displayBtn && (
            <button
              className={cx('prev')}
              data-mode={SLIDE_TO_PREV}
              onMouseEnter={this.handleMouseEnter}
              onMouseLeave={this.handleMouseLeave}
              onClick={this.handleClick}
            />
          )}
          {displayBtn && (
            <button
              className={cx('next')}
              data-mode={SLIDE_TO_NEXT}
              onMouseEnter={this.handleMouseEnter}
              onMouseLeave={this.handleMouseLeave}
              onClick={this.handleClick}
            />
          )}
          {displayBtn && (
            <div className={cx('dots')}>
              {Array(displayedItemsLength)
                .fill(0)
                .map((item, i) => (
                  <button
                    // eslint-disable-next-line react/no-array-index-key
                    key={i}
                    data-index={i}
                    className={cx('dot', {
                      active: i === 0 ? index === 0 || index === displayedItemsLength : index === i,
                    })}
                    data-mode={SLIDE_TO_INDEX}
                    onMouseEnter={this.handleMouseEnter}
                    onMouseLeave={this.handleMouseLeave}
                    onClick={this.handleClick}
                  />
                ))}
            </div>
          )}
        </div>
      </DFPProvider>
    );
  }
}

export default SlidingNews;
