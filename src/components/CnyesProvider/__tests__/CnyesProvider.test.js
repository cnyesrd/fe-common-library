/* eslint-disable import/first,func-names,global-require,prefer-arrow-callback */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { mount } from 'enzyme';
import CnyesProvider from '../CnyesProvider';
import { authType } from '../../../utils/propTypes';

describe('CnyesProvider', function() {
  let auth;
  let request;
  let makeSubject;
  let context;

  beforeEach(() => {
    jest.resetModules();
    request = jest.fn();
    context = undefined;
    jest.mock('../../../utils/cnyesAuth/auth');
    auth = require('../../../utils/cnyesAuth/auth').default;

    class GetContext extends Component {
      static contextTypes = {
        auth: authType,
        request: PropTypes.func,
      };

      constructor(props, ctx) {
        super(props, ctx);
        context = ctx;
      }

      render() {
        return <div className="children">children</div>;
      }
    }

    makeSubject = () => {
      return mount(
        <CnyesProvider auth={auth} request={request}>
          <GetContext />
        </CnyesProvider>
      );
    };
  });

  it('should match snapshot', () => {
    const subject = makeSubject();
    const tree = subject.html();

    expect(tree).toMatchSnapshot();
  });

  it('should render children', () => {
    const subject = makeSubject();
    const target = subject.find('div.children');

    expect(target.length).toBe(1);
  });

  it('should get auth context', () => {
    makeSubject();

    expect(context.auth).toBe(auth);
  });

  it('should get auth context', () => {
    makeSubject();

    expect(context.request).toBe(request);
  });
});
