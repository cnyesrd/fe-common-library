/* eslint-disable import/first */
import React from 'react';
import { shallow } from 'enzyme';
import ScrollToTitle from '../ScrollToTitle';

describe('component <ScrollToTitle />', () => {
  let makeSubject;

  beforeEach(() => {
    jest.resetModules();

    const testParams = {
      titles: [
        { title: '台股', id: '_TWStockWrapper' },
        { title: '基金', id: '_FundWrapper' },
        { title: '國際股', id: '_WDStockWrapper' },
        { title: '外匯', id: '_ForexWrapper' },
      ],
    };

    makeSubject = (params = {}) => {
      const props = {
        ...testParams,
        ...params,
      };

      return shallow(<ScrollToTitle {...props} />);
    };
  });

  it('should be same as snapshot', () => {
    const subject = makeSubject();

    expect(subject).toMatchSnapshot();
  });

  describe('scroll to title button', () => {
    it('should show if state: show = true as default ', () => {
      const subject = makeSubject();

      expect(subject.find('.scroll-to-title').length).toBe(1);
    });

    it('should call animateScroll.scrollTo on click', () => {
      const subject = makeSubject();

      subject.instance().scrollTo = jest.fn();
      subject
        .find('.title')
        .first()
        .simulate('click');
      expect(subject.instance().scrollTo).toBeCalled();
    });

    it('should hide title list if btn on click', () => {
      const subject = makeSubject();

      subject.find('.scroll-to-title').simulate('click');
      expect(subject.find('.title-list').length).toBe(0);
    });
  });
});
