## Usage

```
import ScrollToTitle from 'fe-common-library/dest/components/ScrollToTitle/ScrollToTitle';
import 'fe-common-library/dest/components/ScrollToTitle/style.css';

// ...

render() {
  return (
    <ScrollToTitle title={{title:'A', id:'a'}, {title:'B', id:'b'}}>
  );
}
```