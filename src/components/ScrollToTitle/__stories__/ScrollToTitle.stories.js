/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { withNotes } from '@storybook/addon-notes';
import * as knobs from '@storybook/addon-knobs/react';
import ScrollToTitle from '../ScrollToTitle';
import Footer from '../../Footer/Footer';
import defaultNotes from './ScrollToTitle.md';

const stories = storiesOf('ScrollToTitle', module);
const titlesA = [
  { title: 'A區塊', id: 'div_1' },
  { title: 'B區塊', id: 'div_2' },
  { title: 'C區塊', id: 'div_3' },
  { title: 'D區塊', id: 'div_4' },
];

const titlesB = [
  { title: '第A區塊', id: 'div_1' },
  { title: '第B區塊', id: 'div_2' },
  { title: '第C區塊', id: 'div_3' },
  { title: '第D區塊', id: 'div_4' },
];

stories.addDecorator(knobs.withKnobs);
stories.addDecorator(story => (
  <div>
    <div style={{ width: '100%', height: '1800px', padding: '40px 200px' }}>
      <div id={'div_1'} style={{ paddingTop: '30px' }}>
        A區塊
      </div>
      <p>long content here...</p>
      <p>long content here...</p>
      <p>long content here...</p>
      <p>long content here...</p>
      <p>long content here...</p>
      <div style={{ width: '100%', height: '300px' }} />
      <div id={'div_2'} style={{ paddingTop: '30px' }}>
        B區塊
      </div>
      <p>long content here...</p>
      <p>long content here...</p>
      <p>long content here...</p>
      <p>long content here...</p>
      <p>long content here...</p>
      <div style={{ width: '100%', height: '400px' }} />
      <div id={'div_3'} style={{ paddingTop: '30px' }}>
        C區塊
      </div>
      <p>long content here...</p>
      <p>long content here...</p>
      <p>long content here...</p>
      <p>long content here...</p>
      <p>long content here...</p>
      <div style={{ width: '100%', height: '500px' }} />
      <div id={'div_4'} style={{ paddingTop: '30px' }}>
        D區塊
      </div>
      <p>long content here...</p>
      <p>long content here...</p>
      <p>long content here...</p>
      <p>long content here...</p>
      <p>long content here...</p>
    </div>
    {story()}
    <Footer />
  </div>
));

stories.add(
  'ScrollToTitle',
  withNotes(defaultNotes)(
    withInfo()(() => {
      return <ScrollToTitle titles={titlesA} hasFooter />;
    })
  )
);

stories.add(
  'ScrollToTitle - customScrollMore',
  withNotes(defaultNotes)(
    withInfo()(() => {
      return <ScrollToTitle titles={titlesA} customScrollMore={20} hasFooter />;
    })
  )
);

stories.add(
  'ScrollToTitle - size m',
  withNotes(defaultNotes)(
    withInfo()(() => {
      return <ScrollToTitle titles={titlesB} customScrollMore={20} hasFooter size="m" />;
    })
  )
);
