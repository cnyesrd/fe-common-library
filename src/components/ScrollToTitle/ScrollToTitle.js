import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames/bind';
import { animateScroll } from 'react-scroll';
import throttle from 'lodash.throttle';
import footerStyles from '../Footer/Footer.scss';
import styles from './ScrollToTitle.scss';
import getDataObj from '../../utils/getDataObj';

const cx = classnames.bind(styles);
const defaultBottom = 90; // higher than ScrollToTop
const defaultFooterHeight = parseInt(footerStyles.footerHeight, 10) + 68; // ScrollToTop 36 + total margin 32

export const titlesPropType = PropTypes.shape({
  title: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
});

class ScrollToTitle extends React.Component {
  static propTypes = {
    customScrollMore: PropTypes.number,
    customFooterHeight: PropTypes.number,
    hasFooter: PropTypes.bool,
    titles: PropTypes.arrayOf(titlesPropType),
    size: PropTypes.oneOf(['s', 'm']),
    scrollCb: PropTypes.func,
  };

  static defaultProps = {
    customScrollMore: 0,
    customFooterHeight: 0,
    hasFooter: false,
    titles: [],
    size: 's',
    scrollCb: null,
  };

  state = {
    showTitleList: true,
    bottom: defaultBottom,
  };

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll = throttle(() => {
    const { hasFooter, customFooterHeight } = this.props;
    let customBottom = defaultBottom;

    if (hasFooter || customFooterHeight) {
      const footerHeight = customFooterHeight || defaultFooterHeight;
      const scrollHeight = document.body.scrollHeight;
      const offsetTop = window.innerHeight + window.pageYOffset;
      const offsetBottom = scrollHeight - offsetTop;
      const diff = footerHeight - defaultBottom;

      if (diff >= offsetBottom) {
        customBottom = footerHeight - offsetBottom;
      }
    }
    this.setState({ bottom: customBottom });
  }, 50);

  scrollTo = (e, id) => {
    const { customScrollMore, scrollCb } = this.props;

    if (e) {
      e.preventDefault();
    }
    const element = document.getElementById(id);

    if (element && element.offsetTop) {
      animateScroll.scrollTo(element.offsetTop + customScrollMore);
      if (scrollCb) {
        scrollCb(id);
      }
    }
  };

  toggleTitleList = () => {
    this.setState({ showTitleList: !this.state.showTitleList });
  };

  render() {
    const { titles, size } = this.props;
    const { bottom, showTitleList } = this.state;

    return (
      <div>
        {showTitleList &&
          titles.length > 0 && (
            <div className={cx('title-list', `title-list--${size}`)} style={{ bottom: `${bottom + 48}px` }}>
              {titles.map(obj => {
                const dataObj = getDataObj(obj);

                return (
                  <button
                    className={cx('title', `title--${size}`)}
                    key={obj.id}
                    onClick={e => this.scrollTo(e, obj.id)}
                    {...dataObj}
                  >
                    {obj.title}
                  </button>
                );
              })}
            </div>
          )}
        <button
          onClick={() => this.toggleTitleList()}
          className={cx('scroll-to-title', `scroll-to-title--${size}`, showTitleList ? 'active' : '')}
          style={{ bottom: `${bottom}px` }}
        >
          <div className={cx('icon-bar')} />
          <div className={cx('icon-bar')} />
          <div className={cx('icon-bar')} />
        </button>
      </div>
    );
  }
}

export default ScrollToTitle;
