## Usage

```
import Survey from 'fe-common-library/dest/components/Survey';
import 'fe-common-library/dest/components/Survey/style.css';

// ...

render() {
  return (
    <Survey newsId={4100137} />;
  );
}

```