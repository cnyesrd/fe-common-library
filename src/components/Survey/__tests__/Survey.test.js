/* eslint-disable import/first,func-names,global-require,prefer-arrow-callback */
import React from 'react';
import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import Survey from '../Survey';
import ls from '../../../utils/localStorageWrapper';

describe('Survey component', () => {
  let makeSubject;

  beforeEach(() => {
    jest.resetModules();
    localStorage.clear();

    makeSubject = (params = {}) => {
      const testParams = {
        newsId: 4100137,
      };
      const props = {
        ...testParams,
        ...params,
      };

      return mount(<Survey {...props} />);
    };
  });

  it('should show 2 buttons if surveyNewsId not exists in localStorage', () => {
    const subject = makeSubject();

    expect(toJson(subject)).toMatchSnapshot();
    expect(ls.getItem(`surveyNewsId:${subject.props().newsId}`)).toBeNull();
    expect(subject.find('button').length).toEqual(2);
  });

  it('should hide buttons and store surveyNewsId in localStorage on click button', () => {
    const subject = makeSubject();

    subject
      .find('button')
      .at(0)
      .simulate('click');

    expect(toJson(subject)).toMatchSnapshot();
    expect(ls.getItem(`surveyNewsId:${subject.props().newsId}`)).not.toBeNull();
    expect(subject.find('button').length).toEqual(0);
  });

  it('should show nothing if surveyNewsId exists in localStorage', () => {
    ls.setItem('surveyNewsId:4100137', new Date().getTime());

    const subject = makeSubject();

    expect(toJson(subject)).toMatchSnapshot();
    expect(subject.html()).toEqual(null);
  });
});
