import React, { Component } from 'react';
import cx from 'classnames';
import styles from './LoginPage.scss';
import getStyleName from '../../../utils/getStyleName';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';

export default class LoginPage extends Component {
  render() {
    return (
      <div className={getStyleName(styles, 'fundsyes-login-form')}>
        <script
          type="text/javascript"
          dangerouslySetInnerHTML={{
            __html: `
              (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
              })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
              ga('create', 'TODO_GA_ID', 'auto', { allowLinker: true });
              ga('require', 'linker');
              ga('linker:autoLink', ['TODO_CNYES_HOSTNAME']);
              ga('send', 'pageview', '/anuefund/FundOldDriverSignIn');
            `,
          }}
          async
        />
        <Header />
        <main>
          <div className={getStyleName(styles, 'promotion')}>
            <a
              className={getStyleName(styles, 'banner')}
              title="立即開戶 - 鉅亨投顧基金交易平台"
              href="https://www.anuefund.com/Account/open_notice.aspx?utm_campaign=cnyes&utm_medium=driver_authorize"
              target="_blank"
              rel="noopener noreferrer"
            />
          </div>
          <div className={getStyleName(styles, 'content-wrap')}>
            <h2>登入授權</h2>
            <p>請登入鉅亨網投顧帳戶</p>
            <form method="POST" action="#TODO">
              <div className={getStyleName(styles, 'form-group')}>
                <label htmlFor="account">身分證字號</label>
                <input name="account" id="account" type="text" placeholder="請輸入您的身分證字號" required />
              </div>
              <div className={getStyleName(styles, 'form-group')}>
                <label htmlFor="password">登入密碼</label>
                <input name="password" id="password" type="password" placeholder="請輸入密碼" required />
                <div className={getStyleName(styles, 'annotation')}>
                  <a href="https://www.fundsyes.com/ForgetPwd.aspx" target="_blank" rel="noopener noreferrer">
                    忘記密碼
                  </a>
                </div>
              </div>
              <button
                className={cx(getStyleName(styles, 'button-submit'), getStyleName(styles, 'button-sm'))}
                type="submit"
              >
                登入授權
              </button>
            </form>
          </div>
        </main>
        <Footer />
      </div>
    );
  }
}
