import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { withNotes } from '@storybook/addon-notes';
import * as knobs from '@storybook/addon-knobs/react';
import ContractPage from '../ContractPage';

const stories = storiesOf('FundsyesOldDriver/ContractPage', module);

stories.addDecorator(knobs.withKnobs);

stories.add(
  'default',
  withNotes('')(
    withInfo()(() => {
      return <ContractPage />;
    })
  )
);
