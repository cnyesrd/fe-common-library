import React, { Component } from 'react';
import cx from 'classnames';

import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import styles from './ContractPage.scss';
import getStyleName from '../../../utils/getStyleName';

export default class ContractPage extends Component {
  render() {
    return (
      <div className={getStyleName(styles, 'fundsyes-contract-page')}>
        <script
          type="text/javascript"
          dangerouslySetInnerHTML={{
            __html: `
              (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
              })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
              ga('create', 'TODO_GA_ID', 'auto', { allowLinker: true });
              ga('require', 'linker');
              ga('linker:autoLink', ['TODO_CNYES_HOSTNAME']);
              ga('send', 'pageview', '/anuefund/FundOldDriverContract');
            `,
          }}
          async
        />
        <Header />
        <main>
          <h2 className={getStyleName(styles, 'contract-title')}>個人資料同意暨指示授權書</h2>
          <form method="POST" action="#TODO">
            <div className={getStyleName(styles, 'contract')}>
              <div className={getStyleName(styles, 'contract-body')}>
                <p>
                  本人　茲因欲參與鉅亨網股份有限公司「投資老司機」平台所需，同意並指示授權　貴公司將本人之個人資料，於以下範圍內，即時並持續提供予鉅亨網股份有限公司，作為參與鉅亨網股份有限公司「投資老司機」平台之用。
                </p>
                <p>指示授權轉交之本人個人資料範圍：</p>
                <ul>
                  <li>1. 姓名</li>
                  <li>2. 出生年月日</li>
                  <li>3. 身分證統一編號</li>
                  <li>4. 聯絡方式</li>
                  <li>5. 平台使用紀錄</li>
                  <li>6. 基金之交易資訊、持有部位、持有期間、績效等其他相關資訊</li>
                  <li>7. 其他管理、優化投資老司機平台所需之必要資訊</li>
                </ul>
                <p>
                  本人瞭解本人所指示授權提供之上開個人資料均為本人之個人資料，無涉他人之個人資料；　貴公司僅係依據本人基於運用自身個人資料之權利及意願，受本人指示將上開資料提供予鉅亨網股份有限公司，之後產生之任何個人資料之處理、使用及利用行為，概與　貴公司無涉；本人亦了解本人之個人資料，均受個人資料保護法之保護，本人可隨時透過鉅亨網股份有限公司，要求　貴公司停止上開本人個人資料之提供，　貴公司不得拒絕；惟本人亦了解若停止上開本人個人資料之提供，將導致本人無法繼續參與鉅亨網股份有限公司「投資老司機」平台。
                </p>
                <p>此致　鉅亨網證券投資顧問股份有限公司</p>
              </div>
            </div>
            <div className={getStyleName(styles, 'contract-checkbox-wrapper')}>
              <label className={getStyleName(styles, 'contract-checkbox')} htmlFor="contract-checkbox">
                <input name="contract-checkbox" id="contract-checkbox" type="checkbox" required />
                本人已詳細審閱並同意指示授權以上內容。
              </label>
            </div>
            <button
              className={cx(
                getStyleName(styles, 'contract-submit-button'),
                getStyleName(styles, 'button-submit'),
                getStyleName(styles, 'button-md')
              )}
              type="submit"
            >
              確認
            </button>
          </form>
        </main>
        <Footer />
      </div>
    );
  }
}
