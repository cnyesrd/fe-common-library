import React from 'react';
import styles from './Header.scss';
import getStyleName from '../../../utils/getStyleName';

export default () => {
  return (
    <header className={getStyleName(styles, 'fundsyes-old-driver-header')}>
      <div className={getStyleName(styles, 'header-content-wrapper')}>
        <div className={getStyleName(styles, 'logo')} />
      </div>
    </header>
  );
};
