```
import AccountMenu from 'fe-common-library/dest/components/AccountMenu'
import 'fe-common-library/dest/components/AccountMenu/style.css';

...

render() {
  return (
    <Header
      channel="新聞"
      navs={navs}
      location={window.location}
      customMenu={
        <AccountMenu
          handleClick={click}
          name="Anthony Edward Tony Stark"
          avatar="https://www.impericon.com/360x520x85/media/catalog/product/a/b/abyacc243_lg.jpg"
          isLogin
          highlight={true}
        />
      }
    />
  );
}
```