/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { withNotes } from '@storybook/addon-notes';
import * as knobs from '@storybook/addon-knobs/react';
import Header from '../../Header/Header';
import { MobileMenu } from '../../MobileHeader';
import defaultNotes from './AccountMenu.md';
import AccountMenu from '../AccountMenu';
import styles from './Container.scss';
import iconSearch from './icon-search.svg';

const navs = [
  {
    title: '首頁',
    url: 'https://www.cnyes.com',
  },
  {
    title: '新聞',
    url: 'https://news.cnyes.com/',
    catSlug: 'news',
    isHeadline: true,
    leftList: [
      { title: '頭條', url: 'https://news.cnyes.com/news/cat/headline' },
      { title: '總覽', url: 'https://news.cnyes.com/news/cat/all' },
      { title: '人氣', url: 'https://news.cnyes.com/trending' },
      { title: '虛擬貨幣', url: 'https://news.cnyes.com/search?group=1&q=虛擬貨幣' },
    ],
  },
];

// eslint-disable-next-line react/prop-types
const DesktopHeaderWrapper = ({ menu }) => {
  return (
    <div>
      <Header channel="新聞" navs={navs} location={window.location} customMenu={menu} />
      <div className={styles.content}>long content here...</div>
    </div>
  );
};
// eslint-disable-next-line react/prop-types
const MobileHeaderWrapper = ({ menu }) => {
  return (
    <div className={styles.mobileWrapper}>
      <header className={styles.mobileHeader}>
        <MobileMenu />
        <nav className={styles.mobileLinks}>
          <a>外匯</a>
          <a>市場</a>
          <a>虛擬貨幣</a>
        </nav>
        <div className={styles.accountWrapper}>{menu}</div>
        <div className={styles.search}>
          <img src={iconSearch} alt="search" />
        </div>
      </header>
    </div>
  );
};

const stories = storiesOf('AccountMenu', module);

stories.addDecorator(knobs.withKnobs);

stories.add(
  'Desktop: un-login',
  withNotes(defaultNotes)(
    withInfo()(() => {
      return <DesktopHeaderWrapper menu={<AccountMenu />} />;
    })
  )
);

stories.add(
  'Desktop: un-login highlight',
  withNotes(defaultNotes)(
    withInfo()(() => {
      return <DesktopHeaderWrapper menu={<AccountMenu highlight />} />;
    })
  )
);

stories.add(
  'Desktop: login with avatar',
  withNotes(defaultNotes)(
    withInfo()(() => {
      return (
        <DesktopHeaderWrapper
          menu={
            <AccountMenu
              name="Anthony Edward Tony Stark"
              avatar="https://www.impericon.com/360x520x85/media/catalog/product/a/b/abyacc243_lg.jpg"
              isLogin
            />
          }
        />
      );
    })
  )
);

stories.add(
  'Desktop: custom popup',
  withNotes(defaultNotes)(
    withInfo()(() => {
      return (
        <DesktopHeaderWrapper
          menu={
            <AccountMenu
              name="Anthony Edward Tony Stark"
              avatar="https://www.impericon.com/360x520x85/media/catalog/product/a/b/abyacc243_lg.jpg"
              isLogin
              popup={{
                row: [
                  [
                    { name: '選單A', link: 'https://www.cnyes.com' },
                    { name: '選單B', link: 'https://www.cnyes.com' },
                    { name: '選單C', link: 'https://www.cnyes.com' },
                  ],
                  [
                    { name: '選單A', link: 'https://www.cnyes.com' },
                    { name: '選單B', link: 'https://www.cnyes.com' },
                    { name: '選單C', link: 'https://www.cnyes.com' },
                  ],
                  [{ name: '會員中心', link: 'https://www.cnyes.com' }, { name: '登出', onClick: () => alert('登出') }],
                ],
              }}
            />
          }
        />
      );
    })
  )
);

stories.add(
  'Desktop: login wo avatar',
  withNotes(defaultNotes)(
    withInfo()(() => {
      return <DesktopHeaderWrapper menu={<AccountMenu name="Anthony Edward Tony Stark" isLogin />} />;
    })
  )
);

stories.add(
  'Mobile: un-login',
  withNotes(defaultNotes)(
    withInfo()(() => {
      return <MobileHeaderWrapper menu={<AccountMenu hideName />} />;
    })
  )
);

stories.add(
  'Mobile: un-login highlight',
  withNotes(defaultNotes)(
    withInfo()(() => {
      return <MobileHeaderWrapper menu={<AccountMenu hideName highlight />} />;
    })
  )
);

stories.add(
  'Mobile: login with avatar',
  withNotes(defaultNotes)(
    withInfo()(() => {
      return (
        <MobileHeaderWrapper
          menu={
            <AccountMenu
              align="right"
              avatar="https://www.impericon.com/360x520x85/media/catalog/product/a/b/abyacc243_lg.jpg"
              hideName
              isLogin
              popup={{
                row: [[{ name: '登出', onClick: () => alert('登出') }]],
              }}
            />
          }
        />
      );
    })
  )
);

stories.add(
  'Mobile: login wo avatar',
  withNotes(defaultNotes)(
    withInfo()(() => {
      return <MobileHeaderWrapper menu={<AccountMenu hideName isLogin />} />;
    })
  )
);
