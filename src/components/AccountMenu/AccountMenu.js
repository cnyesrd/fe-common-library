/* eslint-disable react/no-array-index-key */
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames/bind';
import styles from './AccountMenu.scss';
import UserDefault from '../../assets/user-default.svg';
import UserLogin from '../../assets/user-login.svg';

const cx = classnames.bind(styles);

export default class AccountMenu extends PureComponent {
  static propTypes = {
    isLogin: PropTypes.bool,
    highlight: PropTypes.bool,
    hideName: PropTypes.bool,
    name: PropTypes.string,
    avatar: PropTypes.string,
    handleClick: PropTypes.func,
    align: PropTypes.string,
    popup: PropTypes.shape({
      items: PropTypes.arrayOf(
        PropTypes.shape({
          name: PropTypes.string,
          onClick: PropTypes.func,
        })
      ),
    }),
  };

  static defaultProps = {
    isLogin: false,
    highlight: false,
    hideName: false,
    name: null,
    avatar: null,
    handleClick: () => {},
    popup: null,
    align: 'left',
  };

  componentDidUpdate() {
    this.renderPopup();
  }

  rect = { left: 0, top: 0 };

  renderPopup = () => {
    const { popup, align } = this.props;

    if (popup && popup.row) {
      return (
        <ul className={cx('account-menu__popup', `account-menu__popup-${align}`)}>
          {popup.row.map((row, rowIndex) => (
            <li>
              {row.map(({ className, style, onClick, link, name }, index) => (
                <a
                  key={`${rowIndex}_${index}`}
                  className={className}
                  style={style}
                  onClick={onClick}
                  href={link}
                  data-tid={`account-popup-${index}`}
                >
                  {name}
                </a>
              ))}
            </li>
          ))}
        </ul>
      );
    }

    return null;
  };

  render() {
    const { isLogin, hideName, name, avatar, handleClick, highlight } = this.props;
    const displayName = isLogin ? name : '登入';
    const userAvatar = avatar || UserLogin;
    const displayAvatar = isLogin ? userAvatar : UserDefault;

    return (
      <div
        ref={ref => {
          this.ref = ref;
        }}
        className={cx('account-menu__wrapper', { withHighlight: highlight })}
        onClick={handleClick}
        data-tid={isLogin ? 'account-logout' : 'account-login'}
      >
        <img className={cx('account-menu__avatar', { withAvatar: !!avatar })} src={displayAvatar} alt={displayName} />
        {!hideName && <span className={cx('account-menu__name')}>{displayName}</span>}
        {isLogin && this.renderPopup()}
      </div>
    );
  }
}
