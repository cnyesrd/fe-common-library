/* eslint-disable quotes */
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { accountNavsType } from '../../utils/propTypes';
import styles from './AccountPanel.scss';
import getStyleName from '../../utils/getStyleName';
import UserDefault from '../../assets/user-default.svg';
import UserLogin from '../../assets/user-login.svg';
import MoreIcon from './icon_more.svg';
import AnueIcon from './icon_anue_rainbow.svg';
import Config from './config/navs.json';

const getNavConfig = (navCofig, cnyesBaseUrl) => {
  const nav = navCofig || Config.items;
  const replaceLink = str => str.replace('{cnyesBaseUrl}', cnyesBaseUrl);

  nav.forEach((item, i) => {
    // defaultUrl
    nav[i].defaultUrl = replaceLink(item.defaultUrl);
    // notify
    if (item.notify) {
      item.notify.forEach((n, j) => {
        const gotoUrl = nav[i].notify[j].gotoUrl;

        nav[i].notify[j].gotoUrl = replaceLink(gotoUrl);
      });
    }
  });

  return nav;
};

const timeout = 3000;

export default class AccountPanel extends PureComponent {
  static propTypes = {
    isOpen: PropTypes.bool,
    isLogin: PropTypes.bool,
    profile: PropTypes.shape({
      identity_id: PropTypes.string,
      name: PropTypes.string,
      avatar: PropTypes.string,
    }),
    login: PropTypes.func,
    logout: PropTypes.func,
    highlights: PropTypes.arrayOf(PropTypes.string),
    onClosePanel: PropTypes.func,
    onClickNav: PropTypes.func,
    cnyesBaseUrl: PropTypes.string.isRequired,
    navs: accountNavsType,
  };

  static defaultProps = {
    isOpen: false,
    isLogin: false,
    profile: {
      identity_id: '',
      name: '',
      avatar: '',
    },
    onClosePanel: () => {},
    onClickNav: () => {},
    login: () => {},
    logout: () => {},
    highlights: [],
    cnyesBaseUrl: 'cnyes.com',
    navs: undefined,
  };

  constructor(props) {
    super(props);
    this.navConfig = getNavConfig(props.navs, props.cnyesBaseUrl);
    this.memberInfoLink = `https://www.${props.cnyesBaseUrl}/member/info`;
    this.accountPanelTimeout = null;
    this.state = {
      isOpenState: false,
    };
    this.onClickClose = this.onClickClose.bind(this);
    this.onClickNav = this.onClickNav.bind(this);
  }

  componentDidMount() {
    this.accountPanelRef.addEventListener('mouseleave', this.handleHoverOff);
    this.accountPanelRef.addEventListener('mouseenter', this.handleHoverOn);
  }

  componentWillReceiveProps(nextProps) {
    const { isOpen } = nextProps;
    const { isOpenState } = this.state;

    if (isOpen !== isOpenState) {
      this.setState({ isOpenState: isOpen });
    }
  }

  componentWillUnmount() {
    this.accountPanelRef.removeEventListener('mouseleave', this.handleHoverOff);
    this.accountPanelRef.removeEventListener('mouseenter', this.handleHoverOn);
    this.handleClearTimeout();
  }

  onClickClose() {
    this.setState({ isOpenState: false });
    this.props.onClosePanel();
    this.handleClearTimeout();
  }

  onClickNav() {
    this.setState({ isOpenState: false });
    this.props.onClickNav();
  }

  handleHoverOn = () => {
    this.handleClearTimeout();
  };

  handleHoverOff = () => {
    this.accountPanelTimeout = setTimeout(this.onClickClose, timeout);
  };

  handleClearTimeout = () => {
    if (this.accountPanelTimeout) {
      clearTimeout(this.accountPanelTimeout);
    }
  };

  renderMemberInfo() {
    const { isLogin, profile, login } = this.props;
    const avatar = profile.avatar;

    if (isLogin) {
      const userAvatar = avatar || UserLogin;

      return (
        <a href={this.memberInfoLink} className={styles.panel__member} data-tid="account-panel-member">
          <img className={styles.panel__member__avatar} src={userAvatar} alt={profile.name} />
          <div className={cx(styles.panel__member__info, styles['panel__member__info--login'])}>
            <div>{profile.name}</div>
            <span>查看個人資訊</span>
          </div>
          <img className={styles.panel__member__more} alt="前往查看個人資訊" src={MoreIcon} />
        </a>
      );
    }

    return (
      <div className={styles.panel__member}>
        <img className={styles.panel__member__avatar} src={UserDefault} alt="登入會員" />
        <div className={styles.panel__member__info}>
          <div>Hello, 鉅亨會員</div>
        </div>
        <button className={styles.panel__member__login} onClick={login} data-tid="account-panel-login">
          登入
        </button>
      </div>
    );
  }

  render() {
    const { isLogin, logout, highlights } = this.props;
    const { isOpenState } = this.state;

    return (
      <aside
        className={cx(styles.panel__wrapper, 'theme-gradient', {
          [getStyleName(styles, 'panel__wrapper--active')]: isOpenState,
        })}
        // eslint-disable-next-line no-return-assign
        ref={ref => (this.accountPanelRef = ref)}
      >
        <header>
          <div className={styles.header__title}>
            <img className={styles.header__icon} alt="Anue 會員中心" src={AnueIcon} />
            <span className={styles.header__txt}>會員中心</span>
          </div>
          <button className={styles.header__close} onClick={this.onClickClose} data-tid="account-panel-close" />
        </header>
        <main>
          {this.renderMemberInfo()}
          <nav className={styles.panel__nav}>
            {this.navConfig.map(item => {
              let isHighlight = false;
              let link = item.defaultUrl;

              if (item.notify) {
                const notify = item.notify;
                const filteredNotify = notify.filter(n => highlights.indexOf(n.id) > -1);

                if (filteredNotify.length > 0) {
                  isHighlight = true;
                  link = filteredNotify[0].gotoUrl;
                }
              }

              return (
                <a
                  className={cx(styles.panel__link, {
                    [getStyleName(styles, 'panel__link--highlight')]: isHighlight,
                  })}
                  key={item.id}
                  href={link}
                  onClick={this.onClickNav}
                  data-tid={`account-panel-${item.id}`}
                >
                  {item.title}
                  <img className={styles.panel__link__more} alt={`前往${item.title}`} src={MoreIcon} />
                </a>
              );
            })}
            <div className={styles.nav__line} />
            {isLogin && (
              <button className={styles.nav__logout} onClick={logout} data-tid="account-panel-logout">
                登出帳號
              </button>
            )}
          </nav>
        </main>
      </aside>
    );
  }
}
