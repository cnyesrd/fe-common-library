/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { withNotes } from '@storybook/addon-notes';
import { withState } from '@dump247/storybook-state';
import * as knobs from '@storybook/addon-knobs/react';
import Header from '../../Header/Header';
import { MobileMenu } from '../../MobileHeader';
import AccountMenu from '../../AccountMenu/AccountMenu';
import iconSearch from '../../../assets/icon-search.svg';
import AccountPanel from '../AccountPanel';
import defaultNotes from './AccountPanel.md';
import styles from './Container.scss';

const navs = [
  {
    title: '首頁',
    url: 'https://www.cnyes.com',
  },
  {
    title: '新聞',
    url: 'https://news.cnyes.com/',
    catSlug: 'news',
    isHeadline: true,
    leftList: [
      { title: '頭條', url: 'https://news.cnyes.com/news/cat/headline' },
      { title: '總覽', url: 'https://news.cnyes.com/news/cat/all' },
      { title: '人氣', url: 'https://news.cnyes.com/trending' },
      {
        title: '虛擬貨幣',
        url: 'https://news.cnyes.com/search?group=1&q=虛擬貨幣',
      },
    ],
  },
];

// eslint-disable-next-line react/prop-types
const DesktopHeaderWrapper = ({ menu, panel }) => {
  return (
    <div>
      <Header channel="新聞" navs={navs} location={window.location} customMenu={menu} />
      <div className={styles.content}>long content here...</div>
      {panel}
    </div>
  );
};

// eslint-disable-next-line react/prop-types
const MobileHeaderWrapper = ({ menu, panel }) => {
  return (
    <div className={styles.mobileWrapper}>
      <header className={styles.mobileHeader}>
        <MobileMenu />
        <nav className={styles.mobileLinks}>
          <a>外匯</a>
          <a>市場</a>
          <a>虛擬貨幣</a>
        </nav>
        <div className={styles.accountWrapper}>{menu}</div>
        <div className={styles.search}>
          <img src={iconSearch} alt="search" />
        </div>
      </header>
      {panel}
      {'請自行縮小screen...'}
    </div>
  );
};

const stories = storiesOf('AccountPanel', module);

stories.addDecorator(knobs.withKnobs);

stories.add(
  'Desktop: un-login',
  withState({ isOpen: false })(
    withNotes(defaultNotes)(
      withInfo()(({ store }) => {
        return (
          <DesktopHeaderWrapper
            menu={
              <AccountMenu
                handleClick={() => {
                  store.set({ isOpen: !store.state.isOpen });
                }}
              />
            }
            panel={
              <AccountPanel
                isOpen={store.state.isOpen}
                onClosePanel={() => {
                  store.set({ isOpen: false });
                }}
              />
            }
          />
        );
      })
    )
  )
);

stories.add(
  'Desktop: login avatar',
  withState({ isOpen: false })(
    withNotes(defaultNotes)(
      withInfo()(({ store }) => {
        return (
          <DesktopHeaderWrapper
            menu={
              <AccountMenu
                isLogin
                name="Anthony Edward Tony Stark"
                avatar="https://www.impericon.com/360x520x85/media/catalog/product/a/b/abyacc243_lg.jpg"
                handleClick={() => {
                  store.set({ isOpen: !store.state.isOpen });
                }}
              />
            }
            panel={
              <AccountPanel
                isLogin
                isOpen={store.state.isOpen}
                profile={{
                  name: 'Anthony Edward Tony Stark',
                  avatar: 'https://www.impericon.com/360x520x85/media/catalog/product/a/b/abyacc243_lg.jpg',
                }}
                onClosePanel={() => {
                  store.set({ isOpen: false });
                }}
              />
            }
          />
        );
      })
    )
  )
);

stories.add(
  'Desktop: login and highlight',
  withState({ isOpen: false })(
    withNotes(defaultNotes)(
      withInfo()(({ store }) => {
        return (
          <DesktopHeaderWrapper
            menu={
              <AccountMenu
                isLogin
                name="UserName"
                highlight
                handleClick={() => {
                  store.set({ isOpen: !store.state.isOpen });
                }}
              />
            }
            panel={
              <AccountPanel
                isLogin
                isOpen={store.state.isOpen}
                profile={{
                  name: 'UserName',
                  avatar: '',
                }}
                highlights={['myTag']}
                onClosePanel={() => {
                  store.set({ isOpen: false });
                }}
              />
            }
          />
        );
      })
    )
  )
);

stories.add(
  'Mobile: un-login',
  withState({ isOpen: false })(
    withNotes(defaultNotes)(
      withInfo()(({ store }) => {
        return (
          <MobileHeaderWrapper
            menu={
              <AccountMenu
                hideName
                handleClick={() => {
                  store.set({ isOpen: true });
                }}
              />
            }
            panel={
              <AccountPanel
                isOpen={store.state.isOpen}
                onClosePanel={() => {
                  store.set({ isOpen: false });
                }}
              />
            }
          />
        );
      })
    )
  )
);

stories.add(
  'Mobile: login avatar',
  withState({ isOpen: false })(
    withNotes(defaultNotes)(
      withInfo()(({ store }) => {
        return (
          <MobileHeaderWrapper
            menu={
              <AccountMenu
                isLogin
                name="Anthony Edward Tony Stark"
                avatar="https://www.impericon.com/360x520x85/media/catalog/product/a/b/abyacc243_lg.jpg"
                hideName
                handleClick={() => {
                  store.set({ isOpen: true });
                }}
              />
            }
            panel={
              <AccountPanel
                isLogin
                isOpen={store.state.isOpen}
                profile={{
                  name: 'Anthony Edward Tony Stark',
                  avatar: 'https://www.impericon.com/360x520x85/media/catalog/product/a/b/abyacc243_lg.jpg',
                }}
                onClosePanel={() => {
                  store.set({ isOpen: false });
                }}
              />
            }
          />
        );
      })
    )
  )
);

stories.add(
  'Mobile: login and highlight',
  withState({ isOpen: false })(
    withNotes(defaultNotes)(
      withInfo()(({ store }) => {
        return (
          <MobileHeaderWrapper
            menu={
              <AccountMenu
                isLogin
                highlight
                hideName
                handleClick={() => {
                  store.set({ isOpen: true });
                }}
              />
            }
            panel={
              <AccountPanel
                isLogin
                isOpen={store.state.isOpen}
                profile={{
                  name: 'UserName',
                  avatar: '',
                }}
                highlights={['myTag']}
                onClosePanel={() => {
                  store.set({ isOpen: false });
                }}
              />
            }
          />
        );
      })
    )
  )
);
