## Usage

```
import { MobileMenu, MobileNavBoard } from 'fe-common-library/dest/components/MobileHeader';
import DesktopHeader from 'fe-common-library/dest/components/Header';
import AccountMenu from 'fe-common-library/dest/components/AccountMenu';
import AccountPanel from 'fe-common-library/dest/components/AccountPanel';
import 'fe-common-library/dest/components/AccountMenu/style.css';
import 'fe-common-library/dest/components/AccountPanel/style.css';
import 'fe-common-library/dest/components/Header/style.css';
import 'fe-common-library/dest/components/MobileHeader/style.css';

...

render() {
  return (
    <div>
      <DesktopHeader {...others} />
      <header className="mobileHeader">
        <MobileMenu
          showCatBoard={() => {
            toggleLoginButton(false);
            setIsCatBoardOpen(true);
          }}
        />
        <AccountMenu/>
      </header>
      <MobileNavBoard {...others} />
      <AccountPanel 
        isOpen={isAccountPanelOpen} 
        isLogin={isLogin} 
        profile={{
          identity_id: 'userId',
          name: 'userName',
          avatar: 'userAvatar',
        }}
        onClosePanel={() => {}}
        onClickNav={() => {}}
        login={() => {}}
        logout={() => {}}
        highlights={[]}
        cnyesBaseUrl='cnyes.com'
      />
    </div>
  );
}
```
