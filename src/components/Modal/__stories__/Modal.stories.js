/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { withNotes } from '@storybook/addon-notes';
import { withState } from '@dump247/storybook-state';
import * as knobs from '@storybook/addon-knobs/react';
import Modal from '../Modal';
import defaultNotes from './Modal.md';
import Coin from './coin.svg';

const stories = storiesOf('Modal', module);

stories.addDecorator(knobs.withKnobs);

stories.add(
  'Modal',
  withNotes(defaultNotes)(
    withState({ isOpen: true })(
      withInfo()(({ store }) => {
        return (
          <div>
            <Modal.MajorButton onClick={() => store.set({ isOpen: true })}>顯示 Modal</Modal.MajorButton>
            <Modal isOpen={store.state.isOpen}>
              <Modal.Image>
                <img src={Coin} width="100" height="59" alt="提領確認" />
              </Modal.Image>
              <Modal.Title>提領確認</Modal.Title>
              <Modal.Description>確認後將無法取消</Modal.Description>
              <Modal.MinorButton onClick={() => store.set({ isOpen: false })}>取消</Modal.MinorButton>
              <Modal.MajorButton onClick={() => store.set({ isOpen: false })}>確認</Modal.MajorButton>
            </Modal>
          </div>
        );
      })
    )
  )
);

stories.add(
  'Modal - with MinHeight',
  withNotes(defaultNotes)(
    withInfo()(() => {
      return (
        <div>
          <Modal minHeight={80} isOpen>
            loading..
          </Modal>
        </div>
      );
    })
  )
);
