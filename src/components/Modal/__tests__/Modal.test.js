jest.mock('prevent-scroll', () => ({
  on: jest.fn(),
  off: jest.fn(),
}));

/* eslint-disable import/first */
import React from 'react';
import { shallow, mount } from 'enzyme';
import preventScroll from 'prevent-scroll';
import Modal from '../Modal';

describe('component <Modal />', () => {
  let makeSubject;

  beforeEach(() => {
    jest.resetModules();

    const testProps = {
      isOpen: false,
    };

    makeSubject = (params = {}) => {
      const props = {
        ...testProps,
        ...params,
      };

      return shallow(<Modal {...props} />);
    };
  });

  it('should render modal wrapper', () => {
    const subject = makeSubject();

    expect(subject.find('.modal-wrapper').length).toBe(1);
  });

  it('should be same as snapshot', () => {
    const subject = makeSubject();

    expect(subject).toMatchSnapshot();
  });

  describe('modal wrapper', () => {
    it('should has className: invisible if state: isOpen = false', () => {
      const subject = makeSubject();

      expect(subject.find('.modal-wrapper').hasClass('invisible')).toBeTruthy();
      expect(subject.find('.modal-wrapper').hasClass('visible')).toBeFalsy();
    });

    it('should has className: visible if state: isOpen = true', () => {
      const subject = makeSubject({ isOpen: true });

      expect(subject.find('.modal-wrapper').hasClass('invisible')).toBeFalsy();
      expect(subject.find('.modal-wrapper').hasClass('visible')).toBeTruthy();
    });

    it('should pass id to wrapper', () => {
      const id = 'my-modal';
      const subject = makeSubject({ id });

      expect(subject.find('.modal-wrapper').prop('id')).toBe(id);
    });

    it('should append className to wrapper', () => {
      const className = 'class1 class2';
      const subject = makeSubject({ className });

      expect(subject.find('.modal-wrapper').hasClass('invisible')).toBeTruthy();
      expect(subject.find('.modal-wrapper').hasClass('class1')).toBeTruthy();
      expect(subject.find('.modal-wrapper').hasClass('class2')).toBeTruthy();
    });
  });

  describe('preventScroll', () => {
    it('should call on() if prop: isOpen change', () => {
      const subject = makeSubject();

      expect(preventScroll.on).not.toHaveBeenCalled();
      expect(preventScroll.off).not.toHaveBeenCalled();

      subject.setProps({ isOpen: true });
      expect(preventScroll.on).toHaveBeenCalledTimes(1);
      expect(preventScroll.off).not.toHaveBeenCalled();

      subject.setProps({ isOpen: false });
      expect(preventScroll.on).toHaveBeenCalledTimes(1);
      expect(preventScroll.off).toHaveBeenCalledTimes(1);
    });
  });
});

describe('component <Modal.Title />', () => {
  let makeSubject;

  beforeEach(() => {
    jest.resetModules();

    const testProps = {
      children: 'title',
    };

    makeSubject = (params = {}) => {
      const props = {
        ...testProps,
        ...params,
      };

      return shallow(<Modal.Title {...props} />);
    };
  });

  it('should render modal title', () => {
    const subject = makeSubject();

    expect(subject.find('.modal__title').length).toBe(1);
  });

  it('should be same as snapshot', () => {
    const subject = makeSubject();

    expect(subject).toMatchSnapshot();
  });
});

describe('component <Modal.Description />', () => {
  let makeSubject;

  beforeEach(() => {
    jest.resetModules();

    const testProps = {
      children: 'description',
    };

    makeSubject = (params = {}) => {
      const props = {
        ...testProps,
        ...params,
      };

      return shallow(<Modal.Description {...props} />);
    };
  });

  it('should render modal description', () => {
    const subject = makeSubject();

    expect(subject.find('.modal__description').length).toBe(1);
  });

  it('should be same as snapshot', () => {
    const subject = makeSubject();

    expect(subject).toMatchSnapshot();
  });
});

describe('component <Modal.MajorButton />', () => {
  let makeSubject;

  beforeEach(() => {
    jest.resetModules();

    const testProps = {
      children: 'major-button',
      onClick: jest.fn(),
      disabled: false,
    };

    makeSubject = (params = {}) => {
      const props = {
        ...testProps,
        ...params,
      };

      return mount(<Modal.MajorButton {...props} />);
    };
  });

  it('should render modal major button', () => {
    const subject = makeSubject();

    expect(subject.find('.modal__button--major').length).toBe(1);
  });

  it('should call onClick when clicked', () => {
    const subject = makeSubject();

    expect(subject.props().onClick).not.toHaveBeenCalled();
    subject.find('.modal__button--major').simulate('click');
    expect(subject.props().onClick).toHaveBeenCalled();
  });

  it('should be same as snapshot', () => {
    const subject = makeSubject();

    expect(subject).toMatchSnapshot();
  });
});

describe('component <Modal.MinorButton />', () => {
  let makeSubject;

  beforeEach(() => {
    jest.resetModules();

    const testProps = {
      children: 'minor-button',
      onClick: jest.fn(),
      disabled: false,
    };

    makeSubject = (params = {}) => {
      const props = {
        ...testProps,
        ...params,
      };

      return mount(<Modal.MinorButton {...props} />);
    };
  });

  it('should render modal minor button', () => {
    const subject = makeSubject();

    expect(subject.find('.modal__button--minor').length).toBe(1);
  });

  it('should call onClick when clicked', () => {
    const subject = makeSubject();

    expect(subject.props().onClick).not.toHaveBeenCalled();
    subject.find('.modal__button--minor').simulate('click');
    expect(subject.props().onClick).toHaveBeenCalled();
  });

  it('should be same as snapshot', () => {
    const subject = makeSubject();

    expect(subject).toMatchSnapshot();
  });
});
