import React from 'react';
import ReactDOM from 'react-dom/server';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import AllConfig from './components/Header/config/AllConfig';
import headerConfig from './components/Header/config/navs.json';
import './styles/static.scss';

const navs = headerConfig.items;
const allHeaders = Object.keys(AllConfig).reduce((previous, keyName) => {
  const { name, catNavs, ...otherConfig } = AllConfig[keyName];

  return {
    ...previous,
    [`/common/${keyName}Header`]: ReactDOM.renderToString(
      <Header
        navs={navs}
        catNavs={catNavs}
        channel={name}
        fixedHeaderType="FIXED_HEADER_NONE"
        location={{ pathname: '/' }}
        {...otherConfig}
      />
    ),
  };
}, {});

module.exports = function render() {
  return {
    ...allHeaders,
    '/common/footer': ReactDOM.renderToString(<Footer now={new Date().getTime()} />),
  };
};
