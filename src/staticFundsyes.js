import React from 'react';
import ReactDOM from 'react-dom/server';
import LoginPage from './components/FundsyesOldDriver/LoginPage/LoginPage';
import ContractPage from './components/FundsyesOldDriver/ContractPage/ContractPage';

module.exports = function render() {
  return {
    '/fundsyes/LoginPage': ReactDOM.renderToString(<LoginPage />),
    '/fundsyes/ContractPage': ReactDOM.renderToString(<ContractPage />),
  };
};
