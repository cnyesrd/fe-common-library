// @flow

import cnyesAuth from './cnyesAuth';
import syncAuthWithStore from './syncAuthWithStore';

export default {
  syncAuthWithStore,
  init: cnyesAuth.init,
  loginFB: cnyesAuth.loginFB,
  loginGoogle: cnyesAuth.loginGoogle,
  logout: cnyesAuth.logout,
  getToken: cnyesAuth.getToken,
  refreshToken: cnyesAuth.refreshToken,
  getProfile: cnyesAuth.getProfile,
  showLogin: cnyesAuth.showLogin,
  hideLogin: cnyesAuth.hideLogin,
};
