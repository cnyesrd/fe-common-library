// @flow

/**
 * Google Sign-in for websites document: https://developers.google.com/identity/sign-in/web/reference
 *   manager linked applications: https://myaccount.google.com/permissions
 */
import SimpleThenable from '../SimpleThenable';

import type { AuthResponseType, ProviderInitParamsType, LoginInfo, AuthCoreType } from '../types';

import type { GoogleUserType, GoogleAuthResponseType, GoogleAuthType, GoogleApiAuth2InitParam } from './googleType';

// eslint-disable-next-line no-unused-vars
type GoogleApi = {
  auth2: {
    init(params: GoogleApiAuth2InitParam): GoogleAuthType,
    getAuthInstance(): GoogleAuthType,
  },
};

declare var gapi: GoogleApi;

let providerLoginPressed: boolean = false;
let providerResponse: ?GoogleAuthResponseType = null;
let providerInited: boolean = false;
let isLogining: boolean = false;

let authCore: AuthCoreType;

let lastLoginId: string = '';
let currentLoginInfo: LoginInfo;

function setLastLoginId(id: string): void {
  lastLoginId = id;
}

function getCurrentLoginInfo(): ?LoginInfo {
  return currentLoginInfo;
}

function handleLogin(googleUser?: GoogleUserType): Promise<AuthResponseType> {
  return new Promise((resolve, reject): void => {
    const user = googleUser || (gapi.auth2 && gapi.auth2.getAuthInstance().currentUser.get()) || undefined;
    const isSignedIn = (user && user.isSignedIn()) || false;

    providerResponse = (isSignedIn && user && user.getAuthResponse()) || undefined;

    if (!providerResponse || !providerResponse.id_token) {
      // not login, do nothing
      reject('googlelogin: google is not signedin');

      return;
    }

    const userID = (user && user.getId()) || '--';

    const info = {
      provider: 'accounts.google.com',
      credentials: providerResponse.id_token,
      temp: providerResponse.access_token,
      userID,
    };

    currentLoginInfo = info;

    authCore.onLogin(info).then(resolve, reject);
  });
}

function statusChangeCallback(googleAuth: GoogleAuthType): void {
  const googleUser =
    (googleAuth && googleAuth.currentUser && googleAuth.currentUser.get()) ||
    (gapi.auth2 && gapi.auth2.getAuthInstance().currentUser.get());

  const isSignedIn = (googleUser && googleUser.isSignedIn()) || false;

  providerResponse = (isSignedIn && googleUser && googleUser.getAuthResponse()) || undefined;

  if (!providerLoginPressed && !`${lastLoginId}`.startsWith('accounts.google.com:')) {
    // not login via google
    return;
  }

  if (isSignedIn) {
    const userID = googleUser && googleUser.getBasicProfile().getId();

    if (lastLoginId && `${lastLoginId}` !== `accounts.google.com:${userID}`) {
      // userId not same, it should logout!
      authCore.doLogout();

      return;
    }

    if (!isLogining) {
      handleLogin(googleUser).then(undefined, () => {
        authCore.doLogout();
      });
    }
  } else {
    authCore.publishEvent('auth.authResponseChange', { status: 'unknown' });
  }
}

function init(params: ProviderInitParamsType): void {
  lastLoginId = params.lastLoginId;
  authCore = params.authCore;

  global.gAsyncInited = new SimpleThenable();
  global.gAsyncInit = window.gAsyncInited.resolve;

  let asyncInited = false;

  window.gAsyncInited.then(() => {
    if (asyncInited) {
      return;
    }
    asyncInited = true;

    gapi.auth2
      .init({
        client_id: params.appId,
        apiKey: params.appKey,
        scope: 'profile',
      })
      .then(() => {
        providerInited = true;
        const googleAuthInstance = gapi.auth2.getAuthInstance();

        googleAuthInstance.currentUser.listen(statusChangeCallback);
        if (googleAuthInstance.isSignedIn.get()) {
          statusChangeCallback();
        }

        // FT-258 make a custom callback for any action wanna occur right after auth2 is inited.
        if (window.__gapiAuth2Inited) {
          window.__gapiAuth2Inited();
        }
      });

    if (window.__myInit) {
      window.__myInit();
    }
  });
}

function login(): Promise<AuthResponseType> {
  providerLoginPressed = true;
  let _promise;

  if (providerInited) {
    isLogining = true;
    _promise = gapi.auth2
      .getAuthInstance()
      .signIn()
      .then(handleLogin);
  }

  if (_promise) {
    _promise.then(
      () => {
        isLogining = false;
      },
      () => {
        isLogining = false;
      }
    );

    return _promise.then(undefined, (): AuthResponseType => {
      authCore.doLogout();

      return authCore.getAuthResponse();
    });
  }

  return Promise.resolve({ status: 'unknown' });
}

function logout(): void {
  const auth = gapi.auth2 && gapi.auth2.getAuthInstance && gapi.auth2.getAuthInstance();

  if (auth) {
    auth.signOut();
  }
}

export default {
  init,
  login,
  logout,
  handleLogin,
  setLastLoginId,
  getCurrentLoginInfo,
};
