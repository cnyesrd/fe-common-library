// @flow
import type { AuthResponseType, ProviderInitParamsType, LoginInfo, AuthCoreType } from '../types';

type FBAuthResponseType = {
  accessToken: string,
  userID: string,
  expiresIn: number,
  signedRequest: string,
};

type FBLoginStatusType =
  | { status: 'connected', authResponse: FBAuthResponseType }
  | { status: 'unknown' }
  | { status: 'not_authorized' };

let providerLoginPressed: boolean = false;
let providerResponse: ?FBLoginStatusType = null;
let providerInited: boolean = false;
let isLogining: boolean = false;

let lastLoginId: string = '';
let currentLoginInfo: LoginInfo;

let authCore: AuthCoreType;

function setLastLoginId(id: string): void {
  lastLoginId = id;
}

function getCurrentLoginInfo(): ?LoginInfo {
  return currentLoginInfo;
}

/**
 * reject when failed
 */
function handleLogin(loginStatus?: FBLoginStatusType): Promise<AuthResponseType> {
  return new Promise((resolve, reject): void => {
    const loginStatus2 = loginStatus || providerResponse;

    const authResponse = loginStatus2 && loginStatus2.status === 'connected' && loginStatus2.authResponse;

    if (!authResponse) {
      // not login, do nothing
      reject('fblogin: fb is not connected');

      return;
    }

    const info = {
      provider: 'graph.facebook.com',
      credentials: authResponse.accessToken,
      userID: authResponse.userID,
    };

    currentLoginInfo = info;

    authCore.onLogin(info).then(resolve, reject);
  });
}

function fbStatusChangeCallback(response: FBLoginStatusType): void {
  providerResponse = response;
  if (!providerLoginPressed && !`${lastLoginId}`.startsWith('graph.facebook.com:')) {
    // not login via fb
    return;
  }

  if (response.status === 'connected') {
    const userID = (response && response.authResponse && response.authResponse.userID) || '';

    if (lastLoginId && `${lastLoginId}` !== `graph.facebook.com:${userID}`) {
      // fb userId not same, it should logout!
      authCore.doLogout();

      return;
    }

    if (!isLogining) {
      handleLogin(response).catch(() => {}); // catch the reject
    }
  } else if (response.status === 'not_authorized') {
    authCore.publishEvent('auth.authResponseChange', { status: 'not_authorized' });
  } else {
    authCore.publishEvent('auth.authResponseChange', { status: 'unknown' });
  }
}

function init(params: ProviderInitParamsType): void {
  lastLoginId = params.lastLoginId;
  authCore = params.authCore;
  // FB init {

  let fbAsyncInited = false;

  window.fbAsyncInit = function fbAsyncInit(): void {
    if (fbAsyncInited) {
      return;
    }
    fbAsyncInited = true;
    window.FB.Event.subscribe('auth.authResponseChange', fbStatusChangeCallback);

    window.FB.init({
      appId: params.appId,
      cookie: false, // enable cookies to allow the server to access the session
      xfbml: false, // parse social plugins on this page
      version: 'v2.7', // use graph api version 2.7
      status: true,
    });
    providerInited = true;

    if (window.__myInit) {
      window.__myInit();
    }
  };
  if (window.FB && !fbAsyncInited) {
    window.fbAsyncInit();
  }
  // }
}

function logout(): void {}

function login(): Promise<AuthResponseType> {
  providerLoginPressed = true;
  let _promise;

  // dont use cached providerResponse, it would failed if user logout FB
  if (providerInited) {
    _promise = new Promise(resolve => {
      isLogining = true;
      window.FB.login(resolve, { scope: 'email,user_birthday' });
    }).then(handleLogin);
  }

  if (_promise) {
    _promise.then(
      () => {
        isLogining = false;
      },
      () => {
        isLogining = false;
      }
    );

    return _promise.catch((): AuthResponseType => {
      authCore.doLogout(); // registration error, need to logout first

      return authCore.getAuthResponse();
    });
  }

  return Promise.resolve({ status: 'unknown' });
}

export default {
  init,
  login,
  logout,
  handleLogin,
  setLastLoginId,
  getCurrentLoginInfo,
  fbStatusChangeCallback,
};
