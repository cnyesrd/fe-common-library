// https://developers.google.com/identity/sign-in/web/reference
export type GoogleAuthResponseType = {
  access_token: string,
  expires_at: number,
  expires_in: number,
  first_issued_at: number,
  id_token: string,
  idpId: string,
  login_hint: string,
  scope: string,
  token_type: string,
  // session_state:
};

export type GoogleAuthSignInParam = {
  app_package_name?: string,
  fetch_basic_profile?: boolean,
  prompt?: string,
  scope?: string,
};

export type GoogleBasicProfileType = {
  getId(): string,
  getName(): string,
  getGivenName(): string,
  getFamilyName(): string,
  getImageUrl(): string,
  getEmail(): string,
};

export type GoogleUserType = {
  getId(): string,
  isSignedIn(): boolean,
  getHostedDomain(): string,
  getGrantedScopes(): string,
  getBasicProfile(): GoogleBasicProfileType,
  getAuthResponse(): GoogleAuthResponseType,
  reloadAuthResponse(): Promise<GoogleAuthResponseType>,
  hasGrantedScopes(scopes: string): boolean,
  grant(options?: GoogleAuthSignInParam): Promise<*>,
  grantOfflineAccess(options: Object): Promise<*>,
  disconnect(): void,
};

export type GoogleAuthType = {
  then(onInit: Function): void,
  signIn(options?: GoogleAuthSignInParam): Promise<*>,
  signOut(): Promise<*>,
  disconnect(): void,
  isSignedIn: {
    get(): boolean,
    listen(listener: Function): void,
  },
  currentUser: {
    get(): GoogleUserType,
    listen(listener: Function): void,
  },
  grantOfflineAccess(options: Object): Promise<*>,
  attachClickHandler(container: string, options: Object, onsuccess: Function, onfailure: Function): void,
};

export type GoogleApiAuth2InitParam = {
  client_id: string,
  cookie_policy?: string,
  scope?: string,
  fetch_basic_profile?: boolean,
  hosted_domain?: string,
  openid_realm?: string,
};
