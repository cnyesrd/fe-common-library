// @flow

import type { AuthCoreType } from '../../types';

const __verifyCredentials = (info, cb) => {
  let payload;

  if (info && info.credentials && info.credentials.startsWith('signed-')) {
    payload = {
      Token: `cognito-${info.credentials}`,
    };
    cb(undefined, payload);
  } else {
    cb('error');
  }
};

const publishEvent = jest.fn();

export default function authCoreFactory(): AuthCoreType {
  let _authResponse = { status: 'unknown' };
  let _currentLoginMethod;

  const _retriveCognitoOpenId = (userID, firsttime, info) => {
    return new Promise((resolve, reject) => {
      __verifyCredentials(info, (err, token) => {
        if (err || !token) {
          reject(err);

          return;
        }

        if (firsttime) {
          publishEvent('profile.change', { name: 'fakeProfile' });
        }

        _currentLoginMethod = info.provider;
        _authResponse = {
          status: 'connected',
          token: `cognito-${info.credentials}`,
        };
        publishEvent('auth.authResponseChange', _authResponse);
        publishEvent('auth.statusChange', _authResponse);
        publishEvent('auth.login', _authResponse.token);

        resolve(_authResponse);
      });
    });
  };

  const _authCore = {
    onLogin: jest.fn(info => {
      let needPublishAuthEvent = false;
      const _promise = new Promise((resolve, reject) => {
        if (_currentLoginMethod === info.provider) {
          _retriveCognitoOpenId(undefined, undefined, info).then(resolve, reject);

          return;
        } else if (_currentLoginMethod) {
          reject(`${info.provider}: already login via other provider`);

          return;
        }

        if (_authResponse.status !== 'connected') {
          needPublishAuthEvent = true;
          _authCore.publishEvent('auth.init');
        }

        _retriveCognitoOpenId(info.userID, true, info).then(resolve, reject);
      });

      _promise.then(
        () => {
          if (needPublishAuthEvent) _authCore.publishEvent('auth.inited');
        },
        () => {
          if (needPublishAuthEvent) _authCore.publishEvent('auth.inited');
        }
      );

      return _promise;
    }),
    doLogout: jest.fn(() => {
      _authResponse = { status: 'unknown' };
      _authCore.publishEvent('auth.authResponseChange', _authResponse);
      _authCore.publishEvent('auth.statusChange', _authResponse);
      _authCore.publishEvent('auth.logout');
    }),
    getAuthResponse: jest.fn(() => _authResponse),
    publishEvent,
  };

  return _authCore;
}
