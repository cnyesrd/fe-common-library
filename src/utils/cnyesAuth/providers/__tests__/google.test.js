/* eslint-disable import/first,func-names,global-require,prefer-arrow-callback */

// process.on('unhandledRejection', (reason, p) => {
//   console.log('Unhandled Rejection at: Promise', p, 'reason:', reason);
//   // application specific logging, throwing an error, or other logic here
// });

function makeResolveAfter(msec) {
  return new Promise(resolve => {
    setTimeout(resolve, msec);
  });
}

describe('cnyesAuth/providers/google', () => {
  let googleProvider;
  let gapiLoader;
  let authCore;
  let initParams;
  let makeSubjectPromise;

  let userID;
  let accessToken;
  let idToken;
  let lastUserID;

  beforeEach(() => {
    jest.resetModules();
    googleProvider = require('../google').default;
    gapiLoader = require('./gapiLoader').default;
    authCore = require('./authCoreFactory').default();
    window.__myInit = undefined;
    initParams = {
      appId: 'google-app-id',
      appKey: 'google-app-key',
      lastLoginId: '',
      authCore,
    };
    userID = undefined;
    accessToken = undefined;
    idToken = undefined;
    lastUserID = undefined;

    makeSubjectPromise = () => {
      const _promise = new Promise(resolve => {
        window.__myInit = function() {
          setTimeout(resolve, 50);
        };
      });

      window.gAsyncInit = jest.fn();

      googleProvider.init(initParams);
      gapiLoader('gAsyncInit');

      return _promise;
    };
  });

  describe('#gAsyncInit', () => {
    beforeEach(() => {
      window.__gapiBehaviors = {
        initSignin: false,
        signin: false,
      };

      makeSubjectPromise = func => {
        const _promise = new Promise(resolve => {
          window.__myInit = function() {
            setTimeout(resolve, 50);
          };
        });

        window.gAsyncInit = jest.fn();

        func();

        return _promise;
      };
    });

    describe('when gapi load', () => {
      it('should call fake gAsyncInit', () => {
        return makeSubjectPromise(() => {
          gapiLoader('gAsyncInit');
          window.__myInit();
        }).then(() => {
          expect(window.gAsyncInit).toHaveBeenCalledTimes(1);
          expect(window.gAsyncInit).toHaveBeenCalledWith();
        });
      });

      it('should have empyt gapi.auth2 first and then have gapi.auth2 instance', () => {
        return makeSubjectPromise(() => {
          gapiLoader();
          expect(window.gapi.auth2).toBeUndefined();
          window.__myInit();
        }).then(() => {
          expect(window.gapi.auth2).toBeDefined();
          expect(window.gapi.auth2.init).toBeDefined();
        });
      });

      it('should not contain gapi.auth2.init before loaded', () => {
        return makeSubjectPromise(() => {
          googleProvider.init(initParams);
          gapiLoader('gAsyncInit');
        }).then(() => {
          expect(window.gapi.auth2.init).toHaveBeenCalledTimes(1);
          expect(window.gapi.auth2.init).toHaveBeenCalledWith({
            client_id: 'google-app-id',
            apiKey: 'google-app-key',
            scope: 'profile',
          });
        });
      });
    });

    describe('when provider loads before gapi', () => {
      it('should load correctly', () => {
        return makeSubjectPromise(() => {
          googleProvider.init(initParams);
          gapiLoader('gAsyncInit');
        }).then(() => {
          expect(window.gapi.auth2.init).toHaveBeenCalledTimes(1);
          expect(window.gapi.auth2.init).toHaveBeenCalledWith({
            client_id: 'google-app-id',
            apiKey: 'google-app-key',
            scope: 'profile',
          });
        });
      });
    });

    describe('when gapi loads before provider', () => {
      it('should load correctly', () => {
        return makeSubjectPromise(() => {
          gapiLoader('gAsyncInit');
          googleProvider.init(initParams);
        }).then(() => {
          expect(window.gapi.auth2.init).toHaveBeenCalledTimes(1);
          expect(window.gapi.auth2.init).toHaveBeenCalledWith({
            client_id: 'google-app-id',
            apiKey: 'google-app-key',
            scope: 'profile',
          });
        });
      });
    });
  });

  describe('when google isnt connected', () => {
    beforeEach(() => {
      window.__gapiBehaviors = {
        initSignin: false,
        signin: false,
      };
    });

    describe('init', () => {
      it('gapi.auth2.init should be loaded', () => {
        return makeSubjectPromise().then(() => {
          expect(window.gapi.auth2.init).toHaveBeenCalledTimes(1);
          expect(window.gapi.auth2.init).toHaveBeenCalledWith({
            client_id: 'google-app-id',
            apiKey: 'google-app-key',
            scope: 'profile',
          });
        });
      });

      ['publishEvent', 'getAuthResponse', 'onLogin', 'doLogout'].forEach(testTarget => {
        it(`authCore.${testTarget} should not been called`, () => {
          return makeSubjectPromise().then(() => {
            expect(authCore[testTarget]).toHaveBeenCalledTimes(0);
          });
        });
      });
    });

    describe('and do login success', () => {
      let makeSubjectPromise2;
      let loginPromise;

      beforeEach(() => {
        userID = 'user-A';
        accessToken = 'signed-accessToken-A';
        idToken = 'signed-idToken-A';
        lastUserID = `graph.facebook.com:${userID}`;

        loginPromise = undefined;
        makeSubjectPromise2 = () => {
          return makeSubjectPromise().then(() => {
            window.__gapiBehaviors = {
              ...(window.__gapiBehaviors || {}),
              signIn: true,
              nextAuthResponse: {
                access_token: accessToken,
                expires_at: 1484638868307,
                expires_in: 3600,
                id_token: idToken,
                token_type: 'Bearer',
              },
              nextBasicProfile: {
                imageUrl:
                  'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50',
                email: 'email',
                givenName: 'given',
                familyName: 'family',
                name: 'given family',
                id: userID,
              },
            };
            loginPromise = googleProvider.login();

            return Promise.all([loginPromise, makeResolveAfter(2000)]);
          });
        };
      });

      it('should call authCore.onLogin', () => {
        return makeSubjectPromise2().then(
          () => {
            expect(authCore.onLogin).toHaveBeenCalledTimes(1);
            expect(authCore.onLogin).toHaveBeenCalledWith({
              provider: 'accounts.google.com',
              temp: accessToken,
              credentials: idToken,
              userID,
            });
          },
          error => {
            console.error(error);
            throw error;
          }
        );
      });

      it('should call login promise', () => {
        return makeSubjectPromise2().then(() => {
          return loginPromise.then(response => {
            expect(response).toEqual({
              status: 'connected',
              token: `cognito-${idToken}`,
            });
          });
        });
      });
    });

    describe('and do login cancel', () => {});
  });

  describe('when google is connected', () => {
    beforeEach(() => {
      userID = 'user-A';
      accessToken = 'accessToken-A';
      idToken = 'signed-idToken-A';
      lastUserID = `accounts.google.com:${userID}`;

      window.__gapiBehaviors = {
        ...(window.__gapiBehaviors || {}),
        initSignin: true,
        nextAuthResponse: {
          access_token: accessToken,
          expires_at: 1484638868307,
          expires_in: 3600,
          id_token: idToken,
          token_type: 'Bearer',
        },
        nextBasicProfile: {
          imageUrl:
            'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=50',
          email: 'email',
          givenName: 'given',
          familyName: 'family',
          name: 'given family',
          id: userID,
        },
      };
    });

    describe('and login as same last user', () => {
      beforeEach(() => {
        lastUserID = `accounts.google.com:${userID}`;
        initParams.lastLoginId = lastUserID;
      });

      describe('init', () => {
        it('gapi.auth2.init should be loaded', () => {
          return makeSubjectPromise().then(() => {
            expect(window.gapi.auth2.init).toHaveBeenCalledTimes(1);
            expect(window.gapi.auth2.init).toHaveBeenCalledWith({
              client_id: 'google-app-id',
              apiKey: 'google-app-key',
              scope: 'profile',
            });
          });
        });

        it('should call authCore.publishEvent 5 times with auth.init and auth.inited ...', () => {
          return makeSubjectPromise().then(() => {
            expect(authCore.publishEvent).toHaveBeenCalledWith('auth.init');
            expect(authCore.publishEvent).toHaveBeenCalledWith('profile.change', { name: 'fakeProfile' });
            expect(authCore.publishEvent).toHaveBeenCalledWith('auth.authResponseChange', {
              status: 'connected',
              token: 'cognito-signed-idToken-A',
            });
            expect(authCore.publishEvent).toHaveBeenCalledWith('auth.statusChange', {
              status: 'connected',
              token: 'cognito-signed-idToken-A',
            });
            expect(authCore.publishEvent).toHaveBeenCalledWith('auth.login', 'cognito-signed-idToken-A');
            expect(authCore.publishEvent).toHaveBeenCalledWith('auth.inited');
            expect(authCore.publishEvent).toHaveBeenCalledTimes(6);
          });
        });

        it('should call authCore.onLogin 1 time', () => {
          return makeSubjectPromise().then(() => {
            expect(authCore.onLogin).toHaveBeenCalledWith({
              provider: 'accounts.google.com',
              temp: accessToken,
              credentials: idToken,
              userID,
            });
            expect(authCore.onLogin).toHaveBeenCalledTimes(1);
          });
        });

        ['doLogout', 'getAuthResponse'].forEach(testTarget => {
          it(`should not call authCore.${testTarget}`, () => {
            return makeSubjectPromise().then(() => {
              expect(authCore[testTarget]).toHaveBeenCalledTimes(0);
            });
          });
        });
      });
    });
  });
});
