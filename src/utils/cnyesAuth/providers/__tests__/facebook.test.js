/* eslint-disable import/first,func-names,global-require,prefer-arrow-callback */

function makeResolveAfter(msec) {
  return new Promise(resolve => {
    setTimeout(resolve, msec);
  });
}

describe('facebook/providers/facebook', () => {
  let facebookProvider;
  let fbjsloader;
  let authCore;
  let makeSubjectPromise;

  let initParams;
  let userID;
  let accessToken;
  let lastUserID;

  beforeEach(() => {
    jest.resetModules();
    jest.mock('../../fbjsloader');
    facebookProvider = require('../facebook').default;
    fbjsloader = require('../../fbjsloader').default;
    authCore = require('./authCoreFactory').default();
    window.document.head.innerHTML = '<script/>';
    window.__myInit = undefined;
    initParams = {
      appId: 'fb-app-id',
      authCore,
    };
    userID = undefined;
    accessToken = undefined;
    lastUserID = undefined;

    makeSubjectPromise = () => {
      // set hook for inited
      const _promise = new Promise(resolve => {
        window.__myInit = function() {
          setTimeout(resolve, 100);
        };
      });

      fbjsloader();
      facebookProvider.init(initParams);
      window.fbAsyncInit();

      return _promise;
    };
  });

  describe('when facebook isnt connected', () => {
    beforeEach(() => {
      window.__realFBOpts = {};
    });

    describe('init', () => {
      it('FB.init should be loaded', () => {
        return makeSubjectPromise().then(() => {
          expect(window.FB.init).toHaveBeenCalledTimes(1);
          expect(window.FB.init).toHaveBeenCalledWith({
            appId: 'fb-app-id',
            cookie: false,
            xfbml: false,
            version: 'v2.7',
            status: true,
          });
        });
      });

      ['publishEvent', 'getAuthResponse', 'onLogin', 'doLogout'].forEach(testTarget => {
        it(`authCore.${testTarget} should not been called`, () => {
          return makeSubjectPromise().then(() => {
            expect(authCore[testTarget]).toHaveBeenCalledTimes(0);
          });
        });
      });
    });

    describe('and do login success', () => {
      let makeSubjectPromise2;
      let loginPromise;

      beforeEach(() => {
        userID = 'user-A';
        accessToken = 'signed-accessToken-A';
        lastUserID = `graph.facebook.com:${userID}`;

        loginPromise = undefined;
        makeSubjectPromise2 = () => {
          return makeSubjectPromise().then(() => {
            window.realFB._nextAuthResponse = {
              status: 'connected',
              authResponse: {
                accessToken,
                expiresIn: 6000,
                signedRequest: 'signed-A',
                userID,
              },
            };
            loginPromise = facebookProvider.login();

            return makeResolveAfter(500);
          });
        };
      });

      it('should call authCore.onLogin', () => {
        return makeSubjectPromise2().then(() => {
          expect(authCore.onLogin).toHaveBeenCalledTimes(1);
          expect(authCore.onLogin).toHaveBeenCalledWith({
            provider: 'graph.facebook.com',
            credentials: accessToken,
            userID,
          });
        });
      });

      it('should call login promise', () => {
        return makeSubjectPromise2().then(() => {
          return loginPromise.then(response => {
            expect(response).toEqual({
              status: 'connected',
              token: `cognito-${accessToken}`,
            });
          });
        });
      });
    });

    describe('and do login cancel', () => {});
  });

  describe('when facebook is connected', () => {
    beforeEach(() => {
      userID = 'user-A';
      accessToken = 'signed-accessToken-A';
      lastUserID = `graph.facebook.com:${userID}`;

      window.__realFBOpts = {
        _nextAuthResponse: {
          status: 'connected',
          authResponse: {
            accessToken,
            expiresIn: 6000,
            signedRequest: 'signed-A',
            userID,
          },
        },
      };
    });

    describe('and login as same last user', () => {
      beforeEach(() => {
        lastUserID = `graph.facebook.com:${userID}`;
        initParams.lastLoginId = lastUserID;
      });

      describe('init', () => {
        it('FB.init should be loaded', () => {
          return makeSubjectPromise().then(() => {
            expect(window.FB.init).toHaveBeenCalledTimes(1);
            expect(window.FB.init).toHaveBeenCalledWith({
              appId: 'fb-app-id',
              cookie: false,
              xfbml: false,
              version: 'v2.7',
              status: true,
            });
          });
        });

        it('should call authCore.publishEvent 5 times with auth.init and auth.inited ...', () => {
          return makeSubjectPromise().then(() => {
            expect(authCore.publishEvent).toHaveBeenCalledWith('auth.init');
            expect(authCore.publishEvent).toHaveBeenCalledWith('profile.change', { name: 'fakeProfile' });
            expect(authCore.publishEvent).toHaveBeenCalledWith('auth.authResponseChange', {
              status: 'connected',
              token: 'cognito-signed-accessToken-A',
            });
            expect(authCore.publishEvent).toHaveBeenCalledWith('auth.statusChange', {
              status: 'connected',
              token: 'cognito-signed-accessToken-A',
            });
            expect(authCore.publishEvent).toHaveBeenCalledWith('auth.login', 'cognito-signed-accessToken-A');
            expect(authCore.publishEvent).toHaveBeenCalledWith('auth.inited');
            expect(authCore.publishEvent).toHaveBeenCalledTimes(6);
          });
        });

        it('should call authCore.onLogin 1 time', () => {
          return makeSubjectPromise().then(() => {
            expect(authCore.onLogin).toHaveBeenCalledWith({
              provider: 'graph.facebook.com',
              credentials: accessToken,
              userID,
            });
            expect(authCore.onLogin).toHaveBeenCalledTimes(1);
          });
        });

        ['doLogout', 'getAuthResponse'].forEach(testTarget => {
          it(`should not call authCore.${testTarget}`, () => {
            return makeSubjectPromise().then(() => {
              expect(authCore[testTarget]).toHaveBeenCalledTimes(0);
            });
          });
        });
      });
    });

    describe('and login not as same last user', () => {
      beforeEach(() => {
        lastUserID = `graph.facebook.com:${userID}A`;
        initParams.lastLoginId = lastUserID;
      });

      describe('init', () => {
        it('FB.init should be loaded', () => {
          return makeSubjectPromise().then(() => {
            expect(window.FB.init).toHaveBeenCalledTimes(1);
            expect(window.FB.init).toHaveBeenCalledWith({
              appId: 'fb-app-id',
              cookie: false,
              xfbml: false,
              version: 'v2.7',
              status: true,
            });
          });
        });

        it('should call authCore.doLogout 1 time', () => {
          return makeSubjectPromise().then(() => {
            expect(authCore.doLogout).toHaveBeenCalledWith();
            expect(authCore.doLogout).toHaveBeenCalledTimes(1);
          });
        });

        it('should call authCore.publishEvent 3 times', () => {
          return makeSubjectPromise().then(() => {
            expect(authCore.publishEvent).toHaveBeenCalledWith('auth.logout');
            expect(authCore.publishEvent).toHaveBeenCalledWith('auth.statusChange', { status: 'unknown' });
            expect(authCore.publishEvent).toHaveBeenCalledWith('auth.authResponseChange', { status: 'unknown' });
            expect(authCore.publishEvent).toHaveBeenCalledTimes(3);
          });
        });

        ['getAuthResponse', 'onLogin'].forEach(testTarget => {
          it(`should not call authCore.${testTarget}`, () => {
            return makeSubjectPromise().then(() => {
              expect(authCore[testTarget]).toHaveBeenCalledTimes(0);
            });
          });
        });
      });
    });
  });
});
