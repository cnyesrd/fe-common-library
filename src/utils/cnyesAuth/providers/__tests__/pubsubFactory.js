export default function pubsubFactory() {
  const _subscriptions = [];

  return {
    subscribe: jest.fn((name, callback) => {
      _subscriptions[name] = _subscriptions[name] || [];
      _subscriptions[name].push(callback);
    }),
    publish: jest.fn((name, ...messages) => {
      if (_subscriptions[name]) {
        _subscriptions[name].forEach(cb => {
          cb(...messages);
        });
      }
    }),
  };
}
