// @flow

import type { Store } from 'redux';
import cnyesAuth from './cnyesAuth';
import * as actions from './reducer';
import type { AuthResponseType, ProfileType } from './types';

export default function syncAuthWithStore(store: Store<*, *>): void {
  cnyesAuth.Event.subscribe('auth.init', (): void => {
    store.dispatch(actions.init());
  });

  cnyesAuth.Event.subscribe('auth.inited', (): void => {
    store.dispatch(actions.inited());
  });

  cnyesAuth.Event.subscribe('auth.authResponseChange', (msg: string, authResponse: AuthResponseType): void => {
    if (authResponse.status === 'connected') {
      store.dispatch(actions.login(authResponse.token));
    } else {
      store.dispatch(actions.logout());
    }
  });

  cnyesAuth.Event.subscribe('profile.change', (msg: string, profile: ProfileType): void => {
    store.dispatch(actions.profilePut(profile));
  });
}
