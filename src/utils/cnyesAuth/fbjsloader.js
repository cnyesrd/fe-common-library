// @flow

export default function fbjsloader(lang: string = 'zh_TW'): void {
  // Load the SDK asynchronously
  /* eslint-disable */
  (function(d, s, id) {
    var js,
      fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s);
    js.id = id;
    js.src = `https://connect.facebook.net/${lang}/sdk.js`;
    fjs.parentNode && fjs.parentNode.insertBefore(js, fjs);
  })(document, 'script', 'facebook-jssdk');
  /* eslint-enable */
}
