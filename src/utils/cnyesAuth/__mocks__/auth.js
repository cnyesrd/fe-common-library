export default {
  init: jest.fn(),
  loginFB: jest.fn(),
  loginGoogle: jest.fn(),
  logout: jest.fn(),
  showLogin: jest.fn(),
  hideLogin: jest.fn(),
  getToken: jest.fn(),
  refreshToken: jest.fn(),
  getProfile: jest.fn(),
};
