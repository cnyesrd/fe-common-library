/* eslint-disable import/first,func-names,global-require,prefer-arrow-callback */
// @flow

describe('cnyesAuth actions', () => {
  let actions;

  beforeEach(() => {
    jest.resetModules();
    actions = require('../reducer');
  });

  describe('login', () => {
    let token;
    let subject;

    beforeEach(() => {
      token = 'token';
      subject = actions.login(token);
    });

    it('should get AUTH_CONNECTED action', () => {
      expect(subject.type).toBe('cnyes/me/AUTH_CONNECTED');
      expect(subject.token).toBe(token);
    });

    it('should match snapshot', () => {
      expect(subject).toMatchSnapshot();
    });
  });

  describe('logout', () => {
    let subject;

    beforeEach(() => {
      subject = actions.logout();
    });

    it('should get AUTH_LOGOUT action', () => {
      expect(subject.type).toBe('cnyes/me/AUTH_LOGOUT');
    });

    it('should match snapshot', () => {
      expect(subject).toMatchSnapshot();
    });
  });

  describe('init', () => {
    let subject;

    beforeEach(() => {
      subject = actions.init();
    });

    it('should get AUTH_INIT action', () => {
      expect(subject.type).toBe('cnyes/me/AUTH_INIT');
    });

    it('should match snapshot', () => {
      expect(subject).toMatchSnapshot();
    });
  });

  describe('inited', () => {
    let subject;

    beforeEach(() => {
      subject = actions.inited();
    });

    it('should get AUTH_INITED action', () => {
      expect(subject.type).toBe('cnyes/me/AUTH_INITED');
    });

    it('should match snapshot', () => {
      expect(subject).toMatchSnapshot();
    });
  });

  describe('profilePut', () => {
    let profile;
    let subject;

    beforeEach(() => {
      profile = {
        uid: '435436fedf',
        avatar: 'https://example.org/avatar.png',
        name: 'fwef fewwaa',
        gender: '',
        vip: 0,
      };
      subject = actions.profilePut(profile);
    });

    it('should get PROFILE_PUT action', () => {
      expect(subject.type).toBe('cnyes/me/PROFILE_PUT');
      expect(subject.profile).toBe(profile);
    });

    it('should match snapshot', () => {
      expect(subject).toMatchSnapshot();
    });
  });
});
