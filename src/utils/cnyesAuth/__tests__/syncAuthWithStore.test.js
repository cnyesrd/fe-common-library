/* eslint-disable import/first,func-names,global-require,prefer-arrow-callback */

import { AUTH_INIT, AUTH_INITED, AUTH_CONNECTED, AUTH_LOGOUT, PROFILE_PUT } from '../reducer';

describe('syncAuthWithStore', () => {
  let syncAuthWithStore;
  let PubSub;
  let store;

  beforeEach(() => {
    jest.resetModules();

    PubSub = require('pubsub-js');
    const mockCnyesAuth = {
      Event: {
        subscribe: PubSub.subscribe,
      },
    };

    jest.mock('../cnyesAuth', () => mockCnyesAuth);
    syncAuthWithStore = require('../syncAuthWithStore').default;

    store = {
      dispatch: jest.fn(),
    };
    syncAuthWithStore(store);
  });

  describe('profile', () => {
    describe('when event profile.change', () => {
      let profile;

      beforeEach(() => {
        profile = {
          avatar: 'https://example.net/avatar.png',
        };
        PubSub.publishSync('profile.change', profile);
      });

      it('should call store.dispatch', () => {
        expect(store.dispatch).toHaveBeenCalledWith({ type: PROFILE_PUT, profile });
        expect(store.dispatch).toHaveBeenCalledTimes(1);
      });
    });
  });

  describe('auth', () => {
    describe('when event auth.init', () => {
      beforeEach(() => {
        PubSub.publishSync('auth.init');
      });

      it('should call store.dispatch', () => {
        expect(store.dispatch).toHaveBeenCalledWith({ type: AUTH_INIT });
        expect(store.dispatch).toHaveBeenCalledTimes(1);
      });
    });

    describe('when event auth.inited', () => {
      beforeEach(() => {
        PubSub.publishSync('auth.inited');
      });

      it('should call store.dispatch', () => {
        expect(store.dispatch).toHaveBeenCalledWith({ type: AUTH_INITED });
        expect(store.dispatch).toHaveBeenCalledTimes(1);
      });
    });

    describe('when event auth.authResponseChange', () => {
      let authResponse;

      describe('with auth status is connected', () => {
        beforeEach(() => {
          authResponse = {
            status: 'connected',
            token: 'token',
          };
          PubSub.publishSync('auth.authResponseChange', authResponse);
        });

        it('should call store.dispatch', () => {
          expect(store.dispatch).toHaveBeenCalledWith({ type: AUTH_CONNECTED, token: 'token' });
          expect(store.dispatch).toHaveBeenCalledTimes(1);
        });
      });

      describe('with auth status is not connected', () => {
        beforeEach(() => {
          authResponse = {
            status: 'unknown',
          };
          PubSub.publishSync('auth.authResponseChange', authResponse);
        });

        it('should call store.dispatch', () => {
          expect(store.dispatch).toHaveBeenCalledWith({ type: AUTH_LOGOUT });
          expect(store.dispatch).toHaveBeenCalledTimes(1);
        });
      });
    });
  });
});
