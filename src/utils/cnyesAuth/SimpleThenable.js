/* eslint-disable func-names, object-shorthand, no-var, no-plusplus, comma-dangle */
/**
 * this file is used in public/index.html, so keep this simple and excutable without bable on browser
 */

function execute(thenable) {
  var handlers = thenable.callbacks;
  var i;

  for (i = 0; i < handlers.length; i++) {
    handlers[i].execute();
  }
}
function Resolver(cb) {
  this.cb = cb;
  this.executed = false;
}
Resolver.prototype = {
  execute: function() {
    if (!this.executed) {
      this.executed = true;
      this.cb();
    }
  },
};
function SimpleThenable() {
  this.callbacks = [];
  this.state = false;
  this.resolve = this.resolve.bind(this);
}
SimpleThenable.prototype = {
  resolve: function() {
    this.state = true;
    execute(this);
  },
  then: function(cb) {
    this.callbacks.push(new Resolver(cb));
    if (this.state) {
      execute(this);
    }
  },
};

export default SimpleThenable;
