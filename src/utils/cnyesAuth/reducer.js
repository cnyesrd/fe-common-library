// @flow
import type { ProfileType } from './types';

export const AUTH_INIT = 'cnyes/me/AUTH_INIT';
export const AUTH_INITED = 'cnyes/me/AUTH_INITED';
export const AUTH_CONNECTED = 'cnyes/me/AUTH_CONNECTED';
export const AUTH_LOGOUT = 'cnyes/me/AUTH_LOGOUT';
export const PROFILE_MERGE = 'cnyes/me/PROFILE_MERGE';
export const PROFILE_PUT = 'cnyes/me/PROFILE_PUT';
export const PROFILE_CLEAN = 'cnyes/me/PROFILE_CLEAN';

export type ProfileStateType = ProfileType | void;

export type AuthStateType = {
  isFetching: boolean,
  status: 'connected' | 'unknown',
  token: null | string,
};

export type StateType = {
  auth?: AuthStateType,
  profile?: ProfileStateType,
};

export type ActionType = {
  type: string,
  profile?: ProfileType,
  token?: string,
};

export type Reducer<S, A> = (state?: S, action?: A) => S;
export type ProfileReducerType = Reducer<ProfileStateType, ActionType>;

const authInitialState: AuthStateType = {
  isFetching: false,
  status: 'unknown',
  token: null,
};

const profileInitialState: ProfileStateType = undefined;

function authReducer(state?: AuthStateType = authInitialState, action?: ActionType): AuthStateType {
  if (!action) {
    return state;
  }

  switch (action.type) {
    case AUTH_INIT:
      return {
        ...state,
        isFetching: true,
      };
    case AUTH_INITED:
      return {
        ...state,
        isFetching: false,
      };
    case AUTH_CONNECTED:
      return {
        ...state,
        isFetching: false,
        status: 'connected',
        token: action.token,
      };
    case AUTH_LOGOUT:
      return {
        ...state,
        isFetching: false,
        status: 'unknown',
        token: null,
      };
    default:
      return state;
  }
}

function ifChangedCallTrackJs(innerReducer: ProfileReducerType): ProfileReducerType {
  return function doIfChangedCallTrackJs(state?: ProfileStateType, action?: ActionType): ProfileStateType {
    const oldState = state;
    const newState = innerReducer(state, action);

    if (global.trackJs && (oldState && oldState.uid) !== (newState && newState.uid)) {
      global.trackJs.configure({
        userId: newState && newState.uid,
      });
    }

    return newState;
  };
}

function profileReducer(state?: ProfileStateType = profileInitialState, action?: ActionType): ProfileStateType {
  if (!action) {
    return state;
  }

  switch (action.type) {
    case PROFILE_MERGE:
      return {
        ...state,
        ...action.profile,
      };
    case PROFILE_PUT:
      return {
        ...action.profile,
      };
    case AUTH_LOGOUT:
    case PROFILE_CLEAN:
      return profileInitialState;
    default:
      return state;
  }
}

const enhancedProfileReducer = ifChangedCallTrackJs(profileReducer);

export default function reducer(state?: StateType = {}, action?: ActionType): StateType {
  return {
    auth: authReducer(state.auth, action),
    profile: enhancedProfileReducer(state.profile, action),
  };
}

export function init(): ActionType {
  return {
    type: AUTH_INIT,
  };
}

export function inited(): ActionType {
  return {
    type: AUTH_INITED,
  };
}

export function login(token: string): ActionType {
  return {
    type: AUTH_CONNECTED,
    token,
  };
}

export function logout(): ActionType {
  return {
    type: AUTH_LOGOUT,
  };
}

export function profilePut(profile: ProfileType): ActionType {
  return {
    type: PROFILE_PUT,
    profile,
  };
}
