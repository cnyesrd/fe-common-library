// @flow
import typeof { Cookies } from 'react-cookie';

const LOGIN = 'cnl';
const oneDay = 60 * 60 * 24 * 1000;

export default class AuthStore {
  cookie: Cookies;

  constructor(cookie: Cookies) {
    this.cookie = cookie;
  }

  loadLastLoginId(): string {
    return this.cookie.get(LOGIN);
  }

  saveLastLoginId(val: string): void {
    const expiresAt = new Date(+new Date() + 7 * oneDay);

    this.cookie.set(LOGIN, val, { expires: expiresAt, path: '/' });
  }

  destroyLastLoginId(): void {
    this.cookie.remove(LOGIN, { path: '/' });
  }
}
