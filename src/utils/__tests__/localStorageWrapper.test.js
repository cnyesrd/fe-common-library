/* eslint-disable import/first,func-names,global-require,prefer-arrow-callback */

import ls from '../localStorageWrapper';

describe('localStorateWrapper', () => {
  beforeEach(() => {
    localStorage.clear();
  });

  it('.setItem() should be fine', () => {
    ls.setItem('testKey', 'testValue');
    expect(localStorage.__STORE__.testKey).toEqual('testValue');
  });

  it('.getItem() should be fine', () => {
    ls.setItem('testKey', 'testValue');
    expect(ls.getItem('testKey')).toEqual('testValue');
  });

  it('.removeItem() should be fine', () => {
    ls.setItem('testKey', 'testValue');
    ls.removeItem('testKey');
    expect(localStorage.__STORE__.testKey).toBeUndefined();
  });

  it('.filter() should be fine', () => {
    ls.setItem('testKey', 'testValue');
    ls.setItem('testKey2', 'testValue2');
    expect(ls.filter(key => key === 'testKey')).toEqual(['testKey']);
    expect(ls.filter(key => key !== 'testKey')).toEqual(['testKey2']);
  });
});
