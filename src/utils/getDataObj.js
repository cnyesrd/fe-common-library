const getDataObj = titleObj => {
  const dataObj = {};
  const dataRegExp = /^data-.+/;

  // eslint-disable-next-line no-restricted-syntax
  for (const key in titleObj) {
    if (dataRegExp.test(key)) {
      Object.assign(dataObj, { [key]: titleObj[key] });
    }
  }

  return dataObj;
};

export default getDataObj;
