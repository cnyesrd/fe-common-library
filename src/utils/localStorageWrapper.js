export default {
  setItem: (key, value) => {
    try {
      window.localStorage.setItem(key, value);
    } catch (e) {
      // ignore
    }
  },
  getItem: key => {
    try {
      return window.localStorage.getItem(key);
    } catch (e) {
      // ignore
    }

    return null;
  },
  removeItem: key => {
    try {
      window.localStorage.removeItem(key);
    } catch (e) {
      // ignore
    }
  },
  filter: filterFunction => {
    let localStorageLength;

    try {
      localStorageLength = window.localStorage.length;
    } catch (e) {
      // ignore

      return [];
    }

    const keys = [];

    for (let i = 0; i < localStorageLength; i += 1) {
      const key = window.localStorage.key(i);

      if (filterFunction(key)) keys.push(key);
    }

    return keys;
  },
};
