const gaDataset = ({ dataPrefix, category, action, label }) => {
  if (dataPrefix.length === 0 || !dataPrefix) {
    return null;
  }

  const gaRegExp = /^data-.*ga$/; // format restriction
  const eventsObj = dataPrefix.reduce((p, prefix) => {
    const _p = p;

    if (gaRegExp.test(prefix)) {
      _p[`${prefix}-category`] = category;
      _p[`${prefix}-action`] = action;
      _p[`${prefix}-label`] = label;
    }

    return _p;
  }, {});

  return eventsObj;
};

export default gaDataset;
