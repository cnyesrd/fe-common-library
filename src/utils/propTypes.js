/* eslint-disable import/prefer-default-export */
import PropTypes from 'prop-types';

export const requestType = PropTypes.func;

export const navDownloadShape = PropTypes.shape({
  title: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
});

export const navDownloadType = PropTypes.arrayOf(navDownloadShape);

export const navUrlShape = PropTypes.shape({
  title: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
});

export const navItemShape = PropTypes.shape({
  title: PropTypes.string.isRequired,
  url: PropTypes.string,
  leftList: PropTypes.arrayOf(navUrlShape),
  rightListTitle: PropTypes.string,
  rightList: PropTypes.arrayOf(navUrlShape),
});

export const navsType = PropTypes.arrayOf(navItemShape);

export const accountNavItemShape = PropTypes.shape({
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  defaultUrl: PropTypes.string.isRequired,
  notify: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      gotoUrl: PropTypes.string.isRequired,
    })
  ),
});

export const accountNavsType = PropTypes.arrayOf(accountNavItemShape);

export const navItemMobilShape = PropTypes.shape({
  name: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
  external: PropTypes.bool,
});

export const navItemMobileShape = PropTypes.shape({
  name: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  items: PropTypes.arrayOf(navItemMobilShape),
});

export const navsMobileType = PropTypes.arrayOf(navItemMobileShape);

export const catNavSubItemShape = PropTypes.shape({
  name: PropTypes.string,
  url: PropTypes.string,
  title: PropTypes.string,
  external: PropTypes.bool,
});

export const catNavItemShape = PropTypes.shape({
  name: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  external: PropTypes.bool,
  subItems: PropTypes.arrayOf(catNavSubItemShape),
});

export const catNavsType = PropTypes.arrayOf(catNavItemShape);

export const footerNavItem = PropTypes.shape({
  title: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  url: PropTypes.string,
  onClick: PropTypes.func,
});

export const locationShape = PropTypes.shape({
  key: PropTypes.string,
  pathname: PropTypes.string,
  search: PropTypes.string,
  hash: PropTypes.string,
  state: PropTypes.object,
});

export const authType = PropTypes.shape({
  init: PropTypes.func.isRequired,
  loginFB: PropTypes.func.isRequired,
  loginGoogle: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired,
  showLogin: PropTypes.func.isRequired,
  hideLogin: PropTypes.func.isRequired,
  getToken: PropTypes.func.isRequired,
  refreshToken: PropTypes.func.isRequired,
  getProfile: PropTypes.func.isRequired,
});

export const userProfileType = PropTypes.shape({
  uid: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  nickname: PropTypes.string,
  email: PropTypes.string,
  avatar: PropTypes.string.isRequired,
  gender: PropTypes.oneOf(['', 'male', 'female']),
  vip: PropTypes.oneOf([0, 1]),
});

export const newsItemType = PropTypes.shape({
  newsId: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  hasCoverPhoto: PropTypes.oneOf([0, 1]).isRequired,
  coverSrc: PropTypes.shape({
    m: PropTypes.shape({ src: PropTypes.string }),
    l: PropTypes.shape({ src: PropTypes.string }),
    xl: PropTypes.shape({ src: PropTypes.string }),
  }),
});

export const adProfileType = PropTypes.shape({
  name: PropTypes.string.isRequired,
  path: PropTypes.string.isRequired,
  hideOnInitial: PropTypes.bool,
});
