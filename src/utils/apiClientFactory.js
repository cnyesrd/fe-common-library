/* globals: __SERVER__:false */
// @flow
import axios from 'axios';
import type { $AxiosXHR, $AxiosXHRConfig } from 'axios'; // eslint-disable-line no-duplicate-imports

const DEFAULT_PROJECT_NAME = 'fe-default';
const DEFAULT_APP_NAME = 'default';

function makeHandleAuthHeader(
  getToken: () => ?string,
  appName: ?string
): (config: $AxiosXHRConfig<*>) => $AxiosXHRConfig<*> {
  return function handleAuthHeader(config: $AxiosXHRConfig<*>): $AxiosXHRConfig<*> {
    if (config && config.cnyesauth) {
      const token = getToken();
      // console.log('token', token);

      switch (config.cnyesauth) {
        case 'required':
          if (!token) {
            throw new Error('Auth is Required');
          }
          // eslint-disable-next-line no-param-reassign
          config.headers = {
            ...config.headers,
            Authorization: token,
            ...(appName ? { 'X-CNYES-APP': appName } : {}),
          };
          break;
        case 'optional':
          if (token) {
            // eslint-disable-next-line no-param-reassign
            config.headers = {
              ...config.headers,
              Authorization: token,
              ...(appName ? { 'X-CNYES-APP': appName } : {}),
            };
          }
          break;
        default:
        // do nothing
      }
    }

    return config;
  };
}

function makeHandleAuthFailedRefresh(refreshToken: () => Promise<null>): (error: mixed) => mixed {
  return function handleAuthFailedRefresh(error: mixed): mixed {
    if (typeof error === 'object' && error !== null && error.response) {
      const errorResponse: $AxiosXHR<*> = error.response;

      if (errorResponse.status === 401) {
        return refreshToken().then(() => {
          const originalConfig = errorResponse.config;
          const client = axios.create();

          if (originalConfig.getToken) {
            client.interceptors.request.use(makeHandleAuthHeader(originalConfig.getToken));
          }

          return client.request(originalConfig);
        });
      }
    }

    return Promise.reject(error);
  };
}

/**
 * @param {Object} defaultConfig - default config for axios
 * @param {string} defaultConfig.baseURL - baseURL for axios
 * @param {Object} param - config for customized functions
 * @param {Function} param.getToken - get auth token
 * @param {Function} param.refreshToken - refresh auth token
 * @param {string} param.hostname - hostname to be filled in user-agent header
 * @param {string} param.projectName - project name to be filled in user-agent header
 * @param {string} param.appName - app name for auth request
 * @returns {Function} api client
 */
function apiClientFactory(
  defaultConfig: Object, // { baseURL }
  // { getToken, refreshToken }: { getToken: () => ?string, refreshToken: () => Promise<AuthResponseType> } = {}
  { getToken, refreshToken, hostname = '', projectName = DEFAULT_PROJECT_NAME, appName = DEFAULT_APP_NAME } = {}
) {
  const userAgentHeader = {};

  if (global.__SERVER__) {
    const agentUa = 'axios'; // TODO determine axios version
    const ua = `${agentUa} ${projectName} ${hostname}`;

    userAgentHeader['User-Agent'] = ua;
  }

  const client = axios.create({
    headers: {
      ...userAgentHeader,
      ...defaultConfig.headers,
    },
    getToken,
    refreshToken,
    ...defaultConfig,
  });

  if (getToken) {
    client.interceptors.request.use(makeHandleAuthHeader(getToken, appName));
  }
  if (refreshToken) {
    client.interceptors.response.use(undefined, makeHandleAuthFailedRefresh(refreshToken));
  }

  return client;
}

export default apiClientFactory;
