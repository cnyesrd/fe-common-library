const fetch = require('node-fetch');
const fs = require('fs');
const path = require('path');

// config file is host by fe-share-config, please check the repository for more information
// https://bitbucket.org/cnyesrd/fe-share-config/src/master/
const timestamp = Date.now();
const s3ConfigUrl = 'https://s3-ap-northeast-1.amazonaws.com/cnyes-fe-share-config/config';
const navsUrl = `${s3ConfigUrl}/desktopNavs.json?${timestamp}`;
const navsFile = path.resolve(__dirname, './src/components/Header/config/navs.json');
const accountNavsUrl = `${s3ConfigUrl}/accountPanelNavs.json?${timestamp}`;
const accountNavsFile = path.resolve(__dirname, './src/components/AccountPanel/config/navs.json');
const s3CatNavsUrl = `${s3ConfigUrl}/desktopCatNavs`;
const srcCatNavsPath = './src/components/Header/config/catNavs';

fetch(navsUrl)
  .then(res => res.json())
  .then(res => {
    fs.writeFile(navsFile, JSON.stringify(res), 'utf8', err => {
      if (err) throw err;
    });
  });

fetch(accountNavsUrl)
  .then(res => res.json())
  .then(res => {
    fs.writeFile(accountNavsFile, JSON.stringify(res), 'utf8', err => {
      if (err) throw err;
    });
  });

const navConfig = {
  fund: {
    url: `${s3CatNavsUrl}/fund.json?${timestamp}`,
    filePath: `${srcCatNavsPath}/fund.json`,
  },
  twstock: {
    url: `${s3CatNavsUrl}/twstock.json?${timestamp}`,
    filePath: `${srcCatNavsPath}/twstock.json`,
  },
  forex: {
    url: `${s3CatNavsUrl}/forex.json?${timestamp}`,
    filePath: `${srcCatNavsPath}/forex.json`,
  },
  global: {
    url: `${s3CatNavsUrl}/global.json?${timestamp}`,
    filePath: `${srcCatNavsPath}/global.json`,
  },
  money: {
    url: `${s3CatNavsUrl}/money.json?${timestamp}`,
    filePath: `${srcCatNavsPath}/money.json`,
  },
  about: {
    url: `${s3CatNavsUrl}/about.json?${timestamp}`,
    filePath: `${srcCatNavsPath}/about.json`,
  },
};

Object.values(navConfig).map(item => {
  return fetch(item.url)
    .then(res => res.json())
    .then(res => {
      const filePath = path.resolve(__dirname, item.filePath);

      fs.writeFile(filePath, JSON.stringify(res), 'utf8', err => {
        if (err) throw err;
      });
    });
});
