import { configure, addDecorator, setAddon } from '@storybook/react';
import './styles/storybook.scss';

const req = require.context('../src', true, /\.stories\.js$/);

function loadStories() {
  req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);
