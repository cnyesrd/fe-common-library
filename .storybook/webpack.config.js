require('babel-polyfill');
require('dotenv').config();

var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var assetsLoader = 'base64-inline-loader?limit=50000&name=[name].[ext]';

module.exports = {
  module: {
    rules: [
      {
        test: /\.scss$/,
        loader: ['css-hot-loader'].concat(
          ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use:
              'css-loader?-autoprefixer&modules&importLoaders=2&localIdentName=_[hash:base64:5]!postcss-loader!sass-loader?outputStyle=expanded',
          })
        ),
      },
      {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract('style-loader', 'css-loader?-autoprefixer&importLoaders=1!postcss-loader'),
      },
      {
        test: /\.(jpe?g|png|ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/,
        exclude: /\.inline\.svg$/,
        loader: assetsLoader,
      },
    ],
  },
  resolve: {
    modules: ['node_modules'],
    extensions: ['.js', '.scss', '.json', '.jsx'],
  },
  plugins: [
    // css files from the extract-text-plugin loader
    new ExtractTextPlugin('[name].css', { allChunks: true }),

    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"',
      },

      __CLIENT__: true,
      __SERVER__: false,
      __DEVELOPMENT__: false,
      __DEVTOOLS__: false,
    }),

    new webpack.ContextReplacementPlugin(/moment[\\/]locale$/, /^\.\/(en|zh-tw)$/),
  ],
  node: {
    fs: 'empty',
  },
};
